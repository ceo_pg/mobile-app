/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { Component } from 'react';
import AppNavigation from "./app/src/appNavigator/AppNavigation"

import { NavigationContainer } from '@react-navigation/native';
import { Provider } from "react-redux";
import Store from "./app/src/redux/store/Store"


class App extends Component {
  render() {
    return (
      <Provider store={Store}>
        <NavigationContainer>
          <AppNavigation />
        </NavigationContainer>
       




      </Provider>
    );
  }
}

export default App;