const fonts = {
    regular: 'Montserrat-Regular',
    bold: 'Montserrat-Bold',
    semiBold: 'Montserrat-SemiBold',
    medium: 'Montserrat-Medium',
    Sregular: 'SourceSansPro-Regular',
    SsemiBold: 'SourceSansPro-SemiBold',
    Sbold: 'SourceSansPro-Bold'
};
export default fonts;