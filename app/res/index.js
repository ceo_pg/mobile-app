import images from "./images";
import fonts from "./constants/fonts";
import strings from "./constants/strings";
import colors from './colors'
import AsyncStorageContants from './constants/asyncStorageContants'
const resources = {
    "images": images,
    "fonts": fonts,
    "colors": colors,
    "strings": strings,
    "AsyncStorageContants": AsyncStorageContants
};
export default resources;