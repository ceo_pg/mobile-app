import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import { View, Text, TouchableOpacity, Image, ImageBackground, Linking } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

import { hitCustomerRegistrationApi } from '../../redux/actions/SignUpAction'
import styles from './styles';
import PGInputField from '../../components/input/TextInput'
import { renderInputError, focusTo, validateEmail, validateMobileNumber, widthScale } from '../../utility/Utils'
import res from '../../../res';
import AppUser from '../../utility/AppUser'
import ApiFetch from '../../apiManager/ApiFetch';
import { SIGNUP_ENDPOINTS } from '../../apiManager/ApiEndpoints';
import { MyStatusBar } from '../../components/header/Header'
import { PGButton, Loader } from '../../components/index'

class SignUpScreen extends Component {
    _isMounted = false;
    constructor(props) {
        super(props);
        this.userNameInputRef = React.createRef();
        this.emailInputRef = React.createRef();
        this.phoneInputRef = React.createRef();
        this.passwordInputRef = React.createRef();
        this.confirmPasswordInputRef = React.createRef();

        this.state = {
            userName: "",
            email: "",
            phone: "",
            password: "",
            confirmPassword: "",
            isFocused: false,
            error: {},
            isLoading: false,
            isChecked: true
        }
    }
    componentDidMount() {
        this._isMounted = true;
    }
    componentWillMount() {
        this._isMounted = false;
    }
    onUserNameEntered = (text) => {
        this.setState({
            userName: text
        })
    }
    onEmailIdEntered = (text) => {
        this.setState({
            email: text
        })
    }
    onPhoneEntered = (text) => {
        this.setState({
            phone: text
        })
    }
    onConfirmPasswordEntered = (text) => {
        this.setState({
            confirmPassword: text
        })
    }
    onPasswordEntered = (text) => {
        this.setState({
            password: text
        })
    }
    focusToNext = (ref) => {
        focusTo(ref)
    }

    handleFocus = (key) => {
        const { isFocused } = this.state
        this.setState({ isFocused: !isFocused })
    }
    gotoEmailVerify = () => {
        console.log("ISCHECKED", this.state.isChecked)
        if (this.state.isChecked == true) {
            this.props.navigation.navigate("EmailVarificationScreen", {
                emailVerify: 1
            })
        } else {
            alert("Please accept terms and conditions")
        }


    }
    saveToAsynStorage = async (data) => {
        console.log("ASYNCDATA", data.data)
        let appUsrObj = AppUser.getInstance();
        appUsrObj.token = data.data.token;
        appUsrObj.userId = data.data._id;
        appUsrObj.phone = data.data.phone;
        const userToken = [res.AsyncStorageContants.TOKEN, data.data.token];
        const userId = [res.AsyncStorageContants.USERID, data.data._id];
        const phone = [res.AsyncStorageContants.PHONE, data.data.phone];
        const signup = [res.AsyncStorageContants.SIGNUPTOKEN, "1"];

        try {
            await AsyncStorage.multiSet([userToken, userId, phone, signup])
            console.log(userToken, userId)
            this.gotoEmailVerify()
        } catch (e) {
            console.log("Error saving user details", e);
        }
    }
    validationBeforeSignUp = () => {
        const { userName, email, phone, password, confirmPassword } = this.state;
        let errorObject = {};
        if (userName.trim() == "") {
            errorObject.name = res.strings.nameCannotBeEmpty;
        } else if (userName.length > 20) {
            errorObject.name = res.strings.name20length;
        }
        if (phone.trim() == "") {
            errorObject.mobileNumber = res.strings.phoneCannotBeEmpty;
        } else if (phone && phone.length > 10) {
            errorObject.mobileNumber = res.strings.phone10length;
        } else if (!validateMobileNumber(phone)) {
            errorObject.mobileNumber = res.strings.enterValidPhone;
        }
        if (email.trim() == "") {
            errorObject.email = res.strings.emailCannotBeEmpty;
        } else if (email && email.trim().length > 100) {
            errorObject.email = res.strings.email100length;
        } else if (!validateEmail(email && email.trim())) {
            errorObject.email = res.strings.enterValidEmail;
        }
        if (password.trim() == "") {
            errorObject.pass = res.strings.passwordCannotBeEmpty;
        } else if (password && password.length < 6) {
            errorObject.pass = res.strings.password6length;
        }
        else if (password && password.length > 16) {
            errorObject.pass = res.strings.password16length;
        } else if (password != confirmPassword) {
            errorObject.cpass = res.strings.samePasword;
        }

        this.setState({ error: errorObject });
        return Object.keys(errorObject).length == 0;
    }
    customerRegistration = () => {
        const isValid = this.validationBeforeSignUp();
        if (!isValid) { return; }

        const { userName, email, phone, password } = this.state
        const parameters = JSON.stringify({
            userName: userName,
            email: email,
            phone: phone,
            password: password
        })
        this.setState({ isLoading: true });
        ApiFetch.fetchPost(SIGNUP_ENDPOINTS, parameters)
            .then((data) => {
                console.log("SIGNUP", data)
                if (data.statusCode == 200) {
                    this.saveToAsynStorage(data)
                    this.setState({ isLoading: false });
                } else {
                    alert(data.message)
                    this.setState({ isLoading: false });
                    console.log(data)
                }

            }).catch((err) => {
                this.setState({ isLoading: false });
                alert(err)
            })


    }
    renderLoader = () => {
        return (
            <Loader
                loading={this.state.isLoading} />
        );
    };
    selectTerms = () => {
        this.setState({
            isChecked: !this.state.isChecked
        })
    }
    moveToTerms = () => {
        let url = "http://propright.in/terms-and-conditions/"
        Linking.canOpenURL(url).then(supported => {
            if (supported) {
                Linking.openURL(url);
            } else {
                console.log("Don't know how to open URI: " + url);
            }
        });
    }
    renderView = () => {
        const { userName, email, phone, password, error, confirmPassword, isChecked } = this.state
        return <View style={{ marginHorizontal: 30, marginTop: 40 }}>

            <Text style={styles.signText}>Sign Up </Text>
            <PGInputField
                label={"Name"}

                onChangeText={this.onUserNameEntered}
                value={userName}
                error={renderInputError("name", error)}
                inputStyles={[styles.inputCont, styles.passwordField,]}
                onSubmitEditing={() => this.focusToNext(this.emailInputRef)}
                inputProps={{

                    maxLength: 50,
                    returnKeyType: "next",
                    keyboardType: "email-address",
                    autoCapitalize: "none",
                    autoCorrect: false,
                    autoFocus: false,
                    numberOfLines: 1,
                }}
            // inputStyles={fieldHasError("email") ? styles.errorUnderLine : { paddingVertical: 5, }}
            />
            <PGInputField
                label={"Email"}
                onChangeText={this.onEmailIdEntered}
                value={email}
                reference={this.emailInputRef}
                onSubmitEditing={() => this.focusToNext(this.phoneInputRef)}
                containerStyle={[styles.inputCont, styles.passwordField]}
                error={renderInputError("email", error)}
                inputProps={{

                    maxLength: 50,
                    returnKeyType: "next",
                    keyboardType: "email-address",

                    autoCapitalize: "none",
                    autoCorrect: false,
                    autoFocus: false,
                    numberOfLines: 1,
                }}
            // inputStyles={fieldHasError("email") ? styles.errorUnderLine : { paddingVertical: 5, }}
            />
            <PGInputField
                label={"Mobile Number"}
                onChangeText={this.onPhoneEntered}
                value={phone}
                reference={this.phoneInputRef}
                onSubmitEditing={() => this.focusToNext(this.passwordInputRef)}
                error={renderInputError("mobileNumber", error)}
                containerStyle={[styles.inputCont, styles.passwordField]}
                maxLength={10}
                inputProps={{

                    maxLength: 10,
                    returnKeyType: "done",
                    keyboardType: "phone-pad",
                    autoCapitalize: "none",
                    autoCorrect: false,
                    autoFocus: false,
                    numberOfLines: 1,
                }}
            // inputStyles={fieldHasError("email") ? styles.errorUnderLine : { paddingVertical: 5, }}
            />
            <PGInputField
                label={"Password"}
                onChangeText={this.onPasswordEntered}
                value={password}
                reference={this.passwordInputRef}
                error={renderInputError("pass", error)}
                onSubmitEditing={() => this.focusToNext(this.confirmPasswordInputRef)}
                containerStyle={[styles.inputCont, styles.passwordField]}
                inputProps={{
                    secureTextEntry: true,
                    maxLength: 50,
                    returnKeyType: "next",
                    keyboardType: 'default',

                    autoCapitalize: "none",
                    autoCorrect: false,
                    autoFocus: false,
                    numberOfLines: 1,
                }}
            // inputStyles={fieldHasError("email") ? styles.errorUnderLine : { paddingVertical: 5, }}
            />
            <PGInputField
                label={"Confirm Password"}
                onChangeText={this.onConfirmPasswordEntered}
                value={confirmPassword}
                reference={this.confirmPasswordInputRef}
                error={renderInputError("cpass", error)}
                containerStyle={[styles.inputCont, styles.passwordField]}

                inputProps={{

                    secureTextEntry: true,
                    maxLength: 50,
                    returnKeyType: "next",
                    keyboardType: 'default',

                    autoCapitalize: "none",
                    autoCorrect: false,
                    autoFocus: false,
                    numberOfLines: 1,
                }}
            // inputStyles={fieldHasError("email") ? styles.errorUnderLine : { paddingVertical: 5, }}
            />
            <View style={styles.terms}>
                <TouchableOpacity onPress={() => this.selectTerms()}>
                    {
                        isChecked ?
                            <View style={styles.squreBox}>
                                <Image source={res.images.icn_BlueCheck} />
                            </View> :
                            <View style={styles.squreBox}>

                            </View>
                    }
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.moveToTerms()} style={{ borderBottomWidth: 1, borderBottomColor: res.colors.appColor, marginLeft: widthScale(10), }}>
                    <Text style={styles.termsText}>I accept Terms and Conditions</Text>
                </TouchableOpacity>


            </View>
            <View style={{ marginTop: 5 }}>
                <PGButton

                    onPress={() => this.customerRegistration()}
                    btnText={'Sign Up'}
                />
            </View>
            {/* <View style={{ marginTop: 10 }}>
                        <PGButton

                            onPress={() => this.gotoEmailVerify()}
                            btnText={'Email Verify'}
                        />
                    </View> */}

            <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', marginTop: 10, marginBottom: 100 }}>
                <Text style={{ color: res.colors.warmGrey, fontFamily: res.fonts.regular, fontSize: 14 }}>Registered User?</Text>
                <TouchableOpacity onPress={() => this.props.navigation.navigate("LoginScreen")}>
                    <Text style={{ color: res.colors.btnBlue, marginLeft: 10, fontFamily: res.fonts.regular, fontSize: 14 }}>Sign In</Text>
                </TouchableOpacity>
            </View>




        </View>


        // </View>
    }
    render() {
        const { isLoading } = this.state
        return (

            <View style={[styles.mainContainer]}>
                <MyStatusBar
                    backgroundColor={res.colors.btnBlue}
                />
                <View style={[styles.container]}>

                    <View style={styles.imageContainer}>
                        <ImageBackground
                            source={res.images.testImage}
                            style={styles.imageBack}
                        />
                    </View>
                    <View style={[styles.roundedContaier, styles.shadow]}>
                        <KeyboardAwareScrollView style={{ height: '100%' }} showsVerticalScrollIndicator={false}>
                            {this.renderView()}
                        </KeyboardAwareScrollView>

                    </View>
                </View>
                {isLoading && this.renderLoader()}

            </View>
        );
    }
}
const mapStateToProps = (state) => {
    return {};
};

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        hitCustomerRegistrationApi
    }, dispatch);
}
let SignUpScreenContainer = connect(mapStateToProps, mapDispatchToProps)(SignUpScreen);
export default SignUpScreenContainer;
