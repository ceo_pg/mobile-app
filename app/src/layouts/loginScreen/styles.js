import { StyleSheet } from 'react-native';
import resources from '../../../res';
import { widthScale, heightScale } from '../../utility/Utils'
const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        backgroundColor: resources.colors.white,

    }, inputCointainer: {
        flex: 1,

        backgroundColor: resources.colors.white,
    },
    container: {
        flex: 1,
        // justifyContent: 'center',
        // alignItems: 'center',
        backgroundColor: resources.colors.white,
    },
    content: {
        flexDirection: "column",
        justifyContent: "flex-start",
        alignItems: "center",
    }, inputCont: {

        // borderRadius: 4,
        // borderColor: resources.colors.black,


    }, errorUnderLine: {
        borderColor: "red",
    }, imageBack: {
        height: 216,
        width: '100%',
        top: 0
    }, inputFieldView: {

        borderTopLeftRadius: 22,
        borderTopRightRadius: 22,
        position: 'absolute',
        top: -20,
        left: 0,
        right: 0,
        bottom: 0,
        backgroundColor: resources.colors.white
    }, signText: {
        fontFamily: resources.fonts.semiBold,
        fontSize: 32,
        color: resources.colors.slate
    }, passwordField: {
        marginTop: 35
    }, forgotPassText: {
        fontFamily: resources.fonts.regular,
        fontSize: 12,
        color: resources.colors.btnBlue,
        // marginVertical: 25,
        alignSelf: 'center',
        textAlign: 'center'
    }, shadow: {
        shadowColor: resources.colors.shadowColor,
        shadowOffset: {
            width: 0,
            height: -10,
        },
        shadowOpacity: 20.39,
        shadowRadius: 10.30,

        elevation: 13,
        zIndex: 10
    }, imageContainer: {
        height: 216,
        width: '100%',
        // borderRadius: widthScale(50),
        backgroundColor: "white",
        justifyContent: "center",
        alignItems: "center",
    }, roundedContaier: {
        borderTopRightRadius: 25,
        borderTopLeftRadius: 25,
        marginTop: -18,
        flex: 1,
        // alignItems: 'center',
        backgroundColor: resources.colors.butterscotch,
    }, terms: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: heightScale(20),
        marginRight:widthScale(10)
        // backgroundColor:"red"
    }, unChecklabel: {
        marginLeft: widthScale(10),
        fontFamily: resources.fonts.semiBold,
        fontSize: heightScale(16),
        color: resources.colors.peacockBlueLight
    },
    checkLabel: {
        marginLeft: widthScale(10),
        fontFamily: resources.fonts.semiBold,
        fontSize: heightScale(16),
        color: resources.colors.peacockBlue
    },
    checkBoxView: {
        flexDirection: 'row',
        marginVertical: heightScale(15),
        alignItems: 'center'
    },
    squreBox: {
        width: widthScale(15),
        height: widthScale(15),
        borderRadius: 4,
        borderWidth: 2,
        borderColor: resources.colors.peacockBlue,
        alignItems: 'center',
        justifyContent: 'center'
    }, termsText: {
        color: resources.colors.appColor,
        fontSize: widthScale(12),
        fontFamily: resources.fonts.regular,
        textAlign: 'center',
        
    }

});

export default styles;
