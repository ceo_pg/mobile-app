import React, { Component } from 'react';
import { View, Text, TouchableOpacity, ImageBackground, Image,Linking } from 'react-native';
import { connect } from 'react-redux';
import { CommonActions } from '@react-navigation/native';
import { bindActionCreators } from "redux";
import AsyncStorage from '@react-native-community/async-storage'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

import styles from './styles';
import PGInputField from '../../components/input/TextInput'
import res from '../../../res';
import { renderInputError, validateEmail, widthScale } from '../../utility/Utils'
import { hitUserLoginApi } from '../../redux/actions/LoginAction'
import { LOGIN_ENDPOINT } from '../../apiManager/ApiEndpoints';
import ApiFetch from '../../apiManager/ApiFetch';
import AppUser from '../../utility/AppUser'
import { MyStatusBar } from '../../components/header/Header'
import { PGButton, Loader } from '../../components/index'
import { getProfile } from '../../redux/actions/Profile/ProfileAction'
class LoginScreen extends Component {
    constructor(props) {
        super(props);
        this.passwordInputRef = React.createRef();
        this.state = {
            email: "",
            password: "",
            error: {},
            isLoading: false,
            isFocused: false,
            isBorderThick: false,
            profileImageName: "",
            isChecked: true
        }
    }
    componentDidMount() {
        let appUsrObj = AppUser.getInstance();
        this.setState({
            email: appUsrObj.email
        })
    }
    clearValue = () => {
        this.setState({
            email: "",
            password: "",
        })
    }
    gotoHome = () => {
        if (this.state.isChecked == true) {
            const resetAction = CommonActions.reset({
                index: 0,
                routes: [{ name: 'DashBoard', }],
            });
            this.props.navigation.dispatch(resetAction);
        } else {
            alert("Please accept terms and conditions")
        }

        // this.props.navigation.navigate("HomeScreen")
    }
    gotoPropertyDetails = () => {
        if (this.state.isChecked == true) {
            const resetAction = CommonActions.reset({
                index: 0,
                routes: [{ name: 'PropertyDetailsScreen' }],
            });
            this.props.navigation.dispatch(resetAction, { isBack: false });
        } else {
            alert("Please accept terms and conditions")
        }

    }
    saveToAsynStorage = async (data) => {
        console.log("ASYNCDATA", data.data)
        let appUsrObj = AppUser.getInstance();
        appUsrObj.token = data.data.token;
        appUsrObj.userId = data.data._id
        appUsrObj.email = this.state.email
        appUsrObj.phone = data.data.phone
        appUsrObj.name = data.data.userName
        appUsrObj.isUnitAssign = data.data.isUnitAssigned
        const userToken = [res.AsyncStorageContants.TOKEN, data.data.token];
        const userId = [res.AsyncStorageContants.USERID, data.data._id];
        const phone = [res.AsyncStorageContants.PHONE, data.data.phone];
        const name = [res.AsyncStorageContants.NAME, data.data.userName];
        const email = [res.AsyncStorageContants.EMAILID, data.data.email];
        const isUnitAssigned = [res.AsyncStorageContants.UNITASSIGN, JSON.stringify(data.data.isUnitAssigned)]
        const userName = [res.AsyncStorageContants.USERNAME, data.data.userName];
        const signup = [res.AsyncStorageContants.SIGNUPTOKEN, "0"];

        // const aadhar = [[res.AsyncStorageContants.EMAILID, data.data._id],]
        try {
            await AsyncStorage.multiSet([userToken, userId, phone, userName, isUnitAssigned, name, email, signup])
            data.data.isUnitAssigned == true ?
                this.gotoHome() : this.gotoPropertyDetails()
        } catch (e) {
            console.log("Error saving user details", e);
        }
    }
    validateBeforeLogin = () => {
        const { email, password } = this.state
        let errorObject = {};
        if (email.trim() == "") {
            errorObject.email = res.strings.emailCannotBeEmpty;
        } else if (email.trim().length > 100) {
            errorObject.email = res.strings.email100length;
        } else if (!validateEmail(email.trim())) {
            errorObject.email = res.strings.enterValidEmail;
        }
        if (password.trim() == "") {
            errorObject.pass = res.strings.passwordCannotBeEmpty;
        } else if (password.length < 6) {
            errorObject.pass = res.strings.password6length;
        }
        else if (password.length > 16) {
            errorObject.pass = res.strings.password16length;
        }
        this.setState({ error: errorObject });
        return Object.keys(errorObject).length == 0;
    }
    renderLoader = () => {
        return (
            <Loader
                loading={this.state.isLoading} />
        );
    };
    loginApi = () => {

        const isValid = this.validateBeforeLogin();
        if (!isValid) { return; }
        const { email, password } = this.state
        const parameters = JSON.stringify({
            email: email,
            password: password
        })
        this.setState({ isLoading: false });
        ApiFetch.fetchPost(LOGIN_ENDPOINT, parameters)
            .then((data) => {
                if (data.statusCode == 200) {
                    this.saveToAsynStorage(data)
                    this.profileListApi()
                } else {
                    alert(data.message)
                    this.setState({
                        password: ""
                    })
                }
                this.setState({ isLoading: false });


            }).catch((error) => {
                this.setState({ isLoading: false });
                console.log("Error inside login", error)
            })
    }
    profileListApi = () => {
        this.setState({ isLoading: true })
        this.props.getProfile().then((res) => {
            console.log("profileListApi", res)
            this.setState({ isLoading: false })
            if (!res || res.statusCode != 200 || !(res.status)) {
                Toast.show(res && res.message ? res.message : resources.strings.PLEASE_TRY_AFTER_SOMETIME);
                return
            }
            let appUsrObj = AppUser.getInstance();
            appUsrObj.profileurl = res.data.profileImageUrl
            this.saveProfileUrl(res.data.profileImageUrl ? res.data.profileImageUrl : '')

        })
            .catch(error => {
                this.setState({ isLoading: false });
                Toast.show(res.strings.PLEASE_TRY_AFTER_SOMETIME, Toast.LONG);
                console.log(error, "error");
            })
    }

    saveProfileUrl = async (url) => {
        console.log("saveProfileUrl", url)
        try {
            await AsyncStorage.setItem(res.AsyncStorageContants.PROFILEURL, url)

        } catch (e) {
            console.log("Error saving user details", e);
        }
    }
    focusPasswordInput = () => {
        this.passwordInputRef.current.focus();
    };
    onEmailIdEntered = (text) => {
        this.setState({
            email: text
        })
    }
    onPasswordEntered = (text) => {
        this.setState({
            password: text
        })
    }


    moveToCustomerRegistration = () => {
        this.props.navigation.navigate("SignUpScreen")
    }
    moveToForgotPassword = () => {
        this.props.navigation.navigate("ForgotPasswordScreen")
    }
    onFoucusPasswordField = () => {
        this.setState({
            isBorderThick: true
        })
    }
    onFoucusEmailField = () => {
        this.setState({
            isBorderThick: true
        })
    }
    selectTerms = () => {
        this.setState({
            isChecked: !this.state.isChecked
        })
    }
    moveToTerms = () => {
        let url = "http://propright.in/terms-and-conditions/"
        Linking.canOpenURL(url).then(supported => {
            if (supported) {
                Linking.openURL(url);
            } else {
                console.log("Don't know how to open URI: " + url);
            }
        });
    }
    renderView = () => {
        const { email, password, isFocused, error, isBorderThick, isChecked } = this.state
        console.log(isFocused)
        return <View style={{ marginHorizontal: 30, marginTop: 40 }}>
            <Text style={styles.signText}>Sign In </Text>
            <View >
                <PGInputField
                    onFocus={this.onFoucusEmailField}
                    label={res.strings.EMAIL}
                    value={email}
                    onChangeText={this.onEmailIdEntered}
                    error={renderInputError("email", error)}
                    inputStyles={[styles.inputCont, styles.passwordField]}
                    inputProps={{

                        autoCaptialize: 'none',
                        returnKeyType: 'next'
                    }}
                    onSubmitEditing={this.focusPasswordInput}
                    isBorderThick={isBorderThick}
                />

                <PGInputField
                    onFocus={this.onFoucusPasswordField}
                    label={res.strings.PASSWORD}
                    value={password}
                    onChangeText={this.onPasswordEntered}
                    containerStyle={[styles.inputCont, styles.passwordField]}
                    error={renderInputError("pass", error)}
                    reference={this.passwordInputRef}
                    inputProps={{
                        secureTextEntry: true,
                        maxLength: 50,
                        returnKeyType: "next",
                        keyboardType: 'default',

                        autoCapitalize: "none",
                        autoCorrect: false,
                        autoFocus: false,
                        numberOfLines: 1,
                    }}

                />
            </View>


            <View >
                <TouchableOpacity onPress={() => this.moveToForgotPassword()}>
                    <Text style={styles.forgotPassText}>Forgot Password?</Text>
                </TouchableOpacity>
            </View>
            <View style={styles.terms}>
                <TouchableOpacity onPress={() => this.selectTerms()}>
                    {
                        isChecked ?
                            <View style={styles.squreBox}>
                                <Image source={res.images.icn_BlueCheck} />
                            </View> :
                            <View style={styles.squreBox}>

                            </View>
                    }
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.moveToTerms()} style={{ borderBottomWidth: 1, borderBottomColor: res.colors.appColor, marginLeft: widthScale(10), }}>
                    <Text style={styles.termsText}>I accept Terms and Conditions</Text>
                </TouchableOpacity>


            </View>
            <View style={{ marginTop: 30 }}>
                <PGButton

                    btnText={'Sign In'}
                    onPress={() => this.loginApi()}
                />
            </View>

            <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', marginTop: 15, marginBottom: 50 }}>
                <Text style={{ color: res.colors.black, fontFamily: res.fonts.regular, fontSize: 14 }}>
                    New User?
                    </Text>
                <TouchableOpacity onPress={() => this.moveToCustomerRegistration()}>
                    <Text style={{ color: res.colors.btnBlue, fontFamily: res.fonts.regular, fontSize: 14, marginLeft: 8 }}>
                        Sign Up
                        </Text>
                </TouchableOpacity>
            </View>
        </View>
    }
    render() {
        const { isLoading } = this.state
        return (
            <View style={[styles.mainContainer]}>
                <MyStatusBar
                    backgroundColor={res.colors.btnBlue}
                />
                <View style={[styles.container]}>
                    <View style={styles.imageContainer}>
                        <ImageBackground
                            source={res.images.testImage}
                            style={styles.imageBack}
                        />
                    </View>
                    <View style={[styles.roundedContaier, styles.shadow]}>
                        <KeyboardAwareScrollView showsVerticalScrollIndicator={false}>

                            {this.renderView()}
                        </KeyboardAwareScrollView>

                    </View>
                    {isLoading && this.renderLoader()}
                </View>

            </View>

        );
    }
}
const mapStateToProps = (state) => {
    return {};
};

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        hitUserLoginApi,
        getProfile
    }, dispatch);
}
let LoginScreenContainer = connect(mapStateToProps, mapDispatchToProps)(LoginScreen);
export default LoginScreenContainer;

