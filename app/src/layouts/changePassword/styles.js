import { StyleSheet,Platform } from 'react-native';
import resources from '../../../res';
const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        backgroundColor: resources.colors.white
    },
    container: {
        flex: 1,
        justifyContent: 'center',
        marginTop: Platform.OS == 'ios' ? 35 : 20,
    }, inputCont: {

        //  paddingHorizontal:20,

    }, imageBack: {
        height: 216,
        width: '100%',
        top: 0
    }, inputCointainer: {
        flex: 1,
        borderTopLeftRadius: 30,
        borderTopRightRadius: 30,
        // backgroundColor:'red'
    }, inputFieldView: {

        borderTopLeftRadius: 22,
        borderTopRightRadius: 22,
        position: 'absolute',
        top: -20,
        left: 0,
        right: 0,
        bottom: 0,
        backgroundColor: resources.colors.butterscotch
    }, forgotText: {
        fontFamily: "Montserrat-SemiBold",
        fontStyle: 'normal',
        fontSize: 24
    },shadow: {
        shadowColor: resources.colors.shadowColor,
        shadowOffset: {
            width: 0,
            height: 0,
        },
        shadowOpacity: 20.39,
        shadowRadius: 10.30,

        elevation: 13,
        zIndex: 10
    }

});

export default styles;
