import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import { View, Text, TouchableOpacity, ImageBackground } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

import styles from './styles';
import ApiFetch from '../../apiManager/ApiFetch'
import PGInputField from '../../components/input/TextInput'
import PGButton from '../../components/button/Button'
import { FORGOT_PASSWORD_ENDPOINTS, RESET_PASSWORD_ENDPOINTS, CHANGE_PASSSWORD_ENDPOINTS } from '../../apiManager/ApiEndpoints'
import res from '../../../res'
import { MyStatusBar } from '../../components/header/Header'
class ChangePasswordScreen extends Component {
    constructor(props) {
        super(props);
        this.oldPasswordInputRef = React.createRef();
        this.newPasswordInputRef = React.createRef();
        this.confirmPasswordInputRef = React.createRef();
        this.state = {
            oldPassword: "",
            newPassword: "",
            confirmPassword: ""
        }
    }
    componentDidMount() {

    }
    focusNewPasswordInput = () => {
        this.newPasswordInputRef.current.focus();
    };
    focusConfirmPasswordInput = () => {
        this.confirmPasswordInputRef.current.focus();
    };
    onOldPasswordEnter = (text) => {
        this.setState({
            oldPassword: text
        })
    }
    onNewPasswordEnter = (text) => {
        this.setState({
            newPassword: text
        })
    }
    onConfirmPasswordEnter = (text) => {
        this.setState({
            confirmPassword: text
        })
    }
    hitResetPasswordApi = () => {
        const { oldPassword, newPassword, confirmPassword } = this.state
        const parameters = JSON.stringify({
            password: newPassword,
            oldPassword: oldPassword,
            confirmPassword: confirmPassword
        })
        ApiFetch.fetchPut(CHANGE_PASSSWORD_ENDPOINTS, parameters)
            .then((data) => {
                if (data.statusCode == 200) {
                    alert(data.message)
                    this.props.navigation.goBack()
                } else {
                    alert(data.message)
                }


            }).catch((err) => {
                console.log(err)
            })
    }
    renderView = () => {
        const { oldPassword, newPassword, confirmPassword } = this.state
        return <View style={styles.inputFieldView}>
            <KeyboardAwareScrollView
                bounces={false}
            >
                <View style={{ marginHorizontal: 30, marginTop: 40, }}>
                    <Text style={styles.forgotText}>Change Password</Text>
                    <PGInputField
                        label={"Old Password"}
                        onChangeText={this.onOldPasswordEnter}
                        value={oldPassword}
                        containerStyle={[styles.inputCont, { marginTop: 20 }]}
                        inputProps={{
                            ref: this.oldPasswordInputRef,
                            maxLength: 50,
                            returnKeyType: "next",
                            keyboardType: "email-address",
                            onSubmitEditing: this.focusNewPasswordInput,
                            autoCapitalize: "none",
                            autoCorrect: false,
                            autoFocus: false,
                            numberOfLines: 1,
                        }}

                    />
                    <PGInputField
                        label={"New Password"}
                        onChangeText={this.onNewPasswordEnter}
                        value={newPassword}
                        containerStyle={[styles.inputCont, { marginTop: 20 }]}
                        inputProps={{
                            ref: this.newPasswordInputRef,
                            maxLength: 50,
                            returnKeyType: "next",
                            keyboardType: "email-address",
                            onSubmitEditing: this.onConfirmPasswordEnter,
                            autoCapitalize: "none",
                            autoCorrect: false,
                            autoFocus: false,
                            numberOfLines: 1,
                        }}

                    />
                    <PGInputField
                        label={"Confirm Password"}
                        onChangeText={this.onConfirmPasswordEnter}
                        value={confirmPassword}
                        containerStyle={[styles.inputCont, { marginTop: 20 }]}
                        inputProps={{
                            ref: this.confirmPasswordInputRef,
                            maxLength: 50,
                            returnKeyType: "next",
                            keyboardType: "email-address",

                            autoCapitalize: "none",
                            autoCorrect: false,
                            autoFocus: false,
                            numberOfLines: 1,
                        }}

                    />
                    <View style={{ marginTop: 100 }}>
                        <PGButton

                            btnText={res.strings.SUBMIT}
                            onPress={() => this.hitResetPasswordApi()}
                        />
                    </View>
                </View>
            </KeyboardAwareScrollView>

        </View>


    }
    render() {
        return (
            <View style={[styles.mainContainer]}>
                <MyStatusBar
                    backgroundColor={res.colors.btnBlue}
                    barStyle="light-content"
                />
                <ImageBackground
                    source={res.images.testImage}
                    style={styles.imageBack}
                />

                <View style={[styles.inputCointainer]}>

                    {this.renderView()}

                </View>

            </View>

        );
    }
}
const mapStateToProps = (state) => {
    return {};
};

function mapDispatchToProps(dispatch) {
    return bindActionCreators({

    }, dispatch);
}
let ChangePasswordScreenContainer = connect(mapStateToProps, mapDispatchToProps)(ChangePasswordScreen);
export default ChangePasswordScreenContainer;
