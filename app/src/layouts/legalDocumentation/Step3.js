import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import { View, Text } from 'react-native';
import styles from './styles'
import strings from '../../../res/constants/strings';
// import { formFields } from '../../../res/constants/formFields'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import Header from './Header'
import RoundedInput from '../../components/input/RoundedInput'
import { PGButton } from '../../components/index'
import { saveLegalDocumentsFromData, updateDocumentsAPI } from '../../redux/actions/LegalDocumentation/LegalDocumentationAction'
import Toast from 'react-native-simple-toast'
import { Loader } from '../../components/index'
import { Dropdown } from 'react-native-material-dropdown';
import { heightScale } from '../../utility/Utils';
class Step3 extends Component {
    constructor(props) {
        super(props);
        this.state = {
            numberOfYears: '',
            penalityForTermination: '',
            penalityForLate: '',
            transferCharges: "",
            intersetForLate: "",
            isLoading: false,
            transferChargesUom: null,
            penalityLateDeliveryUom: null,
            transferChargesValue: "Rs/sq ft",
            penalityLateDeliveryOfProjectValue: "Rs/Month",
            transferChargesForSellingBeforePossession: [{
                value: "Rs/sq ft",
            }, {
                value: "%",
            }, {
                value: "Rs",
            }],
            penalityLateDeliveryOfProject: [{
                value: "Rs/Month"
            }, {
                value: " Rs/sq ft/month"
            }]

        }
    }
    componentDidMount() {

    }

    backPress = () => {
        const { changePage } = this.props;
        changePage(-1)
    }
    renderLoader = () => {
        return (
            <Loader
                loading={this.state.isLoading} />
        );
    };

    step3Submit = () => {
        const { numberOfYears, penalityForTermination, penalityForLate, transferCharges, intersetForLate, transferChargesValue, penalityLateDeliveryOfProjectValue } = this.state
        let temDocObject = this.props.legalDocumentsFormData;
        console.log(temDocObject, "temDocObject");
        const keyClausesObject = {
            defectLiabilityPeriodInYears: { value: numberOfYears },
            penalityTerminationOfContractInPercentage: { value: penalityForTermination },
            penalityLateDeliveryOfProject: { value: penalityForLate, uom: penalityLateDeliveryOfProjectValue },
            transferChargesForSellingBeforePossession: { value: transferCharges, uom: transferChargesValue },
            interestForLatePaymentInPercentage: { value: intersetForLate }
        }

        temDocObject = {
            ...temDocObject,
            keyClauses: keyClausesObject
        }
        console.log(JSON.stringify(temDocObject), "finalObject");
        const params = {
            legalAndDoc: temDocObject
        }
        this.props.saveLegalDocumentsFromData(temDocObject);
        this.props.updateDocumentsAPI(temDocObject).then((res => {
            console.log(res, "updateDocumentsAPI");
            if (res.statusCode == 200) {
                Toast.show(res.message, Toast.LONG);
                if (this.props.onFormComplete) {
                    this.props.onFormComplete();
                }
            }
        }))
            .catch(error => {
                this.setState({ isLoading: false })
                console.log(error, "error");
            })

    }

    onChangeNumberOfYears = (text) => {
        this.setState({ numberOfYears: text })

    }
    onChangePenalityForTermination = (text) => {
        this.setState({ penalityForTermination: text })

    }

    onChangePenalityForLate = (text) => {
        this.setState({ penalityForLate: text })

    }

    onChangeTransferCharges = (text) => {
        this.setState({ transferCharges: text })

    }

    onChangeInterestForLate = (text) => {
        this.setState({ intersetForLate: text })
    }
    onChangeTransferChargesUom = (text) => {
        this.setState({
            transferChargesValue: text
        })
    }
    onChangePenalityLateDeliveryUom = (text) => {
        this.setState({
            penalityLateDeliveryOfProjectValue: text
        })
    }
    render() {
        const { isLoading } = this.state
        return (
            <KeyboardAwareScrollView style={styles.costSheetViewContainer}>
                <Header title={strings.KEY_CLAUSE_OF_BBA} />
                <View style={[styles.innerPageContainer]}>
                    <View>
                        <RoundedInput label={strings.DEFECT_LIABILITY_PERIOD} placeholder={strings.NUMBER_OF_YEARS} onChange={this.onChangeNumberOfYears} value={this.state.numberOfYears} />
                    </View>
                    <View style={styles.textInputMarginTop}>
                        <RoundedInput label={strings.PENALITY_FOR_TERMINATION_OF_CONTRACT_BY_ALLOTTE} placeholder={strings.DEDUCTION_OF_CONSIDERATION} onChange={this.onChangePenalityForTermination} value={this.state.penalityForTermination} />
                    </View>
                    <Text style={styles.label}>{strings.PANALITY_FOR_LATE_DELIVERY_BUILDER}</Text>
                    <View style={styles.UnitView}>
                        <View style={{ width: "55%" }}>
                            <RoundedInput placeholder={"Enter value"} onChange={this.onChangePenalityForLate} value={this.state.penalityForLate} />
                        </View>
                        <View style={{ width: "40%" }}>
                            <Dropdown

                                useNativeDriver={false}
                                pickerStyle={{ borderRadius: 10, alignItems: 'center', right: 0 }}
                                animationDuration={1}
                                rippleDuration={1}
                                onChangeText={(val, i, d) => {
                                    this.setState({ penalityLateDeliveryUom: d[i], penalityLateDeliveryOfProjectValue: val })
                                }}
                                data={this.state.penalityLateDeliveryOfProject.map(item => ({
                                    value: item.value, ...item
                                }))}
                                value={this.state && this.state.penalityLateDeliveryOfProject.length > 0 ? "" : ""}
                                dropdownPosition={-3}
                                renderBase={(props) => (
                                    <View >
                                        <RoundedInput placeholder={"Rs/Month"} onChange={this.onChangePenalityLateDeliveryUom} value={this.state.penalityLateDeliveryOfProjectValue} />
                                    </View>
                                )}

                            />
                        </View>
                    </View>
                    <Text style={styles.label}>{strings.TRANSFER_CHARGES_FOR_SELLING}</Text>
                    <View style={styles.UnitView}>
                        <View style={{ width: "55%" }}>
                            <RoundedInput placeholder={"Enter value"} onChange={this.onChangeTransferCharges} value={this.state.transferCharges} />
                        </View>
                        <View style={{ width: "40%" }} >
                            <Dropdown

                                useNativeDriver={false}
                                pickerStyle={{ borderRadius: 10, alignItems: 'center', right: 0 }}
                                animationDuration={1}
                                rippleDuration={1}
                                onChangeText={(val, i, d) => {
                                    this.setState({ transferChargesUom: d[i], transferChargesValue: val })
                                }}
                                data={this.state.transferChargesForSellingBeforePossession.map(item => ({
                                    value: item.value, ...item
                                }))}
                                value={this.state && this.state.transferChargesForSellingBeforePossession.length > 0 ? "" : ""}
                                dropdownPosition={-3}
                                renderBase={(props) => (
                                    <View >
                                        <RoundedInput placeholder={"Rs/sq ft"} onChange={this.onChangeTransferChargesUom} value={this.state.transferChargesValue} />
                                    </View>
                                )}

                            />
                        </View>
                    </View>




                    <View style={styles.textInputMarginTop}>
                        <RoundedInput label={strings.INTEREST_FOR_LATE_PAYMENT_BY_ALLOTTEE} placeholder={strings.PERCENTAGE_SYMBOLE} onChange={this.onChangeInterestForLate} value={this.state.intersetForLate} />
                    </View>


                    <View style={styles.bottomBtnContainer}>
                        <PGButton
                            btnText={strings.SUBMIT}
                            onPress={() => this.step3Submit()}
                        />
                    </View>
                </View>
                {isLoading && this.renderLoader()}
            </KeyboardAwareScrollView>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        legalDocumentsFormData: state.LegalDocReducer.legalDocumentsFormData
    };
};

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        saveLegalDocumentsFromData,
        updateDocumentsAPI

    }, dispatch);
}
let Step3Container = connect(mapStateToProps, mapDispatchToProps)(Step3);
export default Step3Container;

