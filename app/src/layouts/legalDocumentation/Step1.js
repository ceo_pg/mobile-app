import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import { View, Text } from 'react-native';
import styles from './styles'
import strings from '../../../res/constants/strings';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import Header from './Header'
import FileUploadComp from '../../components/fileUpload/FileUploadComp'
import {PGButton} from '../../components/index'
import { fileUpload } from '../../redux/actions/FileUploadAction'
import { saveLegalDocumentsFromData } from '../../redux/actions/LegalDocumentation/LegalDocumentationAction'
import Toast from 'react-native-simple-toast'


class Step1 extends Component {
    constructor(props) {
        super(props);
        this.bookingForm = ''
        this.builderBuyerAgreementOfSale = ''
        this.allotmentLetter = ''
        this.homeLoanAggrement = ''
        this.bluePrintOfUnit = ''
        this.triParties = ''
        this.saleSeed = ''
        this.deletedFileNames = []
        this.state = {
            progress: {},
        }
    }
    componentDidMount() {

    }

    backPress = () => {
        const { changePage } = this.props;
        if (changePage) {
            changePage(-1)
        }
    }

    step1Submit = () => {
        console.log(this.isValidated(), "isValidated");

        if (!this.isValidated()) {
            Toast.show(strings.PLEASE_UPLOAD_ALL_DOCUMENTS, Toast.LONG);
            return
        }
        console.log(this.isValidated(), "isValidated");
        let documentFormData = {};
        if(this.deletedFileNames && this.deletedFileNames.length > 0){
            documentFormData = {
                bookingForm: { name: this.bookingForm },
                builderBuyerAgreementOfSale: { name: this.builderBuyerAgreementOfSale },
                allotmentLetter: { name: this.allotmentLetter },
                homeLoanAggrement: { name: this.homeLoanAggrement },
                bluePrintOfUnit: { name: this.bluePrintOfUnit },
                triPartieAggrement: { name: this.triParties },
                saleDeed: { name: this.triParties },
                deleteFiles:this.deletedFileNames
            }  
        }else{
            documentFormData = {
                bookingForm: { name: this.bookingForm },
                builderBuyerAgreementOfSale: { name: this.builderBuyerAgreementOfSale },
                allotmentLetter: { name: this.allotmentLetter },
                homeLoanAggrement: { name: this.homeLoanAggrement },
                bluePrintOfUnit: { name: this.bluePrintOfUnit },
                triPartieAggrement: { name: this.triParties },
                saleDeed: { name: this.triParties },
                deleteFiles:[]
            }
        }
       
        this.props.saveLegalDocumentsFromData(documentFormData);
        const { changePage } = this.props;
        if (changePage) {
            changePage(1)

        }
    }

    isValidated = () => {
        if ((this.bookingForm.length == 0 ||
            this.builderBuyerAgreementOfSale.length == 0 ||
            this.allotmentLetter.length == 0 ||
            this.homeLoanAggrement.length == 0 ||
            this.bluePrintOfUnit.length == 0 ||
            this.triParties.length == 0 ||
            this.saleSeed.length == 0)) {
            return false
        } else {
            return true
        }
    }
    saveFileName = (uniqueId, fileName) => {
        switch (uniqueId) {
            case "bookingform":
                return this.bookingForm = fileName;
                break;
            case "builderBuyerFrom":
                this.builderBuyerAgreementOfSale = fileName
                break;
            case "allotmentLatter":
                return this.allotmentLetter = fileName
                break
            case "homeLoanAgreement":
                return this.homeLoanAggrement = fileName
                break
            case "bluePrintOfUnit":
                return this.bluePrintOfUnit = fileName
                break
            case "triPartiesAgreement":
                return this.triParties = fileName
                break
            case "saleSeed":
                return this.saleSeed = fileName
                break

            default:
            // code block
        }
    }

    onDeleteFile = (fileName, uniqueID) => {
        console.log(fileName, uniqueID, "onDeleteFile");
        this.setState({ progress: { ...this.state.progress, [uniqueID]: undefined } });
        if(fileName && fileName.length > 0){
            this.deletedFileNames.push({name:fileName});
        }
    }

    hitUploadAPI = (fileDetails, uniqueID) => {
        console.log(fileDetails, "fileDetails");
        this.props.fileUpload(fileDetails, this.progressCallback.bind(this, uniqueID)).then((response => {
            console.log(response, "file updaded response");
            if (response.statusCode == 200) {
                this.saveFileName(uniqueID, response.data.key);
            }

        }))
            .catch(error => {
                console.log(error, "error");
            })

    }

    progressCallback = (uniqueID, progress) => {
        console.log(progress, "progress%");
        if(progress && progress.loaded ){
            this.setState({ progress: { ...this.state.progress, [uniqueID]: (progress.loaded / (progress.total)) } })

        }
    }

    render() {
        return (

            <KeyboardAwareScrollView style={styles.costSheetViewContainer}>
                <Header title={strings.DOCUMENTS} />
                <View style={styles.innerPageContainer}>
                    <FileUploadComp label={strings.BOOKING_FORM} fileSelectCallack={this.hitUploadAPI} uniqueID="bookingform" progress={this.state.progress["bookingform"]} fileName={this.bookingForm} onDeleteFile={this.onDeleteFile} />
                    <FileUploadComp label={strings.BUILDER_BUYER_AGREEMENT} fileSelectCallack={this.hitUploadAPI} uniqueID="builderBuyerFrom" progress={this.state.progress["builderBuyerFrom"]} fileName={this.builderBuyerAgreementOfSale} onDeleteFile={this.onDeleteFile} />
                    <FileUploadComp label={strings.ALLOTMENT_LATTER} fileSelectCallack={this.hitUploadAPI} uniqueID="allotmentLatter" progress={this.state.progress["allotmentLatter"]} fileName={this.allotmentLetter} onDeleteFile={this.onDeleteFile} />
                    <FileUploadComp label={strings.HOME_LOAN_AGGREMENT} fileSelectCallack={this.hitUploadAPI} uniqueID="homeLoanAgreement" progress={this.state.progress["homeLoanAgreement"]} fileName={this.homeLoanAggrement} onDeleteFile={this.onDeleteFile} />
                    <FileUploadComp label={strings.BLUE_PRINT_OF_UNIT} fileSelectCallack={this.hitUploadAPI} uniqueID="bluePrintOfUnit" progress={this.state.progress["bluePrintOfUnit"]} fileName={this.bluePrintOfUnit} onDeleteFile={this.onDeleteFile} />
                    <FileUploadComp label={strings.TRI_PATIES_AGREEMENT} fileSelectCallack={this.hitUploadAPI} uniqueID="triPartiesAgreement" progress={this.state.progress["triPartiesAgreement"]} fileName={this.triParties} onDeleteFile={this.onDeleteFile} />
                    <FileUploadComp label={strings.SALE_SEED} fileSelectCallack={this.hitUploadAPI} uniqueID="saleSeed" progress={this.state.progress["saleSeed"]} fileName={this.saleSeed} onDeleteFile={this.onDeleteFile} />
                    <View style={styles.textContainer}>
                        <Text style={styles.bottomText}>{strings.YOUR_INFORMATION_SAFE_WITH_US}</Text>
                    </View>
                    <View style={styles.bottomBtnContainer}>
                        <PGButton
                            btnText={strings.SUBMIT}
                            onPress={() => this.step1Submit()}
                        />
                    </View>
                </View>
            </KeyboardAwareScrollView>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        legalDocumentsFormData: state.LegalDocReducer.legalDocumentsFormData
    };
};

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        fileUpload,
        saveLegalDocumentsFromData

    }, dispatch);
}
let Step1Container = connect(mapStateToProps, mapDispatchToProps)(Step1);
export default Step1Container;

