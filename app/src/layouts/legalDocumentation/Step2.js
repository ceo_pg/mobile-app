import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import { View, Text } from 'react-native';
import styles from './styles'
import strings from '../../../res/constants/strings';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import Header from './Header'
import FileUploadComp from '../../components/fileUpload/FileUploadComp'
import { fileUpload} from '../../redux/actions/FileUploadAction'
import { saveLegalDocumentsFromData } from '../../redux/actions/LegalDocumentation/LegalDocumentationAction'

import { PGButton, Loader } from '../../components/index'

class Step2 extends Component {
    constructor(props) {
        super(props);
        this.doc1 = ''
        this.doc2 = ''
        this.doc3 = ''
        this.deletedFileNames = []
        this.state = {
            otherDocArray: [],
            progress: {}
        }
    }
    componentDidMount() {

    }

    backPress = () => {
        const { changePage } = this.props;
        changePage(-1)
    }

    onDeleteFile = (fileName, uniqueID) => {
        console.log(fileName, uniqueID, "onDeleteFile");
        this.setState({ progress: { ...this.state.progress, [uniqueID]: undefined } });
        if(fileName && fileName.length > 0){
            this.deletedFileNames.push({name:fileName});
        }
    }

    saveFileName = (uniqueId, fileName) => {
        switch (uniqueId) {
            case "OTHER_DOCUMENTS1":
                return this.doc1 = fileName;
                break;
            case "OTHER_DOCUMENTS2":
                this.doc2 = fileName
                break;
            case "OTHER_DOCUMENTS3":
                return this.doc3 = fileName
                break

            default:
            // code block
        }
    }

    step2Submit = () => {
        let otherDocsArray = []
        if (this.doc1.length > 0) {
            otherDocsArray.push({name:this.doc1});
        } if (this.doc2.length > 0) {
            otherDocsArray.push({name:this.doc2});
        } if (this.doc3.length > 0) {
            otherDocsArray.push({name:this.doc3});
        }
        this.setState({ otherDocArray: otherDocsArray });
        let temDocObject = this.props.legalDocumentsFormData
        temDocObject = {
            ...temDocObject,deleteFiles:[...temDocObject.deleteFiles, ...this.deletedFileNames],
            otherDocuments: otherDocsArray
        }
        console.log(temDocObject, "temDocObject");
        this.props.saveLegalDocumentsFromData(temDocObject);
        const { changePage } = this.props;
        if (changePage) {
            changePage(2)
        }
    }

    hitUploadAPI = (fileDetails, uniqueID) => {
        console.log(fileDetails, "fileDetails");
        this.props.fileUpload(fileDetails, this.progressCallback.bind(this, uniqueID)).then((response => {
            console.log(response, "file updaded response");
            if (response.statusCode == 200) {
                this.saveFileName(uniqueID, response.data.key);
            }

        }))
            .catch(error => {
                console.log(error, "error");
            })

    }

    progressCallback = (uniqueID, progress) => {
        console.log(progress, "progress%")
        this.setState({ progress: { ...this.state.progress, [uniqueID]: (progress.loaded / (progress.total)) } })
    }


    render() {
        console.log(this.state.otherDocArray, "otherDocArray")

        return (

            <KeyboardAwareScrollView style={styles.costSheetViewContainer}>
                <Header title={strings.OTHER_DOCUMENTS}/>
                <View style={styles.innerPageContainer}>
                    <FileUploadComp label={strings.OTHER_DOCUMENTS1} fileSelectCallack={this.hitUploadAPI} uniqueID="OTHER_DOCUMENTS1" progress={this.state.progress["OTHER_DOCUMENTS1"]} fileName={this.doc1} onDeleteFile={this.onDeleteFile}/>
                    <FileUploadComp label={strings.OTHER_DOCUMENTS2} fileSelectCallack={this.hitUploadAPI} uniqueID="OTHER_DOCUMENTS2" progress={this.state.progress["OTHER_DOCUMENTS2"]} fileName={this.doc1} onDeleteFile={this.onDeleteFile}/>
                    <FileUploadComp label={strings.OTHER_DOCUMENTS3} fileSelectCallack={this.hitUploadAPI} uniqueID="OTHER_DOCUMENTS3" progress={this.state.progress["OTHER_DOCUMENTS3"]} fileName={this.doc1} onDeleteFile={this.onDeleteFile}/>
                    <View style={styles.textContainer}>
                        <Text style={styles.bottomText}>{strings.YOUR_INFORMATION_SAFE_WITH_US}</Text>
                    </View>
                    <View style={styles.bottomBtnContainer}>
                        <PGButton
                            btnText={strings.SUBMIT}
                            onPress={() => this.step2Submit()}
                        />
                    </View>
                </View>

            </KeyboardAwareScrollView>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        legalDocumentsFormData:state.LegalDocReducer.legalDocumentsFormData
    };
};

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        fileUpload,
        saveLegalDocumentsFromData
    }, dispatch);
}
let Step2Container = connect(mapStateToProps, mapDispatchToProps)(Step2);
export default Step2Container;

