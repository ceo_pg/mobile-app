import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image } from 'react-native';
import styles from './styles'
import res from '../../../res'


class Header extends Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }

    render() {
        const { title, onBack } = this.props
        return (
            <View>
                <View style={styles.headerView}>
                    <Text style={styles.headerTitle}>{title ? title : ''}</Text>
                </View>
                {onBack &&
                <TouchableOpacity style={styles.headerLeftView} onPress={() => onBack()}>
                <Image source={res.images.icn_back} resizeMode={'contain'} />
            </TouchableOpacity>
                }
                
            </View>

        )
    }
}

export default Header;

