import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import { View, SafeAreaView, TouchableOpacity, Text, Image, ImageBackground, Linking, Dimensions, Platform } from 'react-native';
import { CommonActions } from '@react-navigation/native';
import PDFView from 'react-native-view-pdf';
import ImageZoom from 'react-native-image-pan-zoom';
import Modal from 'react-native-modal'
import Pdf from 'react-native-pdf';

import styles from './styles'
import strings from '../../../res/constants/strings';
import DashBoardHeader from '../../components/header/DashBoardHeader'
import CircleProgress from '../../components/progressComponent/CircleProgress'
import Step1 from './Step1'
import Step2 from './Step2'
import Step3 from './Step3'
import ViewPager from '@react-native-community/viewpager';
import res from '../../../res'
import SupportView from './SupportView'
// import Modal from '../../components/modal/Modal'
import ShareModal from '../../components/modal/ShareModal'
import Input from '../../components/input/OtpField'
import Carousel, { Pagination } from 'react-native-snap-carousel';
import DocumentItemComp from '../../components/legalDoc/DocumentItemComp'
import KeyClauseItemComp from '../../components/legalDoc/KeyClauseItemComp'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { getLegalDocuments, saveLegalDocuments, updateDocumentsAPI } from '../../redux/actions/LegalDocumentation/LegalDocumentationAction'
import { Loader } from '../../components/index'
import Toast from 'react-native-simple-toast'
import NoDataFoundComp from '../../components/noDataFound/NoDataFoundComp'
import { PickerViewModal, CAMERA, DOCUMENT, IMAGE_PICK } from '../../components/Picker/PickerViewModal'
import { heightScale, myWidth, checkIsImageOrPdfFileType, checkValidFileSize, validatePasscode } from '../../utility/Utils'
import ImagePicker from 'react-native-image-picker'
import DocumentPicker from 'react-native-document-picker'
import { fileUpload } from '../../redux/actions/FileUploadAction'

const options = {
    title: 'Select Image',
    mediaType: 'photo',
    allowsEditing: false,
    quality: 1.0,
    maxWidth: 500,
    maxHeight: 400,
    progress: {},
    storageOptions: {
        skipBackup: true,
        path: 'images',
    },
}
const carouselHeaders = [{ name: strings.DOCUMENTS, id: 0 }, { name: strings.OTHER_DOCUMENTS, id: 1 }, { name: strings.KEY_CLAUSE_OF_BBA, id: 2 }]
class LegalDocBaseScreen extends Component {
    constructor(props) {
        super(props);
        this.uniqueID = ''
        this.selectedURL = ''
        this.state = {
            pageIndex: 0,
            progressImg: res.images.step1_L,
            isFormCompleted: false,
            otp1: '',
            otp2: '',
            otp3: '',
            otp4: '',
            activeIndex: 0,
            isLoading: false,
            pickOptionsVisible: false,
            progress: {},
            isModalVisible: false,
            isDataModal: false,
            isShare:false
        }
        this.viewPager = React.createRef();
        this.modalRef = React.createRef();
        this.otpFirst = React.createRef();
        this.otpSecond = React.createRef();
        this.otpThird = React.createRef();
        this.otpFourth = React.createRef();
    }

    componentDidMount() {
        this.getLegalDocuments()
    }

    getLegalDocuments = () => {
        this.setState({ isLoading: true })
        this.props.getLegalDocuments().then((response => {

            this.setState({ isLoading: false });
            if (response.statusCode == 200 && response.status) {
                console.log("getLegalDocuments", response)
                this.props.saveLegalDocuments(response.data);
                if (response.data && response.data.legalAndDoc && response.data.legalAndDoc.bookingForm) {
                    this.setState({ isFormCompleted: true })
                } else {
                    this.setState({ isFormCompleted: false })

                }
            } else {
                Toast.show(response.message, Toast.LONG)
            }
        }))
            .catch(error => {
                this.setState({ isLoading: false })
                console.log(error, "error");
            })
    }

    openDrawer = () => {

        const resetAction = CommonActions.reset({
            index: 0,
            routes: [{ name: 'LegalDocBaseScreen' }],
        });
        this.props.navigation.dispatch(resetAction);
        this.props.navigation.openDrawer()
        // this.props.navigation.openDrawer()
    }

    changePageIndex = (index) => {
        if (index == -1) {
            // close the drwr
            return
        }
        this.setState({ pageIndex: index })
        this.viewPager.setPage(index);
        this.setProgressImage(index)
    }

    pageChanged = (e) => {
        if (e && e.nativeEvent && e.nativeEvent) {
            this.setState({ pageIndex: e.nativeEvent.position });
            this.setProgressImage(e.nativeEvent.position)
        }
    }

    setProgressImage = (index) => {
        switch (index) {
            case 0:
                // code block
                this.setState({ progressImg: res.images.step1_L })
                break;
            case 1:
                // code block
                this.setState({ progressImg: res.images.step2_L })
                break;
            case 2:
                this.setState({ progressImg: res.images.step3_L })
                break
            default:
            // code block
        }
    }

    onSchedulePress = () => {
        this.displayShareModal();
    }

    onfocusFirstOtp = () => {

        this.otpFirst.current.focus();
    }
    onfocusSecondOtp = () => {

        this.otpSecond.current.focus();
    }
    onfocusThirdOtp = () => {
        this.otpThird.current.focus();
    }
    onfocusFourthOtp = () => {
        this.otpFourth.current.focus();
    }
    // onfocusFiveOtp = () => {
    //     this.otpFive.current.focus();
    // }
    // onfocusSixOtp = () => {
    //     this.otpSix.current.focus();
    // }
    onOtp1Change = (text) => {
        if (text.length == 1) {
            this.onfocusSecondOtp();
        }
        this.setState({
            otp1: text
        }, () => {
            this.validatePassCode();
        });
    }
    onOtp2Change = (text) => {
        if (text.length == 1) {
            this.onfocusThirdOtp();
        }
        this.setState({
            otp2: text
        }, () => {
            this.validatePassCode();
        });
    }
    onOtp3Change = (text) => {
        if (text.length == 1) {
            this.onfocusFourthOtp();
        }
        this.setState({
            otp3: text
        }, () => {
            this.validatePassCode();
        });
    }
    onOtp4Change = (text) => {
        // if (text.length == 1) {
        //     this.onfocusFiveOtp();
        // }
        this.setState({
            otp4: text
        }, () => {
            this.validatePassCode();
        });

    }

    validatePassCode = () => {
        // let appUsrObj = AppUser.getInstance();
        // let phoneNumber  = appUsrObj.phone;
        // var lastFourDigit = phoneNumber.substr(phoneNumber.length - 4)
        const { otp1, otp2, otp3, otp4,isShare } = this.state;
        if (otp1.length > 0 && otp2.length > 0 && otp3.length > 0 && otp4.length > 0) {
            let passCode = otp1 + otp2 + otp3 + otp4;
            console.log(validatePasscode(passCode), "validatePasscode");
            if (validatePasscode(passCode)) {
                isShare?
                this.OpenWeb(this.selectedURL)
                :
                this.OpenURL(this.selectedURL);
                setTimeout(() => {
                    this.setState({ otp1: '', otp2: '', otp3: '', otp4: '' });
                }, 1000);
            } else {
                Toast.show("Passcode Dosen't matched", Toast.LONG);
            }
            // if(lastFourDigit == otp1 + otp2 + otp3 + otp4){
            //     this.OpenURL();
            // }
        }
    }
    OpenWeb = (url) => {
        this.toggleModal()
        Linking.canOpenURL(url).then(supported => {
            if (supported) {
                Linking.openURL(url);
            } else {
                console.log("Don't know how to open URI: " + url);
            }
        });
    }
    OpenURL = (url) => {
        this.toggleModal()
        console.log("URL", url)
        if (!url) {
            return <Text>No data found</Text>
        } else {
            setTimeout(() => {
                this.toggleDataModal()
            }, 1000);


        }

    }

    onBackFromOtp2 = (event) => {
        if (event === 'Backspace') {
            this.setState({ otp2: '' })
            this.onfocusFirstOtp();
        }
    }
    onBackFromOtp3 = (event) => {
        if (event === 'Backspace') {
            this.setState({ otp3: '' })
            this.onfocusSecondOtp();
        }

    }
    onBackFromOtp4 = (event) => {
        if (event === 'Backspace') {
            this.setState({ otp4: '' })
            this.onfocusThirdOtp();
        }
    }

    showContentView = () => {
        const { isFormCompleted } = this.state;
        if (isFormCompleted) {
            return (
                // <View style={{ flex: 1 }}>
                //     {this.renderCarusalView()}
                // </View>
                <View style={{ flex: 1 }}>
                    <View style={styles.supportCont}>
                        <SupportView onSchedulePress={this.onpenCalendyLink} />
                    </View>
                    <View style={{ flex: 1 }}>
                        {this.renderCarusalView()}
                    </View>

                </View>
            )
        } else {
            return this.showFormView()
        }

    }

    onpenCalendyLink = () => {
        let url = "https://calendly.com/propguide_legal"
        Linking.canOpenURL(url).then(supported => {
            if (supported) {
                Linking.openURL(url);
            } else {
                console.log("Don't know how to open URI: " + url);
            }
        });
        // this.OpenURL("https://calendly.com/propguide_legal")
    }

    showFormView = () => {
        const { progressImg } = this.state

        return (
            <View style={{ flex: 1 }}>
                <View style={styles.progressContainer}>
                    <CircleProgress pageIndex={this.state.pageIndex} progressImg={progressImg} />
                </View>
                <View style={styles.outerViewPager}>
                    <ViewPager style={styles.viewPager} initialPage={0}
                        ref={(viewPager) => { this.viewPager = viewPager }}
                        onPageSelected={(e) => this.pageChanged(e)}
                        scrollEnabled={false}
                    >
                        <View key="1">
                            <Step1 changePage={this.changePageIndex} />
                        </View>
                        <View key="2">
                            <Step2 changePage={this.changePageIndex} />
                        </View>
                        <View key="3">
                            <Step3 changePage={this.changePageIndex} onFormComplete={this.getLegalDocuments} />
                        </View>
                    </ViewPager>
                </View>

            </View>
        )
    }

    displayShareModal = (isShare) => {
        this.toggleModal(isShare)
        // this.modalRef.current.setView(this.renderShareModal);
        // this.modalRef.current.show();
    }

    closeModal = () => {
        this.toggleModal()
    }

    displayOtpModal = (isShare) => {
        this.toggleModal(isShare)
        // this.modalRef.current.setView(this.renderOtpModal);
        // this.modalRef.current.show();
    }

    renderShareModal = () => {
        return (
            <View style={{ justifyContent: 'center' }}>
                <ShareModal closeModal={this.closeModal} />
            </View>
        )
    }


    renderOtpModal = () => {
        return (
            <View style={styles.otpContainerView}>
                <TouchableOpacity style={styles.closeBtnView} onPress={() => this.closeModal()}>
                    <Image source={res.images.icnClose} resizeMode={'contain'} style={{ height: 40, width: 40 }} />
                </TouchableOpacity>
                <Text style={styles.passcodeText}>{strings.SHARE_PASSCODE_SHARED_ON_YOUR_DEVICE}</Text>
                <View style={styles.iconsCOntainer}>
                    <Input
                        value={this.state.otp1}
                        onChangeText={this.onOtp1Change}
                        containerStyle={styles.textInputStyle}
                        inputProps={{
                            maxLength: 1,
                            keyboardType: "numeric",
                            secureTextEntry: true,
                            returnKeyType: "next",
                            ref: this.otpFirst,
                            textAlign: "center",
                        }}
                        inputStyles={styles.inputStyle}
                    />
                    <Input
                        value={this.state.otp2}
                        onBackKeyPress={this.onBackFromOtp2}
                        onChangeText={this.onOtp2Change}
                        containerStyle={styles.textInputStyle}
                        inputProps={{
                            keyboardType: 'numeric',
                            secureTextEntry: true,
                            maxLength: 1,
                            returnKeyType: "next",
                            ref: this.otpSecond,
                            textAlign: "center",
                        }}
                        inputStyles={styles.inputStyle}
                    />
                    <Input
                        value={this.state.otp3}
                        onBackKeyPress={this.onBackFromOtp3}
                        onChangeText={this.onOtp3Change}
                        containerStyle={styles.textInputStyle}
                        inputProps={{
                            keyboardType: 'numeric',
                            returnKeyType: "next",
                            maxLength: 1,
                            secureTextEntry: true,
                            ref: this.otpThird,
                            textAlign: "center",
                        }}
                        inputStyles={styles.inputStyle}
                    />
                    <Input
                        value={this.state.otp4}
                        onBackKeyPress={this.onBackFromOtp4}
                        onChangeText={this.onOtp4Change}
                        containerStyle={styles.textInputStyle}
                        inputProps={{
                            keyboardType: 'numeric',
                            maxLength: 1,
                            returnKeyType: "next",
                            secureTextEntry: true,
                            ref: this.otpFourth,
                            textAlign: "center",
                        }}
                        inputStyles={styles.inputStyle}
                    />
                </View>
            </View>
        )
    }

    renderCarusalView = () => {
        return (
            <View style={{ flex: 1 }}>
                <Carousel
                    data={carouselHeaders}
                    renderItem={this.renderData}
                    sliderWidth={myWidth}
                    itemWidth={306}
                    layout={'default'}
                    removeClippedSubviews={true}
                    onSnapToItem={index => this.setState({ activeIndex: index })}
                    firstItem={this.state.activeIndex}
                />

                <Pagination
                    dotsLength={3}
                    dotStyle={styles.dotStyle}
                    dotColor={styles.dotColor}
                    inactiveDotStyle={styles.inactiveDotStyle}
                    activeDotIndex={this.state.activeIndex}
                    inactiveDotOpacity={0.4}
                    inactiveDotScale={0.6}
                    activeOpacity={1}
                />
            </View>
        )
    }

    renderData = ({ item, index }) => {
        return <View style={styles.carouselView} >
            <View style={[styles.shadowHeader, { height: 42 }]}>
                <Text style={styles.corousalNameText}>{item.name}</Text>
            </View>
            {this.renderCorousalData(item)}

            {/* {this.viewMore(item)} */}
        </View>
    }

    renderCorousalData = (item, index) => {
        const id = item.id
        switch (id) {
            case 0:
                return this.renderDocumentItems()
                break;
            case 1: return this.renderOtherDocuments()
                break;
            case 2:
                return this.renderKeyClause()
                break
            default: return
        }

    }
    shareDocument = (data, isShare) => {
        console.log("SHAREDOC",data,isShare)
        setTimeout(() => {
            this.displayOtpModal(isShare);
            this.selectedURL = data.base ? data.base : ''
        }, 1000);
    }
    renderDocumentItems = () => {
        const { legalAndDoc } = this.props.legalDocuments;
        if (!legalAndDoc) {
            return
        }
        return (
            <KeyboardAwareScrollView>
                <View style={{ flex: 1, paddingBottom: heightScale(60) }}>
                    <DocumentItemComp label={strings.BOOKING_FORM} data={legalAndDoc.bookingForm} onUpdateDoc={this.showPickerOptions} shareDocument={this.shareDocument} uniqueID="bookingform" viewDocument={this.viewDocument} />
                    <DocumentItemComp label={strings.BUILDER_BUYER_AGREEMENT} data={legalAndDoc.builderBuyerAgreementOfSale} onUpdateDoc={this.showPickerOptions} shareDocument={this.shareDocument} uniqueID="builderBuyerFrom" viewDocument={this.viewDocument} />
                    <DocumentItemComp label={strings.ALLOTMENT_LATTER} data={legalAndDoc.allotmentLetter} onUpdateDoc={this.showPickerOptions} shareDocument={this.shareDocument} uniqueID="allotmentLetter" viewDocument={this.viewDocument} />
                    <DocumentItemComp label={strings.HOME_LOAN_AGGREMENT} data={legalAndDoc.homeLoanAggrement} onUpdateDoc={this.showPickerOptions} shareDocument={this.shareDocument} uniqueID="homeLoanAggrement" viewDocument={this.viewDocument} />
                    <DocumentItemComp label={strings.BLUE_PRINT_OF_UNIT} data={legalAndDoc.bluePrintOfUnit} onUpdateDoc={this.showPickerOptions} shareDocument={this.shareDocument} uniqueID="bluePrintOfUnit" viewDocument={this.viewDocument} />
                    <DocumentItemComp label={strings.TRI_PATIES_AGREEMENT} data={legalAndDoc.triPartieAggrement} onUpdateDoc={this.showPickerOptions} shareDocument={this.shareDocument} uniqueID="triPartieAggrement" viewDocument={this.viewDocument} />
                    <DocumentItemComp label={strings.SALE_SEED} data={legalAndDoc.saleDeed} onUpdateDoc={this.showPickerOptions} shareDocument={this.shareDocument} uniqueID="saleDeed" viewDocument={this.viewDocument} />
                </View>
            </KeyboardAwareScrollView>
        )
    }

    renderOtherDocuments = () => {
        const { legalAndDoc } = this.props.legalDocuments;
        let otherDocuments = []
        if (!legalAndDoc || !(legalAndDoc.otherDocuments) || (legalAndDoc.otherDocuments.length == 0)) {
            otherDocuments = [1, 2, 3]
        } else {
            otherDocuments = legalAndDoc.otherDocuments
        }
        return (
            <KeyboardAwareScrollView>
                <View style={{ flex: 1, paddingBottom: heightScale(60) }}>
                    <DocumentItemComp label={strings.OTHER_DOCUMENTS1} data={otherDocuments[0]} onUpdateDoc={this.showPickerOptions} shareDocument={this.shareDocument} uniqueID={strings.OTHER_DOCUMENTS1} viewDocument={this.viewDocument} />
                    <DocumentItemComp label={strings.OTHER_DOCUMENTS2} data={otherDocuments[1]} onUpdateDoc={this.showPickerOptions} shareDocument={this.shareDocument} uniqueID={strings.OTHER_DOCUMENTS2} viewDocument={this.viewDocument} />
                    <DocumentItemComp label={strings.OTHER_DOCUMENTS3} data={otherDocuments[2]} onUpdateDoc={this.showPickerOptions} shareDocument={this.shareDocument} uniqueID={strings.OTHER_DOCUMENTS3} viewDocument={this.viewDocument} />

                </View>
            </KeyboardAwareScrollView>
        )
    }

    renderKeyClause = () => {
        const { legalAndDoc } = this.props.legalDocuments;

        const keyClauses = legalAndDoc && legalAndDoc.keyClauses ? legalAndDoc.keyClauses : null
        console.log("LEGALDOCBASE", keyClauses)
        if (!keyClauses) {
            return (
                <NoDataFoundComp />
            )
        }
        return (
            <KeyboardAwareScrollView>
                <View style={{ flex: 1 }}>
                    <KeyClauseItemComp icon={res.images.numbYear_ic} marketDifference={keyClauses.defectLiabilityPeriodInYears.marketDifference} value={keyClauses.defectLiabilityPeriodInYears.value} arrowDirection={keyClauses.defectLiabilityPeriodInYears.marketComparision} label={" " + strings.YEARS} img={res.images.proof_ic} subLabel={strings.DEFECT_LIABILITY_PERIOD} />
                    <KeyClauseItemComp icon={res.images.percent_ic} marketDifference={keyClauses.penalityTerminationOfContractInPercentage.marketDifference} value={keyClauses.penalityTerminationOfContractInPercentage.value} arrowDirection={keyClauses.penalityTerminationOfContractInPercentage.marketComparision} label={strings.PERCENTAGE_SYMBOLE} img={res.images.proof_ic} subLabel={strings.PENALITY_FOR_TERMINATION_OF_CONTRACT_BY_ALLOTTE} />
                    <KeyClauseItemComp icon={res.images.percent_ic} marketDifference={keyClauses.penalityLateDeliveryOfProject.marketDifference} value={keyClauses.penalityLateDeliveryOfProject.value} arrowDirection={keyClauses.penalityLateDeliveryOfProject.marketComparision} label={" " + keyClauses.penalityLateDeliveryOfProject.uom} img={res.images.proof_ic} subLabel={strings.PANALITY_FOR_LATE_DELIVERY_BUILDER} />
                    <KeyClauseItemComp icon={res.images.percent_ic} marketDifference={keyClauses.transferChargesForSellingBeforePossession.marketDifference} value={keyClauses.transferChargesForSellingBeforePossession.value} arrowDirection={keyClauses.transferChargesForSellingBeforePossession.marketComparision} label={" " + keyClauses.transferChargesForSellingBeforePossession.uom} img={res.images.proof_ic} subLabel={strings.TRANSFER_CHARGES_FOR_SELLING} />
                    <KeyClauseItemComp icon={res.images.percent_ic} marketDifference={keyClauses.interestForLatePaymentInPercentage.marketDifference} value={keyClauses.interestForLatePaymentInPercentage.value} arrowDirection={keyClauses.interestForLatePaymentInPercentage.marketComparision} label={strings.PERCENTAGE_SYMBOLE} img={res.images.proof_ic} subLabel={strings.INTEREST_FOR_LATE_PAYMENT_BY_ALLOTTEE} />

                </View>
            </KeyboardAwareScrollView>
        )
    }

    renderLoader = () => {
        return (
            <Loader
                loading={this.state.isLoading} />
        );
    };

    onClickPickType = (type) => {
        this.setState({
            pickOptionsVisible: null
        })
        setTimeout(() => {
            switch (type) {
                case DOCUMENT:
                    // Open File picker
                    this.openDocument()
                    break;
                case IMAGE_PICK:
                    // Open Image Library:
                    ImagePicker.launchImageLibrary(options, this.onResultFromImagePicker);
                    break;
                case CAMERA:
                    // Launch Camera:
                    ImagePicker.launchCamera(options, this.onResultFromImagePicker);
                    break;
                default:
                    break;
            }
        }, 500)

    }

    openDocument = async () => {
        try {
            const res = await DocumentPicker.pick({
                type: [DocumentPicker.types.pdf, DocumentPicker.types.images],
            });
            console.log("Document", res)
            this.onResultFromPicker(res);
        } catch (err) {
            this.setState({
                pickOptionsVisible: null
            })
            if (DocumentPicker.isCancel(err)) {
                // User cancelled the picker, exit any dialogs or menus and move on
            } else {
                throw err;
            }
        }

    }

    onResultFromImagePicker = (response) => {
        console.log("onResultFromImagePicker", response)

        if (response.didCancel) {
            this.setState({
                pickOptionsVisible: null
            })
        } else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
            this.setState({
                pickOptionsVisible: null
            })
        } else {
            this.onResultFromPicker(response);
        }
    }

    onResultFromPicker = (response) => {
        if (!response) {
            Toast.show("Please try again")
            return
        }
        console.log("uri => " + response.uri + "\n " +
            "type => " + response.type + "\n Size => " +
            response.size ? response.size : response.fileSize)
        this.setState({
            pickOptionsVisible: null
        })
        if (checkIsImageOrPdfFileType(response.type)) {
            if (checkValidFileSize(response.size ? response.size : response.fileSize)) {
                console.log("checkValidFileSize", response.size)
            } else {
                return Toast.show("Size can not exceeds 10 MB")
            }
        } else {
            return Toast.show("Please select pdf,jpg,jpeg,png format documents only")
        }
        // UPLOAD File then hit update DOC API
        this.hitUploadAPI(response)

    }

    hitUploadAPI = (fileDetails) => {
        this.props.fileUpload(fileDetails, this.progressCallback.bind(this, this.uniqueID)).then((response => {
            console.log(response, "file updaded response");
            if (response.statusCode == 200 && response.status) {
                this.updateDocument(response.data.key);
            } else {
                Toast.show(response.message ? response.message : strings.PLEASE_TRY_AFTER_SOMETIME);
            }

        }))
            .catch(error => {
                console.log(error, "error");
            })

    }

    progressCallback = (uniqueID, progress) => {
        this.setState({ progress: { ...this.state.progress, [uniqueID]: (progress.loaded / (progress.total)) } }, () => {
            console.log(this.state.progress, "progress value")
        })
    }

    updateDocument = (fileName) => {
        const { legalAndDoc } = this.props.legalDocuments;
        let temDocObject = legalAndDoc;
        if (temDocObject.bookingForm) {
            delete temDocObject.bookingForm.base
        } if (temDocObject.builderBuyerAgreementOfSale) {
            delete temDocObject.builderBuyerAgreementOfSale.base
        } if (temDocObject.allotmentLetter) {
            delete temDocObject.allotmentLetter.base
        } if (temDocObject.homeLoanAggrement) {
            delete temDocObject.homeLoanAggrement.base
        } if (temDocObject.bluePrintOfUnit) {
            delete temDocObject.bluePrintOfUnit.base
        } if (temDocObject.triPartieAggrement) {
            delete temDocObject.triPartieAggrement.base
        }
        if (temDocObject.saleDeed) {
            delete temDocObject.saleDeed.base
        }
        for (var i = 0; i < 3; i++) {
            if (temDocObject.otherDocuments && temDocObject.otherDocuments.length > i) {
                if (temDocObject.otherDocuments[i] && temDocObject.otherDocuments[i].base) {
                    delete temDocObject.otherDocuments[i].base;
                }
            }
            else {
                temDocObject.otherDocuments.push({ name: '' })
            }
        }
        // let newArr = [];

        // for(let i= 0; i< 3 ;i++)
        // {
        //     newArr.push(temDocObject.otherDocuments[i]);
        // }

        // temDocObject.otherDocuments = newArr;

        if (this.uniqueID == 'Other Dcoument1') {
            temDocObject.otherDocuments[0].name = fileName
        }
        else if (this.uniqueID == 'Other Dcoument2' && temDocObject.otherDocuments.length > 1) {
            temDocObject.otherDocuments[1].name = fileName
        }
        else if (this.uniqueID == 'Other Dcoument3' && temDocObject.otherDocuments.length > 2) {
            temDocObject.otherDocuments[2].name = fileName
        }


        switch (this.uniqueID) {
            case "bookingform":
                temDocObject.bookingForm.name = fileName
                break;
            case "builderBuyerFrom":
                temDocObject.builderBuyerAgreementOfSale.name = fileName
                break;
            case "allotmentLetter":
                temDocObject.allotmentLetter.name = fileName
                break
            case "homeLoanAggrement":
                temDocObject.homeLoanAggrement.name = fileName
                break
            case "bluePrintOfUnit":
                temDocObject.bluePrintOfUnit.name = fileName
                break
            case "triPartieAggrement":
                temDocObject.triPartieAggrement.name = fileName
                break
            case "saleDeed":
                temDocObject.saleDeed.name = fileName
                break

            // for other documents


            default:
            // code block
        }
        this.setState({ isLoading: true })
        //  alert(this.uniqueID)
        // console.log('temDocObject', temDocObject);
        this.props.updateDocumentsAPI(temDocObject).then((res => {
            this.setState({ isLoading: false })
            if (res.statusCode == 200 && res.status) {
                this.getLegalDocuments();
                setTimeout(() => {
                    Toast.show(res.message, Toast.LONG);
                }, 2000);
            } else {
                Toast.show(res.message, Toast.LONG);
            }
        }))
            .catch(error => {
                this.setState({ isLoading: false })
                console.log(error, "error");
            })
    }

    onPressBackDrop = () => {
        this.setState({
            pickOptionsVisible: null
        })
    }
    showPickerOptions = (uniqueID) => {
        setTimeout(() => {
            this.setState({
                pickOptionsVisible: 'bottom'
            })
        }, 1000);
        this.uniqueID = uniqueID;
    }

    viewDocument = (data) => {
        //  this.closeModal();
        setTimeout(() => {
            this.displayOtpModal();
            this.selectedURL = data.base ? data.base : ''
        }, 1000);
    }
    openSupport = () => {
        this.props.navigation.navigate("SupportBaseScreen")
    }
    renderDataModal = () => {
        const brochurePdf = this.selectedURL
        const source = { uri: brochurePdf, cache: true }
        // console.log("BROUCHER", brochure)
        const resources = {
            file: Platform.OS === 'ios' ? 'downloadedDocument.pdf' : '/sdcard/Download/downloadedDocument.pdf',
            url: brochurePdf,
            base64: 'JVBERi0xLjMKJcfs...',
        }
        const resourceType = 'url';
        console.log("renderDataModal====.", brochurePdf)
        if (!brochurePdf) {
            return <View />
        }
        return (
            brochurePdf.toLowerCase().includes(".pdf") ?
                <View style={{ borderRadius: 10, flex: 1 }}>

                    <View style={styles.closeBtn}>
                        <TouchableOpacity onPress={() => this.toggleDataModal()}>
                            <Image source={res.images.icnClose} style={{ width: 20, height: 20, tintColor: res.colors.white }} resizeMode={'center'} />
                        </TouchableOpacity>
                    </View>
                    {

                        Platform.OS == 'ios' ?
                            <Pdf
                                source={source}
                                style={{ flex: 1 }}
                            />
                            :
                            <PDFView
                                // fadeInDuration={250.0}
                                style={{ flex: 1, }}
                                resource={resources[resourceType]}
                                resourceType={resourceType}
                                onLoad={() => console.log(`PDF rendered from ${resourceType}`)}
                                onError={(error) => console.log('Cannot render PDF', error)}
                            />
                    }
                </View>
                :

                <View style={{ borderRadius: 10, flex: 1 }}>
                    <View style={styles.closeBtn}>
                        <TouchableOpacity onPress={() => this.toggleDataModal()}>
                            <Image source={res.images.icnClose} style={{ width: 20, height: 20, tintColor: res.colors.white }} resizeMode={'center'} />
                        </TouchableOpacity>
                    </View>
                    <View style={{ width: "100%", height: "100%", zIndex: 9999, overflow: 'hidden' }}>
                        <ImageZoom cropWidth={Dimensions.get('window').width}
                            cropHeight={Dimensions.get('window').height}
                            imageWidth={Dimensions.get('window').width}
                            imageHeight={Dimensions.get('window').height}>

                            <Image source={brochurePdf ? { uri: brochurePdf } : res.images.parking} style={{ flex: 1 }} resizeMode={'contain'} />

                        </ImageZoom>

                    </View>
                </View>



        )
    }
    toggleModal = (isShare) => {
        console.log("ISSHARE",isShare)
        const { isModalVisible } = this.state
        isShare ?

            this.setState({ isModalVisible: !isModalVisible, isShare: true })
            :
            this.setState({ isModalVisible: !isModalVisible, isShare: false })
    }
    toggleDataModal = () => {
        const { isDataModal } = this.state
        console.log("toggleDataModal", isDataModal)
        this.setState({ isDataModal: !isDataModal })
    }
    render() {
        const { isLoading, isModalVisible, isDataModal } = this.state;
        console.log("MODALDATA", isModalVisible, isDataModal)
        return (
            <SafeAreaView style={styles.fullScreen}>
                <Modal isVisible={this.state.isModalVisible}
                    // onSwipeComplete={() => this.toggleModal()}
                    // swipeDirection={'down'}
                    onBackdropPress={() => this.toggleModal()}
                    style={styles.view}>
                    {this.props ? this.renderOtpModal() : <View />}
                </Modal>
                <Modal isVisible={this.state.isDataModal}
                    // onSwipeComplete={() => this.toggleModal()}
                    // swipeDirection={'down'}
                    onBackdropPress={() => this.toggleDataModal()}
                    style={{ zIndex: 99999 }}>
                    {this.renderDataModal()}
                </Modal>
                <ImageBackground source={res.images.mainBackground} style={{ flex: 1 }}>
                    <DashBoardHeader
                        openSupport={() => this.openSupport()}
                        openDrawer={() => this.openDrawer()}
                        headerTitle={strings.LEGAL_DOCUMENTATION}
                        settingVisible={false}
                        notificationVisible={false}
                    />
                    {this.showContentView()}

                    {/* <Modal ref={this.modalRef} /> */}
                    {isLoading && this.renderLoader()}
                    {this.state.pickOptionsVisible ?
                        <PickerViewModal
                            onClickPickType={this.onClickPickType}
                            visibleModal={this.state.pickOptionsVisible}
                            onPressBackDrop={this.onPressBackDrop} /> : <View />}
                </ImageBackground>


            </SafeAreaView>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        legalDocuments: state.LegalDocReducer.legalDocuments
    };
};

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        getLegalDocuments,
        saveLegalDocuments,
        fileUpload,
        updateDocumentsAPI
    }, dispatch);
}
let LegalDocBaseScreenContainer = connect(mapStateToProps, mapDispatchToProps)(LegalDocBaseScreen);
export default LegalDocBaseScreenContainer;

