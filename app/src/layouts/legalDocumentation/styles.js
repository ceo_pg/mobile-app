import { StyleSheet, StatusBar } from 'react-native'
import res from '../../../res'
import { heightScale, widthScale } from '../../utility/Utils'
const styles = StyleSheet.create({
  fullScreen: {
    flex: 1,
    backgroundColor: res.colors.appColor,
    paddingTop: Platform.OS === "android" ? StatusBar.currentHeight + 10 : 0
  },
  progressContainer: {
    marginTop: heightScale(17),
    alignItems: 'center'
  },
  viewPager: {
    flex: 1,
    backgroundColor: res.colors.butterscotch,

  },
  outerViewPager: {
    flex: 1,
    marginHorizontal: widthScale(20),
    borderRadius: widthScale(20),
    marginTop: heightScale(18),
    marginBottom: heightScale(20),
    overflow: 'hidden'
  },
  costSheetViewContainer: {
    margin: widthScale(15),
    borderRadius: widthScale(20),
    flex: 1,
    backgroundColor: res.colors.white,

  },
  headerView: {
    backgroundColor: res.colors.white,
    height: heightScale(50),
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    shadowColor: "rgba(0,0,0, 0.1)",
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.8,
    shadowRadius: 10,
    elevation: 10,
    borderTopRightRadius: widthScale(20),
    borderTopLeftRadius: widthScale(20),

  },
  headerTitle: {
    fontFamily: res.fonts.semiBold,
    fontSize: widthScale(18),
    color: res.colors.greyishBrown
  },
  headerLeftView: {
    position: 'absolute',
    left: widthScale(22),
    top: heightScale(18)
  },
  innerPageContainer: {
    marginHorizontal: widthScale(20),
    marginTop: heightScale(17)
  },
  textContainer: {
    marginHorizontal: widthScale(30),
    alignItems: 'center',
    justifyContent: 'center'
  },
  bottomText: {
    fontFamily: res.fonts.semiBold,
    fontSize: widthScale(10),
    color: res.colors.formLabelGrey,
    letterSpacing: 0.48,
    lineHeight: 18,
    textAlign: 'center'
  },
  bottomBtnContainer: {
    marginTop: heightScale(2),
    marginBottom: heightScale(20)
  },
  textInputMarginTop: {
    // backgroundColor:"blue",
    marginTop: heightScale(24)
  },
  legalSuportView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  legalAndSupportText: {
    fontFamily: res.fonts.semiBold,
    fontSize: widthScale(32),
    color: res.colors.white,
    letterSpacing: 1.54,
  },
  supportCont: {
    marginHorizontal: widthScale(20),
    marginTop: heightScale(16)
  },
  supportTextView: {
    marginRight: widthScale(50)
  },
  supportImageView: {
    flex: 1,
    alignItems: 'flex-end',
    justifyContent: 'center'
  },
  talkToExpert: {
    fontFamily: res.fonts.regular,
    fontSize: widthScale(10),
    color: res.colors.white,
    marginTop: heightScale(13)
  },
  supportBtnStyle: {
    backgroundColor: res.colors.pinkishTan
  },
  supportBtnText: {
    fontFamily: res.fonts.semiBold,
    fontSize: widthScale(16),
    color: res.colors.appColor,
  },
  textInputStyle: {
    paddingBottom: 0,
    flex: 0.25,
    alignItems: 'center',
    marginLeft: 7,
    alignSelf: 'center',
    marginRight: 7,
    paddingLeft: 0,
    color: '#1D262C',
    fontFamily: res.fonts.regular,
    borderColor: res.colors.formLabelGrey,
    borderWidth: 1,
    borderRadius: 5
    // borderBottomWidth: 1.5,
    // borderBottomColor: res.colors.underlineColor
  },
  otpContainerView: {
    backgroundColor: res.colors.butterscotch,
    height: heightScale(250),
    marginTop: heightScale(150),
    borderRadius: widthScale(24),
    // marginHorizontal: widthScale(15)
  },
  closeBtnView: {
    position: 'absolute',
    right: 10,
    height: 40,
    width: 40,
    top: 10
  },
  iconsCOntainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: widthScale(30),
    marginTop: heightScale(40)
  },
  passcodeText: {
    fontSize: widthScale(18),
    color: res.colors.blackTwo,
    fontFamily: res.fonts.semiBold,
    textAlign: 'center',
    marginTop: heightScale(52),
    marginHorizontal: widthScale(20),
    fontWeight: 'bold'
  },
  inputStyle: {
    borderBottomWidth: 0,
    width: widthScale(50),
    //  paddingVertical: 8, 
    height: heightScale(44),
    fontSize: widthScale(18),
  },
  corousalNameText: {
    // fontWeight:"bold",
    fontFamily: res.fonts.semiBold,
    fontSize: widthScale(18),
    letterSpacing: 0.3,
    textAlign: 'center',
    color: res.colors.black,
    marginTop: 10
  },
  shadowHeader: {
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    backgroundColor: res.colors.butterscotch,
    shadowColor: res.colors.shadowColor,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 10.58,
    shadowRadius: 10.00,
    elevation: 24,
  },
  dotStyle: {
    // width: 10,
    // height: 10,
    borderRadius: 5,
    // marginHorizontal: 8,
    backgroundColor: res.colors.pinkishTan
  }, dotColor: {
    width: 10,
    height: 10,
    borderRadius: 5,
    marginHorizontal: -5,
    backgroundColor: res.colors.pinkishTan
  }, inactiveDotStyle: {
    width: 10,
    height: 10,
    borderRadius: 5,
    marginHorizontal: -5,
    backgroundColor: res.colors.pinkishTan
  },
  carouselView: {
    // height: heightScale(464), 
    backgroundColor: res.colors.butterscotch,
    borderRadius: widthScale(24),
    marginTop: heightScale(30),
    width: 306,
    flex: 1
  }, container: {
    flex: 1,
    top: Platform.OS == 'ios' ? 100 : 100,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    backgroundColor: res.colors.butterscotch,

  },
  closeBtn: {
    zIndex: 999999999,
    position: "absolute",
    top: 30,
    right: 30,
    backgroundColor: res.colors.black,

    width: 30,
    height: 30,
    borderRadius: 15,
    alignItems: 'center',
    justifyContent: 'center'
  }, UnitView: {
    flexDirection: 'row',
    width: "100%",
    alignItems: 'center',
    justifyContent: 'space-between',
    // backgroundColor:'red'
  }, label: {
    marginTop:heightScale(30),
    fontFamily: res.fonts.semiBold,
    fontSize: widthScale(16),
    color: res.colors.formLabelGrey
  },

})
export default styles