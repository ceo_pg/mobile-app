import React from "react"
import { View, Text, Image, TouchableOpacity } from "react-native"
import styles from "./styles"
import res from '../../../res'
import strings from "../../../res/constants/strings";
import { PGButton} from '../../components/index'

/**
 * @function
 * 
 * This is a functional component to render a text imput with label on top.
 * @param {Object} props
 * @returns {XML} view.
 */
function SupportView(props) {
    const { label, onSchedulePress } = props;

   
    return (
        <View>
             <View style={styles.legalSuportView}>
                    <View style={styles.supportTextView}> 
                        <Text style={styles.legalAndSupportText}>{strings.LEGAL_AND_SUPPORT}</Text>
                    </View>
                    <View style={styles.supportImageView}>
                        <Image source={res.images.support_ic} resizeMode={'contain'} />
                    </View>
                </View>
           <Text style={styles.talkToExpert}>{strings.TALK_TO_LEGAL_EXPERT}</Text>
           <PGButton
                    btnText={strings.SCHEDULE_CALL}
                    onPress={()=>onSchedulePress()}
                    btnStyle={styles.supportBtnStyle}
                    textStyleOver={styles.supportBtnText}
                />
        </View>

    );
}



export default SupportView;