import { StyleSheet, Platform, StatusBar } from 'react-native';
import resources from '../../../res';
const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        backgroundColor: resources.colors.white,
        paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0

    },
    container: {
        flex: 1,
        justifyContent: 'center',
        // marginTop: Platform.OS == 'ios' ? 10 : 20,
    }, inputCont: {
        paddingBottom: 0,
        flex: 0.25,
        alignItems: 'center',
        marginLeft: 7,
        alignSelf: 'center',
        marginRight: 7,
        paddingLeft: 0,
        color: '#1D262C',
        fontFamily: resources.fonts.regular,
        // borderBottomWidth: 1.5,
        // borderColor: '#D3DFEF'

    }, imageBack: {
        height: 216,
        width: '100%',
        top: -15
    }, inputCointainer: {
        flex: 1,
        borderTopLeftRadius: 30,
        borderTopRightRadius: 30,
    }, otpText: {
        fontFamily: resources.fonts.semiBold,
        fontSize: 24,
        color: resources.colors.slate
    }, otpInstructionText: {
        fontFamily: resources.fonts.regular,
        fontSize: 14,
        color: resources.colors.warmGrey,
        marginTop: 6
    }, containerForOtp: {
        marginTop: 30,
        height: 50,
        flex: 1,
        justifyContent: 'space-evenly',
        alignItems: 'center',
        flexDirection: 'row',
        // backgroundColor:'red'
    }, resendBtnText: {
        fontFamily: resources.fonts.bold,
        fontSize: 14,
        color: resources.colors.brightSkyBlue
    }, resendBtn: {
        alignItems: 'center',
        marginTop: 5
    }, inputFieldView: {
        borderTopLeftRadius: 22,
        borderTopRightRadius: 22,
        position: 'absolute',
        // top: -20,
        left: 0,
        right: 0,
        bottom: 0,
        backgroundColor: resources.colors.white
    }, timerBtnText: {
        textAlign: 'center',
        fontFamily: resources.fonts.regular,
        fontSize: 14,
        color: resources.colors.peacockBlue,
        marginBottom: 6
    }, inputContainer: {
        flexDirection: 'row',
        justifyContent: 'space-evenly',
    }, input: {
        width: 47,
        height: 47,
        fontSize: 20,
        textAlign: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        fontFamily: resources.fonts.regular,
        borderBottomWidth: 2,
        color: resources.colors.black,
        borderColor: resources.colors.underlineColor
    },
    navbarContainer: {
        paddingBottom: 10

    },
    inputContainer: {
        marginTop: 44,
        flexDirection: 'row',
        justifyContent: 'space-evenly',
    },
    buttonContainer: {
        marginTop: 44,
        alignItems: 'center'
    },
    forgetText: {
        fontSize: 14,
        color: resources.colors.txtGetOTP,
        marginRight: 28
    },
    // textContainer: {
    //     marginVertical: 18,
    //     width: '100%',
    //     flexDirection: 'column',
    // },
    text: {
        fontSize: 16,
        fontFamily: resources.fonts.regular,
        color: resources.colors.hint,
        alignSelf: 'center',
        marginVertical: 10,
        marginHorizontal: 15,
    },
    notRecievedContainer: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    notRecText: {
        color: resources.colors.labelColor,
        fontSize: 16,
        fontFamily: resources.fonts.regular,
    },

    resendContainer: {
        marginTop: 77,
        alignItems: 'center',
        justifyContent: 'center'
    },
    resend: {
        color: resources.colors.txtGetOTP,
        fontSize: 16,
        fontFamily: resources.fonts.bold,
        // borderBottomWidth: 2,
        // borderColor: resources.colors.txtGetOTP,
    },
    number: {
        fontSize: 17,
        fontFamily: resources.fonts.bold,
        color: resources.colors.hint,
        marginVertical: 10,
        alignSelf: 'center',
    },
    enterOtpText: {
        fontSize: 16,
        alignSelf: 'center',
        fontFamily: resources.fonts.regular,
        color: resources.colors.hint
    },
    parentView: {
        marginTop: 44,
        marginHorizontal: 27
    }, shadow: {
        shadowColor: resources.colors.shadowColor,
        shadowOffset: {
            width: 0,
            height: -10,
        },
        shadowOpacity: 20.39,
        shadowRadius: 10.30,

        elevation: 13,
        zIndex: 10
    }, imageContainer: {
        height: 216,
        width: '100%',
        top: 10,
        backgroundColor: "white",
        justifyContent: "center",
        alignItems: "center",
    }, roundedContaier: {
        borderTopRightRadius: 25,
        borderTopLeftRadius: 25,
        marginTop: -18,
        flex: 1,
        // alignItems: 'center',
        backgroundColor: resources.colors.butterscotch,
    }, textInputStyle: {
        paddingBottom: 0,
        flex: 0.25,
        alignItems: 'center',
        marginLeft: 7,
        alignSelf: 'center',
        marginRight: 7,
        paddingLeft: 0,
        color: '#1D262C',
        fontFamily: 'Montserrat-Medium',
        borderBottomWidth: 1.5,
        borderBottomColor: resources.colors.underlineColor
    },

});

export default styles;
