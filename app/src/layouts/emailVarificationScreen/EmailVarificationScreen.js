import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import { View, Text, TouchableOpacity, ImageBackground, StyleSheet, SafeAreaView } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import SimpleToast from 'react-native-simple-toast'
import { CommonActions } from '@react-navigation/native';

import styles from './styles';
import ApiFetch from '../../apiManager/ApiFetch'
import Input from '../../components/input/OtpField'
import PGButton from '../../components/button/Button'
import { EMAIL_VERIFY_ENDPOINTS, VERIFY_OTP_FORGOT_PASS_ENDPOINTS, RESEND_OTP_ENDPOINTS } from '../../apiManager/ApiEndpoints'
import res from '../../../res';
import HeaderAndStatusBar from '../../components/header/Header'
import { validateEmail } from '../../utility/Utils'
class EmailVarificationScreen extends Component {
    constructor(props) {
        super(props);
        this.data = this.props.route.params && this.props.route.params.data;
        this.emailVerify = this.props.route.params && this.props.route.params.emailVerify;
        this.otpFirst = React.createRef();
        this.otpSecond = React.createRef();
        this.otpThird = React.createRef();
        this.otpFourth = React.createRef();
        this.otpFive = React.createRef();
        this.otpSix = React.createRef();
        this.state = {
            otp: ["", "", "", "", "", ""],
            current: 0,
            timer: null,
            // counter: 5,
            isTimerFinished: false,
            counter: 60,
            otp1: '',
            otp2: '',
            otp3: '',
            otp4: '',
            otp5: '',
            otp6: '',
            minutes: 10,
            seconds: 0,

        }
    }
    componentDidMount() {
        this.startCounter()
        this.minuteCount()
    }
    clearValue = () => {
        this.setState({
            otp1: '',
            otp2: '',
            otp3: '',
            otp4: '',
            otp5: '',
            otp6: '',
        })
    }
    back = () => {
        this.props.navigation.goBack()
    }
    onFinishCounter = () => {
        this.setState({
            isTimerFinished: !this.state.isTimerFinished
        })
    }
    reSendOTP = () => {

        this.setState({
            minutes: 10,
            seconds:0,
            isTimerFinished: !this.state.isTimerFinished,
            otp: ["", "", "", "", "", ""],
            current: 0,
        }, () => this.hitResendOtpApi())

    }
    componentWillUnmount() {
        clearInterval(this.interval);
        clearInterval(this.myInterval)
    }
    cleanUp = () => {
        clearInterval(this.interval);
        this.onFinishCounter();
    }

    decreaseCounter = () => {
        if (this.state.counter == 0) {
            return this.cleanUp();
        }
        this.setState({ counter: this.state.counter - 1 });
    }

    startCounter = () => {
        this.interval = setInterval(this.decreaseCounter, 1000);

    }
    minuteCount = () => {
        this.myInterval = setInterval(() => {
            const { seconds, minutes } = this.state

            if (seconds > 0) {
                this.setState(({ seconds }) => ({
                    seconds: seconds - 1
                }))
            }
            if (seconds === 0) {
                if (minutes === 0) {
                    clearInterval(this.myInterval)
                } else {
                    this.setState(({ minutes }) => ({
                        minutes: minutes - 1,
                        seconds: 59
                    }))
                }
            }
        }, 1000)
    }
    getCounter = () => {
        const { counter } = this.state
        console.log(counter)
        let len = counter.toString().length;
        if (len > 1) {
            return counter
        } else {
            return '0' + counter
        }
    }

    onBackFromOtp2 = (event) => {
        if (event === 'Backspace') {
            this.setState({ otp2: '' })
            this.onfocusFirstOtp();
        }
    }
    onBackFromOtp3 = (event) => {
        if (event === 'Backspace') {
            this.setState({ otp3: '' })
            this.onfocusSecondOtp();
        }

    }
    onBackFromOtp4 = (event) => {
        if (event === 'Backspace') {
            this.setState({ otp4: '' })
            this.onfocusThirdOtp();
        }
    }
    onBackFromOtp5 = (event) => {
        if (event === 'Backspace') {
            this.setState({ otp5: '' })
            this.onfocusFourthOtp();
        }
    }
    onBackFromOtp6 = (event) => {
        if (event === 'Backspace') {
            this.setState({ otp6: '' })
            this.onfocusFiveOtp();
        }
    }
    onfocusFirstOtp = () => {

        this.otpFirst.current.focus();
    }
    onfocusSecondOtp = () => {

        this.otpSecond.current.focus();
    }
    onfocusThirdOtp = () => {
        this.otpThird.current.focus();
    }
    onfocusFourthOtp = () => {
        this.otpFourth.current.focus();
    }
    onfocusFiveOtp = () => {
        this.otpFive.current.focus();
    }
    onfocusSixOtp = () => {
        this.otpSix.current.focus();
    }
    onOtp1Change = (text) => {
        if (text.length == 1) {
            this.onfocusSecondOtp();
        }
        this.setState({
            otp1: text
        });
    }
    onOtp2Change = (text) => {
        if (text.length == 1) {
            this.onfocusThirdOtp();
        }
        this.setState({
            otp2: text
        });
    }
    onOtp3Change = (text) => {
        if (text.length == 1) {
            this.onfocusFourthOtp();
        }
        this.setState({
            otp3: text
        });
    }
    onOtp4Change = (text) => {
        if (text.length == 1) {
            this.onfocusFiveOtp();
        }
        this.setState({
            otp4: text
        });
    }
    onOtp5Change = (text) => {
        if (text.length == 1) {
            this.onfocusSixOtp();
        }
        this.setState({
            otp5: text
        });
    }
    onOtp6Change = (text) => {
        this.setState({
            otp6: text
        });
    }

    getOtp = () => {
        const { otp1, otp2, otp3, otp4, otp5, otp6 } = this.state
        // console.log(  otp1, otp2, otp3, otp4, otp5, otp6)
        let otpString = otp1 + otp2 + otp3 + otp4 + otp5 + otp6;
        return otpString;
    }

    navigate = () => {
        this.props.navigation.goBack();
    }

    moveToLoginScreen = (data) => {
        if (data.statusCode = 200) {
            SimpleToast.show(data.message)
            this.props.navigation.navigate("LoginScreen")
        } else {
            alert(data.message)
        }
    }
    getOtp = () => {
        const { otp1, otp2, otp3, otp4, otp5, otp6 } = this.state
        let otpString = otp1 + otp2 + otp3 + otp4 + otp5 + otp6;
        return otpString;
    }
    hitEmailVerifyApi = () => {
        console.log(parseInt(this.getOtp()))
        if (this.emailVerify == 1) {
            console.log("EMAILVERIFY")
            const parameters = JSON.stringify({
                otp: JSON.stringify(parseInt(this.getOtp())),

            })
            console.log(parameters)
            ApiFetch.fetchPost(EMAIL_VERIFY_ENDPOINTS, parameters)
                .then((data) => {
                    if (data.statusCode == 200) {
                        this.moveToLoginScreen(data)
                    } else {
                        this.clearValue()
                        alert(data.message)
                    }

                }).catch((err) => {
                    this.clearValue()
                    console.log(err)
                })
        } else {

            const parameters = JSON.stringify({
                otp: JSON.stringify(parseInt(this.getOtp())),
                email: this.data
            })
            ApiFetch.fetchPost(VERIFY_OTP_FORGOT_PASS_ENDPOINTS, parameters)
                .then((data) => {
                    if (data.statusCode == 200) {
                        this.moveToForgotPassword()
                    } else {
                        this.clearValue()
                        alert(data.message)
                    }

                }).catch((err) => {
                    this.clearValue()
                    console.log(err)
                })
        }
    }
    moveToForgotPassword = () => {
        const resetAction = CommonActions.reset({
            index: 0,
            routes: [{ name: 'ResetPasswordScreen' }],
        });
        this.props.navigation.dispatch(resetAction);
        // this.props.navigation.navigate("ResetPasswordScreen")
    }
    hitResendOtpApi = () => {

        ApiFetch.fetchPost(RESEND_OTP_ENDPOINTS)
            .then((data) => {
                SimpleToast.show(data.message)
                this.startCounter()
            }).catch((err) => {
                console.log(err)
            })
    }
    renderView = () => {
        const { isTimerFinished, minutes, seconds } = this.state

        return <View style={styles.parentView}>
            <Text style={styles.otpText}>OTP</Text>
            <Text style={styles.otpInstructionText}>{'"' + res.strings.OTP_INSTRUCTION + '"'}</Text>

            <View style={styles.containerForOtp}>
                <Input
                    value={this.state.otp1}
                    onChangeText={this.onOtp1Change}
                    containerStyle={styles.textInputStyle}
                    inputProps={{
                        maxLength: 1,
                        keyboardType: "numeric",
                        secureTextEntry: true,
                        returnKeyType: "next",
                        ref: this.otpFirst,
                        textAlign: "center",
                    }}
                    inputStyles={{ borderBottomWidth: 0, width: 40, paddingVertical: 5, fontSize: 18, }}
                />
                <Input
                    value={this.state.otp2}

                    onBackKeyPress={this.onBackFromOtp2}
                    onChangeText={this.onOtp2Change}
                    containerStyle={styles.textInputStyle}
                    inputProps={{
                        keyboardType: 'numeric',
                        secureTextEntry: true,
                        maxLength: 1,
                        returnKeyType: "next",
                        ref: this.otpSecond,
                        textAlign: "center",
                    }}
                    inputStyles={{ borderBottomWidth: 0, width: 40, paddingVertical: 5, fontSize: 18, }}
                />
                <Input
                    value={this.state.otp3}
                    onBackKeyPress={this.onBackFromOtp3}
                    onChangeText={this.onOtp3Change}
                    containerStyle={styles.textInputStyle}
                    inputProps={{
                        keyboardType: 'numeric',
                        returnKeyType: "next",
                        maxLength: 1,
                        secureTextEntry: true,
                        ref: this.otpThird,
                        textAlign: "center",
                    }}
                    inputStyles={{ borderBottomWidth: 0, width: 40, paddingVertical: 5, fontSize: 18, }}
                />
                <Input
                    value={this.state.otp4}
                    onBackKeyPress={this.onBackFromOtp4}
                    onChangeText={this.onOtp4Change}
                    containerStyle={styles.textInputStyle}
                    inputProps={{
                        keyboardType: 'numeric',
                        maxLength: 1,
                        returnKeyType: "next",
                        secureTextEntry: true,
                        ref: this.otpFourth,
                        textAlign: "center",
                    }}
                    inputStyles={{ borderBottomWidth: 0, width: 40, paddingVertical: 5, fontSize: 18, }}
                />
                <Input
                    value={this.state.otp5}
                    onBackKeyPress={this.onBackFromOtp5}
                    onChangeText={this.onOtp5Change}
                    containerStyle={styles.textInputStyle}
                    inputProps={{
                        keyboardType: 'numeric',
                        maxLength: 1,
                        returnKeyType: "next",
                        secureTextEntry: true,
                        ref: this.otpFive,
                        textAlign: "center",
                    }}
                    inputStyles={{ borderBottomWidth: 0, width: 40, paddingVertical: 5, fontSize: 18, }}
                />
                <Input
                    value={this.state.otp6}
                    onBackKeyPress={this.onBackFromOtp6}
                    onChangeText={this.onOtp6Change}
                    containerStyle={styles.textInputStyle}
                    inputProps={{
                        keyboardType: 'numeric',
                        maxLength: 1,
                        returnKeyType: "next",
                        secureTextEntry: true,
                        ref: this.otpSix,
                        textAlign: "center",
                    }}
                    inputStyles={{ borderBottomWidth: 0, width: 40, paddingVertical: 5, fontSize: 18, }}
                />
            </View>
            <View style={styles.buttonContainer}>
                <PGButton rounded btnText={res.strings.SUBMIT} onPress={() => this.hitEmailVerifyApi()} />
            </View>


            <TouchableOpacity
                // disabled={!isTimerFinished}
                onPress={() => this.reSendOTP()}>
                <View style={styles.resendContainer}>
                    {minutes === 0 && seconds === 0
                        ? <View /> : <View>
                            <Text style={styles.timerBtnText}> {minutes}:{seconds < 10 ? `0${seconds}` : seconds} </Text>
                            <Text style={[styles.resend, { color: isTimerFinished ? res.colors.brightSkyBlue : res.colors.brightSkyBlue }]}>{res.strings.RESEND_OTP}</Text>
                        </View>

                    }

                </View>
            </TouchableOpacity>

        </View>
    }
    render() {

        return (
            <SafeAreaView style={[styles.mainContainer]}>
                <HeaderAndStatusBar
                    onBackClick={() => this.back()}
                />
                <View style={[styles.container]}>
                        <View style={styles.imageContainer}>
                            <ImageBackground
                                source={res.images.testImage}
                                style={styles.imageBack}
                            />
                        </View>
                        <View style={[styles.roundedContaier, styles.shadow]}>
                        <KeyboardAwareScrollView showsVerticalScrollIndicator={false}>

                            {this.renderView()}
                            </KeyboardAwareScrollView>


                        </View>
                </View>


            </SafeAreaView>

        );
    }
}
const mapStateToProps = (state) => {
    return {};
};

function mapDispatchToProps(dispatch) {
    return bindActionCreators({

    }, dispatch);
}
let EmailVarificationScreenContainer = connect(mapStateToProps, mapDispatchToProps)(EmailVarificationScreen);
export default EmailVarificationScreenContainer;
