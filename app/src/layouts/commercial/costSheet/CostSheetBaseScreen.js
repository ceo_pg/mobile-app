import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import { View, SafeAreaView } from 'react-native';
import styles from './styles'
import strings from '../../../../res/constants/strings';
import DashBoardHeader from '../../../components/header/DashBoardHeader'
import CircleProgress from '../../../components/progressComponent/CircleProgress'

import ViewPager from '@react-native-community/viewpager';
import CostSheet from './CostSheet'
import ConstSheetStep2 from './CostSheetStep2'
import PaymentPlan from './PaymentPlan'
import AccountStatement from './AccountStatement'
import res from '../../../../res'



class CostSheetBaseScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            pageIndex: 0,
            progressImg:res.images.step1
        }
        this.viewPager = React.createRef()

    }
    componentDidMount() {

    }
    openDrawer = () => {
        this.props.navigation.openDrawer()
    }

    changePageIndex = (index) =>{
        console.log(index, "changePageIndex");
        if(index == -1){
            // close the drwr
            return
        }
        this.setState({pageIndex:index})
        this.viewPager.setPage(index)
        this.setProgressImage(index);
    }

    pageChanged = (e) =>{
    console.log(e.nativeEvent.position, "pageChanged");
    if(e && e.nativeEvent && e.nativeEvent){
        this.setState({pageIndex:e.nativeEvent.position}); 
        this.setProgressImage(e.nativeEvent.position);
    }
    }

    setProgressImage = (index) =>{
        switch (index) {
            case 0:
                // code block
                 this.setState({progressImg:res.images.step1})
                break;
            case 1:
                // code block
                this.setState({progressImg:res.images.step2})
                break;
            case 2:
                this.setState({progressImg:res.images.step3})
                break
                case 3:
                    this.setState({progressImg:res.images.step4})
                    break
            default:
            // code block
        }
    }

    render() {
        const { CostSheetReducer } = this.props
        console.log(CostSheetReducer, "CostSheetReducer");
     
        return (
            <SafeAreaView style={styles.fullScreen}>
                <DashBoardHeader
                    openDrawer={() => this.openDrawer()}
                    headerTitle={strings.COMMERCIAL}
                    settingVisible={false}
                    notificationVisible={false}
                />
                <View style={styles.progressContainer}>
                    <CircleProgress pageIndex={this.state.pageIndex} progressImg={this.state.progressImg}/>
                </View>
                <ViewPager style={styles.viewPager} initialPage={0}
                ref={(viewPager) => { this.viewPager = viewPager }}
                onPageSelected = {(e) => this.pageChanged(e)}
                >
                    <View key="1">
                       <CostSheet changePage={this.changePageIndex}/>
                    </View>
                    <View key="2">
                       <ConstSheetStep2 changePage={this.changePageIndex}/>
                    </View>
                    <View key="3">
                       <PaymentPlan changePage={this.changePageIndex}/>
                    </View>
                    <View key="4">
                       <AccountStatement changePage={this.changePageIndex}/>
                    </View>
                </ViewPager>


            </SafeAreaView>
        )
    }


}
const mapStateToProps = (state) => {
    return {
        CostSheetReducer: state.CostSheetReducer
    };
};

function mapDispatchToProps(dispatch) {
    return bindActionCreators({

    }, dispatch);
}
let CostSheetBaseScreenContainer = connect(mapStateToProps, mapDispatchToProps)(CostSheetBaseScreen);
export default CostSheetBaseScreenContainer;

