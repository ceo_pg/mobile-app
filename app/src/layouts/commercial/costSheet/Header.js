import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image } from 'react-native';
import styles from './styles'
import strings from '../../../../res/constants/strings';
import res from '../../../../res'


class Header extends Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }
    renderBackIcon = () => {
        const {  onBack } = this.props
        return <TouchableOpacity style={styles.headerLeftView} onPress={() => onBack()}>
            <Image source={res.images.icn_back} resizeMode={'contain'} />
        </TouchableOpacity>
    }
    render() {
        const { title, headerTitle, headerView, isBackIconVisisble } = this.props
        return (
            <View>
                <View style={[styles.headerView, headerView]}>
                    <Text style={[styles.headerTitle, headerTitle]}>{title ? title : ''}</Text>
                </View>
                {isBackIconVisisble ? this.renderBackIcon() : <View />}
            </View>

        )
    }


}

export default Header;

