import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import { View, Text, TouchableOpacity, ImageBackground, SafeAreaView } from 'react-native';
import styles from './styles'
import strings from '../../../../res/constants/strings';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import Header from './Header'
import {changePageIndex} from '../../../redux/actions/commercial/CostSheetAction'


class ConstSheetStep2 extends Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }
    componentDidMount() {

    }

    backPress = () =>{
        const {changePage} = this.props;
        changePage(0)
    }

    render() {
        return (
            
           <KeyboardAwareScrollView style={styles.costSheetViewContainer}>
            <Header title={strings.COST_SHEET} onBack={this.backPress}/>
            <View style={styles.costSheetContentView}>
            <Text style={styles.subHeadingText}>{strings.HOW_DO_PLAN_TO_FINANCE_PROPERTY}</Text> 
            </View>
           </KeyboardAwareScrollView>
        )
    }


}
const mapStateToProps = (state) => {
    return {};
};

function mapDispatchToProps(dispatch) {
    return bindActionCreators({

    }, dispatch);
}
let CostSheetStep2Container = connect(mapStateToProps)(ConstSheetStep2);
export default CostSheetStep2Container;

