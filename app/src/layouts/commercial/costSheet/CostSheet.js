import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import { View, Text, TouchableOpacity, ImageBackground, SafeAreaView } from 'react-native';
import styles from './styles'
import strings from '../../../../res/constants/strings';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import Header from './Header'
import OutLinedInput from '../../../components/input/OutLinedInput'
import resources from '../../../../res';

class CostSheet extends Component {
    constructor(props) {
        super(props);
        this.state = {
            amount: "",
            unit: "",
            gstRate: "",
            otherAmount: "",
            otherUnit: "",
            otherGstRate: ""
        }
    }
    componentDidMount() {

    }

    backPress = () => {
        const { changePage } = this.props;
        changePage(-1)
    }

    render() {
        return (

            <KeyboardAwareScrollView style={styles.costSheetViewContainer}>
                <Header title={strings.COST_SHEET} onBack={this.backPress} />
                <View style={styles.costSheetContentView}>
                    <Text style={styles.subHeadingText}>{strings.BEST_SELLING_PRICE_RATE}</Text>
                    <View style={{ marginHorizontal: 20, bottom: 23 }}>
                        <OutLinedInput
                            label={"Amount"}
                            placeholder={resources.strings.IN_INR}
                        />
                        <OutLinedInput
                            placeholder={"Unit"}
                        />
                        <OutLinedInput
                            placeholder={"GST Rate"}
                        />
                    </View>
                </View>

                <View style={styles.costSheetContentView}>
                    <Text style={styles.subHeadingText}>{strings.OTHER_CHARGES}</Text>
                    <View style={{ marginHorizontal: 20, bottom: 23 }}>
                        <OutLinedInput
                            placeholder={"Amount"}
                        />
                        <OutLinedInput
                            placeholder={"Unit"}
                        />
                        <OutLinedInput
                            placeholder={"GST Rate"}
                        />
                    </View>
                </View>
            </KeyboardAwareScrollView>
        )
    }


}
const mapStateToProps = (state) => {
    return {};
};

function mapDispatchToProps(dispatch) {
    return bindActionCreators({

    }, dispatch);
}
let CostSheetContainer = connect(mapStateToProps, mapDispatchToProps)(CostSheet);
export default CostSheetContainer;

