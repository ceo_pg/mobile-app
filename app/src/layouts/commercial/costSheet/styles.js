import { StyleSheet, StatusBar } from 'react-native'
import res from '../../../../res'
import { heightScale, widthScale } from '../../../utility/Utils'
const styles = StyleSheet.create({
  fullScreen: {
    flex: 1,
    backgroundColor: res.colors.appColor,
    paddingTop: Platform.OS === "android" ? StatusBar.currentHeight + 10 : 0
  },
  progressContainer: {
    marginTop: heightScale(17),
    alignItems: 'center'
  },
  viewPager: {
    flex: 1,
    backgroundColor: res.colors.butterscotch,
    marginHorizontal: widthScale(20),
    borderRadius: widthScale(20),
    marginTop:heightScale(18)
  },
  costSheetViewContainer: {
    margin: widthScale(15),
    borderRadius: widthScale(20),
    flex: 1,
    backgroundColor: res.colors.white
  },
  headerView: {
    backgroundColor: res.colors.white,
    height: heightScale(50),
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    shadowColor: "rgba(0,0,0, 0.1)",
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.8,
    shadowRadius: 10,
    elevation: 10,
    borderTopLeftRadius:20,
    borderTopRightRadius:20

  },
  headerTitle: {
    fontWeight:'bold',
    fontFamily: res.fonts.semiBold,
    fontSize: widthScale(18),
    color: res.colors.greyishBrown
  },
  headerLeftView:{
    position:'absolute',
    left:widthScale(22),
    top:heightScale(18)
  },
  costSheetContentView:{
    margin:widthScale(10),
    borderRadius:widthScale(10),
    backgroundColor: res.colors.white,
    flex: 1,
    justifyContent: 'center',
    // alignItems: 'center',
    shadowColor: "rgba(0,0,0, 0.1)",
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.8,
    shadowRadius: 10,
    elevation: 10,
  },
  subHeadingText:{
    textAlign:'center',
    fontFamily: res.fonts.semiBold,
    fontSize: widthScale(16),
    color: res.colors.greyishBrown,
    paddingVertical:heightScale(20) 
  }
})
export default styles