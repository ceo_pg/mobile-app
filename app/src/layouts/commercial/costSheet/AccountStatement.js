import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import { View, Text, TouchableOpacity, ImageBackground, SafeAreaView } from 'react-native';
import styles from './styles'
import strings from '../../../../res/constants/strings';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import Header from './Header'


class AccountStatement extends Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }
    componentDidMount() {

    }

    backPress = () =>{
        const {changePage} = this.props;
        changePage(2)
    }

    render() {
        return (
            
           <KeyboardAwareScrollView style={styles.costSheetViewContainer}>
            <Header title={strings.ACCOUNT_STATMENT} onBack={this.backPress}/>
            <View style={styles.costSheetContentView}>
            <Text style={styles.subHeadingText}>{strings.CONSTRUCTION_LINKED_PAYMENT_PLAN}</Text> 
            </View>
           </KeyboardAwareScrollView>
        )
    }


}
const mapStateToProps = (state) => {
    return {};
};

function mapDispatchToProps(dispatch) {
    return bindActionCreators({

    }, dispatch);
}
let AccountStatementContainer = connect(mapStateToProps, mapDispatchToProps)(AccountStatement);
export default AccountStatementContainer;

