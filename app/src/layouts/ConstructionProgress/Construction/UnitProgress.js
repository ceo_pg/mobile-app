import React, { Component } from 'react'
import { View, Dimensions, Text, SafeAreaView, ScrollView } from 'react-native'
import Carousel, { Pagination } from 'react-native-snap-carousel';

import Modal from 'react-native-modal'

import styles from './styles'
import res from '../../../../res'
import { widthScale, heightScale } from '../../../utility/Utils';
import ConstructionBar from '../../../components/progressComponent/ConstructionBar'

class UnitProgress extends Component {
    constructor(props) {
        super(props)
        this.state = {

        }

    }



    render() {
        const blockworkProgress = this.props.unit && this.props.unit.blockwork && this.props.unit.blockwork.percentOfCompletionOfFollowingItem ? this.props.unit.blockwork.percentOfCompletionOfFollowingItem : "0"
        const plumbingLinesProgress = this.props.unit && this.props.unit.plumbingLines && this.props.unit.plumbingLines.percentOfCompletionOfFollowingItem ? this.props.unit.plumbingLines.percentOfCompletionOfFollowingItem : "0"
        const electricalWallConduitProgress = this.props.unit && this.props.unit.electricalWallConduit && this.props.unit.electricalWallConduit.percentOfCompletionOfFollowingItem ? this.props.unit.electricalWallConduit.percentOfCompletionOfFollowingItem : "0"
        const plasteringProgress = this.props.unit && this.props.unit.plastering && this.props.unit.plastering.percentOfCompletionOfFollowingItem ? this.props.unit.plastering.percentOfCompletionOfFollowingItem : "0"

        return (
            <View style={styles.amenitiesFullScreen}>
            <ScrollView>
                <ConstructionBar
                    header={"Blockwork"}
                    // value={"WaterStorageSystem, WaterSupplyPiping, Drain,GasPiping, WaterTreatment, WasteDisposal, RainWaterHarvesting, SewageChamber,SewageLine,SepticTank,STP,WTP"}
                    src={res.images.blockwork}
                    percentage={parseFloat(blockworkProgress) / 100}
                    percentageText={parseFloat(blockworkProgress)}
                />
                <ConstructionBar
                    src={res.images.pipeline}
                    header={"Plumbing Lines"}
                    // value={"Substation, Transformer, PanelsAndSubPanels, RoughWiring, Generator, StreetAndCommonAreaLighting, SolarPowerUnit, SolarFencing"}
                    percentage={parseFloat(plumbingLinesProgress) / 100}
                    percentageText={parseFloat(plumbingLinesProgress)}
                />
                <ConstructionBar

                    header={"Electrical Wall Conduit"}
                    value={"Cellars, Floors"}
                    percentage={parseFloat(electricalWallConduitProgress) / 100}
                    percentageText={parseFloat(electricalWallConduitProgress)}
                    src={res.images.electrical}
                />
                <ConstructionBar

                    header={"Plastering"}
                    // value={"AirHandler, DuctWork, AirFilter,Piping,Radiators,HVACControls"}
                    percentage={parseFloat(plasteringProgress) / 100}
                    percentageText={parseFloat(plasteringProgress)}
                    src={res.images.plastering}
                />
                </ScrollView>
            </View>
        )

    }
}
export default UnitProgress