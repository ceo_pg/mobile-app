import React, { Component } from 'react'
import { View, Dimensions, Text, SafeAreaView , ScrollView} from 'react-native'
import Carousel, { Pagination } from 'react-native-snap-carousel';

import Modal from 'react-native-modal'

import styles from './styles'
import res from '../../../../res'
import { widthScale, heightScale } from '../../../utility/Utils';
import ConstructionBar from '../../../components/progressComponent/ConstructionBar'

class TowerProgress extends Component {
    constructor(props) {
        super(props)
        this.state = {

        }

    }



    render() {
        const structureProgress = this.props.tower && this.props.tower.structure && this.props.tower.structure.percentOfCompletionOfFollowingItem ? this.props.tower.structure.percentOfCompletionOfFollowingItem : "0"
        const excavationProgress = this.props.tower && this.props.tower.excavation && this.props.tower.excavation.percentOfCompletionOfFollowingItem ? this.props.tower.excavation.percentOfCompletionOfFollowingItem : "0"
        const footingsProgress = this.props.tower && this.props.tower.footings && this.props.tower.footings.percentOfCompletionOfFollowingItem ? this.props.tower.footings.percentOfCompletionOfFollowingItem : "0"
        const doorWindowFramingFixations = this.props.tower && this.props.tower.doorWindowFramingFixations && this.props.tower.doorWindowFramingFixations.percentOfCompletionOfFollowingItem ? this.props.tower.doorWindowFramingFixations.percentOfCompletionOfFollowingItem : "0"

        return (
            <View style={styles.amenitiesFullScreen}>
            <ScrollView>
                <ConstructionBar
                    header={"Excavation"}
                    // value={"WaterStorageSystem, WaterSupplyPiping, Drain,GasPiping, WaterTreatment, WasteDisposal, RainWaterHarvesting, SewageChamber,SewageLine,SepticTank,STP,WTP"}
                    src={res.images.excavation}
                    percentage={parseFloat(excavationProgress) / 100}
                    percentageText={parseFloat(excavationProgress)}
                />
                <ConstructionBar
                    src={res.images.flooting}
                    header={"Footings"}
                    // value={"Substation, Transformer, PanelsAndSubPanels, RoughWiring, Generator, StreetAndCommonAreaLighting, SolarPowerUnit, SolarFencing"}
                    percentage={parseFloat(footingsProgress) / 100}
                    percentageText={parseFloat(footingsProgress)}
                />
                <ConstructionBar

                    header={"Structure"}
                    value={"Cellars, Floors"}
                    percentage={parseFloat(structureProgress) / 100}
                    percentageText={parseFloat(structureProgress)}
                    src={res.images.structure_tower}
                />
                <ConstructionBar
                    header={"Door Window Framing Fixations"}
                    // value={"WaterStorageSystem, WaterSupplyPiping, Drain,GasPiping, WaterTreatment, WasteDisposal, RainWaterHarvesting, SewageChamber,SewageLine,SepticTank,STP,WTP"}
                    src={res.images.doorWindowFramingFixations}
                    percentage={parseFloat(doorWindowFramingFixations) / 100}
                    percentageText={parseFloat(doorWindowFramingFixations)}
                />
                </ScrollView>
            </View>
        )

    }
}
export default TowerProgress