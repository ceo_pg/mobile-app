import React, { Component } from 'react'
import { View, Dimensions, Text, SafeAreaView, ScrollView } from 'react-native'
import Carousel, { Pagination } from 'react-native-snap-carousel';

import Modal from 'react-native-modal'

import styles from './styles'
import res from '../../../../res'
import { widthScale, heightScale } from '../../../utility/Utils';
import ConstructionBar from '../../../components/progressComponent/ConstructionBar'

class AmenitiesProgress extends Component {
    constructor(props) {
        super(props)
        this.state = {

        }

    }



    render() {
        const pheProgress = this.props.infra && this.props.infra.plumbing && this.props.infra.plumbing.percentOfCompletionOfFollowingItem ? this.props.infra.plumbing.percentOfCompletionOfFollowingItem : "0"
        const electricalProgress = this.props.infra && this.props.infra.electrical && this.props.infra.electrical.percentOfCompletionOfFollowingItem ? this.props.infra.electrical.percentOfCompletionOfFollowingItem : "0"
        const fireProgress = this.props.infra && this.props.infra.fireFighting && this.props.infra.fireFighting.percentOfCompletionOfFollowingItem ? this.props.infra.fireFighting.percentOfCompletionOfFollowingItem : "0"
        
        return (
            <View style={styles.amenitiesFullScreen}>
            <ScrollView>
                <ConstructionBar
                    header={"PHE"}
                    // value={"WaterStorageSystem, WaterSupplyPiping, Drain,GasPiping, WaterTreatment, WasteDisposal, RainWaterHarvesting, SewageChamber,SewageLine,SepticTank,STP,WTP"}
                    src={res.images.plumber}
                    percentage={parseFloat(pheProgress) / 100}
                    percentageText={parseFloat(pheProgress)}
                />
                <ConstructionBar
                    src={res.images.electricle}
                    header={"Electrical"}
                    value={"Substation, Transformer, PanelsAndSubPanels, RoughWiring, Generator, StreetAndCommonAreaLighting, SolarPowerUnit, SolarFencing"}
                    percentage={parseFloat(electricalProgress) / 100}
                    percentageText={parseFloat(electricalProgress)}
                />
                <ConstructionBar

                    header={"Fire Safety"}
                    value={"FireStorageTanks, PumpingSystem, Hydrant,Sprinklers"}
                    percentage={parseFloat(fireProgress) / 100}
                    percentageText={parseFloat(fireProgress)}
                    src={res.images.fireExtinguisher}
                />
                </ScrollView>
            </View>
        )

    }
}
export default AmenitiesProgress