import React, { Component } from 'react'
import { View, Dimensions, Text, TouchableOpacity } from 'react-native'
import Carousel, { Pagination } from 'react-native-snap-carousel';
import Modal from 'react-native-modal'
import { bindActionCreators } from "redux"
import { connect } from 'react-redux';

import styles from './styles'
import res from '../../../../res'
import AmenitiesProgress from './AmenitiesProgress'
import InfrastructureProgress from './InfrastructureProgress'
import TowerProgress from './TowerProgress'
import UnitProgress from './UnitProgress'
import { AmenitiesProgressModal, InfraModal, UnitProgressModal, TowerProgressModal } from '../../../components/index'
import style from '../../../components/button/styles';
class
    Construction extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isLoading: false,
            isModalVisible: false,
            activeIndex: 0,
            constructionList: [
                //     {
                //     name: "Amenities Progress",
                //     id: "1"
                // }, 
                {
                    name: "Infrastructure Level",
                    id: "1"
                }, {
                    name: "Tower Level",
                    id: "2"
                }, {
                    name: "Unit Level",
                    id: "3"
                }]
        }

    }

    toggleModal = () => {
        const { isModalVisible } = this.state
        this.setState({
            isModalVisible: !isModalVisible
        })
    }
    modalView = () => {
        const { activeIndex } = this.state
        const { projectConstructionProgress, towerLevelConstructionProgress, unitLevelConstructionProgress } = this.props.progressData

        switch (activeIndex) {
            // case 0:
            //     return <AmenitiesProgressModal
            //         infra={projectConstructionProgress}
            //     />
            //     break;
            case 0:
                return <InfraModal
                    tower={towerLevelConstructionProgress}
                    unit={unitLevelConstructionProgress}
                    infra={projectConstructionProgress}
                />
                break;
            case 1:
                return <TowerProgressModal
                    tower={towerLevelConstructionProgress}
                />
                break;
            case 2:
                return <UnitProgressModal
                    unit={unitLevelConstructionProgress}
                />
                break;
            default:
                break;
        }
    }
    renderAmenities = () => {
        const { projectConstructionProgress, towerLevelConstructionProgress, unitLevelConstructionProgress } = this.props.progressData
        if (!projectConstructionProgress) {
            return <View style={styles.noDataFound}>
                <Text>{res.strings.NO_DATA_FOUND}</Text>
            </View>
        }
        return (<AmenitiesProgress
            tower={towerLevelConstructionProgress}
            unit={unitLevelConstructionProgress}
            infra={projectConstructionProgress}
        />



        )
    }
    renderInfrastructure = () => {
        const { projectConstructionProgress, towerLevelConstructionProgress, unitLevelConstructionProgress } = this.props.progressData
        if (!projectConstructionProgress) {
            return <View style={styles.noDataFound}>
                <Text>{res.strings.NO_DATA_FOUND}</Text>
            </View>
        }
        return <InfrastructureProgress
            tower={towerLevelConstructionProgress}
            unit={unitLevelConstructionProgress}
            infra={projectConstructionProgress}
        />
    }
    renderTower = () => {
        const { towerLevelConstructionProgress } = this.props.progressData
        if (!towerLevelConstructionProgress) {
            return <View style={styles.noDataFound}>
                <Text>{res.strings.NO_DATA_FOUND}</Text>
            </View>
        }
        return <TowerProgress
            tower={towerLevelConstructionProgress}
        />
    }
    renderUnit = () => {
        const { unitLevelConstructionProgress } = this.props.progressData
        if (!unitLevelConstructionProgress) {
            return <View style={styles.noDataFound}>
                <Text>{res.strings.NO_DATA_FOUND}</Text>
            </View>
        }
        return <UnitProgress
            unit={unitLevelConstructionProgress}
        />
    }
    renderCorousalData = (id) => {
        return this.renderConstructionCard(id)

    }
    renderConstructionCard = (id) => {
        console.log("renderConstructionCard", id)
        // if (id == 1) {
        //     return this.renderAmenities()
        // }
        if (id == 1) {
            return this.renderInfrastructure()
        }
        if (id == 2) {
            return this.renderTower()
        }
        if (id == 3) {
            return this.renderUnit()
        }
        // switch (id) {
        //     case 1:
        //         return this.renderAmenities()
        //         break;
        //     case 2:
        //         return this.renderInfrastructure()
        //         break;
        //     case 3:
        //         return this.renderTower()
        //         break;
        //     case 4:
        //         return this.renderUnit()
        //         break;
        //     default:

        // }
    }
    viewMore = () => {
        return <View style={{ alignItems: 'center', justifyContent: 'center', bottom: 0, marginVertical: 5 }}>
            <TouchableOpacity onPress={() => this.toggleModal(false)} activeOpacity={1}>

                <Text style={styles.viewMoreText}>View More</Text>
            </TouchableOpacity>
        </View>
    }
    renderViewMore = (id) => {
        console.log("CONSTRUCTION VIEW MORE", id)

        if (id == 1) {
            return this.viewMore()
        }

        if (id == 2) {
            return this.viewMore()
        }

        if (id == 3) {
            return this.viewMore()
        }

        if (id == 4) {
            return this.viewMore()
        }
    }
    renderData = ({ item, index }) => {
        return <View style={{ flex: 1, backgroundColor: res.colors.butterscotch, borderRadius: 24, marginTop: 5, width: 306, }} >

            <View style={[styles.shadowHeader, { height: 42 }]}>
                <Text style={styles.corousalNameText}>{item.name}</Text>
            </View>
            {this.renderCorousalData(item.id)}


            {this.renderViewMore(item.id)}
        </View>

    }
    render() {
        console.log("CONSTRUCTIONPROGRESS===>", this.props)
        const { constructionList } = this.state
        return (
            <View style={styles.fullScreen}>

                <Modal isVisible={this.state.isModalVisible}

                    onBackdropPress={() => this.toggleModal()}
                    style={styles.view}>
                    {this.props ? this.modalView() : <View />}
                </Modal>

                <View style={{ flex: 1 }}>
                    <Carousel
                        data={constructionList}
                        renderItem={this.renderData}
                        sliderWidth={Dimensions.get('window').width}
                        itemWidth={306}
                        layout={'default'}
                        containerCustomStyle={styles.carouselData}
                        removeClippedSubviews={true}
                        onSnapToItem={index => this.setState({ activeIndex: index })}
                        firstItem={this.state.activeIndex}
                    />
                    <View style={{ height: 30 }}>
                        <Pagination
                            containerStyle={{ height: 20, marginTop: -10 }}
                            dotContainerStyle={{ height: 20 }}
                            dotsLength={constructionList.length}
                            dotStyle={styles.dotStyle}
                            dotColor={styles.dotColor}
                            inactiveDotStyle={styles.inactiveDotStyle}
                            activeDotIndex={this.state.activeIndex}
                            inactiveDotOpacity={0.4}
                            inactiveDotScale={0.6}
                            activeOpacity={1}
                        />
                    </View>


                </View>



            </View>
        )

    }
}
const mapStateToProps = (state) => {
    console.log("PERCNTAGE", state)
    return {
        progressData: state.ConstructionReducer.progressData
    };
};

function mapDispatchToProps(dispatch) {
    return bindActionCreators({

    }, dispatch);
}
let ConstructionContainer = connect(mapStateToProps, mapDispatchToProps)(Construction);
export default ConstructionContainer;
// export default Construction