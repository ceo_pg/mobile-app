import { StyleSheet, Platform, StatusBar } from 'react-native'
import res from '../../../../res'
import { heightScale,widthScale } from '../../../utility/Utils'
const styles = StyleSheet.create({
  fullScreen: {
     flex: 1,
    // backgroundColor: res.colors.appColor,
  }, carouselData: {
    zIndex: 99,
  }, dotStyle: {

    borderRadius: 5,

    backgroundColor: res.colors.pinkishTan
  }, dotColor: {
    width: 10,
    height: 10,
    borderRadius: 5,
    marginHorizontal: -5,
    backgroundColor: res.colors.pinkishTan
  }, inactiveDotStyle: {
    width: 10,
    height: 10,
    borderRadius: 5,
    marginHorizontal: -5,
    backgroundColor: res.colors.pinkishTan
  }, shadowHeader: {
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    backgroundColor: res.colors.butterscotch,
    shadowColor: res.colors.shadowColor,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 10.58,
    shadowRadius: 10.00,
    elevation: 24,
    justifyContent:'center'
  }, corousalNameText: {
    fontWeight: "bold",
    fontFamily: res.fonts.semiBold,
    fontSize: 18,
    letterSpacing: 0.3,
    textAlign: 'center',
    color: res.colors.greyishBrown,
    // marginTop: 10
  }, amenitiesFullScreen: {
    flex: 1,
    borderRadius: 20,
    // backgroundColor:'red'
  }, container: {
    flex: 1,
    top: Platform.OS == 'ios' ? 100 : 100,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    backgroundColor: res.colors.butterscotch
  },noDataFound:{
    flex:1,
    justifyContent:'center',
    alignItems:'center'
  },
  percentageText: {
      fontFamily: res.fonts.semiBold,
      fontSize: 10,
      color: res.colors.white,
      textAlign: 'center'
  }, projectLevelValueText: {
      top: heightScale(5),
      fontFamily: res.fonts.medium,
      fontSize: 12,
      color: res.colors.white,
      textAlign: 'center'
  }, seprater: {

      height: '85%',
      width: "0.1%",
      backgroundColor: res.colors.sepratorColor,
      marginRight: 30
  },
  graphView: {
      flexDirection: 'row',
      justifyContent: 'space-around',
      marginTop: heightScale(24),
      alignItems: 'center'
  },
  graphSubView: {
      backgroundColor: res.colors.sepratorColor,
      height: '60%',
      width: 0.5
  },
  filledView: {
      width: 45,
      justifyContent: 'flex-end',
      backgroundColor: res.colors.toupe,
      position: 'absolute',
      bottom: 0,
      opacity: 1
  }, graphViewStyles: {
      alignItems: 'center',
      width: '33.33%',
  }, projectLevelText: {
    fontWeight: 'bold',
    fontFamily: res.fonts.semiBold,
    fontSize: heightScale(32),
    color: res.colors.white
}, squreFitView: {
    // paddingHorizontal:20,
    marginHorizontal: 22,
    backgroundColor: res.colors.cobalt,
    minHeight: 72,
    borderRadius: 8,
    marginTop: heightScale(10),
    flexDirection: 'row',
    alignItems: 'center',
    // justifyContent: 'space-between'
}, viewMoreText: {
  fontWeight:'bold',
  color: res.colors.peacockBlue,
  fontFamily: res.fonts.semiBold,
  fontSize: 14
},
})
export default styles