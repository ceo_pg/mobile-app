import React, { Component } from 'react'
import { View, Dimensions, Text, SafeAreaView, ScrollView } from 'react-native'
import Carousel, { Pagination } from 'react-native-snap-carousel';

import Modal from 'react-native-modal'

import styles from './styles'
import res from '../../../../res'
import { widthScale, heightScale } from '../../../utility/Utils';
import ConstructionBar from '../../../components/progressComponent/ConstructionBar'

class InfrastructureProgress extends Component {
    constructor(props) {
        super(props)
        this.state = {

        }

    }



    render() {
        const pheProgress = this.props.infra && this.props.infra.plumbing && this.props.infra.plumbing.percentOfCompletionOfFollowingItem ? this.props.infra.plumbing.percentOfCompletionOfFollowingItem : "0"
        const electricalProgress = this.props.infra && this.props.infra.electrical && this.props.infra.electrical.percentOfCompletionOfFollowingItem ? this.props.infra.electrical.percentOfCompletionOfFollowingItem : "0"
        const fireProgress = this.props.infra && this.props.infra.fireFighting && this.props.infra.fireFighting.percentOfCompletionOfFollowingItem ? this.props.infra.fireFighting.percentOfCompletionOfFollowingItem : "0"

        const projectApproachRoadsProgress = this.props.infra && this.props.infra.projectApproachRoads && this.props.infra.projectApproachRoads.percentOfCompletionOfFollowingItem ? this.props.infra.projectApproachRoads.percentOfCompletionOfFollowingItem : "0"
        const communityBuildingProgress = this.props.infra && this.props.infra.communityBuilding && this.props.infra.communityBuilding.percentOfCompletionOfFollowingItem ? this.props.infra.communityBuilding.percentOfCompletionOfFollowingItem : "0"
        const internalRoadsProgress = this.props.infra && this.props.infra.internalRoads && this.props.infra.internalRoads.percentOfCompletionOfFollowingItem ? this.props.infra.internalRoads.percentOfCompletionOfFollowingItem : "0"

        return (
            <View style={styles.amenitiesFullScreen}>
                <ScrollView>
                    <ConstructionBar
                        header={"PHE"}
                        // value={"WaterStorageSystem, WaterSupplyPiping, Drain,GasPiping, WaterTreatment, WasteDisposal, RainWaterHarvesting, SewageChamber,SewageLine,SepticTank,STP,WTP"}
                        src={res.images.plumber}
                        percentage={parseFloat(pheProgress) / 100}
                        percentageText={parseFloat(pheProgress)}
                    />
                    <ConstructionBar
                        src={res.images.electricle}
                        header={"Electrical"}
                        value={"Substation, Transformer, PanelsAndSubPanels, RoughWiring, Generator, StreetAndCommonAreaLighting, SolarPowerUnit, SolarFencing"}
                        percentage={parseFloat(electricalProgress) / 100}
                        percentageText={parseFloat(electricalProgress)}
                    />
                    <ConstructionBar

                        header={"Fire Safety"}
                        value={"FireStorageTanks, PumpingSystem, Hydrant,Sprinklers"}
                        percentage={parseFloat(fireProgress) / 100}
                        percentageText={parseFloat(fireProgress)}
                        src={res.images.fireExtinguisher}
                    />
                    <ConstructionBar
                        header={"Project Approach Roads"}
                        value={"WaterStorageSystem, WaterSupplyPiping, Drain,GasPiping, WaterTreatment, WasteDisposal, RainWaterHarvesting, SewageChamber,SewageLine,SepticTank,STP,WTP"}
                        src={res.images.road}
                        percentage={parseFloat(projectApproachRoadsProgress) / 100}
                        percentageText={parseFloat(projectApproachRoadsProgress)}
                    />
                     {/* <ConstructionBar
                        src={res.images.electricle}
                        header={"Community Building"}
                        value={"Substation, Transformer, PanelsAndSubPanels, RoughWiring, Generator, StreetAndCommonAreaLighting, SolarPowerUnit, SolarFencing"}
                        percentage={parseFloat(communityBuildingProgress) / 100}
                        percentageText={parseFloat(communityBuildingProgress)}
                    />
                    <ConstructionBar

                        header={"Internal Roads"}
                        value={"FireStorageTanks, PumpingSystem, Hydrant,Sprinklers"}
                        percentage={parseFloat(internalRoadsProgress) / 100}
                        percentageText={parseFloat(internalRoadsProgress)}
                        src={res.images.road}
                    />  */}
                </ScrollView>
            </View>
        )

    }
}
export default InfrastructureProgress