import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import { View, Text, FlatList, Image } from 'react-native';
import styles from './styles'
import strings from '../../../../res/constants/strings';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import Header from '../../commercial/costSheet/Header'
import res from '../../../../res'
import CustumizationCard from '../../../components/card/custumizationCard'
class Step2 extends Component {
    constructor(props) {
        super(props);
        this.state = {
            Customization: [{
                name: res.strings.Request,
                images: res.images.icn_request,
                isChecked: true
            }, {
                name: res.strings.Technical_Feasibility,
                images: res.images.icn_TechnicalFea,
                isChecked: true
            }, {
                name: res.strings.Estimate,
                images: res.images.icn_Estimate,
                isChecked: false
            }, {
                name: res.strings.Payment,
                images: res.images.icn_Paymnet,
                isChecked: false
            }, {
                name: res.strings.Acknowledgement,
                images: res.images.icn_Acknowledment,
                isChecked: false
            }, {
                name: res.strings.Execution,
                images: res.images.icn_Execution,
                isChecked: false
            }, {
                name: res.strings.Handover,
                images: res.images.icn_Handover,
                isChecked: false
            }]
        }
    }
    componentDidMount() {

    }

    backPress = () => {
        const { changePage } = this.props;
        changePage(-1)
    }
    renderCustomizationItem = ((item, index) => {
        // console.log("renderCustomizationItem", item)
        return (<View style={[styles.progresBox, styles.shadow]}>
            <View style={styles.imageView}>
                <Image source={item.item.images} />
            </View>
            <View style={styles.processText}>
                <Text>{item.item.name}</Text>
            </View>
            <View>
                {
                    item.item.isChecked ?
                        <Image source={res.images.icn_check} /> :
                        <View />
                }

            </View>
        </View>)
    })

    render() {
        console.log("CustomizationSTEP2===>", this.props.customizationApproval)
        const { acknowledgment, estimate, execution, handover, payment, request, technicalFeasibility } = this.props.customizationApproval

        return (

            <KeyboardAwareScrollView style={styles.customizatinContainer} scrollEnabled={false}>
                <Header title={strings.CUSTOMIZATION_PROCESS} onBack={this.backPress} />
                <View style={{ backgroundColor: res.colors.white, top: 0 }}>
                    {/* <View style={styles.requestBuilderView}> */}
                        <CustumizationCard
                            src={res.images.icn_request}
                            name={res.strings.Request}
                            isChecked={request}
                        />
                        <CustumizationCard
                            src={res.images.icn_TechnicalFea}
                            name={res.strings.Technical_Feasibility}
                            isChecked={technicalFeasibility}
                        />
                        <CustumizationCard
                            src={res.images.icn_Estimate}
                            name={res.strings.Estimate}
                            isChecked={estimate}
                        />
                        <CustumizationCard
                            src={res.images.icn_Paymnet}
                            name={res.strings.Payment}
                            isChecked={payment}
                        />
                        <CustumizationCard
                            src={res.images.icn_Acknowledment}
                            name={res.strings.Acknowledgement}
                            isChecked={acknowledgment}
                        />
                        <CustumizationCard
                            src={res.images.icn_Execution}
                            name={res.strings.Execution}
                            isChecked={execution}
                        />
                        <CustumizationCard
                            src={res.images.icn_Handover}
                            name={res.strings.Handover}
                            isChecked={handover}
                        />
                    {/* </View> */}
                </View>



            </KeyboardAwareScrollView>
        )
    }


}
const mapStateToProps = (state) => {
    return {};
};

function mapDispatchToProps(dispatch) {
    return bindActionCreators({

    }, dispatch);
}
let Step1Container = connect(mapStateToProps, mapDispatchToProps)(Step2);
export default Step1Container;

