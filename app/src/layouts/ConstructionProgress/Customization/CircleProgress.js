import React, { Component } from 'react';
import { View, Text, TouchableOpacity, ImageBackground, SafeAreaView, Image } from 'react-native';

import res from '../../../../res/images'
import {widthScale, heightScale} from  '../../../utility/Utils'


class CircleProgress extends Component {
    constructor(props) {
        super(props);
        this.state = {
           stepImg:res.customizationStep1
        }
    }
    componentDidMount() {

    }

    render(){
        let img = res.customizationStep1;
        const {pageIndex} = this.props
        console.log(pageIndex, "pageIndex in image")
        if(pageIndex == 0){
            img = res.customizationStep1;
        }else if(pageIndex == 1){
            img = res.customizationStep2
        }
        
        return(
            <View style={{flexDirection:'row', width:widthScale(140), height:heightScale(24), alignItems:'center', justifyContent:'center'}}>
                <Image source={img} resizeMode={'cover'}/>
            </View>
        )
    }
}

export default CircleProgress;

