import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import { View, Text, TouchableOpacity, Image } from 'react-native';

import styles from './styles'
import strings from '../../../../res/constants/strings';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import Header from '../../commercial/costSheet/Header'
import resources from '../../../../res';
import Button from '../../../components/button/Button'
import FileUploadComp from '../../../components/fileUpload/FileUploadComp'
import { widthScale } from '../../../utility/Utils';
import RoundedInput from '../../../components/input/RoundedInput'
import { PickerViewModal, CAMERA, DOCUMENT, IMAGE_PICK } from '../../../components/Picker/PickerViewModal'
import CheckBox from '../../../components/CheckBox/CheckBox'
class Step1 extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isChecked: false,
            pickOptionsVisible: false
        }
    }
    componentDidMount() {

    }

    backPress = () => {
        const { changePage } = this.props;
        changePage(-1)
    }
    sumbitDetails = () => {

    }

    render() {
        const { isChecked } = this.state
        return (

            <KeyboardAwareScrollView style={styles.customizatinContainer}
            scrollEnabled={false}
            >

                <Header title={strings.Customization} onBack={this.backPress} />
                <View style={styles.customizatinView}>
                    <View style={{ marginHorizontal: 10 }}>
                        <Text style={styles.subHeadingText}>{strings.HAVE_REQUEST_BUILDER}</Text>
                        <View style={styles.isCustomizUnit}>
                            <View style={{ width: '50%'}}>

                                <CheckBox
                                    isChecked={false}
                                    label={"Yes"}
                                />
                            </View>
                            <CheckBox
                                isChecked={false}
                                label={"No"}
                            />

                        </View>


                    </View>


                </View>



                <View style={{ marginHorizontal: widthScale(20) }}>
                    <Text style={styles.enterRequestDetails}>{resources.strings.Enter_the_detail_note}</Text>

                    <RoundedInput
                        placeholder={resources.strings.WRITE_NOTE}
                    />
                    <FileUploadComp label={strings.UPLOAD_FILE} />
                </View>
                <View style={styles.customizatinView}>
                    <View style={{ marginHorizontal: 10 }}>
                        <Text style={styles.enterRequestDetails}>{resources.strings.What_Is_Status}</Text>
                        <View style={styles.isCustomizUnit}>
                            <View style={{ width: '50%' }}>
                                <CheckBox
                                    isChecked={false}
                                    label={"Accept"}
                                />
                            </View>

                            <CheckBox
                                isChecked={false}
                                label={"Rejected"}
                            />

                        </View>
                        <CheckBox
                            isChecked={false}
                            label={"Accepted with Changes"}
                        />
                        <Text style={styles.enterRequestDetails}>{resources.strings.APPROVE_MODIFICATION}</Text>
                        <View style={styles.isCustomizUnit}>
                            <View style={{ width: '50%' }}>
                                <CheckBox
                                    isChecked={false}
                                    label={"Yes"}
                                />
                            </View>
                            <CheckBox
                                isChecked={false}
                                label={"No"}
                            />

                        </View>
                        <Text style={styles.enterRequestDetails}>{resources.strings.SEND_APPROVER_TO_BUILDER}</Text>
                        <View style={styles.isCustomizUnit}>
                            <View style={{ width: '50%', }}>
                                <CheckBox
                                    isChecked={false}
                                    label={"Yes"}
                                />
                            </View>
                            <CheckBox
                                isChecked={false}
                                label={"No"}
                            />

                        </View>
                        <Text style={styles.enterRequestDetails}>{resources.strings.Please_enter_the_estimate}</Text>
                        <RoundedInput
                            placeholder={resources.strings.AMOUNT}
                        />
                        <FileUploadComp label={strings.UPLOAD_DRAWING} />
                        <Text style={styles.enterRequestDetails}>{resources.strings.Have_you_approved_the_estimate}</Text>
                        <View style={styles.isCustomizUnit}>
                            <View style={{ width: '50%' }}>
                                <CheckBox
                                    isChecked={false}
                                    label={"Yes"}
                                />
                            </View>
                            <CheckBox
                                isChecked={false}
                                label={"No"}
                            />

                        </View>
                        <FileUploadComp label={strings.UPLOAD_PROOF} />

                        <Text style={styles.enterRequestDetails}>{resources.strings.Approve_the_Estimate}</Text>
                        <View style={styles.isCustomizUnit}>
                        <View style={{width:'50%'}}>
                            <CheckBox
                                isChecked={false}
                                label={"Yes"}
                            />
                            </View>
                            <CheckBox
                                isChecked={false}
                                label={"No"}
                            />

                        </View>
                    </View>


                </View>
                <View style={{marginHorizontal:10}}>
                    <Button
                        onPress={() => this.sumbitDetails()}
                        btnText={resources.strings.SUBMIT}
                    />
                </View>

            </KeyboardAwareScrollView>
        )
    }


}
const mapStateToProps = (state) => {
    return {};
};

function mapDispatchToProps(dispatch) {
    return bindActionCreators({

    }, dispatch);
}
let Step1Container = connect(mapStateToProps, mapDispatchToProps)(Step1);
export default Step1Container;

