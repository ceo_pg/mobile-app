import { StyleSheet, Platform, StatusBar } from 'react-native'
import res from '../../../../res'
import { heightScale, widthScale, myHeight } from '../../../utility/Utils'
const styles = StyleSheet.create({
  fullScreen: {
    flex: 1,
    // backgroundColor: res.colors.appColor,
    // paddingTop: Platform.OS === "android" ? StatusBar.currentHeight + 10 : 0
  }, customizatinContainer: {
    margin: widthScale(15),
    borderRadius: widthScale(20),
    flex: 1,
    backgroundColor: res.colors.white,
    // height: myHeight,
    // marginBottom: heightScale(200)
  },
  customizatinView: {
    marginVertical: heightScale(10),
    marginHorizontal: widthScale(10),
    borderRadius: widthScale(10),
    backgroundColor: res.colors.white,
    flex: 1,
    justifyContent: 'center',
    // alignItems: 'center',
    shadowColor: "rgba(0,0,0, 0.1)",
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.8,
    shadowRadius: 10,
    elevation: 10,
  },
  progressContainer: {
    marginTop: heightScale(17),
    alignItems: 'center'
  },viewPager2:{
    flex: 1,
    backgroundColor: res.colors.butterscotch,
    marginHorizontal: widthScale(20),
    borderRadius: widthScale(20),
    marginTop: heightScale(18),
    height: myHeight ,
  },
  viewPager: {
    flex: 1,
    backgroundColor: res.colors.butterscotch,
    marginHorizontal: widthScale(20),
    borderRadius: widthScale(20),
    marginTop: heightScale(18),
    height: myHeight + heightScale(850),
    // marginBottom: heightScale(200)
  },
  subHeadingText: {
    // textAlign: 'center',
    fontFamily: res.fonts.semiBold,
    fontSize: widthScale(16),
    color: res.colors.greyishBrown,
    paddingVertical: heightScale(20)
  }, requestBuilderView: {
    // marginHorizontal:10,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
  }, enterRequestDetails: {
    // textAlign: 'center',
    fontFamily: res.fonts.semiBold,
    fontSize: widthScale(15),
    color: res.colors.greyishBrown,
    paddingVertical: heightScale(20)
  }, progresBox: {
    marginTop: 10,
    height: heightScale(52),
    flexDirection: 'row',
    alignItems: 'center',
    width: '95%',
    backgroundColor: res.colors.white,
    borderRadius: 5,
    marginHorizontal: 10,
    marginBottom: 10
  }, imageView: {
    width: '20%',
    alignItems: 'center',
    justifyContent: 'center'
  }, processText: {
    width: '60%',
    justifyContent: 'center'
  }, shadow: {
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.30,
    shadowRadius: 4.65,

    elevation: 8,
  }, isCustomizUnit: {
    flexDirection: 'row',
    //  justifyContent:'space-around',
    // backgroundColor:"red",
    alignItems: 'flex-end'
  }
})
export default styles