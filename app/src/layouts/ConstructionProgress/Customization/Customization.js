import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import { View, SafeAreaView } from 'react-native';
import styles from './styles'

import DashBoardHeader from '../../../components/header/DashBoardHeader'
import CircleProgress from './CircleProgress'
import CustomizationStep1 from './CustomizationStep1'
import CustomizationStep2 from './CustomizationStep2'
import ApiFetch from '../../../apiManager/ApiFetch'
import ViewPager from '@react-native-community/viewpager';
import res from '../../../../res'
import { CUSTUMIZATION_ENDPOINTS } from '../../../apiManager/ApiEndpoints'

class Customization extends Component {
    constructor(props) {
        super(props);
        this.state = {
            pageIndex: 0,
            progressImg: res.images.customizationStep1
        }
        this.viewPager = React.createRef()
    }

    componentDidMount() {
        // this.hitCustomizationApi()
    }
    hitCustomizationApi = () => {
        ApiFetch.fetchPut(CUSTUMIZATION_ENDPOINTS + "/" + "5ef20fa4fe2625947b83f118" + "/" + "5f1ebe0ac83c27548e9daa5b")
            .then((res) => {
                console.log("CUSTUMIZATION", res)
            }).catch((err) => {
                console.log("Error inside custumization", err)
            })
    }


    changePageIndex = (index) => {
        if (index == -1) {
            // close the drwr
            return
        }
        this.setState({ pageIndex: index })
        this.viewPager.setPage(index);
        this.setProgressImage(index)
    }

    pageChanged = (e) => {
        if (e && e.nativeEvent && e.nativeEvent) {
            this.setState({ pageIndex: e.nativeEvent.position });
            this.setProgressImage(e.nativeEvent.position)
        }
    }

    setProgressImage = (index) => {
        switch (index) {
            case 0:
                // code block
                this.setState({ progressImg: res.images.customizationStep1 })
                break;
            case 1:
                // code block
                this.setState({ progressImg: res.images.customizationStep2 })
                break;

            default:
            // code block
        }
    }


    render() {
        console.log("CUSTOMIZATION STEP", this.props)
        const { progressImg, pageIndex } = this.state
        return (
            <SafeAreaView style={styles.fullScreen}>

                <View style={styles.progressContainer}>
                    <CircleProgress pageIndex={this.state.pageIndex} progressImg={progressImg} />
                </View>
                <ViewPager style={[pageIndex == 1 ? styles.viewPager : styles.viewPager2]} initialPage={0}
                    ref={(viewPager) => { this.viewPager = viewPager }}
                    onPageSelected={(e) => this.pageChanged(e)}
                >
                    {/* <View key="1" >
                        <CustomizationStep1 changePage={this.changePageIndex} />
                    </View> */}
                    <View key="1">
                        <CustomizationStep2
                            customizationApproval={this.props.customizationApproval}
                            changePage={this.changePageIndex} />
                    </View>

                </ViewPager>
            </SafeAreaView>
        )
    }
}

const mapStateToProps = (state) => {
    return {
    };
};

function mapDispatchToProps(dispatch) {
    return bindActionCreators({

    }, dispatch);
}
let CustomizationScreenContainer = connect(mapStateToProps, mapDispatchToProps)(Customization);
export default CustomizationScreenContainer;

