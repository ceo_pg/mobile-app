import React, { Component } from 'react'
import { View, Text, SafeAreaView, ImageBackground } from 'react-native'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { CommonActions } from '@react-navigation/native';
import { bindActionCreators } from "redux"
import { connect } from 'react-redux';
import Toast from 'react-native-simple-toast'

import styles from './styles'
import DashBoardHeader from '../../components/header/DashBoardHeader'
import res from '../../../res'
import { TabView, Loader, HalfCircular } from '../../components'
import { heightScale } from '../../utility/Utils';
import Construction from './Construction/Construction'
import Gallery from './Gallery/Gallery'
import Customization from './Customization/Customization'
import { getConstructionProgress, saveGallery, getProgress } from '../../redux/actions/ConstructionProgress/ConstructionProgressAction'
import UnlockViewComp from '../../components/unlock/UnlockViewComp'

class ConstructionProgress extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isLoading: false,
            selectedIndex: 0,
            cellList: [{
                image: res.images.icn_contract,
                number: "123",
                cell: res.strings.CONSRUCTION_PROGRESS
            }, {
                image: res.images.icn_contract,
                number: "123",
                cell: res.strings.Gallery
            },
                //  {
                //     image: res.images.icn_contract,
                //     number: "123",
                //     cell: "Customization"
                // }
            ],
            totalConstructionStatus: "",
            galleryData: [],
            unitLevel: "",
            towerLevel: "",
            infrastructureLevel: "",
            projectConstructionProgress: {},
            unitLevelConstructionProgress: {},
            towerLevelConstructionProgress: {},
            customizationApproval: {},
            constructionData: {},
            isSubscribed: false
        }
    }
    componentDidMount() {

        this.getConstructionProgressResponse()
    }
    getConstructionProgressResponse = () => {
        this.setState({ isLoading: true })
        this.props.getConstructionProgress().then((res => {
            console.log("getConstructionProgress", res)
            this.setState({ isLoading: false });
            if (res.statusCode == 200 && res.status) {
                this.props.saveGallery(res.galleryData);
                this.props.getProgress(res.data)
            } else {
                Toast.show(res.message, Toast.LONG)
            }
        }))
            .catch(error => {
                this.setState({ isLoading: false })
                console.log(error, "error");
            })
    }
    openDrawer = () => {

        this.props.navigation.openDrawer()
        const resetAction = CommonActions.reset({
            index: 0,
            routes: [{ name: 'KnowYourProperty' }],
        });
        this.props.navigation.dispatch(resetAction);
    }
    selectedIndex = (index) => {
        this.setState({
            selectedIndex: index
        })
    }
    renderTabView = () => {
        const { cellList } = this.state
        return <TabView
            listDataItem={cellList}
            selectedIndex={this.selectedIndex}
            tabBarUnderlineStyle={{ backgroundColor: res.colors.pinkishTan, height: 2 }}
            tabBarInactiveTextColor={res.colors.white}
            tabBarActiveTextColor={res.colors.pinkishTan}

        />
    }
    renderProjectLevel = () => {

        const projectConstructionProgress = this.props && this.props.progressData && this.props.progressData.projectConstructionProgress && this.props.progressData.projectConstructionProgress.totalConstructionStatus ? this.props.progressData.projectConstructionProgress.totalConstructionStatus : "0"
        const unitConstructionProgress = this.props && this.props.progressData && this.props.progressData.unitLevelConstructionProgress && this.props.progressData.unitLevelConstructionProgress.totalConstructionStatus ? this.props.progressData.unitLevelConstructionProgress.totalConstructionStatus : "0"
        const towerConstructionProgress = this.props && this.props.progressData && this.props.progressData.towerLevelConstructionProgress && this.props.progressData.towerLevelConstructionProgress.totalConstructionStatus ? this.props.progressData.towerLevelConstructionProgress.totalConstructionStatus : "0"

        const totalProgress = (projectConstructionProgress * 0.2 + unitConstructionProgress * 0.5 + towerConstructionProgress * 0.3)
        // const { projectConstructionProgress } = this.props.progressData
        // console.log("projectConstructionProgress", totalProgress,projectConstructionProgress,unitConstructionProgress,to)
        const filledView = totalProgress ? totalProgress / 100 : 0
        // if (!totalProgress) {
        //     return null
        // }

        return (
            <View >
                <View style={styles.graphView}>
                    <View style={{ alignItems: 'center', marginLeft: 20 }}>

                        <Text style={styles.projectLevelText}>{res.strings.PROJECT_LEVEL}</Text>

                    </View>
                    <View style={styles.graphSubView}></View>
                    <View style={{ flexDirection: 'column' }}>
                        <View style={{ alignItems: 'center' }} >

                            <View
                                style={[styles.filledView,
                                { height: 100 * filledView, width: 38 },
                                ]}
                            />
                            <ImageBackground source={res.images.icn_Project_Level} style={{ width: 75, height: 100, alignItems: 'center' }} resizeMode={'stretch'} >


                            </ImageBackground>
                        </View>
                        <View style={{ marginTop: heightScale(5) }}>

                            <Text style={styles.percentageText}>{totalProgress ? Math.round(totalProgress) : "0"}%</Text>
                        </View>
                    </View>

                </View>



            </View>


        )
    }
    renderOtherLevel = () => {
        const { projectConstructionProgress, towerLevelConstructionProgress, unitLevelConstructionProgress } = this.props.progressData
        if (!projectConstructionProgress && !towerLevelConstructionProgress && !unitLevelConstructionProgress) {
            return <View style={styles.squreFitView}></View>
        }
        return <View style={styles.squreFitView}>


            <View style={styles.graphViewStyles}>
                <Text style={styles.halfCircleHeaderText}>{res.strings.UnitLevel}</Text>
                <HalfCircular
                    percentage={unitLevelConstructionProgress.totalConstructionStatus ? Math.round(unitLevelConstructionProgress.totalConstructionStatus) : "0"}
                    progressColor={res.colors.toupe}
                    circleRadius={28}
                    interiorCircleColor={res.colors.cobalt}
                >
                    <Text style={styles.percentageText}>{unitLevelConstructionProgress.totalConstructionStatus ? Math.round(unitLevelConstructionProgress.totalConstructionStatus) : "0"}%</Text>
                </HalfCircular>
            </View>
            <View style={styles.graphViewStyles}>
                <Text style={styles.halfCircleHeaderText}>{res.strings.TowerLevel}</Text>
                <HalfCircular
                    percentage={towerLevelConstructionProgress.totalConstructionStatus ? Math.round(towerLevelConstructionProgress.totalConstructionStatus) : "0"}
                    progressColor={res.colors.toupe}
                    circleRadius={28}
                    interiorCircleColor={res.colors.cobalt}
                >
                    <Text style={styles.percentageText}>{towerLevelConstructionProgress.totalConstructionStatus ? Math.round(towerLevelConstructionProgress.totalConstructionStatus) : "0"}%</Text>
                </HalfCircular>
            </View>
            <View style={styles.graphViewStyles}>
                <Text style={styles.halfCircleHeaderText}>{res.strings.InfrastructureLevel}</Text>
                <HalfCircular
                    percentage={projectConstructionProgress.totalConstructionStatus ? Math.round(projectConstructionProgress.totalConstructionStatus) : "0"}
                    progressColor={res.colors.toupe}
                    circleRadius={28}
                    interiorCircleColor={res.colors.cobalt}
                >
                    <Text style={styles.percentageText}>{projectConstructionProgress.totalConstructionStatus ? Math.round(projectConstructionProgress.totalConstructionStatus) : "0"}%</Text>
                </HalfCircular>
            </View>
        </View>
    }
    renderConstruction = () => {

        return <Construction
        // data={this.state.constructionData}
        />
    }
    renderGallery = () => {
        return <Gallery
        // galleryData={this.state.galleryData}
        />
    }
    renderCustomization = () => {
        return <Customization
        // customizationApproval={this.state.customizationApproval}
        />
    }
    renderCorousalView = () => {
        const { selectedIndex } = this.state
        switch (selectedIndex) {
            case 0:
                // code block
                return this.renderConstruction()
                break;

            case 1:
                // code block
                return this.renderGallery()
                break;

            case 2:
                // code block
                return this.renderCustomization()
                break;
            default: return

        }


    }
    renderLevel = () => {
        // console.log("renderLevel", selectedIndex)
        const { selectedIndex } = this.state
        if (selectedIndex == 2) {
            return <View />
        } else {
            return this.renderProjectLevel()
            // this.renderOtherLevel()
        }
    }
    renderLoader = () => {
        return (
            <Loader
                loading={this.state.isLoading} />
        );
    };
    renderTab = () => {
        return <View style={{ overflow: 'hidden', paddingBottom: 5 }}>
            <View
                style={styles.headerShadow}
            >
                {this.renderTabView()}
            </View>
        </View>
    }

    renderUnLockView = () => {
        return (
            <UnlockViewComp onNextPress={this.onNextPress} />
        )
    }
    onNextPress = () => {
        this.props.navigation.navigate("Subscription");

    }
    openSupport = () => {
        this.props.navigation.navigate("SupportBaseScreen")
    }
    render() {
        const { isLoading, isSubscribed } = this.state
        const { progressData } = this.props
        if (!this.props.progressData.towerLevelConstructionProgress) {
            return <View />
        }
        return (
            <SafeAreaView style={styles.fullScreen}>
                <DashBoardHeader
                    isShowShadow={true}
                    openSupport={() => this.openSupport()}
                    openDrawer={() => this.openDrawer()}
                    headerTitle={res.strings.CONSRUCTION_PROGRESS}
                    settingVisible={false}
                    notificationVisible={false}
                />
                <ImageBackground source={res.images.mainBackground} style={{ flex: 1 }}>

                    {this.renderTab()}
                    {isSubscribed ?
                        this.renderUnLockView() :

                        <View style={{ flex: 1 }}>

                            {
                                this.state.selectedIndex == 2 ?
                                    <View /> :
                                    this.renderProjectLevel()

                            }
                            {
                                this.state.selectedIndex == 2 ?
                                    <View /> :
                                    this.renderOtherLevel()

                            }

                            {this.renderCorousalView()}
                        </View>

                    }





                    {isLoading && this.renderLoader()}
                </ImageBackground>

            </SafeAreaView>
        )
    }

}
const mapStateToProps = (state) => {
    console.log("mapStateToProps====>", state)
    return {
        progressData: state.ConstructionReducer.progressData

    };
};

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        getConstructionProgress,
        saveGallery,
        getProgress
    }, dispatch);
}
let ConstructionProgressContainer = connect(mapStateToProps, mapDispatchToProps)(ConstructionProgress);
export default ConstructionProgressContainer;