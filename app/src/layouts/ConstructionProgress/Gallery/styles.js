import { StyleSheet, Platform, StatusBar } from 'react-native'
import res from '../../../../res'
import { heightScale } from '../../../utility/Utils'
const styles = StyleSheet.create({
    fullScreen: {
        flex: 1,
        backgroundColor: res.colors.appColor,
        paddingTop: Platform.OS === "android" ? StatusBar.currentHeight + 10 : 0
    }, carouselData: {
        zIndex: 99,
    }, dotStyle: {

        borderRadius: 5,

        backgroundColor: res.colors.pinkishTan
    }, dotColor: {
        width: 10,
        height: 10,
        borderRadius: 5,
        marginHorizontal: -5,
        backgroundColor: res.colors.pinkishTan
    }, inactiveDotStyle: {
        width: 10,
        height: 10,
        borderRadius: 5,
        marginHorizontal: -5,
        backgroundColor: res.colors.pinkishTan
    }, shadowHeader: {
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        backgroundColor: res.colors.butterscotch,
        shadowColor: res.colors.shadowColor,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 10.58,
        shadowRadius: 10.00,
        elevation: 24,
    }, corousalNameText: {
        fontWeight: "bold",
        fontFamily: res.fonts.semiBold,
        fontSize: 18,
        letterSpacing: 0.3,
        textAlign: 'center',
        color: res.colors.greyishBrown,
        marginTop: 10
    }, dateText: {
        fontWeight: 'bold',
        fontFamily: res.fonts.semiBold,
        fontSize: 16,
        color: res.colors.peacockBlue
    }, instructionText: {
        fontFamily: res.fonts.regular,
        fontSize: 14,
        color: res.colors.greyishBrown
    }, imageView: {
        flex: 1, width: '100%', borderRadius: 10, height: heightScale(247)
    }, galleyImages: {
        flex: 1, borderBottomLeftRadius: 5, borderBottomRightRadius: 5,marginHorizontal:10
    }, closeBtn: {
        position: "absolute",
        top: 30,
        right: 30,
        backgroundColor: res.colors.white,
        zIndex: 999,
        width:30,
        height:30,
        borderRadius:15,
        alignItems:'center',
        justifyContent:'center'
    }, dateModalText: {
        fontWeight: 'bold',
        fontFamily: res.fonts.semiBold,
        fontSize: 16,
        color: res.colors.white
    }, instructionModalText: {
        fontFamily: res.fonts.regular,
        fontSize: 14,
        color: res.colors.white
    }, ShareBtn: {
        position: 'absolute',
        bottom: 30,
        right: 30,
        backgroundColor:res.colors.black,
        borderRadius:25,
        zIndex: 9999999,
        width:50,
        height:50,
        overflow:'hidden',
        justifyContent:'center',
        alignItems:'center'
      },shareView:{
        backgroundColor:res.colors.white,
        borderRadius:15,
        width:30,
        height:30
      }
})
export default styles