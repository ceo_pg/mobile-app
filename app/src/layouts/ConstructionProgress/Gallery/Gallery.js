import React, { Component } from 'react'
import { View, Dimensions, Text, Image, TouchableOpacity, FlatList, Linking } from 'react-native'
import Carousel, { Pagination } from 'react-native-snap-carousel';
import moment from 'moment';
import Modal from 'react-native-modal'
import ImageZoom from 'react-native-image-pan-zoom';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import styles from './styles'
import res from '../../../../res'
import { widthScale, heightScale } from '../../../utility/Utils';
import { Loader } from '../../../components/index'
class Gallery extends Component {
    constructor(props) {
        super(props)
        this.state = {
            selectedIndex: 0,
            isLoading: false,
            isModalVisible: false,
            activeIndex: 0,
            uri: "",
            constructionList: [{
                name: "Construction Progress Gallery",
                id: "1"
            }]
        }

    }
    componentDidMount() {

        // this.props.saveGallery().then((res) => {
        //     console.log("GALLERY")
        // })
    }
    ShareImage = (url) => {
        Linking.canOpenURL(url).then(supported => {
            if (supported) {
                Linking.openURL(url);
            } else {
                console.log("Don't know how to open URI: " + url);
            }
        });
    }
    renderGalleryData = ((item, index) => {
        const { uri } = this.state
        console.log("renderGalleryData", item)
        return (
            <View style={{ flex: 1 }}>
                <View style={styles.closeBtn}>
                    <TouchableOpacity onPress={() => this.toggleModal(index)}>
                        <Image source={res.images.icnClose} style={{ width: 20, height: 20, tintColor: res.colors.black }} resizeMode={'center'} />
                    </TouchableOpacity>
                </View>
                <View style={{ flex: 1, zIndex: 9, overflow: 'hidden' }}>
                    <ImageZoom


                        cropWidth={Dimensions.get('window').width}
                        cropHeight={Dimensions.get('window').height}
                        imageWidth={Dimensions.get('window').width}
                        imageHeight={Dimensions.get('window').height}>
                        {
                            item.item.image ?
                                <Image source={item.item.image ? { uri: item.item.image } : ""} style={{ width: "100%", height: "100%" }} resizeMode={'center'} /> :
                                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                                    <Text style={{ textAlign: 'center', color: 'white' }}>Unable to load</Text>
                                </View>


                        }

                    </ImageZoom>
                </View>
                <View style={{ position: 'absolute', bottom: heightScale(30), right: 20, left: 20, zIndex: 10, overflow: 'hidden' }}>
                    <Text style={styles.dateModalText}>{item.item.date ? moment(item.item.date).format("DD MMMM,YYYY") : moment(Date).format("DD MMMM,YYYY")}</Text>
                    <Text style={styles.instructionModalText} textBreakStrategy={'simple'} numberOfLines={4}>{item.item.description}</Text>
                </View>
                {
                    item.item.image ?
                        <TouchableOpacity style={styles.ShareBtn} onPress={() => this.ShareImage(item.item.image)}>

                            <View style={styles.shareView}>
                                <Image source={res.images.icn_share} />
                            </View>

                        </TouchableOpacity> : null
                }

            </View>)
    })

    goIndex = () => {
        this.flatListRef.scrollToIndex({ animated: true, index: this.selectedImageIndex });
    }

    modalView = () => {
        const { galleryData } = this.props
        var newgalleryData = galleryData.filter((value, index) => {
            if (value.image != '') {
                return true
            }
            else {
                return false
            }
        })
        return <View style={{ flex: 1 }}>
            {/* <View style={styles.closeBtn}>
                <TouchableOpacity onPress={() => this.toggleModal()}>
                    <Image source={res.images.icnClose} style={{ width: 20, height: 20, tintColor: res.colors.black }} resizeMode={'center'} />
                </TouchableOpacity>
            </View> */}
            <FlatList
                ref={(ref) => { this.flatListRef = ref; }}
                keyExtractor={(item, index) => index.toString()}
                bounces={false}
                style={{ marginHorizontal: -20 }}
                horizontal
                pagingEnabled={true}
                data={newgalleryData}
                renderItem={this.renderGalleryData}
                initialScrollIndex={this.state.selectedIndex}
                onScrollToIndexFailed={() => { }}
            />
        </View>

    }
    toggleModal = (index) => {
        console.log("TOGGLEIMGES", index)
        const { isModalVisible } = this.state
        this.selectedImageIndex = index
        this.setState({
            selectedIndex: index,
            isModalVisible: !isModalVisible,
            // uri: images,
        }, () => {
            if (index) {
                this.goIndex()
            }
        })
    }

    renderData = ({ item, index }) => {

        if (!item.image) {
            return <View style={{ flex: 1, backgroundColor: res.colors.butterscotch, borderRadius: 24, marginTop: 5, overflow: 'hidden', alignItems: 'center', justifyContent: 'center' }}>
                <Text>{res.strings.NO_IMAGE_FOUND}</Text>
            </View>
        }
        return <View style={{ flex: 1, backgroundColor: res.colors.butterscotch, borderRadius: 24, marginTop: 5, overflow: 'hidden' }} onPress={() => this.toggleModal(index)} activeOpacity={1}>

            <View style={[styles.shadowHeader, { height: 42 }]}>
                <Text style={styles.corousalNameText}>{res.strings.Construction_Progress_Gallery}</Text>
            </View>
            <TouchableOpacity activeOpacity={1} onPress={() => this.toggleModal(index)} style={styles.imageView}>
                <Image source={item.image ? { uri: item.image } : res.images.parking} resizeMode={'cover'} style={styles.galleyImages} />
            </TouchableOpacity>
            {/* <Image source={item.image ? { uri: item.image } : res.images.parking} style={{ alignSelf: 'center' }} /> */}
            <View style={{ marginHorizontal: 20, marginTop: 20, bottom: 10 }}>
                <Text style={styles.dateText}>{moment(item.date).format("DD MMMM YYYY")}</Text>
                <Text style={styles.instructionText} textBreakStrategy={'simple'} numberOfLines={4}>{item.description}</Text>
            </View>
        </View>
    }
    renderLoader = () => {
        return (
            <Loader
                loading={this.state.isLoading} />
        );
    };
    render() {
        // console.log("CONSTRUCTIONGALLERY", this.props)
        const { isLoading, constructionList } = this.state
        const { galleryData } = this.props
        var newgalleryData = galleryData.filter((value, index) => {
            if (value.image != '') {
                return true
            }
            else {
                return false
            }
        })
        if (!newgalleryData) {
            return null
        }

        return (
            <View style={styles.fullScreen}>
                <Modal isVisible={this.state.isModalVisible}

                    onBackdropPress={() => this.toggleModal()}
                    style={styles.view}>
                    {this.props ? this.modalView() : <View />}
                </Modal>

                <View style={{ flex: 1 }}>
                    <Carousel
                        data={newgalleryData}
                        renderItem={this.renderData}
                        sliderWidth={Dimensions.get('window').width}
                        itemWidth={widthScale(307)}
                        layout={'default'}
                        // containerCustomStyle={{ height: Dimensions.get('window').height * 0.87 }}
                        removeClippedSubviews={true}
                        onSnapToItem={index => this.setState({ activeIndex: index })}
                        firstItem={this.state.activeIndex}
                    />
                    <View style={{ height: 20, justifyContent: 'center', width: "10%", alignSelf: 'center' }}>
                        <Pagination
                            containerStyle={{ height: 20 }}
                            dotContainerStyle={{ height: 20 }}
                            dotsLength={newgalleryData.length}
                            dotStyle={styles.dotStyle}
                            dotColor={styles.dotColor}
                            inactiveDotStyle={styles.inactiveDotStyle}
                            activeDotIndex={this.state.activeIndex}
                            inactiveDotOpacity={0.4}
                            inactiveDotScale={0.6}
                            activeOpacity={1}
                        />
                    </View>


                </View>
                {isLoading && this.renderLoader()}
            </View>
        )

    }
}
const mapStateToProps = (state) => {
    // console.log("mapStateToProps",state)
    return {
        galleryData: state.ConstructionReducer.galleryData
    };
};

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
    }, dispatch);
}
let GalleryContainer = connect(mapStateToProps, mapDispatchToProps)(Gallery);
export default GalleryContainer;
// export default Gallery