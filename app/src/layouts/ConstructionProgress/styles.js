import { StyleSheet, Platform, StatusBar } from 'react-native'
import res from '../../../res'
import { heightScale, widthScale } from '../../utility/Utils'
const styles = StyleSheet.create({
    fullScreen: {
        flex: 1,
        backgroundColor: res.colors.appColor,
        paddingTop: Platform.OS === "android" ? StatusBar.currentHeight + 10 : 0
    }, headerShadow: {
        backgroundColor: res.colors.appColor,

        height: 50,
        shadowColor: '#000',
        shadowOffset: { width: 1, height: 1 },
        shadowOpacity: 0.4,
        shadowRadius: 3,
        elevation: 5,
    }, container: {
        flex: 1,
        backgroundColor: res.colors.appColor,
        overflow: 'hidden'
        // marginTop: Platform.OS == 'ios' ? 35 : 20
    }, projectLevelText: {
        fontWeight: 'bold',
        fontFamily: res.fonts.semiBold,
        fontSize: heightScale(32),
        color: res.colors.white
    }, squreFitView: {
        // paddingHorizontal:20,
        marginHorizontal: 22,
        backgroundColor: res.colors.cobalt,
        minHeight: 72,
        borderRadius: 8,
        marginTop: heightScale(10),
        flexDirection: 'row',
        alignItems: 'center',
        // justifyContent: 'space-between'
    }, halfCircleHeaderText: {
        color: res.colors.toupe,
        fontFamily: res.fonts.semiBold,
        fontSize: 10,
        // marginBottom: 11,
        textAlign: 'center'
    },
    percentageText: {
        fontFamily: res.fonts.semiBold,
        fontSize: 10,
        color: res.colors.white,
        textAlign: 'center'
    }, projectLevelValueText: {
        top: heightScale(5),
        fontFamily: res.fonts.medium,
        fontSize: 12,
        color: res.colors.white,
        textAlign: 'center'
    }, seprater: {

        height: '85%',
        width: "0.1%",
        backgroundColor: res.colors.sepratorColor,
        marginRight: 30
    },
    graphView: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        marginTop: heightScale(24),
        alignItems: 'center',
        // backgroundColor: 'white'
    },
    graphSubView: {
        backgroundColor: res.colors.sepratorColor,
        height: '60%',
        width: 0.5
    },
    filledView: {
        width: 45,
        justifyContent: 'flex-end',
        backgroundColor: res.colors.toupe,
        position: 'absolute',
        bottom: 0,
        opacity: 1
    }, graphViewStyles: {
        alignItems: 'center',
        width: '33.33%',
    },
    unlockOuterView: {
        flex: 1,
        backgroundColor: res.colors.butterscotch,
        marginHorizontal: widthScale(20),
        borderRadius: widthScale(20),
        marginTop: heightScale(64),
        // height:heightScale(568)

    },
    


})

export default styles