import { StyleSheet, Platform, StatusBar } from 'react-native'
import res from '../../../res'
import {widthScale} from '../../utility/Utils'
const styles = StyleSheet.create({
  fullScreen: {
    flex: 1,
    backgroundColor: res.colors.appColor,
    paddingTop: Platform.OS === "android" ? StatusBar.currentHeight + 10 : 0
  }, unitFullScreen: {
    flex: 1,
    // backgroundColor: res.colors.white,

  }, shadow: {

    shadowColor: res.colors.shadowColor,
    shadowOffset: {
      width: 10,
      height: 20,
    },
    shadowOpacity: 20.39,
    shadowRadius: 10.30,

    elevation: 20,
    zIndex: 10
  }, container: {
    flex: 1,
    backgroundColor: res.colors.white,
    overflow: 'hidden'
    // marginTop: Platform.OS == 'ios' ? 35 : 20
  }, firstTitle: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginTop: 22,
    marginLeft: 20
  }, districtText: {
    fontFamily: res.fonts.semiBold,
    fontSize: 32,
    color: res.colors.white
  }, addressText: {
    fontFamily: res.fonts.regular,
    fontSize: 10,
    color: res.colors.white,
    letterSpacing: 0.48,
    marginTop: 5,
    paddingRight:10
  }, lineView: {
    height: '100%',
    width: 1,
    backgroundColor: res.colors.sepratorColor
  }, squreFitView: {
    marginHorizontal: 22,
    backgroundColor: res.colors.cobalt,
    height: 55,
    borderRadius: 8,
    marginTop: 27,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center'
  }, carousel: {

    height: 50, zIndex: 99,
    marginTop: 56
  }, carouselItemContainer: {
    justifyContent: "center",
    alignItems: 'center'
  }, titleText: {
    fontSize: 18,
    fontFamily: res.fonts.semiBold,
    color: res.colors.white,
    letterSpacing: 0.86
  }, carouselData: {

    // borderRadius: 100,
    // backgroundColor: 'red',
    zIndex: 99,
    // marginTop: 56
  }, carouselDataContainer: {
    borderRadius: 15,
    height: 80,
    marginLeft: 5,
    marginRight: 5,

    backgroundColor: res.colors.white
  }, containerModal: {
    padding: 25,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  button: {
    display: 'flex',
    height: 60,
    borderRadius: 6,
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    backgroundColor: '#2AC062',
    shadowColor: '#2AC062',
    shadowOpacity: 0.5,
    shadowOffset: {
      height: 10,
      width: 0
    },
    shadowRadius: 25,
  },
  closeButton: {
    display: 'flex',
    height: 60,
    borderRadius: 6,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#FF3974',
    shadowColor: '#2AC062',
    shadowOpacity: 0.5,
    shadowOffset: {
      height: 10,
      width: 0
    },
    shadowRadius: 25,
  },
  buttonText: {
    color: '#FFFFFF',
    fontSize: 22,
  },
  image: {
    marginTop: 150,
    marginBottom: 10,
    width: '100%',
    height: 350,
  },
  text: {
    fontSize: 24,
    marginBottom: 30,
    padding: 40,
  },
  closeText: {
    fontSize: 24,
    color: '#00479e',
    textAlign: 'left',
  }, view: {
    justifyContent: 'flex-end',
    marginHorizontal: 20,

  }, titleTextBox: {
    fontSize: 15,
    fontFamily: res.fonts.regular,
    color: res.colors.black,
    letterSpacing: 0.86
  }, modalHeaderText: {
    marginTop: 10,
    fontFamily: res.fonts.semiBold,
    fontSize: 18,
    letterSpacing: 0.86,
    textAlign: 'left',
    color: res.colors.greyishBrown,
    // marginTop: 18
  }, boxShadow: {
    backgroundColor: "white",
    shadowColor: "#000000",
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowOpacity: 0.25,
    shadowRadius: 1.84,
    elevation: 5,
  }, addCarText: {
    marginHorizontal: 14,
    fontFamily: res.fonts.regular,
    marginVertical: 10,
    fontSize: 12,
    color: res.colors.black
  }, unitNumber: {
    fontFamily: res.fonts.regular,
    fontSize: 22,
    color: res.colors.liteBlack
  }, unitText: {
    fontFamily: res.fonts.regular,
    fontSize: 12,
    color: res.colors.liteBlack
  }, facingText: {
    fontFamily: res.fonts.regular,
    fontSize: 16,
    color: res.colors.liteBlack
  }, UnitTypeText: {
    fontFamily: res.fonts.regular,
    fontSize: 12,
    color: res.colors.liteBlack
  }, bottomBtn: {
    height: 35,
    flexDirection: 'row',
    justifyContent: 'space-between',

  }, addMoreBtn: {
    borderTopWidth: 1, borderTopColor: res.colors.black, flex: 1, alignItems: 'center', justifyContent: 'center',
    borderBottomLeftRadius: 12, borderBottomWidth: 1, borderBottomColor: res.colors.black,
  }, saveBtn: {
    borderBottomWidth: 1, borderBottomColor: res.colors.black, flex: 1, alignItems: 'center', justifyContent: 'center',
    borderBottomRightRadius: 12, borderTopWidth: 1, borderTopColor: res.colors.black, borderLeftWidth: 1, borderLeftColor: res.colors.black,
  }, addMoreText: {
    fontFamily: res.fonts.semiBold,
    fontSize: 12,
    color: res.colors.peacockBlue,

  }, textInputStyle: {
    paddingLeft: 10,
    width: '70%',
    marginLeft: 45,
    height: 27,
    borderWidth: 1.5,
    borderColor: res.colors.sepratorColor,
    borderRadius: 3
  }, textInputCheckStyle: {
    paddingLeft: 10,
    width: '70%',
    marginLeft: 45,
    height: 27,
    borderWidth: 1,
    borderColor: res.colors.inputBorderColor,
    borderRadius: 3
  }, squreHeading: {
    fontWeight:'bold',
    fontFamily: res.fonts.semiBold,
    fontSize: 14,
    color: res.colors.pinkishTan,
    // marginLeft: 10,

  }, squreSubHeading: {
    fontWeight:'bold',
    fontFamily: res.fonts.semiBold,
    fontSize: 9,
    color: res.colors.white,
    // marginHorizontal: 10,
    marginTop:5
  }, corousalNameText: {
    fontWeight:"bold",
    fontFamily: res.fonts.semiBold,
    fontSize: 18,
    letterSpacing: 0.3,
    textAlign: 'center',
    color: res.colors.greyishBrown,
    marginTop: 10
  }, footerBox: {
    flex: 1,
    margin: 10,
    borderRadius: 12,
    marginBottom: 30,
    borderTopColor: res.colors.black,
    borderTopWidth: 1,
    borderLeftColor: res.colors.black,
    borderLeftWidth: 1,
    borderRightColor: res.colors.black,
    borderRightWidth: 1
  }, dotStyle: {
    // width: 10,
    // height: 10,
    borderRadius: 5,
    // marginHorizontal: 8,
    backgroundColor: res.colors.pinkishTan
  }, dotColor: {
    width: 10,
    height: 10,
    borderRadius: 5,
    marginHorizontal: -5,
    backgroundColor: res.colors.pinkishTan
  }, inactiveDotStyle: {
    width: 10,
    height: 10,
    borderRadius: 5,
    marginHorizontal: -5,
    backgroundColor: res.colors.pinkishTan
  }, shadowHeader: {
    
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    backgroundColor: res.colors.butterscotch,
    shadowColor: res.colors.shadowColor,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 10.58,
    shadowRadius: 10.00,
    elevation: 24,
  }, unitHeaderData: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',

    marginHorizontal: 10,

    backgroundColor: res.colors.shadowColor,

    marginTop: 10,
    height: 22,
    borderTopRightRadius: 5,
    borderTopLeftRadius: 5,

  }, unitHeaderText: {
    fontFamily: res.fonts.medium,
    fontSize: 12.2,
    color: res.colors.purpleBrown,
    // marginLeft:-30
  }, unitAreaHeaderText: {
    fontFamily: res.fonts.medium,
    fontSize: 12.2,
    color: res.colors.purpleBrown,
    marginRight: 30
  },
  unitFloorData: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',

    marginHorizontal: 10,

  }, unitFloorText: {
    fontFamily: res.fonts.regular,
    fontSize: 8.7,
    color: res.colors.black,
    textAlign: 'left',
    marginLeft: 10
  }, unitOddFloorData: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    height: 22,
    backgroundColor: res.colors.shadowColor,
    marginHorizontal: 10,

  }, separator: {
    flex: 1,
    borderWidth: 1,
    borderColor: res.colors.shadowColor,
    // alignItems: 'center',
    height: 22,
    justifyContent: 'center'
  }, separatorShadow: {
    flex: 1,

    //  alignItems: 'center',
    height: 22,
    justifyContent: 'center'
  }, shadowTab: {
    shadowColor: "black",
    shadowOffset: { width: 0, height: 4 },
    shadowRadius: 6,
    shadowOpacity: 0.2,
    elevation: 3,
  }, UnitLevel: {
    height: 110,
    marginHorizontal: 10,
    backgroundColor: res.colors.butterscotch,
    marginTop: 10,
    borderRadius: 5
  }, modalStyel: {
    flex: 1, backgroundColor: res.colors.butterscotch, zIndex: 99, borderTopLeftRadius: 30, borderTopRightRadius: 30, marginTop: 10
  }, nonModalStyle: {
    marginHorizontal: 15, backgroundColor: res.colors.butterscotch, zIndex: 99, borderTopLeftRadius: 30, borderTopRightRadius: 30, marginTop: 100
  }, slotNumberText: {
    marginTop: 31,
    fontFamily: res.fonts.medium,
    fontSize: 14,
    color: res.colors.greyishBrown,
  }, slotValueText: {
    fontFamily: res.fonts.bold,
    fontSize: 24,
    color: res.colors.peacockBlue,
  }, vastuText: {
    fontFamily: res.fonts.bold,
    fontSize: 10,
    color: res.colors.white,
    marginTop: 13
  }, viewMoreText: {
    fontWeight:'bold',
    color: res.colors.peacockBlue,
    fontFamily: res.fonts.semiBold,
    fontSize: 14
},


})
export default styles