import React, { Component } from 'react'
import { View, Text, Dimensions, TouchableOpacity } from 'react-native'
import Carousel, { Pagination } from 'react-native-snap-carousel';
import Modal from 'react-native-modal'

import { Amenitieslist, Project_Specifications, HomeLoanApprovedBy, ProjectOwnerShip } from './Project/ProjectData'
import styles from './styles'
import res from '../../../res'
import { VastuMeter, AmenitiesModal, ProjectSpecificationModal, ConsultantsModal, ProjectOwnerShipModal, ComplianceModal, HomeLoanApprovedByModal } from '../../components'
import Brochure from './Project/Brochure'
import SitePlan from './Project/SitePlan'
import moment from 'moment';
import Consultants from './Project/Consultants'
import Location from './Project/Location'
import Compliance from './Project/Compliance'
import FloorPlan from './Project/FloorPlan'
import { heightScale } from '../../utility/Utils';
class ProjectData extends Component {
    constructor(props) {
        super(props)
        this.state = {
            activeIndex: 0,
            isModalVisible: false,
            brochure: "",
            sitePlan: ""
        }
    }
    componentDidMount() {
        let brouchar = this.props.data.projectInfo && this.props.data.projectInfo.brochure ? this.props.data.projectInfo.brochure : []
        let sitePlan = this.props.data.projectInfo && this.props.data.projectInfo.sitePlan ? this.props.data.projectInfo.sitePlan : []
        brouchar.map((item, index) => {
            this.setState({
                brochure: item.fileName,

            })
        })
        sitePlan.map((item, index) => {
            this.setState({
                sitePlan: item.fileName
            })
        })
        // console.log("brouchar", brouchar, sitePlan)
    }
    toggleModal = () => {
        // console.log(this.state.isModalVisible)
        this.setState({ isModalVisible: !this.state.isModalVisible });

    };
    renderBrochure = () => {
        const Brocher = this.props.brochure
        if (!Brocher || Brocher.length == 0) {
            return (
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <Text>{res.strings.NO_DATA_FOUND}</Text>
                </View>
            )
        } else {
            return <Brochure
                brochure={Brocher}
                navigation={this.props.navigation}
            />
        }
    }
    renderSitePlan = () => {
        const sitePlan = this.props.sitePlan
        if (!sitePlan || sitePlan.length == 0) {
            return (
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <Text>{res.strings.NO_DATA_FOUND}</Text>
                </View>
            )
        } else {
            return <SitePlan
                sitePlan={sitePlan}
            />
        }

    }
    renderFloorPlan = () => {
        const floorPlans = this.props.floorPlans
        if (!floorPlans || floorPlans.length == 0) {
            return (
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <Text>{res.strings.NO_DATA_FOUND}</Text>
                </View>
            )
        } else {
            return <FloorPlan
                sitePlan={floorPlans}
            />
        }

    }
    renderHomeLoanApproved = () => {
        const data = this.props.data.projectInfo && this.props.data.projectInfo.homeLoanApprovedBy && this.props.data.projectInfo.homeLoanApprovedBy.length > 0 ? this.props.data.projectInfo.homeLoanApprovedBy : []
        console.log("renderHomeLoanApproved", data)
        if (!data||data.length==0) {
            return <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', }}>
                <Text>{res.strings.NO_DATA_FOUND}</Text>
            </View>
        }
        return <HomeLoanApprovedBy
            data={data}
        />
    }
    renderCorousalData = (item, index) => {
        const id = item.id
        switch (id) {
            case 0: return <Location
                projectLocation={this.props.projectLocation}
                projectInfo={this.props.projectInfo}
            />
            case 1: return this.renderSitePlan()
            case 2: return <Amenitieslist
                data={this.props.data.amenities && this.props.data.amenities.length > 0 && this.props.data.amenities ? this.props.data.amenities : []}
            />
            case 3: return <Project_Specifications
                data={this.props.data.projectSpecifications}
            />
            case 4: return <Consultants
                navigation={this.props.navigation}
                data={this.props.data.consultants}
            />
            case 5: return <Compliance
                documents={this.props.documents}
            />
            case 6: return this.renderHomeLoanApproved()
            case 7: return <ProjectOwnerShip
                projectOwnershipStructure={this.props.projectOwnershipStructure}
            />

            case 8: return this.renderBrochure()
            case 9: return this.renderFloorPlan()
            default: break
        }

    }
    renderData = ({ item, index }) => {
        // console.log("PROJECT ITEM", item)
        return <View style={{ flex: 1, backgroundColor: res.colors.butterscotch, borderRadius: 24, marginTop: 50, overflow: 'hidden' }} onPress={() => this.toggleModal()} activeOpacity={1}>

            <View style={[styles.shadowHeader, { height: 42 }]}>
                <Text style={styles.corousalNameText}>{item.name}</Text>
            </View>
            {this.renderCorousalData(item)}
            {this.viewMore(item)}
        </View>
    }
    renderViewMore = () => {
        return <View style={{ alignItems: 'center', height: 31, justifyContent: 'center' }}>

            <TouchableOpacity onPress={() => this.toggleModal()} activeOpacity={1}>

                <Text style={styles.viewMoreText}>View More</Text>
            </TouchableOpacity>
        </View>
    }
    viewMore = (item) => {
        const data = this.props.data.projectInfo && this.props.data.projectInfo.homeLoanApprovedBy && this.props.data.projectInfo.homeLoanApprovedBy.length > 0 ? this.props.data.projectInfo.homeLoanApprovedBy : []
        const id = item.id
        const { activeIndex, } = this.state
        switch (id) {

            case 2: return this.renderViewMore()
            case 3: return this.renderViewMore()
            case 4: return this.renderViewMore()
            case 5: return this.renderViewMore()

            case 6: if (!data) {
                return this.renderViewMore()
            } else return
            // case 8: return this.renderViewMore()
        }


    }


    modalView = () => {
        const { activeIndex } = this.state
        switch (activeIndex) {
            case 2: return <AmenitiesModal
                data={this.props.data.amenities && this.props.data.amenities.length > 0 && this.props.data.amenities ? this.props.data.amenities : []}
            />
            case 3: return <ProjectSpecificationModal
                data={this.props.data.projectSpecifications}
            />
            case 4: return <ConsultantsModal
                navigation={this.props.navigation}
                data={this.props.data.consultants}
            />
            case 5: return <ComplianceModal
                documents={this.props.documents}
            />
            case 6: return <HomeLoanApprovedByModal
                data={this.props.data.projectInfo && this.props.data.projectInfo.homeLoanApprovedBy && this.props.data.projectInfo.homeLoanApprovedBy.length > 0 ? this.props.data.projectInfo.homeLoanApprovedBy : []}
            />
            case 6: return <ProjectOwnerShipModal
                projectOwnershipStructure={this.props.projectOwnershipStructure}
            />

            default: break
        }


    }
    renderSquareFitItem = ({ item, index }) => {
        // console.log("renderSquareFitItem", item)
        return <View style={{ flexDirection: 'column', justifyContent: 'center', alignItems: 'center', marginLeft: 10, }}>
            <Text style={styles.squreHeading}>{item._id}</Text>
            <Text style={styles.squreSubHeading}>{item.unitName}</Text>
            <Text style={styles.squreSubHeading}>{Object.keys(item)}</Text>
            {/* <Text style={styles.squreHeading}>{item.balconyArea.value}+{item.balconyArea.uom}</Text> */}

        </View>
    }
    renderSqureFitArea = () => {
        const projectLaunchDate = this.props && this.props.data && this.props.data.projectInfo && this.props.data.projectInfo.projectLaunchDate ? this.props.data.projectInfo.projectLaunchDate : ""
        const projectEstimatedCompletionDate = this.props && this.props.data && this.props.data.projectInfo && this.props.data.projectInfo.projectEstimatedCompletionDate ? this.props.data.projectInfo.projectEstimatedCompletionDate : ""
        const sizeRange = this.props && this.props.data && this.props.data.projectInfo && this.props.data.projectInfo.sizeRange ? this.props.data.projectInfo.sizeRange : ""
        const landArea = this.props && this.props.data && this.props.data.projectInfo && this.props.data.projectInfo.landArea ? this.props.data.projectInfo.landArea : ""
        console.log("landArea", landArea)

        return <View style={styles.squreFitView}>
            <View style={{ alignItems: 'center' }}>
                <Text style={styles.squreHeading}>{moment(projectLaunchDate).format("MMMM' YYYY")}</Text>

                <Text style={styles.squreSubHeading}>Project Launch Date</Text>
            </View>
            <View style={{ alignItems: 'center' }}>
                <Text style={styles.squreHeading}>{projectEstimatedCompletionDate ? moment(projectEstimatedCompletionDate).format("MMMM' YYYY") : "NA"}</Text>

                <Text style={styles.squreSubHeading}>Completion Date</Text>
            </View>
            <View style={{ alignItems: 'center' }}>
                <Text style={styles.squreHeading}>{Math.round(landArea.value)} {landArea.uom}</Text>

                <Text style={styles.squreSubHeading}>Land Area</Text>
            </View>
            {/* <View style={{ alignItems: 'center' }}>
                <Text style={styles.squreHeading}>Size Range</Text>
                <Text style={styles.squreSubHeading}>{sizeRange}</Text>
            </View> */}
        </View >
    }
    renderTopText = () => {
        console.log("LIFESTYLEINDEX", this.props)
        return <View style={styles.firstTitle}>
            <View style={{ width: '70%' }}>
                <Text style={styles.districtText}>{this.props.data.projectInfo.projectName}</Text>
                <Text style={styles.addressText}>{"RERA Registration No.\n" + this.props.data.projectInfo.reraRegNo + ""}</Text>
            </View>
            <View style={styles.lineView}>

            </View>
            <View style={{ width: '30%', marginLeft: 30, alignItems: 'center' }}>
                <VastuMeter
                    showText={(this.props.lifeStyleIndex.lifeStyleIndex ? Math.round(this.props.lifeStyleIndex.lifeStyleIndex) : 0) + "/10"}
                    percent={this.props.lifeStyleIndex.lifeStyleIndex ? Math.round(this.props.lifeStyleIndex.lifeStyleIndex) : 0}
                    radius={27}
                    bgRingWidth={5}
                    progressRingWidth={5}
                    ringColor={res.colors.pinkishTan}
                    ringBgColor={'grey'}
                    textFontSize={10}
                    textFontColor={res.colors.pinkishTan}
                    textFontWeight={'normal'}
                    clockwise={true}
                    bgColor={'white'}
                    startDegrees={0}
                >
                    {/* <Text>9/10</Text> */}
                </VastuMeter>

                <Text style={styles.vastuText}>Lifestyle Index</Text>
            </View>
        </View>
    }

    render() {
        // console.log("PROJECT", this.props,)
        const { isLoading } = this.state
        return (
            <View style={styles.unitFullScreen}>
                <Modal style={{ marginTop: 100, borderWidth: 10 }} isVisible={this.state.isModalVisible} onBackdropPress={() => this.toggleModal()}
                    style={styles.view}>
                    {this.modalView()}
                </Modal>
                {this.renderTopText()}
                {this.renderSqureFitArea()}
                <View style={{ flex: 0.95 }}>
                    <Carousel
                        data={this.props.data.projectCarouselItems}
                        renderItem={this.renderData}
                        sliderWidth={Dimensions.get('window').width}
                        itemWidth={Dimensions.get('window').width - 80}
                        layout={'default'}
                        containerCustomStyle={styles.carouselData}
                        removeClippedSubviews={true}
                        onSnapToItem={index => this.setState({ activeIndex: index })}
                        firstItem={this.state.activeIndex}
                    />
                    <View style={{ height: 20, width: "10%", alignSelf: 'center' }}>
                        <Pagination
                            dotsLength={this.props.data.projectCarouselItems.length}
                            dotStyle={styles.dotStyle}
                            dotColor={styles.dotColor}
                            inactiveDotStyle={styles.inactiveDotStyle}
                            activeDotIndex={this.state.activeIndex}
                            inactiveDotOpacity={0.4}
                            inactiveDotScale={0.6}
                            activeOpacity={1}
                        />
                    </View>


                </View>

            </View>
        )
    }

}
export default ProjectData