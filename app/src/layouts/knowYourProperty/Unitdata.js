import React, { Component } from 'react'
import { View, Text, Dimensions, TouchableOpacity, TextInput, Image, ImageBackground } from 'react-native'
import Carousel, { Pagination } from 'react-native-snap-carousel';
import Modal from 'react-native-modal'

import styles from './styles'
import res from '../../../res'
import { VastuMeter, Loader, UnitFloorModal, KnowYourUnitModal, UnitLevelSpecificationModal, ParkingSlotModal, VastuModal } from '../../components'
import KnowYourUnit from './unit/KnowYourUnit'
import UnitFloorPlan from './unit/UnitFloorPlan'
import ParkingSlot from './unit/ParkingSlot'
import UnitLevelSpecification from './unit/UnitLevelSpecification'
import { myWidth, myHeight, heightScale } from '../../utility/Utils';
class Unitdata extends Component {
    constructor(props) {
        super(props)
        this.modalRef = React.createRef();
        this.state = {

            isLoading: false,
            activeIndex: 0,
            isModalVisible: false,
            dimensionsData: [],
            isVastu: false

        }
    }

    renderLoader = () => {
        return (
            <Loader
                loading={this.state.isLoading} />
        );
    };
    renderUnitFllorPlan = () => {
        if (!this.props.data.unitDimensions[0]) {
            return <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                <Text>{res.strings.NO_DATA_FOUND}</Text>
                </View>
        }
        return <UnitFloorPlan
            modal={false}
            image={this.props.image}
            data={this.props.data.unitDimensions[0] && this.props.data.unitDimensions[0].dimensionsData.length > 0 && this.props.data.unitDimensions[0].dimensionsData ? this.props.data.unitDimensions[0].dimensionsData : []}
        />

    }
    renderCorousalData = (item, index) => {
        const id = item.id
        switch (id) {
            case 0: return <KnowYourUnit
                parkingSlotValue={this.props.parkingSlotValue}
                refreshData={this.props.refreshData}
                totalNoOfTowers={this.props.totalNoOfTowers}
                noOfBalconies={this.props.noOfBalconies}
                data={this.props.data && this.props.data.listOfTowers && this.props.data.listOfTowers ? this.props.data.listOfTowers : {}}
            />
                break;
            case 1: return this.renderUnitFllorPlan()
                break;

            case 2: return <UnitLevelSpecification
                data={this.props && this.props.data && this.props.data.unitLevelSpecification && this.props.data.unitLevelSpecification ? this.props.data.unitLevelSpecification : {}}
            />
            // case 3: return <ParkingSlot
            //     parkingSlotValue={this.props.parkingSlot}
            //     parkingImageCollection={this.props.data && this.props.data.projectInfo && this.props.data.projectInfo.parkingPlan ? this.props.data.projectInfo.parkingPlan : []}
            //     parkingImage={this.props.parkingImage}
            //     data={this.props.data && this.props.data.listOfTowers && this.props.data.listOfTowers ? this.props.data.listOfTowers : {}}
            // />
            // break;
            default: return
        }

    }
    renderData = ({ item, index }) => {
        // console.log("renderData", item)
        return <View style={{ flex: 1, backgroundColor: res.colors.butterscotch, borderRadius: 24, marginTop: 50, width: 306 }} >

            <View style={[styles.shadowHeader, { height: 42 }]}>
                <Text style={styles.corousalNameText}>{item.name}</Text>
            </View>
            {this.renderCorousalData(item)}

            {this.viewMore(item)}
        </View>


    }
    viewMore = (item) => {

        const id = item.id
        switch (id) {
            case 0:
                return <View style={{ alignItems: 'center', height: 31, justifyContent: 'center' }}>
                    <TouchableOpacity onPress={() => this.toggleModal(false)} activeOpacity={1}>

                        <Text style={styles.viewMoreText}>View More</Text>
                    </TouchableOpacity>
                </View>
            case 1:if (!this.props.data.unitDimensions[0]) {
                return null
            }
                return <View style={{ alignItems: 'center', height: 31, justifyContent: 'center' }}>
                    <TouchableOpacity onPress={() => this.toggleModal(false)} activeOpacity={1}>

                        <Text style={styles.viewMoreText}>View More</Text>
                    </TouchableOpacity>
                </View>
            case 2: return <View style={{ alignItems: 'center', height: 31, justifyContent: 'center' }}>
                <TouchableOpacity onPress={() => this.toggleModal(false)} activeOpacity={1}>

                    <Text style={styles.viewMoreText}>View More</Text>
                </TouchableOpacity>
            </View>

        }
    }


    renderTextinput = () => {
        return <View style={{ flexDirection: 'row', width: '100%', alignItems: 'center' }}>
            <TouchableOpacity onPress={() => this.markCheck()}>
                {this.state.isChecked ? <View style={{ width: 18, height: 18, alignItems: 'center', justifyContent: 'center', borderRadius: 3 }}><Image source={res.images.isChecked} /></View> : <View style={{ width: 18, height: 18, borderColor: res.colors.greyBlack, borderWidth: 1, borderRadius: 3 }}></View>}
            </TouchableOpacity>
            <Text style={{ marginLeft: 10, color: res.colors.greyBlack }}>01</Text>
            <TextInput
                value={this.state.addCarParking}
                placeholder={res.strings.ADD_CAR_PARKING}
                onChangeText={this.onChangeCarParking}
                style={styles.textInputCheckStyle}
                inputProps={{
                    keyboardType: 'default',
                    maxLength: 20,
                    returnKeyType: "next",
                    textAlign: "center",
                }}
                inputStyles={{ borderBottomWidth: 0, width: 40, paddingVertical: 5, fontSize: 18, }}
            />
        </View>

    }


    toggleModal = (isVastu) => {
        const { isModalVisible } = this.state
        if (isVastu) {
            if (!this.props.data.vastuDetails || !this.props.data.vastu || Object.keys(this.props.data.vastu).length == 0 || Object.keys(this.props.data.vastuDetails).length == 0) {
                return
            }
        }
        isVastu ?
            this.setState({ isModalVisible: !isModalVisible, isVastu: true }) : this.setState({ isModalVisible: !isModalVisible, isVastu: false })

    };

    onCancelModalPress = (text) => {
        this.modalRef.current.hide();
    }
    closeRenderModal = () => {
        this.modalRef.current.hide();
    };

    renderUnitFloorModal = () => {
        if (!this.props.data.unitDimensions[0]) {
            return null
        }
        return (
            <UnitFloorModal
                image={this.props.image}
                list={this.props.data.unitDimensions[0] && this.props.data.unitDimensions[0].dimensionsData.length > 0 && this.props.data.unitDimensions[0].dimensionsData ? this.props.data.unitDimensions[0].dimensionsData : []}

            />)
    }
    modalView = () => {
        const { activeIndex, isVastu } = this.state
        console.log("ISVASTU", this.props.data.vastu)
        if (isVastu) {
            if (!this.props.data.vastuDetails || !this.props.data.vastu || Object.keys(this.props.data.vastu).length == 0) {
                return null
            } else {
                return <VastuModal
                    vastuDetails={this.props.data.vastuDetails}
                    vastu={this.props.data.vastu}
                    vastuMinScore={this.props.vastuMinScore}
                    vastuMaxScore={this.props.vastuMaxScore}
                    vastuRange={this.props.vastuRange}
                    vastuScore={this.props.vastuScore}
                />
            }

        } else {
            switch (activeIndex) {
                case 0: return <KnowYourUnitModal
                    noOfBalconies={this.props.noOfBalconies}
                    refreshData={this.props.refreshData}
                    toggleModal={this.toggleModal}
                    parkingSlotValue={this.props.parkingSlotValue}
                    data={this.props.data && this.props.data.listOfTowers && this.props.data.listOfTowers ? this.props.data.listOfTowers : {}}
                />
                case 1: return this.renderUnitFloorModal()

                case 2: return <UnitLevelSpecificationModal
                    data={this.props.data && this.props.data.unitLevelSpecification && this.props.data.unitLevelSpecification ? this.props.data.unitLevelSpecification : {}}
                />
                // case 3: return <ParkingSlotModal
                //     parkingImage={this.props.parkingImage}
                //     data={this.props.data && this.props.data.listOfTowers && this.props.data.listOfTowers ? this.props.data.listOfTowers : {}}


                // />
                default: return <VastuModal />
            }
        }


    }

    renderTopText = () => {
        const projectName = this.props.data && this.props.data.projectInfo && this.props.data.projectInfo.projectName ? this.props.data.projectInfo.projectName : ""
        console.log("this.props.data.vastuScore", this.props)
        return <View style={styles.firstTitle}>
            <View style={{ width: '70%',paddingRight:15}}>
                <Text numberOfLines={2} style={styles.districtText}>{projectName}</Text>
                <Text style={styles.addressText} numberOfLines={3} >{this.props.address.address ? this.props.address.address : ""}</Text>
            </View>
            <View style={styles.lineView}>

            </View>
            <View style={{ width: '29%', marginLeft: 30, justifyContent: 'center',alignItems:'center' }}>
                <TouchableOpacity onPress={() => this.toggleModal(true)}>
                    <VastuMeter
                        showText={(this.props.data.vastuScore ? this.props.data.vastuScore : 0) + "%"}
                        percent={this.props.data.vastuScore ? parseInt(this.props.data.vastuScore) : 0}
                        radius={27}
                        bgRingWidth={5}
                        progressRingWidth={5}
                        ringColor={res.colors.pinkishTan}
                        ringBgColor={'grey'}
                        textFontSize={10}
                        textFontColor={res.colors.pinkishTan}
                        textFontWeight={'normal'}
                        clockwise={true}
                        bgColor={'white'}
                        startDegrees={0}
                    />
                </TouchableOpacity>

                <Text style={styles.vastuText}>Vaastu Score</Text>
                {/* <View style={{backgroundColor:'red'}}>
                    {
                        this.props.parkingSlotValue.map((item, index) => {
                            
                            <Text style={styles.vastuText} > {item},</Text>
                        })
                    }
                </View>  */}



            </View>
        </View >
    }

    renderSqureFitArea = () => {
        // console.log("renderSqureFitArea", this.props.data)
        return <View style={styles.squreFitView}>
            <View style={{ alignItems: 'center' }}>
                <Text style={styles.squreHeading}>{Math.round(this.props.data.sellableArea)} sq ft</Text>
                <Text style={styles.squreSubHeading}>Saleable Area</Text>
            </View>
            <View style={{ alignItems: 'center' }}>
                <Text style={styles.squreHeading}>{Math.round(this.props.data.ReraCarpet)} sq ft</Text>
                <Text style={styles.squreSubHeading}>RERA Carpet Area</Text>
            </View>
            <View style={{ alignItems: 'center' }}>
                <Text style={styles.squreHeading}>{Math.round(this.props.data.balcony)} sq ft</Text>
                <Text style={styles.squreSubHeading}>Balcony</Text>
            </View>
            <View style={{ alignItems: 'center' }}>
                <Text style={styles.squreHeading}>{Math.round(this.props.data.UDSAres)} sq ft</Text>
                <Text style={styles.squreSubHeading}>UDS Area</Text>
            </View>
        </View >
    }
    render() {
        console.log("UnitDATA", this.props)
        return (
            <View style={styles.unitFullScreen}>
                {/* <ImageBackground source={res.images.mainBackground} style={{ width:myWidth,height:"100%" }}> */}
                <Modal isVisible={this.state.isModalVisible}
                    // swipeThreshold={100}
                    // onSwipeComplete={() => this.toggleModal()}
                    // swipeDirection={'down'}
                    onBackdropPress={() => this.toggleModal()}
                    style={styles.view}>
                    {this.props ? this.modalView() : <View />}
                </Modal>
                {this.renderTopText()}
                {this.renderSqureFitArea()}
                <View style={{ flex: 0.95 }}>
                    <Carousel
                        data={this.props.data.array1}
                        renderItem={this.renderData}
                        sliderWidth={Dimensions.get('window').width}
                        itemWidth={306}
                        layout={'default'}
                        containerCustomStyle={styles.carouselData}
                        removeClippedSubviews={true}
                        onSnapToItem={index => this.setState({ activeIndex: index })}
                        firstItem={this.state.activeIndex}
                    />
                    <View style={{ height: 20, width: "10%", alignSelf: 'center' }}>
                        <Pagination
                            dotsLength={this.props.data.array1.length}
                            dotStyle={styles.dotStyle}
                            dotColor={styles.dotColor}
                            inactiveDotStyle={styles.inactiveDotStyle}
                            activeDotIndex={this.state.activeIndex}
                            inactiveDotOpacity={0.4}
                            inactiveDotScale={0.6}
                            activeOpacity={1}
                        />
                    </View>


                </View>


                {/* </ImageBackground> */}





            </View>


        )
    }

}
export default Unitdata