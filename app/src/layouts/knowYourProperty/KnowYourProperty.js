import React, { Component } from 'react'
import { View, Text, Image, SafeAreaView, ImageBackground } from 'react-native'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { CommonActions } from '@react-navigation/native';

import styles from './styles'
import HeaderWithTitle from '../../components/header/HeaderWithTitle'
import resources from '../../../res'
import { TabView, Loader } from '../../components'
import ApiFetch from '../../apiManager/ApiFetch';
import { MY_UNIT_ENDPOINTS } from '../../apiManager/ApiEndpoints';

import Unitdata from './Unitdata';
import ProjectData from './ProjectData'
import DeveloperData from './developer/DeveloperData'

class KnowYourProperty extends Component {
    constructor(props) {
        super(props)
        this.navigation = this.props.route.params && this.props.route.params.navigation ? this.props.route.params.navigation : null

        this.state = {
            selectedIndex: 0,
            isChecked: false,
            addCarParking: "",
            isModalVisible: false,
            isLoading: false,
            activeIndex: 1,
            unitData: [],
            vastuScore: "",
            array1: [
                {
                    "name": "Know Your Unit",
                    "id": 0
                }, {
                    "name": "Unit Floor Plan",
                    "id": 1
                }, {
                    "name": "Unit Level Specification",
                    "id": 2
                }
            ],
            cellList: [{
                image: resources.images.icn_contract,
                number: "123",
                cell: "Unit"
            }, {
                image: resources.images.icn_contract,
                number: "456",
                cell: "Project"
            }, {
                image: resources.images.icn_contract,
                number: "678",
                cell: "Developer"
            }],
            carouselItems: [
                {
                    title: "Unit Floor Plan",

                },
                {
                    title: "Know Your Unit",

                },
                {
                    title: "Unit Level Specification",

                },
            ],
            projectCarouselItems: [
                {
                    name: "Location",
                    "id": 0

                }, {
                    name: "Site Plan ",
                    "id": 1

                }, {
                    name: "Amenities",
                    "id": 2

                }, {
                    name: "Project Specifications ",
                    "id": 3

                }, {
                    name: "Consultants & Contractors",
                    "id": 4

                }, {
                    name: "Compliance",
                    "id": 5

                }, {
                    name: "Project Approved By",
                    "id": 6

                }, 
                // {
                //     name: "Project Ownership Structure",
                //     "id": 7

                // }, 
                {
                    name: "Brochure",
                    "id": 8

                }, {
                    name: "Floor Plan",
                    "id": 9

                }
            ],
            developerCarouselItems: [{
                name: "Profile",
                "id": 1

            }, {
                name: "Projects",
                "id": 2

            },

            // {
            //     name: "Association Memberships",
            //     "id": 3

            // },
            {
                name: "Partners and Locations",
                "id": 4

            },
                // {
                //     name: "Index Of Changes",
                //     "id": 5

                // },
            ],
            amenities: [],
            consultants: [],
            developerID: [],
            documents: [],
            unitCategoriesBySize: [],
            unitDimensions: [],
            dimentions: [],
            unitLevelSpecification: {},
            listOfTowers: {},
            projectInfo: {},
            projectSpecifications: {},
            unitFloorImage: "",
            address: "",
            image: "",
            parkingImage: "",
            sellableArea: "",
            ReraCarpet: "",
            balcony: "",
            UDSAres: "",
            projectName: "",
            noOfBalconies: "",
            parkingSlot: [],
            totalArea: "",
            parkingSlotValue: "",
            projectOwnershipStructure: {},
            Brochure: [],
            sitePlan: [],
            developerData: {},
            indexOfChanges: [],
            developerScore: "",
            vastuDetails: {},
            vastu: {},
            floorPlans: [],
            registeredAddress: "",
            developerLogo: "",
            website: "",
            reraComplaints: 0,
            awardsDetails: 0,
            weightedAvrg: "",
            maxScore: "",
            developerDetailedScore: {},
            developerDetailedWeight: {},
            developerDetailedScoreParameters: {},
            vastuMinScore: "",
            vastuMaxScore: "",
            vastuRange: "",
            projectLocation: "",
            lifeStyleIndex: 0,
            noOfYrsInBusiness: 0,
            projectsDelivered: 0,
            unitsDelivered: 0,
            developerName:""
        }
    }
    componentDidMount() {
        this.hitKnowYourPropertyApi()
    }

    hitKnowYourPropertyApi = () => {
        this.setState({
            isLoading: true
        })
        ApiFetch.fetchGet(MY_UNIT_ENDPOINTS)
            .then((res) => {
                // console.log("MY_UNIT_ENDPOINTS", res);
                this.setState({
                    vastuScore: res.data.vastuScore,
                    amenities: res && res.data && res.data.amenities ? res.data.amenities : [],
                    consultants: res && res.data && res.data.consultants ? res.data.consultants : [],
                    developerID: res && res.data && res.data.developerID ? res.data.developerID : [],
                    documents: res && res.data && res.data.documents ? res.data.documents : [],
                    unitCategoriesBySize: res && res.data && res.data.unitCategoriesBySize ? res.data.unitCategoriesBySize : [],
                    unitDimensions: res && res.data && res.data.unitDimensions ? res.data.unitDimensions : [],
                    unitLevelSpecification: res && res.data && res.data.unitSpecifications ? res.data.unitSpecifications : {},
                    listOfTowers: res && res.data && res.data.listOfTowers ? res.data.listOfTowers : {},
                    projectInfo: res && res.data && res.data.projectInfo ? res.data.projectInfo : {},
                    projectSpecifications: res.data && res.data.projectSpecifications ? res.data.projectSpecifications : {},
                    parkingSlot: res && res.data && res.data.listOfTowers && res.data.listOfTowers.floorsList && res.data.listOfTowers.floorsList.unitsData && res.data.listOfTowers.floorsList.unitsData.carParking && res.data.listOfTowers.floorsList.unitsData.carParking.slotNumbers ? res.data.listOfTowers.floorsList.unitsData.carParking.slotNumbers : [],
                    projectOwnershipStructure: res && res.data && res.data.projectOwnershipStructure ? res.data.projectOwnershipStructure : {},
                    brochure: res && res.data && res.data.projectInfo && res.data.projectInfo.brochure ? res.data.projectInfo.brochure : [],
                    sitePlan: res && res.data && res.data.projectInfo && res.data.projectInfo.sitePlan ? res.data.projectInfo.sitePlan : [],
                    floorPlans: res && res.data && res.data.projectInfo && res.data.projectInfo.floorPlans ? res.data.projectInfo.floorPlans : [],
                    developerData: res && res.data && res.data.developerID ? res.data.developerID : [],
                    developerScore: res && res.data && res.data.developerScore ? res.data.developerScore : "",
                    vastuDetails: res && res.data && res.data.vastuDetails ? res.data.vastuDetails : {},
                    vastu: res && res.data && res.data.vastu ? res.data.vastu : {},
                    developerDetailedScore: res.data && res.data.developerDetailedScore ? res.data.developerDetailedScore : {},
                    developerDetailedWeight: res.data && res.data.developerDetailedWeight ? res.data.developerDetailedWeight : {},
                    developerDetailedScoreParameters: res.data && res.data.developerDetailedScoreParameters ? res.data.developerDetailedScoreParameters : {},
                    maxScore: res.data && res.data.maxScore ? res.data.maxScore : "",
                    weightedAvrg: res.data && res.data.weightedAvrg ? res.data.weightedAvrg : "",
                    vastuMinScore: res.data && res.data.vastuMinScore ? res.data.vastuMinScore : "",
                    vastuMaxScore: res.data && res.data.vastuMaxScore ? res.data.vastuMaxScore : "",
                    vastuRange: res.data && res.data.vastuRange ? res.data.vastuRange : "",
                    projectLocation: res.data && res.data.projectInfo && res.data.projectInfo.projectLocation ? res.data.projectInfo.projectLocation : "",
                    lifeStyleIndex: res.data && res.data.lifeStyleIndex ? res.data.lifeStyleIndex : 0,
                    isLoading: false,

                }, () => {
                    this.state.unitCategoriesBySize.map((item, index) => {
                        // console.log("IMAGE COmponent did mount", item)
                        this.setState({
                            image: item.unitPlan,
                            sellableArea: item.size.value ? item.size.value : 0,
                            ReraCarpet: item.reraCarpetArea.value ? item.reraCarpetArea.value : 0,
                            balcony: item.balconyArea.value ? item.balconyArea.value : 0,
                            UDSAres: item.undividendShareOfLand.value ? item.undividendShareOfLand.value : 0,
                            noOfBalconies: item.noOfBalconies ? item.noOfBalconies : 0
                        })
                    })
                    this.state.developerID.map((item, index) => {

                        this.setState({
                            address: item.corporateAddress,
                            developerLogo: item.logo,
                            website: item.website,
                            registeredAddress: item.registeredAddress,
                            reraComplaints: item.reraComplaints,
                            awardsDetails: item.awardsDetails.length,
                            noOfYrsInBusiness: item.noOfYrsInBusiness,
                            projectsDelivered: item.projectsDelivered,
                            unitsDelivered: item.unitsDelivered,
                            developerName:item.developerName

                        })
                    })
                    this.state.projectInfo.parkingPlan.map((item, index) => {

                        this.setState({
                            parkingImage: item.fileName
                        })
                    })
                    this.state.parkingSlot.map((item, index) => {
                        // console.log("parkingSlotITEM", item[0])
                        this.setState({
                            parkingSlotValue: item
                        })
                    })
                    this.state.developerID.map((item, index) => {
                        // console.log("developerIDindexOfChanges", item)
                        this.setState({
                            indexOfChanges: item.indexOfCharges
                        })
                    })

                })
            }).catch((err) => {
                this.setState({
                    isLoading: false
                })
                console.log(err)
            })
    }
    renderLoader = () => {
        return (
            <Loader
                loading={this.state.isLoading} />
        );
    };
    renderTabView = () => {
        const { cellList } = this.state
        return <TabView
            listDataItem={cellList}
            selectedIndex={this.selectedIndex}
            tabBarUnderlineStyle={{ backgroundColor: resources.colors.pinkishTan, height: 2 }}
            tabBarInactiveTextColor={resources.colors.white}
            tabBarActiveTextColor={resources.colors.pinkishTan}

        />
    }
    selectedIndex = (index) => {
        this.setState({
            selectedIndex: index
        })
    }



    _renderItem = ({ item, index }) => {

        return (
            <View style={[styles.carouselItemContainer,]}>
                <Text style={styles.titleText}>{item.title}</Text>

            </View>
        );
    }


    loadUnitFloorImage = () => {
        // console.log("IMAGE", this.props.data.unitCategoriesBySize)
        this.state.unitCategoriesBySize.map((item, index) => {
            // console.log("IMAGE", item)
            this.setState({
                image: item.unitPlan
            })
        })
    }
    renderCarouselView = () => {
        const { selectedIndex } = this.state
        const totalNoOfTowers = this.state.projectInfo && this.state.projectInfo.totalNoOfTowers ? this.state.projectInfo.totalNoOfTowers : ""
        if (selectedIndex == 0) {
            return <Unitdata
                parkingSlot={this.state.parkingSlotValue}
                totalNoOfTowers={totalNoOfTowers}
                projectName={this.state.projectName}
                parkingImage={this.state.parkingImage}
                address={this.state.projectInfo}
                noOfBalconies={this.state.noOfBalconies}
                image={this.state.image}
                data={this.state}
                loadImage={this.loadUnitFloorImage}
                refreshData={this.hitKnowYourPropertyApi}
                parkingSlotValue={this.state.parkingSlot}
                vastuMinScore={this.state.vastuMinScore}
                vastuMaxScore={this.state.vastuMaxScore}
                vastuRange={this.state.vastuRange}
                vastuScore={this.state.vastuScore}
            />
        } else if (selectedIndex == 1) {
            return <ProjectData
                projectLocation={this.state.projectLocation}
                projectInfo={this.state.projectInfo}
                documents={this.state.documents}
                navigation={this.props.navigation}
                sitePlan={this.state.sitePlan}
                brochure={this.state.brochure}
                projectOwnershipStructure={this.state.projectOwnershipStructure}
                data={this.state}
                floorPlans={this.state.floorPlans}
                lifeStyleIndex={this.state.lifeStyleIndex}
            />
        } else if (selectedIndex == 2) {
            return <DeveloperData
                developerLogo={this.state.developerLogo}
                indexOfChanges={this.state.indexOfChanges}
                data={this.state}
                website={this.state.website}
                navigation={this.props.navigation}
                registeredAddress={this.state.registeredAddress}
                reraComplaints={this.state.reraComplaints}
                awardsDetails={this.state.awardsDetails}
                developerDetailedScore={this.state.developerDetailedScore}
                developerDetailedWeight={this.state.developerDetailedWeight}
                developerDetailedScoreParameters={this.state.developerDetailedScoreParameters}
                maxScore={this.state.maxScore}
                weightedAvrg={this.state.weightedAvrg}
                noOfYrsInBusiness={this.state.noOfYrsInBusiness}
                projectsDelivered={this.state.projectsDelivered}
                unitsDelivered={this.state.unitsDelivered}
                developerName={this.state.developerName}
            />
        }

    }

    headerModalTitle = () => {
        const { activeIndex } = this.state

        if (activeIndex == 0) {
            return <Text style={styles.modalHeaderText}>Know Your Floor Plan</Text>
        } else if (activeIndex == 1) {
            return <Text style={styles.modalHeaderText}>Know Your Unit</Text>
        } else {
            return <Text style={styles.modalHeaderText}>Unit level Specification</Text>
        }


    }
    renderFullData = ({ item, index }) => {
        return <View style={[{ flex: 1, margin: 5, borderRadius: 12, }]} >
            <View style={{ flex: 1, margin: 5, height: 80, alignItems: 'center', justifyContent: 'center', flexDirection: 'row' }}>
                <Image style={{ width: 33, height: 40 }} source={item.image} />
                <View style={{ flexDirection: 'column', marginLeft: 10 }}>
                    <Text style={styles.unitNumber}>
                        {item.number}
                    </Text>
                    <Text style={styles.unitText}>
                        {item.cell}
                    </Text>
                </View>

            </View>

        </View>
    }
    onChangeCarParking = (text) => {
        this.setState({
            addCarParking: text
        })
    }
    markCheck = () => {
        const { isChecked } = this.state
        this.setState({
            isChecked: !isChecked
        })
    }


    toggleModal = () => {
        // console.log("Main", this.state.isModalVisible)
        this.setState({ isModalVisible: !this.state.isModalVisible });
    };
    openDrawer = () => {

        this.props.navigation.openDrawer()
        const resetAction = CommonActions.reset({
            index: 0,
            routes: [{ name: 'KnowYourProperty' }],
        });
        this.props.navigation.dispatch(resetAction);
    }
    renderTab = () => {
        return <View style={{ overflow: 'hidden', paddingBottom: 5 }}>
            <View
                style={{
                    backgroundColor: resources.colors.appColor,

                    height: 50,
                    shadowColor: '#000',
                    shadowOffset: { width: 1, height: 1 },
                    shadowOpacity: 0.4,
                    shadowRadius: 3,
                    elevation: 5,
                }}
            >
                {this.renderTabView()}
            </View>
        </View>
    }
    openSupport = () => {
        this.props.navigation.navigate("SupportBaseScreen")
    }
    render() {
        const { isLoading } = this.state
        // console.log("know your property state", this.props.navigation)
        return (
            <SafeAreaView style={styles.fullScreen}>
                <HeaderWithTitle
                    openSupport={() => this.openSupport()}
                    isSupportVisibe={true}
                    isBackIconVisible={false}
                    openDrawer={() => this.openDrawer()}
                    styleHeaderContainer={{ backgroundColor: resources.colors.appColor }}
                    headerTitle={resources.strings.KNOW_YOUR_PROPERTY}
                />


                <ImageBackground source={resources.images.mainBackground} style={{ flex: 1 }}>


                    {this.renderTab()}
                    {/* <KeyboardAwareScrollView con={{ backgroundColor: 'red' }}> */}

                    {this.renderCarouselView()}

                    {/* </KeyboardAwareScrollView> */}
                </ImageBackground>



                {isLoading && this.renderLoader()}


            </SafeAreaView>
        )
    }

}
export default KnowYourProperty