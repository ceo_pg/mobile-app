import React, { Component } from 'react'
import { View, Text, Image, TouchableOpacity, SafeAreaView } from 'react-native'
import styles from './styles'
import res from '../../../../res'
import Modal from 'react-native-modal'
import KnowYourUnitModal from '../../../components/modal/KnowYourProperty/Unit/KnowYourUnitModal'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import ImageWithTitle from '../../../components/card/ImageWithTitle'
import { heightScale } from '../../../utility/Utils'

class KnowYourUnit extends Component {
    constructor(props) {
        super(props)

        this.state = {
            isModalVisible: false
        }

    }
    modalView = () => {
        return <KnowYourUnitModal
            toggleModal={this.toggleModal}
            totalNoOfTowers={this.props.totalNoOfTowers}
            noOfBalconies={this.props.noOfBalconies}
            data={this.props.data}
            refreshData={this.props.refreshData}
            parkingSlotValue={this.props.parkingSlotValue}

        />
    }
    toggleModal = () => {
        const { isModalVisible } = this.state
        this.setState({ isModalVisible: !this.state.isModalVisible });


    };
    render() {
        console.log("noOfBalconiesnoOfBalconies", this.props)
        const unitType = this.props.data.floorsList && this.props.data.floorsList.unitsData && this.props.data.floorsList.unitsData.unitType ? this.props.data.floorsList.unitsData.unitType : ""
        const facing = this.props.data.floorsList && this.props.data.floorsList.unitsData && this.props.data.floorsList.unitsData.unitDirectionFacing ? this.props.data.floorsList.unitsData.unitDirectionFacing : ""
        const parking = this.props.data.floorsList && this.props.data.floorsList.unitsData && this.props.data.floorsList.unitsData.carParking && this.props.data.floorsList.unitsData.carParking.noOfSlots ? this.props.data.floorsList.unitsData.carParking.noOfSlots : ""
        const unitName = this.props.data.floorsList && this.props.data.floorsList.unitsData && this.props.data.floorsList.unitsData.unitName ? this.props.data.floorsList.unitsData.unitName : ""
        const floor = this.props.data.floorsList && this.props.data.floorsList.nameOfFloor && this.props.data.floorsList.nameOfFloor ? this.props.data.floorsList.nameOfFloor : ""
        const nameOfTower = this.props.data.nameOfTower ? this.props.data.nameOfTower : ""
        return (< SafeAreaView style={[styles.modalStyel, { overflow: 'hidden' }]} >
            <KeyboardAwareScrollView keyboardShouldPersistTaps={true} >
                <Modal isVisible={this.state.isModalVisible}
                    // onSwipeComplete={() => this.toggleModal()}
                    // swipeDirection={'down'}
                    onBackdropPress={() => this.toggleModal()}
                    style={styles.view}>
                    {this.modalView()}
                </Modal>


                <View style={
                    [styles.container]
                }>


                    <ImageWithTitle
                        isShadow={true}
                        isdoubleCard={true}
                        src={res.images.icnContract}
                        Value={unitName ? unitName : "0"}
                        Heading={"Unit No"}
                        valueStyle={styles.unitNumber}
                        headingStyle={styles.unitText}
                    />
                    <ImageWithTitle
                        isShadow={true}
                        isdoubleCard={true}
                        src={res.images.icnTower}
                        Value={nameOfTower ? nameOfTower : "NA"}
                        Heading={"Tower Number"}
                        valueStyle={styles.unitNumber}
                        headingStyle={styles.unitText}
                    />


                    <ImageWithTitle
                        isShadow={true}
                        isdoubleCard={true}
                        src={res.images.icnHome}
                        Value={floor ? floor : "0"}
                        Heading={"Floor"}
                        valueStyle={styles.unitNumber}
                        headingStyle={styles.unitText}
                    />
                    <ImageWithTitle
                        isShadow={true}
                        isdoubleCard={true}
                        src={res.images.icnRealEstate}
                        Value={unitType ? unitType : "0"}
                        Heading={"Unit Type"}
                        valueStyle={styles.facingText}
                        headingStyle={styles.unitText}
                    />


                    <ImageWithTitle
                        isShadow={true}
                        isdoubleCard={true}
                        src={res.images.icnUrban}
                        Value={this.props.noOfBalconies ? this.props.noOfBalconies : "0"}
                        Heading={"Balconies"}
                        valueStyle={styles.unitNumber}
                        headingStyle={styles.unitText}
                    />
                    <ImageWithTitle
                        isShadow={true}
                        isdoubleCard={true}
                        src={res.images.icnCompass}
                        Value={facing ? facing : ""}
                        Heading={"Direction Facing"}
                        valueStyle={styles.facingText}
                        headingStyle={styles.unitText}
                    />
                    <ImageWithTitle
                        isShadow={true}
                        isdoubleCard={true}
                        src={res.images.icnLift}
                        Value={this.props.data.noOfLifts ? this.props.data.noOfLifts : "0"}
                        Heading={"No. of Lifts"}
                        valueStyle={styles.unitNumber}
                        headingStyle={styles.unitText}
                    />
                    {/* <TouchableOpacity style={{ width: "100%" }} onPress={() => this.toggleModal()}> */}
                    <ImageWithTitle
                        isShadow={true}
                        toggleModal={() => this.toggleModal()}
                        isTouchable={true}
                        isdoubleCard={true}
                        src={res.images.icnCar}
                        Value={(this.props.parkingSlotValue && this.props.parkingSlotValue.length) ? this.props.parkingSlotValue.length : 0}
                        Heading={"Car Parking"}
                        valueStyle={styles.unitNumber}
                        headingStyle={styles.unitText}
                    />
                    {/* </TouchableOpacity> */}



                </View>

                {this.props.parkingSlotValue && this.props.parkingSlotValue.length > 0  &&
                <View style={[styles.WallFinishLevel, styles.shadow,{marginTop:heightScale(20)}]}>

                    <View style={{ flexDirection: 'row', marginHorizontal: 10, marginTop: 10 }}>

                        <View style={{ marginLeft: 10, justifyContent: 'center' }}>

                            <Text style={styles.parkingNumber}>Parking Slots</Text>
                            <View style={{ flexDirection: 'row',marginTop:heightScale(5) }}>
        
                                <Text>{this.props.parkingSlotValue.join(',')}</Text>
                            </View>


                        </View>

                    </View>
                </View>}

            </KeyboardAwareScrollView>

        </SafeAreaView >)


    }
}
export default KnowYourUnit