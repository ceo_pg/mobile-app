import { StyleSheet, Platform, StatusBar } from 'react-native'
import res from '../../../../res'

const styles = StyleSheet.create({

  modalStyel: {
    flex: 1, backgroundColor: res.colors.butterscotch, zIndex: 99, borderRadius: 30, marginTop: 10
  }, parkingNumber: {
    fontFamily: res.fonts.regular,
    fontSize: 22,
    color: res.colors.peacockBlue,
    textAlign: 'left',
    letterSpacing: 0.35,
   
  }, unitNumber: {
    fontFamily: res.fonts.regular,
    fontSize: 18,
    color: res.colors.liteBlack,
    // textAlign: 'left',
    letterSpacing: 0.35
  }, unitText: {
    fontFamily: res.fonts.regular,
    fontSize: 12,
    color: res.colors.liteBlack,
    letterSpacing: 0.5
  }, facingText: {
    fontFamily: res.fonts.regular,
    fontSize: 16,
    color: res.colors.liteBlack
  }, UnitTypeText: {
    fontFamily: res.fonts.regular,
    fontSize: 12,
    color: res.colors.liteBlack
  }, unitHeaderText: {
    fontFamily: res.fonts.medium,
    fontSize: 12.2,
    color: res.colors.purpleBrown,
    // marginLeft:-30
  }, unitAreaHeaderText: {
    fontFamily: res.fonts.medium,
    fontSize: 12.2,
    color: res.colors.purpleBrown,
    marginRight: 30
  },
  unitFloorData: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    // alignItems: 'center',

    marginHorizontal: 10,

  }, unitFloorText: {
    fontFamily: res.fonts.regular,
    fontSize: 8.7,
    color: res.colors.black,
    textAlign: 'left',
    marginLeft: 10
  }, unitAreaDimentionText: {
    fontFamily: res.fonts.regular,
    fontSize: 8.7,
    color: res.colors.black,
    textAlign: 'left',
    // marginLeft: 10
  }, unitOddFloorData: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    height: 22,
    backgroundColor: res.colors.shadowColor,
    marginHorizontal: 10,

  }, separator: {
    flex: 1,
    flexDirection: 'row',
    borderWidth: 1,
    borderColor: res.colors.shadowColor,
    alignItems: 'center',
    height: 22,
    justifyContent: 'flex-start'
  }, separatorShadow: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    height: 22,
    justifyContent: 'flex-start'
  }, shadowTab: {
    shadowColor: "black",
    shadowOffset: { width: 0, height: 4 },
    shadowRadius: 6,
    shadowOpacity: 0.2,
    elevation: 3,
  }, UnitLevel: {
    // minHeight: 110,
    marginHorizontal: 10,
    backgroundColor: res.colors.butterscotch,
    marginTop: 10,
    borderRadius: 5
  }, unitHeaderData: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',

    // marginHorizontal: 10,

    backgroundColor: res.colors.shadowColor,

    marginTop: 10,
    height: 22,
    borderTopRightRadius: 5,
    borderTopLeftRadius: 5,

  }, slotNumberText: {
    marginTop: 20,
    fontFamily: res.fonts.medium,
    fontSize: 14,
    color: res.colors.greyishBrown,
    textAlign: 'center'
  }, slotValueText: {
    fontWeight: 'bold',
    fontFamily: res.fonts.bold,
    fontSize: 24,
    color: res.colors.peacockBlue,
    textAlign: 'center'
  }, parkingmodalStyel: {
    flex: 1, zIndex: 99, marginTop: 10, marginHorizontal: 20
  }, shadow: {
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  }, view: {
    // bottom:50
  }, closeBtn: {
    position: "absolute",
    top: 30,
    right: 30,
    backgroundColor: res.colors.black,
    zIndex: 999,
    width: 30,
    height: 30,
    borderRadius: 15,
    alignItems: 'center',
    justifyContent: 'center'
  }, totalAreaText: {
    fontWeight: "bold",
    fontFamily: "Montserrat-Bold",
    fontSize: 10,
    color: res.colors.purpleBrown,
    textAlign: 'left',
    marginLeft: 10
  }, totalArea: {

    flex: 1,
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderLeftWidth: 1,
    borderColor: res.colors.shadowColor,
    // alignItems: 'center',
    height: 22,
    justifyContent: 'center'
  }, totalAreaValue: {
    flex: 1,
    flexDirection: 'row',
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderLeftWidth: 1,
    borderRightWidth: 1,
    borderColor: res.colors.shadowColor,
    alignItems: 'center',
    height: 22,
    justifyContent: 'flex-start'
  }, blankArea: {

    flex: 1,

    borderBottomWidth: 1,

    borderColor: res.colors.shadowColor,
    // alignItems: 'center',
    height: 22,
    justifyContent: 'center'
  }, WallFinishLevel: {
    minHeight: 110,
    marginHorizontal: 10,
    backgroundColor: res.colors.butterscotch,
    marginTop: 10,
    borderRadius: 5,
    bottom: 10,
    // justifyContent: 'center'
  }, resultText: {
    fontFamily: res.fonts.regular,
    fontSize: 12,
    color: res.colors.liteBlack
  }, UnitLevell: {
    minHeight: 80,
    marginHorizontal: 10,
    backgroundColor: res.colors.butterscotch,
    marginTop: 10,
    borderRadius: 5,
    //  alignItems: 'center',
    justifyContent: 'center',
    bottom: 10
  }, container: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    width: '100%',
    justifyContent: 'space-evenly'
  },slotView:{
    alignItems:'center',
    justifyContent:'center'
  },slotText:{

  }
})

export default styles