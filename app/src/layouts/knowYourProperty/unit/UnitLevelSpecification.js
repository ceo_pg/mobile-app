import React, { Component } from 'react'
import { View, Text, Image, TouchableOpacity, SafeAreaView, FlatList, ScrollView } from 'react-native'
import styles from './styles'
import res from '../../../../res'


class UnitLevelSpecification extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isModalVisible: false,
            Name: [
                {
                    name: "Master Bedroom"
                }, {
                    name: "Bedroom 2"
                }, {
                    name: "Bedroom 3"
                }
            ]
        }

    }
    modalView = () => {
        return <UnitFloorModal
            data={this.props.data}
        />
    }
    toggleModal = () => {
        const { isModalVisible } = this.state
        this.setState({ isModalVisible: !this.state.isModalVisible });


    };
    headerName = (index) => {
        if (index == 0) {
            return < Text style={{ marginLeft: 10, marginTop: 13, color: res.colors.greyishBrown }
            }>Master Bedroom </Text >
        } if (index == 1) {
            return < Text style={{ marginLeft: 10, marginTop: 13, color: res.colors.greyishBrown }
            }>Bedroom 2</Text >
        } if (index == 2) {
            return < Text style={{ marginLeft: 10, marginTop: 13, color: res.colors.greyishBrown }
            }>Bedroom 3 </Text >
        }

    }
    render() {

        var unitLevelSpecification = []
        var newData = this.props.data.flooring
        for (var prop in newData) {
            if (newData.hasOwnProperty(prop)) {
                var innerObj = {};
                innerObj[prop] = newData[prop];
                unitLevelSpecification.push(innerObj)
            }
        }
        if (unitLevelSpecification && unitLevelSpecification.length == 0 && !newData) {
            return <View />
        }
        // console.log("UNITLEVELSPEC", unitLevelSpecification)
        return (
            <View style={styles.modalStyel} >

                <FlatList
                    extraData={this.state.Name}
                    scrollEnabled={true}
                    data={unitLevelSpecification}
                    keyExtractor={(item, index) => index.toString()}
                    renderItem={({ item, index }) => {
                        // console.log(item, "renderItem");
                        let v = Object.values(item)[0]
                        let u = Object.values(item)[0]
                        let w = Object.values(item)[0]

                        v = v instanceof Object && v.brand ? v.brand : ""
                        u = u instanceof Object && u.dimensions ? u.dimensions : ""
                        w = w instanceof Object && w.name ? w.name : ""

                        if (index <= 2) {

                            return < View style={[styles.UnitLevel, styles.shadow]}>

                                {this.headerName(index)}
                                <View style={{ flexDirection: 'row', marginHorizontal: 10, paddingBottom: 10, marginTop: 10,marginVertical:20 }}>
                                    <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                                        <Image style={{ width: 58, height: 34 }} source={res.images.icnRoom} />
                                    </View>
                                    <View style={{ marginLeft: 10,justifyContent:'center' }}>
                                        {
                                            v ?
                                                <Text style={styles.resultText}>{v}</Text> :
                                                null
                                        }
                                        {
                                            u ?
                                                <Text style={styles.resultText}>{u}</Text> :
                                                null
                                        }
                                        {
                                            w ?
                                                <Text style={styles.resultText}>{w}</Text> :
                                                null
                                        }

                                    </View>

                                </View>

                            </View>
                        }





                    }}

                />



            </View >
        )


    }
}
export default UnitLevelSpecification