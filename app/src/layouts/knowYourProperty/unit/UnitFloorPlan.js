import React, { Component } from 'react'
import { View, Text, Image, FlatList } from 'react-native'
import styles from './styles'
import res from '../../../../res'
import Modal from 'react-native-modal'
import UnitFloorModal from '../../../components/modal/KnowYourProperty/Unit/UnitFloorModal'

class UnitFloorPlan extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isModalVisible: false
        }

    }
    modalView = () => {
        return <UnitFloorModal
            data={this.props.data}
        />
    }
    toggleModal = () => {
        const { isModalVisible } = this.state
        this.setState({ isModalVisible: !this.state.isModalVisible });


    };
    renderFloorPlan = ((item, index) => {

        // if (!item.item && item.item.area.value&&item.item.breadth.feet) {
        //     return <View />
        // }
        const breathFeet = item.item && item.item.breadth && item.item.breadth.feet ? item.item.breadth.feet : "0"
        const breathInches = item.item && item.item.breadth && item.item.breadth.inches ? item.item.breadth.inches : "0"

        const areaUom = item.item && item.item.area && item.item.area.uom ? item.item.area.uom : ""
        const areaValue = item.item && item.item.area && item.item.area.value ? item.item.area.value : "0"

        const lengthFeet = item.item && item.item.length && item.item.length.feet ? item.item.length.feet : "0"
        const lengthInches = item.item && item.item.length && item.item.length.inches ? item.item.length.inches : "0"

        console.log("renderFloorPlan", breathFeet, breathInches, areaUom, areaValue, lengthFeet, lengthInches)
        return (
            (item.index % 2 == 0) ?
                <View style={[styles.unitFloorData]}>
                    <View style={styles.separator}>
                        <Text style={styles.unitFloorText}>{item.item.nameOfSpace ? item.item.nameOfSpace : ""}</Text>
                    </View>
                    <View style={styles.separator}>
                        <Text style={styles.unitFloorText}>{breathFeet + "'" + breathInches + '"' + "*" + lengthFeet + "'" + lengthInches + '"'}</Text>
                    </View>
                    <View style={styles.separator}>
                        <Text style={styles.unitFloorText}>{areaValue}</Text>
                        <Text style={styles.unitAreaDimentionText}>{areaUom + "  sq ft"}</Text>
                    </View>
                </View> :
                <View style={[styles.unitOddFloorData]}>
                    <View style={styles.separatorShadow}>
                        <Text style={styles.unitFloorText}>{item.item.nameOfSpace ? item.item.nameOfSpace : ""}</Text>
                    </View>
                    <View style={styles.separatorShadow}>
                        <Text style={styles.unitFloorText}>{breathFeet + "'" + breathInches + '"' + "*" + lengthFeet + "'" + lengthInches + '"'}</Text>
                    </View>
                    <View style={styles.separatorShadow}>
                        <Text style={styles.unitFloorText}>{areaValue}</Text>
                        <Text style={styles.unitAreaDimentionText}>{areaUom + "  sq ft"}</Text>
                    </View>
                </View>
        )
    })
    render() {
        console.log("UNITFLOORPLAN", this.props)
        const sumArea = this.props.data ? this.props.data : []
        const sum = sumArea
            .map(item => item.area.value)
            .reduce((prev, curr) => (prev + curr), 0);

        if (!this.props.data && this.props.data.length == 0) {
            return <View style={{ flex: 1,justifyContent:'center',alignItems:'center' }} >
                <Text>{res.strings.NO_DATA_FOUND}</Text>
            </View>
        }

        return (<View style={styles.modalStyel}>



            <FlatList
                ListHeaderComponent={({ item }) => {
                    return <View style={{ height: 99, backgroundColor: res.colors.butterscotch, borderRadius: 5, marginTop: 5, marginHorizontal: 10 }}>
                        <Image source={this.props.image ? { uri: this.props.image } : ""} style={{ flex: 1, borderRadius: 5 }} resizeMode={'cover'} />
                        <View style={[styles.unitHeaderData]}>
                            <Text style={styles.unitHeaderText}>{res.strings.Space}</Text>
                            <Text style={styles.unitHeaderText}>{res.strings.Dimension}</Text>
                            <Text style={styles.unitAreaHeaderText}>{res.strings.Area}</Text>
                        </View>
                    </View>
                }}
                keyExtractor={(item, index) => index.toString()}
                data={this.props.data}
                renderItem={this.renderFloorPlan}
                ListFooterComponent={({ item }) => {
                    return <View style={[styles.unitFloorData]}>
                        <View style={styles.totalArea}>
                            <Text style={styles.totalAreaText}>{res.strings.TotalArea}</Text>
                        </View>
                        <View style={styles.blankArea}>
                            {/* <Text style={styles.unitFloorText}>Total Area</Text> */}
                        </View>
                        <View style={styles.totalAreaValue}>
                            <Text style={styles.totalAreaText}>{sum ? Math.round(sum) : "0"}</Text>
                            <Text style={styles.totalAreaText}>{"sq ft"}</Text>
                        </View>

                    </View>
                }}
            />
        </View >)


    }
}
export default UnitFloorPlan