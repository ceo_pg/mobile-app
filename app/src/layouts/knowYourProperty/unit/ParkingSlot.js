import React, { Component } from 'react'
import { View, Text, Image, TouchableOpacity, SafeAreaView, FlatList, Dimensions } from 'react-native'
import styles from './styles'
import res from '../../../../res'
import { ParkingSlotModal } from '../../../components/index'
import ImageZoom from 'react-native-image-pan-zoom';
import Modal from 'react-native-modal'
class ParkingSlot extends Component {
    constructor(props) {
        super(props)
        this.modalRef = React.createRef()
        this.state = {
            isModalVisible: false,
            parking: ""
        }

    }
    componentDidMount() {
        // console.log("COMPONENT DID MOUNT")
        this.setState({
            parking: this.props.parkingImage
        })
    }
    renderSlotsImages = ({ item, index }) => {
        const height = 140
        const step = 1 / (2 * height);
        return (
            <View style={{ flex: 1 }}>
                <View style={{ width: "100%", height: "100%", zIndex: 9999, overflow: 'hidden' }}>
                    <ImageZoom cropWidth={Dimensions.get('window').width}
                        cropHeight={Dimensions.get('window').height}
                        imageWidth={Dimensions.get('window').width}
                        imageHeight={Dimensions.get('window').height}>

                        <Image source={item.fileName ? { uri: item.fileName } : res.images.parking} style={{ flex: 1 }} resizeMode={'cover'} />

                    </ImageZoom>
                </View>

            </View>



        )
    }
    modalView = () => {
        return (
            <View style={{ borderRadius: 10 }}>
                <View style={styles.closeBtn}>
                    <TouchableOpacity onPress={() => this.toggleModal()}>
                        <Image source={res.images.icnClose} style={{ width: 20, height: 20, tintColor: res.colors.white }} resizeMode={'center'} />
                    </TouchableOpacity>
                </View>
                <FlatList
                    keyExtractor={(item, index) => index.toString()}
                    pagingEnabled={true}
                    bounces={false}
                    style={{ marginHorizontal: -20 }}
                    horizontal={true}
                    data={this.props.parkingImageCollection}
                    renderItem={this.renderSlotsImages}
                />
            </View>



        )

        // return <ParkingSlotModal
        //     parking={this.props.parkingImage}
        //     toggleModal={this.toggleModal}
        // />
    }
    toggleModal = () => {

        this.setState({
            isModalVisible: !this.state.isModalVisible
        })

    };
    render() {
      console.log('>>>>>>>>>>>>>>>>>>>>>>>>>>.',this.props.parkingSlotValue);

        const slotNumber = this.props && this.props.unitsData && this.props.unitsData.carParking && this.props.unitsData.carParking.slotNumbers ? this.props.unitsData.carParking.slotNumbers : []
         console.log("parkingImageCollection", this.props)
        return (< SafeAreaView style={styles.parkingmodalStyel} >
            <Modal
                style={{ borderRadius: 10 }}
                isVisible={this.state.isModalVisible}
            >
                {this.modalView()}
            </Modal>
            {
                this.props.parkingImage ?
                    <TouchableOpacity onPress={() => this.toggleModal()} style={{ flex: 1, borderRadius: 10, }}>
                        <Image source={this.props.parkingImage ? { uri: this.props.parkingImage } : "No Data found"} style={{ flex: 1 }} resizeMode={'cover'} />
                    </TouchableOpacity> :
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                        <Text>No Data Found</Text>
                    </View>

            }


            <Text style={styles.slotNumberText}>Slot Number</Text>
         {/* {
                this.props.parkingSlotValue.map((item, index) => {
                    <Text style={styles.slotValueText}>{item ? item : "NA"}</Text>
                })
            } */}





        </SafeAreaView >

        )


    }
}
export default ParkingSlot