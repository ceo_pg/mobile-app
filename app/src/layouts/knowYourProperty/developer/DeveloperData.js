import React, { Component } from 'react'
import { View, Text, Dimensions, TouchableOpacity, SafeAreaView, Image, Linking } from 'react-native'
import Carousel, { Pagination } from 'react-native-snap-carousel';
import Modal from 'react-native-modal'
// import WebView from 'react-native-webview'

import styles from './styles'
import res from '../../../../res'
import { VastuMeter } from '../../../components'
// import moment from 'moment';
import ImageWithTitle from '../../../components/card/ImageWithTitle'
import CardWithTitle from '../../../components/card/CardWithTitle'
import CardWithHeader from '../../../components/card/CardWithHeader'
import strings from '../../../../res/constants/strings';
import DeveloperDetailModal from '../../..//components/modal/DeveloperDetailModal'
import IndexOfCharges from './IndexOfCharges'
import {
    IndexOfChargesModal, PartnersAndLocationsModal, AssociationMemberModal,
    OnGoingProjectModal, DeveloperScoreModal
} from '../../../components/index'
import { widthScale, myWidth, heightScale } from '../../../utility/Utils';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';


class DeveloperData extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isDeveloperScoreModal: false,
            activeIndex: 0,
            isModalVisible: false,
            brochure: "",
            sitePlan: "",
            Name: [
                {
                    name: "SRN"
                }, {
                    name: "Charge ID"
                }, {
                    name: "Holder Name"
                }, {
                    name: "Date of Creation"
                }, {
                    name: "Date of Modification"
                }, {
                    name: "Amount"
                }, {
                    name: "Address"
                }
            ]
        }
    }
    componentDidMount() {
        // console.log(this.props.data, "devloper data")

    }

    renderDeveloperCards = (id) => {
        switch (id) {
            case 1: return this.renderDeveloperDetail()
                break;
            // code block

            case 2:
                // code block

                return this.renderOnGoingProjects()
                break;
            case 3:
                return this.renderAssociationMemberShip();
                break
            case 4:
                return this.renderPartnerAndLocation();
                break
            // case 5:
            //     return this.renderIndexOfCharges();
            //     break
            default:
            // code block
        }
    }

    renderOnGoingProjects = () => {
        const { developerID } = this.props.data;

        if (!developerID && !developerID.ongoingProjects && !developerID.deliveredProjects) {

            return (
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <Text>{strings.NO_DATA_FOUND}</Text>
                </View>
            )

        } else {
            let onGoingProject = developerID[0].ongoingProjects
            let deliveredProjects = developerID[0].deliveredProjects
            return (
                <SafeAreaView style={{ flex: 1, marginTop: 10 }}>
                    <KeyboardAwareScrollView>
                        <Text style={styles.projectsText}>Ongoing Projects</Text>
                        <View style={{ flexDirection: 'row', flexWrap: 'wrap', width: '100%', justifyContent: 'space-evenly' }}>
                            {onGoingProject.map((item, index) => {
                                return (

                                    <CardWithTitle isProjectSpecs={false} uri={item.logo} />)
                            })}
                            {/* <CardWithTitle isShodow={true} isProjectSpecs={false} /> */}
                        </View>
                        <Text style={styles.projectsText}>Delivered Projects </Text>
                        <View style={{ flexDirection: 'row', flexWrap: 'wrap', width: '100%', justifyContent: 'space-evenly' }}>

                            {deliveredProjects.map((item, index) => {
                                return (

                                    <CardWithTitle uri={item.logo} />)
                            })}
                            {/* <CardWithTitle isShodow={true} isProjectSpecs={false} /> */}
                        </View>
                    </KeyboardAwareScrollView>


                </SafeAreaView>
            )
        }

    }

    renderDeveloperDetail = () => {

        const { developerID } = this.props && this.props.data ? this.props.data : [];

        let memberShip = developerID[0].membership;
        let memberArray = []
        if (memberShip) {
            for (let index = 0; index < memberShip.length; index++) {
                if (memberShip[index].name) {
                    memberArray.push(memberShip[index].name ? memberShip[index].name : "")
                  

                }
            }
        }
        console.log(developerID, "renderDeveloperDetail");
        if (!developerID || developerID.length == 0) {
            return (
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <Text>{strings.NO_DATA_FOUND}</Text>
                </View>
            )
        } else {
            return (
                <SafeAreaView style={{ flex: 1, marginTop: 10, overflow: 'hidden' }}>
                    <KeyboardAwareScrollView>
                        <ImageWithTitle
                            isShadow={true}
                            src={res.images.complience}
                            Value={developerID[0].reraCompliance ? developerID[0].reraCompliance : "0"}
                            Heading={strings.RERA_COMPLIANCE} />
                        <ImageWithTitle
                            isShadow={true}
                            src={res.images.compaints}
                            Value={developerID[0].reraComplaints ? developerID[0].reraComplaints : "0"}
                            Heading={strings.RERA_COMPLAINTS} />
                        <ImageWithTitle
                            isShadow={true}
                            src={res.images.principalamount}
                            Value={developerID[0].noOfYrsInBusiness ? developerID[0].noOfYrsInBusiness : "NA"}
                            Heading={strings.YEAR_IN_BUSINES} />
                        <ImageWithTitle
                            isShadow={true}
                            src={res.images.project}
                            Value={developerID[0].projectsDelivered ? developerID[0].projectsDelivered : "NA"}
                            Heading={strings.PROJECT_DELIVERED} />
                        {
                            memberArray.length > 0 &&
                            <ImageWithTitle
                                isShadow={true}
                                src={res.images.partners}

                                Value={memberArray.join(', ')}
                                Heading={strings.Association_Memberships} />

                        }


                    </KeyboardAwareScrollView>

                </SafeAreaView>

            )
        }
    }


    renderAssociationMemberShip = () => {
        const { developerID } = this.props.data;
        // console.log("indexOfCharges===>", developerID)
        if (!developerID && !developerID.membership && developerID.membership.length == 0) {
            return (
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <Text>{strings.NO_DATA_FOUND}</Text>
                </View>
            )
        } else {
            let memberShip = developerID[0].membership;
            return (

                <SafeAreaView style={{ flex: 1, marginTop: 10, alignSelf: 'center' }}>
                    <Text style={styles.AssociationText}>{res.strings.Association_Memberships}</Text>
                    <View style={{ flexDirection: 'row', flexWrap: 'wrap', width: '100%', justifyContent: 'space-evenly' }}>
                        {memberShip.map((item, index) => {
                            return (
                                <CardWithTitle titleStyle={styles.addMoreText} uri={item.url} />
                            )
                        })}
                        <CardWithTitle isShodow={true} imageStyle={{ resizeMode: 'stretch', width: 90, height: 58 }} title={""} />
                    </View>

                </SafeAreaView>
            )
        }
    }
    renderIndexOfCharges = () => {
        const indexOfCharges = this.props.indexOfChanges

        if (!indexOfCharges || indexOfCharges.length == 0) {
            return (
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <Text>{strings.NO_DATA_FOUND}</Text>
                </View>
            )
        } else {
            return (
                <SafeAreaView style={{ flex: 1 }}>
                    <IndexOfCharges
                        indexOfChanges={indexOfCharges}
                    />
                </SafeAreaView>

            )
        }
    }
    renderPartnerAndLocation = () => {
        const { developerID } = this.props.data;
        if (!developerID || developerID.length == 0) {
            return (
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <Text>{strings.NO_DATA_FOUND}</Text>
                </View>
            )
        } else {
            let nameOfPartners = developerID[0].namesOfPartners;
            let corporateAdress = developerID[0].registeredAddress
            // console.log(nameOfPartners, "nameOfPartners", developerID)
            return (
                <SafeAreaView style={{ flex: 1, marginTop: 10, overflow: 'hidden' }}>
                    <KeyboardAwareScrollView>
                        <View>
                            <CardWithHeader title={strings.PARTNERS} Value={nameOfPartners ? nameOfPartners : ""} src={res.images.partners} titleStyle={{ textAlign: 'left' }} />
                        </View>
                        <View>
                            <CardWithHeader title={strings.REGISTER_ADDRESS} Value={corporateAdress ? corporateAdress : ""} src={res.images.address} titleStyle={{ textAlign: 'left' }} />
                        </View>
                    </KeyboardAwareScrollView>


                </SafeAreaView>
            )
        }
    }

    toggleModal = (isDeveloperScoreModal) => {
        const { isModalVisible } = this.state
        if (this.props.developerDetailedScore == {} || this.props.developerDetailedWeight == {} || this.props.developerDetailedScoreParameters == {}) {
            return null
        }
        isDeveloperScoreModal ?

            this.setState({ isModalVisible: !isModalVisible, isDeveloperScoreModal: true }) :
            this.setState({ isModalVisible: !isModalVisible, isDeveloperScoreModal: false })
    };
    renderCorousalData = (item, index) => {
        return this.renderDeveloperCards(item.id)

    }
    renderData = ({ item, index }) => {
        return <View style={{ flex: 1, backgroundColor: res.colors.butterscotch, borderRadius: 24, marginTop: 50, overflow: 'hidden' }} onPress={() => this.toggleModal()} activeOpacity={1}>

            <View style={[styles.shadowHeader, { height: 42 }]}>
                <Text style={styles.corousalNameText}>{item.name}</Text>
            </View>
            {this.renderCorousalData(item)}
            {this.renderViewMore()}
        </View>
    }


    renderViewMore = () => {
        const { activeIndex } = this.state
        switch (activeIndex) {
            case 0: return this.viewMore()
                break;

            case 1: return this.viewMore()
                break;
            // code block

            case 2:
                return this.viewMore()
                break
            case 3:
                return this.viewMore()
                break
            case 4:
                return this.viewMore()
                break
            default:
            // code block
        }

    }
    viewMore = () => {
        const { developerID } = this.props.data;
        if (!developerID || developerID.length == 0) {
            return null
        } else {
            return (<View style={{ alignItems: 'center', height: 31, justifyContent: 'center' }}>
                <TouchableOpacity onPress={() => this.toggleModal()} activeOpacity={1}>
                    <Text style={styles.viewMoreText}>View More</Text>
                </TouchableOpacity>
            </View>)
        }

    }

    modalView = () => {
        const { activeIndex, isDeveloperScoreModal } = this.state
        if (isDeveloperScoreModal) {
            return this.renderDeveloperScoreModal()
        } else {
            return this.openActiveIndexModal(activeIndex)
        }


    }
    renderDeveloperScoreModal = () => {
        const { developerID } = this.props.data;

        if (this.props.developerDetailedScore == {} || this.props.developerDetailedWeight == {} || this.props.developerDetailedScoreParameters == {}) {
            return null
        } else {
            return <DeveloperScoreModal
                developerDetailedScore={this.props.developerDetailedScore}
                developerDetailedWeight={this.props.developerDetailedWeight}
                developerDetailedScoreParameters={this.props.developerDetailedScoreParameters}
                maxScore={this.props.maxScore ? this.props.maxScore : 0}
                weightedAvrg={this.props.weightedAvrg ? this.props.weightedAvrg : 0}

            />
        }

    }

    openActiveIndexModal = (activeIndex) => {
        switch (activeIndex) {
            case 0:
                // code block
                return this.renderDeveloperDetailModal()
                break;
            case 1:
                // code block
                return this.renderOnGoingModal()
                break;
            // case 2:
            //     // code block
            //     return this.renderAssociationModal()
            //     break;
            case 2:
                // code block
                return this.renderParnetLocationModal()
                break;
            case 4:
                // code block
                return this.renderIndexOfChargesModal()
                break;
            default:
            // code block
        }
    }
    renderOnGoingModal = () => {
        const { developerID } = this.props.data;

        if (!developerID && !developerID.ongoingProjects && !developerID.deliveredProjects) {

            return (
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <Text>{strings.NO_DATA_FOUND}</Text>
                </View>
            )

        } else {
            let onGoingProject = developerID[0].ongoingProjects
            let deliveredProjects = developerID[0].deliveredProjects
            return (
                <OnGoingProjectModal deliveredProjects={deliveredProjects} onGoingProject={onGoingProject} />
            )
        }
    }
    renderAssociationModal = () => {
        const { developerID } = this.props.data;

        if (!developerID && !developerID.membership && developerID.membership.length == 0) {
            return (
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <Text>{strings.NO_DATA_FOUND}</Text>
                </View>
            )
        } else {
            let memberShip = developerID[0].membership;
            return (

                <AssociationMemberModal memberShip={memberShip} />
            )
        }
    }
    renderDeveloperDetailModal = () => {



        const { developerID } = this.props.data;
        let memberShip = developerID[0].membership;

        if (!developerID && developerID.length == 0 && !developerID && !developerID.membership && developerID.membership.length == 0) {
            return;
        }

        return (
            <DeveloperDetailModal memberShip={memberShip} developerData={developerID[0]} />
        )
    }
    renderParnetLocationModal = () => {
        const { developerID } = this.props.data;
        if (!developerID || developerID.length == 0) {
            return (
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <Text>{strings.NO_DATA_FOUND}</Text>
                </View>
            )
        } else {
            let nameOfPartners = developerID[0].namesOfPartners;
            let corporateAdress = developerID[0].registeredAddress
            // console.log(nameOfPartners, "nameOfPartners", developerID)
            return (
                <PartnersAndLocationsModal nameOfPartners={nameOfPartners} corporateAdress={corporateAdress} />
            )
        }

    }
    renderIndexOfChargesModal = () => {
        const indexOfCharges = this.props.indexOfChanges
        if (!indexOfCharges && indexOfCharges.length == 0) {
            return;
        }
        return (
            <IndexOfChargesModal
                indexOfCharges={indexOfCharges}
            />
        )

    }
    renderSquareFitItem = ({ item, index }) => {
        return <View style={{ flexDirection: 'column', justifyContent: 'center', alignItems: 'center', marginLeft: 10, }}>
            <Text style={styles.squreHeading}>{item._id}</Text>
            <Text style={styles.squreSubHeading}>{item.unitName}</Text>
            <Text style={styles.squreSubHeading}>{Object.keys(item)}</Text>
        </View>
    }
    moveToWeb = (url) => {
        Linking.canOpenURL(url).then(supported => {
            if (supported) {
                Linking.openURL(url);
            } else {
                console.log("Don't know how to open URI: " + url);
            }
        });
    }
    renderTopText = () => {
        const developerScore = this.props.data.developerScore
        return <View style={styles.firstTitle}>
            <View style={{ width: "60%", marginHorizontal: 20, minHeight: heightScale(80) }}>
                <Image source={{ uri: this.props.developerLogo }} style={{ flex: 1, borderRadius: 10 }} resizeMode={'contain'} />
                <Text numberOfLines={2} style={styles.projectNameText}>{this.props.developerName ? this.props.developerName : ""}</Text>
                <TouchableOpacity style={{ marginHorizontal: 5 }} onPress={() => this.moveToWeb(this.props.website)}>
                    <Text numberOfLines={2} style={[styles.districtText, { textAlign: 'center' }]}>{this.props.website ? this.props.website : ""}</Text>
                </TouchableOpacity>

            </View>
            <View style={styles.lineView}>

            </View>
            <TouchableOpacity onPress={() => this.toggleModal(true)} style={{ width: '30%', marginLeft: 30, alignItems: 'center' }}>
                <VastuMeter
                    showText={(developerScore ? ((Math.round(developerScore)) / 10).toFixed(1) : 0) + "/10"}
                    percent={developerScore ? Math.round(developerScore / 10) : 0}
                    radius={27}
                    bgRingWidth={5}
                    progressRingWidth={5}
                    ringColor={res.colors.pinkishTan}
                    ringBgColor={'grey'}
                    textFontSize={10}
                    textFontColor={res.colors.pinkishTan}
                    textFontWeight={'normal'}
                    clockwise={true}
                    bgColor={'white'}
                    startDegrees={0}
                >
                    <Text>9/10</Text>
                </VastuMeter>

                <Text style={styles.vastuText}>{res.strings.DEVELOPER_SCORE}</Text>
            </TouchableOpacity>

        </View >
    }
    renderMiddleBar = () => {
        // noOfYrsInBusiness= {this.state.noOfYrsInBusiness}
        // projectsDelivered= {this.state.projectsDelivered}
        // unitsDelivered= {this.state.unitsDelivered}
        const { activeIndex } = this.state
        console.log("ACTIOVEINDEX", activeIndex)
        if (activeIndex == 0) {
            return <View style={styles.squreFitView}>
                <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}>
                    <Text style={styles.squreHeading}>{this.props.noOfYrsInBusiness ? this.props.noOfYrsInBusiness : 0}</Text>
                    <Text style={styles.squreSubHeading}>{"Years in Business"}</Text>
                </View>
                <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}>
                    <Text style={styles.squreHeading}>{this.props.projectsDelivered ? this.props.projectsDelivered : 0}</Text>
                    <Text style={styles.squreSubHeading}>{"Projects Delivered"}</Text>
                </View>
                <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}>
                    <Text style={styles.squreHeading}>{this.props.unitsDelivered ? this.props.unitsDelivered : 0}</Text>
                    <Text style={styles.squreSubHeading}> {"Units Delivered"}</Text>
                </View>
            </View>
        } else {
            return <View style={styles.squreFitView}>
                <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}>
                    <Text style={styles.squreHeading}>{this.props.awardsDetails}</Text>
                    <Text style={styles.squreSubHeading}>{"Awards Won"}</Text>
                </View>
                <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}>
                    <Text style={styles.squreHeading}>{this.props.reraComplaints ? this.props.reraComplaints : 0}</Text>
                    <Text style={styles.squreSubHeading}> {"RERA Complaints"}</Text>
                </View>

            </View>
        }

    }
    render() {
        console.log("DEVELOPER", this.props)
        return (
            <View style={styles.unitFullScreen}>
                <Modal style={{ marginTop: 100, borderWidth: 10 }} isVisible={this.state.isModalVisible} onBackdropPress={() => this.toggleModal()}
                    style={styles.view}>
                    {this.modalView()}
                </Modal>
                {this.renderTopText()}
                {/* <View style={{ marginHorizontal: 20, width: myWidth * 0.58 }}>
                    <Text numberOfLines={3} style={styles.addressText}>{this.props.registeredAddress}</Text>
                </View> */}
                {this.renderMiddleBar()}


                <View style={{ flex: 0.9 }}>
                    <Carousel
                        data={this.props.data.developerCarouselItems}
                        renderItem={this.renderData}
                        sliderWidth={Dimensions.get('window').width}
                        itemWidth={Dimensions.get('window').width - 80}
                        layout={'default'}
                        containerCustomStyle={styles.carouselData}
                        removeClippedSubviews={true}
                        onSnapToItem={index => this.setState({ activeIndex: index })}
                        firstItem={this.state.activeIndex}
                    />
                    <View style={{ height: 20, width: "10%", alignSelf: 'center' }}>
                        <Pagination
                            dotsLength={this.props.data.developerCarouselItems.length}
                            dotStyle={styles.dotStyle}
                            dotColor={styles.dotColor}
                            inactiveDotStyle={styles.inactiveDotStyle}
                            activeDotIndex={this.state.activeIndex}
                            inactiveDotOpacity={0.4}
                            inactiveDotScale={0.6}
                            activeOpacity={1}
                        />
                    </View>


                </View>

            </View>
        )
    }

}
export default DeveloperData