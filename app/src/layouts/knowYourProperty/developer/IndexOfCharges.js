import React, { Component } from 'react'
import { SafeAreaView, StyleSheet, View } from 'react-native'
import res from '../../../../res'
import TitleWithValue from '../../../components/card/TitleWithValue'
import moment from 'moment';
import { widthScale, heightScale } from '../../../utility/Utils';
import style from '../../../components/button/styles';

class IndexofCharges extends Component {
    constructor(props) {
        super(props)
        this.state = {

        }
    }
    render() {
        //  console.log("IndexofCharges", this.props)
        const address = this.props.indexOfChanges && this.props.indexOfChanges[0].address ? this.props.indexOfChanges[0].address : "";
        const amount = this.props.indexOfChanges && this.props.indexOfChanges[0].amount ? this.props.indexOfChanges[0].amount : "";
        const chargeHolderName = this.props.indexOfChanges && this.props.indexOfChanges[0].chargeHolderName ? this.props.indexOfChanges[0].chargeHolderName : "";
        const chargeID = this.props.indexOfChanges && this.props.indexOfChanges[0].chargeID ? this.props.indexOfChanges[0].chargeID : "";
        const dateOfCreation = this.props.indexOfChanges && this.props.indexOfChanges[0].dateOfCreation ? this.props.indexOfChanges[0].dateOfCreation : "";
        const dateOfModification = this.props.indexOfChanges && this.props.indexOfChanges[0].dateOfModification ? this.props.indexOfChanges[0].dateOfModification : "";
        const dateOfSatisfaction = this.props.indexOfChanges && this.props.indexOfChanges[0].dateOfSatisfaction ? this.props.indexOfChanges[0].dateOfSatisfaction : "";
        const srn = this.props.indexOfChanges && this.props.indexOfChanges[0].srn ? this.props.indexOfChanges[0].srn : "";

        return (<View style={[{
            flex:1,
            flexDirection: 'row',
            flexWrap: 'wrap',
            width: '95%',
            backgroundColor:'red',
            marginTop:10,
            alignSelf:'center'
            // marginHorizontal:10
            // justifyContent: 'space-evenly'
        }, styles.shadow]}>
            <TitleWithValue
                title={"SRN"}
                Value={srn}
            />
            <TitleWithValue
                title={"Charge ID"}
                Value={chargeID}
            />
            <TitleWithValue
                title={"Holder Name"}
                Value={chargeHolderName}
            />
            <TitleWithValue
                title={"Date of Creation"}
                Value={dateOfCreation?moment(dateOfCreation).format("DD-mm-YYYY"):"NA"}
            />
            <TitleWithValue
                title={"Modification Date"}
                Value={dateOfModification?moment(dateOfModification).format("DD-mm-YYYY"):"NA"}
            />
            <TitleWithValue
                title={"Amount"}
                Value={amount}
            />
            <TitleWithValue
                title={"Address"}
                Value={address}
                
            />

        </View>
        )

    }

}

export default IndexofCharges

export const styles = StyleSheet.create({
    indexOfChangesList: {
        flex: 1,
        margin: 5,
        backgroundColor: res.colors.butterscotch,
        minHeight: 104,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 5,
        flexDirection: 'column'
    }, shadow: {
          backgroundColor: res.colors.butterscotch,
        shadowColor: res.colors.shadowColor,
        shadowOffset: { width: 1, height: 1 },
        shadowOpacity: 0.4,
        shadowRadius: 3,
        elevation: 5,
    }, indexOfChangesView: {
        marginHorizontal: widthScale(4.5),
        marginVertical: widthScale(4.5),
        // minHeight: 70,
        borderRadius: 5,
        flexDirection: 'column',
        width: '100%',
    }
})