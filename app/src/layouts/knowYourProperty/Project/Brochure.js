import React, { Component } from 'react'
import { View, SafeAreaView, TouchableOpacity, Image, FlatList, Text, Dimensions, Platform,Linking} from 'react-native'
import styles from './styles'
import res from '../../../../res'
import PDFView from 'react-native-view-pdf';
import ImageZoom from 'react-native-image-pan-zoom';
import Modal from 'react-native-modal'
import Pdf from 'react-native-pdf';

class Brochure extends Component {
    constructor(props) {
        super(props)
        this.modalRef = React.createRef()
        this.state = {
            isModalVisible: false,
            parking: ""
        }

    }
    componentDidMount() {
        // console.log("COMPONENT DID MOUNT")
        this.setState({
            parking: this.props.parkingImage
        })
    }
    openWebSite = (url) => {
        // console.log("openWebSite", url)
        this.props.navigation.navigate("WebViewDisplay", {
            url: url,

        })
    }
    renderSlotsImages = ({ item, index }) => {

        return (
            <View style={{ flex: 1 }}>
                <View style={{ width: "100%", height: "100%", zIndex: 9999, overflow: 'hidden' }}>
                    <ImageZoom cropWidth={Dimensions.get('window').width}
                        cropHeight={Dimensions.get('window').height}
                        imageWidth={Dimensions.get('window').width}
                        imageHeight={Dimensions.get('window').height}>

                        <Image source={item.fileName ? { uri: item.fileName } : res.images.parking} style={{ flex: 1 }} resizeMode={'cover'} />

                    </ImageZoom>

                </View>

            </View>

        )
    }
    ShareImage=(url)=>{
        Linking.canOpenURL(url).then(supported => {
            if (supported) {
                Linking.openURL(url);
            } else {
                console.log("Don't know how to open URI: " + url);
            }
        });
    }
    modalView = () => {
        const brochurePdf = this.props && this.props.brochure && this.props.brochure[0].fileName ? this.props.brochure[0].fileName : []
        const brochure = this.props.brochure
        const source = { uri: brochurePdf, cache: true }
        const resourceType = 'url';
        console.log("BROUCHER", brochurePdf)
        return (
            brochurePdf.toLowerCase().includes(".pdf") ?
                <View style={{ borderRadius: 10, flex: 1 }}>

                    <View style={styles.closeBtn}>
                        <TouchableOpacity onPress={() => this.toggleModal()}>
                            <Image source={res.images.icnClose} style={{ width: 20, height: 20, tintColor: res.colors.white }} resizeMode={'center'} />
                        </TouchableOpacity>
                    </View>
                    {
                        Platform.OS === 'ios' ?
                            <Pdf
                                source={source}
                                style={{ flex: 1 }}
                            /> :
                            <PDFView
                                // fadeInDuration={250.0}
                                style={{ flex: 1, }}
                                resource={brochurePdf}
                                resourceType={'url'}
                                onLoad={() => console.log(`PDF rendered from ${resourceType}`)}
                                onError={(error) => console.log('Cannot render PDF', error)}
                            />
                    }
                    
                            <TouchableOpacity style={styles.ShareBtn} onPress={() => this.ShareImage(brochurePdf)}>
                                <View style={styles.shareView}>
                                <Image source={res.images.icn_share} />
                                </View>
                                
                            </TouchableOpacity> 
                    
                </View>
                :

                <View style={{ borderRadius: 10 }}>
                    <View style={styles.closeBtn}>
                        <TouchableOpacity onPress={() => this.toggleModal()}>
                            <Image source={res.images.icnClose} style={{ width: 20, height: 20, tintColor: res.colors.white }} resizeMode={'center'} />
                        </TouchableOpacity>
                    </View>
                    <FlatList
                        pagingEnabled={true}
                        style={{ marginHorizontal: -20 }}
                        bounces={false}
                        horizontal={true}
                        data={brochure}
                        renderItem={this.renderSlotsImages}
                    />
                </View>



        )

    }
    toggleModal = () => {

        this.setState({
            isModalVisible: !this.state.isModalVisible
        })

    };
    render() {
        const brochure = this.props && this.props.brochure && this.props.brochure[0].fileName ? this.props.brochure[0].fileName : []
        const source = { uri: brochure, cache: true }

        const resourceType = 'url';
        console.log("BROCHER", brochure)
        return (< SafeAreaView style={styles.brochureModalStyle} >
            <Modal
                style={{ borderRadius: 10 }}
                isVisible={this.state.isModalVisible}
            >

                {this.modalView()}
            </Modal>

            {this.props.brochure.length > 0 ?
                brochure.toLowerCase().includes(".pdf") ? <TouchableOpacity onPress={() => this.toggleModal()} style={{ flex: 1, width: '100%', borderRadius: 10, height: '100%' }}>
                    {
                        Platform.OS === 'ios' ?
                            <Pdf
                                source={source}
                                style={{ flex: 1 }}
                            /> :
                            <PDFView
                                fadeInDuration={250.0}
                                style={{ flex: 1, }}
                                resource={brochure}
                                resourceType={'url'}
                                onLoad={() => console.log(`PDF rendered from ${resourceType}`)}
                                onError={(error) => console.log('Cannot render PDF', error)}
                            />
                    }

                </TouchableOpacity>

                    :
                    <TouchableOpacity onPress={() => this.toggleModal()} style={{ flex: 1, width: '100%', borderRadius: 10, height: '100%' }}>
                        <Image source={brochure ? { uri: brochure } : res.images.parking} style={{ flex: 1, width: '100%', borderRadius: 10, height: '100%' }} resizeMode={'cover'} />
                    </TouchableOpacity> :
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <Text>No Data Found</Text>
                </View>
            }
            {this.props.brochure.length > 0 ?
                <TouchableOpacity onPress={() => this.toggleModal()} style={{ height: 30, justifyContent: 'center' }} >
                    <Text style={styles.viewMoreText}>View More</Text>
                </TouchableOpacity> : <View />
            }



        </SafeAreaView >

        )


    }
}
export default Brochure