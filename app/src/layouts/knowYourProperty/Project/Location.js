import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity, Linking } from 'react-native';

import res from '../../../../res';
import styles from './styles';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { heightScale } from '../../../utility/Utils';
import Modal from 'react-native-modal';
import MapView, { Marker } from 'react-native-maps';

class Location extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isModalVisible: false,
    };
  }
  componentDidMount() { }

  openMaps = (latitude, longitude) => {

    const url = Platform.select({
      ios: "maps:" + latitude + "," + longitude + "?q=" + latitude + "," + longitude,
      android: "geo:" + latitude + "," + longitude + "?q=" + latitude + "," + longitude
    });
    Linking.openURL(url);
  }

  mapView = (isModal) => {
    const { projectInfo } = this.props;
    let latlngArray = this.props.projectLocation.split(',')
    if (latlngArray.length > 1) {
      let latitude = parseFloat(latlngArray[0]);
      let longitude = parseFloat(latlngArray[1]);
      return (
        <View style={{ height: isModal ? heightScale(300) : heightScale(150), width: '100%', marginBottom: 10 }}>
          <MapView
            style={{ flex: 1 }}
            initialRegion={{
              latitude: latitude, // put property latitude
              longitude: longitude, // put property longitude
              latitudeDelta: 0.004,
              longitudeDelta: 0.004,
            }}>
            <Marker
              coordinate={{ latitude: latitude, longitude: longitude }} // put propert lat and lng
              title={projectInfo.projectName}           // put property name here
              description={projectInfo.address} // Put property address here
              onCalloutPress={() => { this.openMaps(latitude, longitude) }} // put property lat lng here

            />
          </MapView>
        </View>
      );
    }

  };

  modalView = () => {
    const { projectInfo } = this.props;
    return (
      <View style={styles.container}>
        <View style={[styles.shadowHeader, { height: 42 }]}>
          <Text style={styles.corousalNameText}>Location</Text>
        </View>
        {this.mapView(true)}
        <View style={{ marginHorizontal: 23, marginTop: 20 }}>
          <Text style={styles.LoactionHeading}>{projectInfo.projectName}</Text>
          <Text style={styles.locationText}>{projectInfo.address}</Text>
          {/* <Text style={[styles.LoactionHeading, { marginTop: 10 }]}>
            {'Home'}
          </Text>
          <Text style={styles.locationText}>
            {projectInfo.noOfUnits}, Tower {projectInfo.totalNoOfTowers}

          </Text> */}
        </View>
      </View>
    );
  };

  toggleModal = () => {
    this.setState({
      isModalVisible: !this.state.isModalVisible,
    });
  };

  render() {
    const { projectInfo, projectLocation } = this.props;
    console.log("LOCATION", projectLocation)
    return (
      <View style={styles.modalStyel}>
        <Modal
          onBackdropPress={() => this.toggleModal()}
          style={{ borderRadius: 10 }}
          isVisible={this.state.isModalVisible}>
          {this.modalView()}
        </Modal>
        <KeyboardAwareScrollView >


          <View style={{ marginHorizontal: 10, marginTop: 10, flex: 1 }}>
            {this.mapView(false)}
            {/* <View style={{ justifyContent: 'center', flex: 1, alignItems: 'center' }}> */}
              <Text style={styles.LoactionHeading}>
                {projectInfo.projectName}
              </Text>
              <Text style={styles.locationText}>{projectInfo.address}</Text>
            {/* </View> */}

            {/* <Text style={[styles.LoactionHeading, { marginTop: 10 }]}>
              {'Home'}
            </Text>
            <Text style={styles.locationText}>
              <Text>{projectInfo.noOfUnits}</Text>
              <Text> Tower {projectInfo.totalNoOfTowers}</Text>
            </Text> */}
          </View>
        </KeyboardAwareScrollView>
        <TouchableOpacity
          onPress={() => this.toggleModal()}
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            marginVertical: heightScale(5),
          }}>
          <Text style={styles.viewMoreText}>View More</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

export default Location;
