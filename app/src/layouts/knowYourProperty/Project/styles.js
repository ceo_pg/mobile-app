import { StyleSheet, Platform } from 'react-native'
import res from '../../../../res'
import { heightScale, widthScale } from '../../../utility/Utils'
const styles = StyleSheet.create({
    fullScreen: {
        flex: 1,
        backgroundColor: res.colors.appColor
    }, amenitiesList: {
        flex: 1,
        margin: 5,
        backgroundColor: res.colors.butterscotch,
        height: 104,
        alignItems: 'center',
        // justifyContent: 'center',
        borderRadius: 5,
        flexDirection: 'column',
       
    }, shadow: {
        backgroundColor: res.colors.butterscotch,
        shadowColor: res.colors.shadowColor,
        shadowOffset: {
            width: 2,
            height: 2,
        },
        shadowOpacity: 5.25,
        shadowRadius: 3.84,
        elevation: 5,
        // overflow:'hidden'
    }, amenitiesText: {
        marginTop: 10,
        fontFamily: res.fonts.medium,
        fontSize: 14,
        color: res.colors.greyishBrown,
        textAlign: 'center',
        paddingLeft: 10,
        paddingRight: 10
    }, modalStyel: {
        flex: 1, backgroundColor: res.colors.butterscotch, marginTop: 10, borderRadius: 30, overflow: 'hidden'
    }, specificationText: {
        fontFamily: res.fonts.medium,
        fontSize: 14,
        color: res.colors.greyishBrown,
        textAlign: 'center',
        top: 10,

        paddingLeft: 10,
        paddingRight: 10
    }, specificationTextValue: {
        fontFamily: res.fonts.regular,
        fontSize: 10,
        color: res.colors.greyishBrown,
        textAlign: 'center',
        top: 10,
        paddingLeft: 10,
        paddingRight: 10,
        paddingBottom: 10
    }, UnitLevel: {
        flex: 1,
        // minHeight: 110,
        marginHorizontal: 10,
        backgroundColor: res.colors.butterscotch,
        marginTop: 10,
        borderRadius: 5
    }, ProjectApprovedModal: {
        flex: 1,
    }, consultentModal: {
        flex: 1, backgroundColor: res.colors.butterscotch, borderRadius: 30, marginHorizontal: 10
    }, bankList: {

        margin: 5,
        backgroundColor: res.colors.butterscotch,
        height: 104,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 5,
        flexDirection: 'column'
    }, sitePlan: {
        flex: 1, backgroundColor: res.colors.butterscotch, borderTopLeftRadius: 30, borderTopRightRadius: 30, marginTop: 10, marginHorizontal: 10
    },
    specificationvalueText: {
        fontFamily: res.fonts.medium,
        fontSize: 11,
        color: res.colors.greyishBrown,
        textAlign: 'center',
        top: 10,

        paddingLeft: 10,
        paddingRight: 10
    },
    jointVenuetext: {
        fontWeight: "bold",
        fontFamily: res.fonts.semiBold,
        fontSize: 14,
        color: res.colors.greyishBrown,
        textAlign: 'center',
        marginVertical: 15
    },
    closeBtn: {
        position: "absolute",
        top: 30,
        right: 30,
        backgroundColor: res.colors.black,
        zIndex: 999,
        width: 30,
        height: 30,
        borderRadius: 15,
        alignItems: 'center',
        justifyContent: 'center'
    },
    brochureModalStyle: {
        flex: 1, zIndex: 99, marginTop: 10, alignItems: 'center', marginHorizontal: 20
    },
    consultentTextStyle: {
        color: res.colors.liteBlack, fontFamily: res.fonts.regular
    },
    consultentOtherTextStyle: {
       
        // marginLeft: 12,
        color: res.colors.liteBlack,
        fontFamily: res.fonts.regular
    },
    consultentWebTextStyle: {
        // marginLeft: 12,
        color: 'rgba(29,29,29,0.5)',
        fontFamily: res.fonts.regular
    },
    complianceView: {

        minHeight: 135,
        marginHorizontal: 10,
        backgroundColor: res.colors.trasparents,
        // marginTop: 10,
        borderRadius: 5,
        marginBottom: heightScale(10)
    },
    reraRegisterText: {
        fontFamily: res.fonts.medium,
        fontSize: 14,
        color: res.colors.greyishBrown,
        // marginTop: 10,

    },
    reraProjectText: {
        fontFamily: res.fonts.regular,
        fontSize: 12,
        color: res.colors.liteBlack,
        letterSpacing: 0.13

    },
    reraNumberText: {
        fontFamily: res.fonts.regular,
        fontSize: 12,
        color: 'rgba(29,29,29,0.5)',
        letterSpacing: 0.13,
        marginTop: 5
    }, clipBoardView: {
        flex: 1,
        flexDirection: 'row',
        height: 30,
        borderWidth: 1,
        borderColor: res.colors.shadowColor,
        borderRadius: 5,
        alignItems: 'center',
        justifyContent: 'space-between',
        padding: 5,
        marginVertical: 5,
        // backgroundColor:'red'
    }, copyText: {
        fontFamily: res.fonts.medium,
        fontSize: 12,
        color: res.colors.peacockBlue,
        letterSpacing: 0.13
    }, couponText: {
        fontFamily: res.fonts.regular,
        fontSize: 12,
        color: 'rgba(29,29,29,0.5)',
        letterSpacing: 0.13
    }, LoactionHeading: {
        fontFamily: res.fonts.bold,
        fontSize: 16,
        color: res.colors.peacockBlue,
        letterSpacing: 0.13
    }, locationText: {
        fontFamily: res.fonts.medium,
        fontSize: 14,
        color: res.colors.greyishBrown,

    }, map: {
        width: "100%",
        height: heightScale(250)
    }, AmenitiesmodalStyel: {
        flex: 1, backgroundColor: res.colors.butterscotch, marginTop: 10, borderRadius: 30
    }, CheckView: {
        flexDirection: 'row',
        justifyContent: 'space-between',

        alignItems: 'center',
        marginTop: heightScale(10)
    }, dataContainer: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        width: '100%',
        justifyContent: 'space-evenly'
    }, container: {
        flex: 1,
        top: Platform.OS == 'ios' ? 100 : 100,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        backgroundColor: res.colors.butterscotch
    }, shadowHeader: {
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        backgroundColor: res.colors.butterscotch,
        shadowColor: res.colors.shadowColor,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 10.58,
        shadowRadius: 10.00,
        elevation: 24,
    }, corousalNameText: {
        fontFamily: res.fonts.semiBold,
        fontSize: 18,
        letterSpacing: 0.3,
        textAlign: 'center',
        color: res.colors.greyishBrown,
        marginTop: 10
    }, viewMoreText: {
        fontWeight: 'bold',
        color: res.colors.peacockBlue,
        fontFamily: res.fonts.semiBold,
        fontSize: 14
    }, ShareBtn: {
        position: 'absolute',
        bottom: 30,
        right: 30,
        backgroundColor:res.colors.black,
        borderRadius:25,
        zIndex: 9999999,
        width:50,
        height:50,
        overflow:'hidden',
        justifyContent:'center',
        alignItems:'center'
      },shareView:{
          backgroundColor:res.colors.white,
          borderRadius:15,
          width:30,
          height:30
      }
})
export default styles