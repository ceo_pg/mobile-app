import React, { Component } from "react";
import { View, Text, Image, TouchableOpacity, TextInput, StyleSheet, FlatList, Keyboard, SafeAreaView, Linking } from "react-native";

import res from '../../../../res'
import styles from './styles'
import { heightScale, getPastProject } from "../../../utility/Utils";
// import WebView from 'react-native-webview'


class Consultants extends Component {

    constructor(props) {
        super(props);

        this.state = {

        };
    }
    componentDidMount() {


    }

    openWebSite = (url) => {
        Linking.canOpenURL(url).then(supported => {
            if (supported) {
                Linking.openURL(url);
            } else {
                console.log("Don't know how to open URI: " + url);
            }
        });
    }



    render() {

        return (
            <View style={styles.modalStyel}>
                <FlatList
                    scrollEnabled={true}
                    data={this.props.data}
                    keyExtractor={(item, index) => index.toString()}
                    renderItem={({ item, index }) => (
                        index <= 2 ?
                            < View style={[styles.UnitLevel, styles.shadow]}>
                                <Text style={{ marginLeft: 20, marginVertical: 13, color: res.colors.greyishBrown }}>{item.consultantType}</Text>

                                <View style={{ flexDirection: 'row', marginHorizontal: 20, flex: 1 }}>
                                    <View style={{ justifyContent: 'center', marginBottom: heightScale(5) }}>
                                        <Image style={{ width: 42, height: 42 }} source={{ uri: item.url }} resizeMode={'contain'} />
                                    </View>
                                    <View style={{ flexDirection: 'column', width: '80%', marginLeft: 20, justifyContent: 'center', marginVertical: 20, flex: 1 }}>
                                        {
                                            item.consultantName ?
                                                <View>
                                                    <Text style={styles.consultentTextStyle}>{item.consultantName ? item.consultantName : "NA"}</Text>
                                                </View> :
                                                null
                                        }

                                        <View>
                                            {
                                                item.website ?
                                                    <TouchableOpacity onPress={() => this.openWebSite(item.website)}>
                                                        <Text style={styles.consultentWebTextStyle}>{item.website ? item.website : ""}</Text>
                                                    </TouchableOpacity> : null
                                            }

                                        </View>
                                        <View >
                                           
                                            {
                                                item.pastProjects.length > 0 &&
                                                <Text numberOfLines={2} style={styles.consultentOtherTextStyle}>{getPastProject(item.pastProjects)}</Text>

                                            }
                                           

                                        </View>


                                    </View>

                                </View>

                            </View> : <View />
                    )}

                />
            </View >
        );
    }
}

export default Consultants;