import React, { useState } from 'react'
import { Text, View, TouchableOpacity, Image, FlatList, SafeAreaView } from 'react-native'
import res from '../../../../res'
import styles from './styles'
import style from '../../../components/button/styles'
import CardWithTitle from '../../../components/card/CardWithTitle'
import CardWithImages from '../../../components/card/CardWithImages'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { widthScale } from '../../../utility/Utils'
const ProjectOwnerShip = (props) => {
    //  console.log("ProjectOwnerShip", props)
    return (
        < SafeAreaView style={styles.ProjectApprovedModal} >

            {
                props.projectOwnershipStructure.developer && props.projectOwnershipStructure.developer.length > 0 ?
                    <FlatList
                        // style={{ marginHorizontal: 20 }}
                        numColumns={2}
                        data={props.projectOwnershipStructure.developer}
                        keyExtractor={(item, index) => index.toString()}
                        ListHeaderComponent={(item, index) => {
                            return <Text style={styles.jointVenuetext}>{props.projectOwnershipStructure.ownerShip}</Text>
                        }}
                        renderItem={({ item, index }) => {

                            return <CardWithImages
                                src={item.image ? { uri: item.image } : ""}
                            />
                        }}

                    /> :
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                        <Text>No Data Found</Text>
                    </View>
            }

        </SafeAreaView >
    )
}
const Amenitieslist = (props) => {
    // console.log("Amenitieslist", props.data)
    return <SafeAreaView style={styles.AmenitiesmodalStyel}>


        <FlatList
            scrollEnabled={true}
            style={{ marginHorizontal: 20 }}
            numColumns={2}
            data={props.data}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({ item, index }) => (
                index <= 5 ?
                    <View style={[styles.amenitiesList, styles.shadow]}>
                        {/* <Text>{item.fileName}</Text> */}
                        <Image style={{ width: widthScale(40), height: widthScale(40) }} source={item.url ? { uri: item.url } : {}} resizeMode={'contain'} />
                        <Text style={styles.amenitiesText}>{item.name}</Text>
                    </View> : <View />
            )}

        />
    </SafeAreaView >

}
const Consultants = (props) => {
    // console.log("Consultants", props.data)
    return <SafeAreaView style={styles.consultentModal}>


        <FlatList
            data={props.data}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({ item, index }) => (

                <View style={[styles.UnitLevel, style.shadow]}>
                    <Text style={{ marginLeft: 10, marginTop: 13, color: res.colors.greyishBrown }}>{item.consultantType}</Text>

                    <View style={{ flexDirection: 'row', marginTop: 10 }}>

                    </View>
                    <View style={{ flexDirection: 'row', marginHorizontal: 10 }}>
                        <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                            <Image style={{ width: 40, height: 36 }} source={{ uri: item.url }} resizeMode={'center'} />

                        </View>
                        <View>
                            <Text style={{ marginLeft: 12, marginTop: 13, color: res.colors.liteBlack }}>{item.consultantName}</Text>
                        </View>

                    </View>

                </View>
            )}

        />
    </SafeAreaView >

}
const ProjectInfo = (props) => {

    return <SafeAreaView style={styles.modalStyel} >

        <Image source={{ uri: props.brochure }} style={{ flex: 1, width: '100%', borderRadius: 10, height: '100%' }} resizeMode={'cover'} />

    </SafeAreaView >

}
const SitePlan = (props) => {

    return (
        <TouchableOpacity style={styles.sitePlan} >
            <Image source={res.images.parking} style={{ flex: 1, width: '100%', borderRadius: 10, height: '100%', paddingLeft: 10 }} resizeMode={'cover'} />
        </TouchableOpacity>

    )
}
const HomeLoanApprovedBy = (props) => {

    return (
        < SafeAreaView style={styles.ProjectApprovedModal} >
            <FlatList
                numColumns={2}
                data={props.data}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({ item, index }) => {

                    return <CardWithImages

                        src={item.url ? { uri: item.url } : ""}

                    />
                }}

            />
        </SafeAreaView >
    )
}
const Project_Specifications = (props) => {

    const projectSpecification = props && props.data ? props.data : {}
    // console.log("projectSpecification", projectSpecification);

    return < SafeAreaView style={styles.modalStyel} >
        <KeyboardAwareScrollView>
            <View style={styles.dataContainer}>
                {
                    projectSpecification.pipedGas == true ?
                        <CardWithTitle
                            isProjectSpecs={true}
                            src={res.images.pipeline}
                            title={"PipedGas"}
                        />
                        :

                        <View />
                }
                <CardWithTitle
                    isProjectSpecs={true}
                    src={res.images.battery}
                    title={"Power Back up"}
                    Value={projectSpecification.powerBackupCapacity}
                />




                {
                    projectSpecification.pipedGas == true ?
                        <CardWithTitle
                            isProjectSpecs={true}
                            src={res.images.blueprint}
                            title={"Structure"}
                            Value={projectSpecification.structure}
                        /> :

                        <View />
                }
                <CardWithTitle
                    isProjectSpecs={true}
                    src={res.images.fireExtinguisher}
                    title={"Fire Fighting"}
                    Value={projectSpecification.fireFighting}
                />



                {
                    projectSpecification.waterSoftener == true ?
                        <CardWithTitle
                            isProjectSpecs={true}
                            src={res.images.plumber}
                            title={"Water Softner"}
                        />
                        : <View />
                }


                {
                    projectSpecification.solarPower == true ?
                        <CardWithTitle
                            isProjectSpecs={true}
                            src={res.images.solarPanel}
                            title={"Solar Power"}
                        />
                        :
                        <View />
                }




                <CardWithTitle
                    isProjectSpecs={true}
                    src={res.images.facade}
                    title={"Facade"}
                    Value={projectSpecification.facade}
                />

                <CardWithTitle
                    imageStyle={{ opacity: 0.7 }}
                    isProjectSpecs={true}
                    src={res.images.water}
                    title={"Water Source"}
                    Value={projectSpecification.waterSource}
                />
                <CardWithTitle
                    isShodow={true}
                    isProjectSpecs={true}
                // src={res.images.water}
                // title={"Water Source"}
                // Value={projectSpecification.waterSource}
                />
            </View>
        </KeyboardAwareScrollView>

    </SafeAreaView >
}
export { Amenitieslist, ProjectInfo, Project_Specifications, SitePlan, Consultants, HomeLoanApprovedBy, ProjectOwnerShip }