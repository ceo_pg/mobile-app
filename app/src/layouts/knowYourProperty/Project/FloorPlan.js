import React, { Component } from 'react'
import { View, SafeAreaView, TouchableOpacity, Image, FlatList, Text, Dimensions, Linking } from 'react-native'
import styles from './styles'
import res from '../../../../res'

import ImageZoom from 'react-native-image-pan-zoom';
import Modal from 'react-native-modal'
class FloorPlan extends Component {
    constructor(props) {
        super(props)
        this.modalRef = React.createRef()
        this.state = {
            isModalVisible: false,
            parking: ""
        }

    }
    componentDidMount() {
        // console.log("COMPONENT DID MOUNT")
        this.setState({
            parking: this.props.parkingImage
        })
    }
    ShareImage = (url) => {
        Linking.canOpenURL(url).then(supported => {
            if (supported) {
                Linking.openURL(url);
            } else {
                console.log("Don't know how to open URI: " + url);
            }
        });
    }
    renderSitePlanImages = ({ item, index }) => {

        return (
            <View style={{ flex: 1 }}>
                <View style={{ width: "100%", height: "100%", zIndex: 9999, overflow: 'hidden' }}>

                    <ImageZoom cropWidth={Dimensions.get('window').width}
                        cropHeight={Dimensions.get('window').height}
                        imageWidth={Dimensions.get('window').width}
                        imageHeight={Dimensions.get('window').height}>

                        <Image source={item.fileName ? { uri: item.fileName } : res.images.parking} style={{ flex: 1 }} resizeMode={'center'} />

                    </ImageZoom>
                </View>
                {
                    item.fileName ?
                        <TouchableOpacity style={styles.ShareBtn} onPress={() => this.ShareImage(item.fileName)}>
                            <View style={styles.shareView}>
                                <Image source={res.images.icn_share} />
                            </View>

                        </TouchableOpacity> : null
                }
            </View>



        )
    }
    modalView = () => {
        return (
            <View style={{ borderRadius: 10 }}>
                <View style={styles.closeBtn}>
                    <TouchableOpacity onPress={() => this.toggleModal()}>
                        <Image source={res.images.icnClose} style={{ width: 20, height: 20, tintColor: res.colors.white }} resizeMode={'center'} />
                    </TouchableOpacity>
                </View>

                <FlatList
                    pagingEnabled={true}
                    bounces={false}
                    style={{ marginHorizontal: - 20 }}
                    horizontal={true}
                    data={this.props.sitePlan}
                    renderItem={this.renderSitePlanImages}
                />
            </View>



        )

        // return <ParkingSlotModal
        //     parking={this.props.parkingImage}
        //     toggleModal={this.toggleModal}
        // />
    }
    toggleModal = () => {

        this.setState({
            isModalVisible: !this.state.isModalVisible
        })

    };
    render() {
        const sitePlan = this.props && this.props.sitePlan && this.props.sitePlan[0].fileName ? this.props.sitePlan[0].fileName : []
        // console.log("sitePlan====>", sitePlan)
        return (< SafeAreaView style={styles.brochureModalStyle} >
            <Modal
                style={{ borderRadius: 10 }}
                isVisible={this.state.isModalVisible}
            >

                {this.modalView()}
            </Modal>
            <TouchableOpacity onPress={() => this.toggleModal()} style={{ flex: 1, width: '100%', borderRadius: 10, height: '100%' }}>
                <Image source={sitePlan ? { uri: sitePlan } : res.images.parking} style={{ flex: 1, width: '100%', borderRadius: 10, height: '100%' }} resizeMode={'cover'} />
            </TouchableOpacity>
            {
                this.props.sitePlan.length > 0 ?
                    <TouchableOpacity onPress={() => this.toggleModal()} style={{ height: 30, justifyContent: 'center' }} >
                        <Text style={styles.viewMoreText}>View More</Text>
                    </TouchableOpacity> : <View />
            }






        </SafeAreaView >

        )


    }
}
export default FloorPlan