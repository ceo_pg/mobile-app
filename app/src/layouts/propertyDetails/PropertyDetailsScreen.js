import React, { Component } from 'react';
import { View, Text, ImageBackground, Alert, SafeAreaView } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import AsyncStorage from '@react-native-community/async-storage'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

import styles from './styles';
import res from '../../../res';
import { PGButton, ModalButton, Modal, SearchModal, DeveloperModal, ProjectModal, TowerModal, FloorModal, UnitModal, Loader } from '../../components/index'
import { UNIT_DETAIL_CREATE_ENDPOINTS, CITY_LIST_ENDPOINTS, LIST_DEVELOPER_ID_ENDPOINTS, LIST_BY_CITY_ENDPOINTS, PROJECT_BY_ID_ENDPOINTS } from '../../apiManager/ApiEndpoints';
import ApiFetch from '../../apiManager/ApiFetch';
import HeaderAndStatusBar from '../../components/header/Header'

class PropertyDetailsScreen extends Component {
    constructor(props) {
        super(props);
        this.isBack = this.props.route.params && this.props.route.params.isBack;
        this.modalRef = React.createRef();
        this.cityInputRef = React.createRef();
        this.developerNameInputRef = React.createRef();
        this.projectNameInputRef = React.createRef();
        this.towerInputRef = React.createRef();
        this.floorNameInputRef = React.createRef();
        this.unitNoInputRef = React.createRef();
        this.state = {
            cityName: "",
            error: {},
            list: [],
            developerList: [],
            developerName: "",
            developer_id: "",
            projectList: [],
            projectName: "",
            project_id: "",
            towerList: [],
            towerName: "",
            tower_id: "",
            floorList: [],
            floor_id: "",
            floorName: "",
            unitList: [],
            unitName: "",
            unit_id: "",
            isLoading: false,
            selectedTowerIndex: 0
        }
    }
    componentDidMount() {

    }
    clearValue = () => {
        this.setState({

            developerName: "",
            developer_id: "",

            projectName: "",
            project_id: "",

            towerName: "",
            tower_id: "",

            floor_id: "",
            floorName: "",

            unitName: "",
            unit_id: "",
        })
    }
    cityLisApi = () => {
        this.setState({ isLoading: true });
        ApiFetch.fetchGet(CITY_LIST_ENDPOINTS)
            .then((data) => {
                if (data.data.length != 0) {
                    this.setState({
                        list: data.data,
                        isLoading: false
                    })
                    this.modalRef.current.setView(this.renderSearchModal);
                    this.modalRef.current.show();
                } else {
                    this.setState({ isLoading: false });
                    alert("No City Found")
                }

            }).catch((err) => {
                console.log(err)
            })
    }
    developerListApi = () => {
        const { cityName } = this.state
        this.setState({ isLoading: true });
        ApiFetch.fetchGet(LIST_BY_CITY_ENDPOINTS + "/" + cityName)
            .then((res) => {

                if (res.statusCode == 200) {
                    this.setState({

                        isLoading: false,
                        developerList: res.data
                    })
                    this.modalRef.current.setView(this.renderDeveloperModal);
                    this.modalRef.current.show();
                } else {
                    Alert.alert(
                        'Developer List Not Found',
                        'Please Enter City Name',
                        [
                            {
                                text: 'Cancel',
                                onPress: () => this.setState({ isLoading: false }),
                                style: 'cancel',
                            },
                            { text: 'OK', onPress: () => this.setState({ isLoading: false }) },
                        ]
                    );

                    // this.setState({ isLoading: false });
                }

            }).catch((err) => {
                this.setState({ isLoading: false });
                alert("Please choose city")
                console.log("error inside developer", err)
            })
    }
    projectNameApi = () => {

        const perPage = 10
        const pageNo = 10
        const { developer_id, cityName } = this.state

        this.setState({ isLoading: true });
        ApiFetch.fetchGet(LIST_DEVELOPER_ID_ENDPOINTS + "/" + developer_id + "?perPage=" + perPage + "& pageNo=" + pageNo + "& sort=" + {} + "& city=" + cityName + " & developerID=" + developer_id)
            .then((res) => {
                // console.log("DEVELOPER LIST", res.data.length)//
                if (res.statusCode == 200) {
                    this.setState({
                        isLoading: false,
                        projectList: res.data
                    })
                    this.modalRef.current.setView(this.renderProjectModal);
                    this.modalRef.current.show();
                } else {
                    Alert.alert(
                        'Project List Not Found',
                        'Please Enter Above details',
                        [
                            {
                                text: 'Cancel',
                                onPress: () => this.setState({ isLoading: false }),
                                style: 'cancel',
                            },
                            { text: 'OK', onPress: () => this.setState({ isLoading: false }) },
                        ]
                    );
                    this.setState({ isLoading: false });
                    alert("No Project Found")
                }

            }).catch((err) => {
                this.setState({ isLoading: false });
                // alert("Please choose above data")
                console.log(err)
            })
    }
    validateBeforeProjectEnter = () => {

        const { cityName, developerName } = this.state
        console.log("=====>", cityName, developerName)
        if (cityName == "" && developerName == "") {
            alert("Enter Above data first")
        }
    }
    validateBeforeTowerEnter = () => {
        const { cityName, developerName, projectName } = this.state

        if (cityName == "" && developerName == "" && projectName == "") {
            alert("Enter Above data first")
        }
    }
    hitProjectInfoApi = () => {
        const { project_id } = this.state

        this.setState({ isLoading: true });
        ApiFetch.fetchGet(PROJECT_BY_ID_ENDPOINTS + "/" + project_id)
            .then((res) => {
                console.log("LENGTH", res.data.listOfTowers)
                if (res.data.listOfTowers.length != 0) {
                    console.log(res.data.listOfTowers)
                    this.setState({
                        isLoading: false,
                        towerList: res.data.listOfTowers,
                    })
                    this.modalRef.current.setView(this.renderTowerModal);
                    this.modalRef.current.show();
                } else {
                    Alert.alert(
                        'Tower List Not Found',
                        'Please Enter valid data',
                        [
                            {
                                text: 'Cancel',
                                onPress: () => this.setState({ isLoading: false }),
                                style: 'cancel',
                            },
                            { text: 'OK', onPress: () => this.setState({ isLoading: false }) },
                        ]
                    );
                    // this.setState({ isLoading: false });
                    // alert("No Tower data found")
                }
            }).catch((err) => {
                this.setState({ isLoading: false });
                // alert("Please choose above data")
                console.log(err)
            })
    }

    moveToKnowYourProperty = () => {
        this.props.navigation.navigate("DashBoard")
    }
    saveToAsyncStorage = async (developer_id, project_id, tower_id, floor_id, unit_id) => {
        const DEVELOPERID = [res.AsyncStorageContants.DEVELOPERID, developer_id];
        const PROJECTD = [res.AsyncStorageContants.PROJECTD, project_id];
        const TOWERID = [res.AsyncStorageContants.TOWERID, tower_id]
        const FLOORID = [res.AsyncStorageContants.FLOORID, floor_id];
        const UNITID = [res.AsyncStorageContants.UNITID, unit_id]
        const UNITASSIGN = [res.AsyncStorageContants.UNITASSIGN, JSON.stringify("true")]

        try {
            await AsyncStorage.multiSet([DEVELOPERID, PROJECTD, TOWERID, FLOORID, UNITID, UNITASSIGN])
            this.moveToKnowYourProperty()
        } catch (e) {
            console.log("Error saving user details", e);
        }
    }
    submitPropertyDetails = async () => {

        const { cityName, developer_id, project_id, tower_id, floor_id, unit_id, } = this.state
        console.log("Property details screen", cityName, developer_id, project_id, tower_id, floor_id, unit_id)
        const parameters = JSON.stringify({
            city: cityName,
            developerID: developer_id,
            projectID: project_id,
            towerID: tower_id,
            floorID: floor_id,
            unitID: unit_id
        })
        this.setState({ isLoading: true });
        ApiFetch.fetchPost(UNIT_DETAIL_CREATE_ENDPOINTS, parameters)
            .then((data) => {
                if (data.statusCode == 200) {
                    this.saveToAsyncStorage(developer_id, project_id, tower_id, floor_id, unit_id,)
                    this.setState({ isLoading: false });

                    alert(data.message)
                } else {
                    Alert.alert(
                        'Enter Valid data',
                        data.message,
                        [
                            {
                                text: 'Cancel',
                                onPress: () => this.setState({ isLoading: false }),
                                style: 'cancel',
                            },
                            { text: 'OK', onPress: () => this.setState({ isLoading: false }) },
                        ]
                    );
                    // this.setState({ isLoading: false });
                    // alert(data.message)

                }

            }).catch((err) => {
                alert(err)
            })
    }

    back = () => {

        this.props.navigation.goBack()
    }
    closeRenderModal = () => {
        this.modalRef.current.hide();
    };
    onCancelModalPress = (text) => {
        console.log("Developer", text,)
        this.setState({
            cityName: text,
            developerName: "",
            projectName: "",
            towerName: "",
            floorName: "",
            unitName: ""
        })
        this.clearValue()
        this.modalRef.current.hide();
    }
    onCloseDeveloperModal = (name, _id) => {
        console.log("Developer", name, _id)
        this.setState({
            developerName: name,
            developer_id: _id,
            projectName: "",
            towerName: "",
            floorName: "",
            unitName: ""
        })
        this.modalRef.current.hide();
    }
    onCloseProjectModal = (name, _id) => {
        console.log("project", name, _id)
        this.setState({
            projectName: name,
            project_id: _id,
            towerName: "",
            floorName: "",
            unitName: ""
        })
        this.modalRef.current.hide();
    }
    onCloseTowerModal = (name, _id, towerIndex) => {
        console.log("tower", name, _id, towerIndex)
        this.setState({
            towerName: name,
            tower_id: _id,
            floorName: "",
            unitName: "",
            selectedTowerIndex: towerIndex,
            towerList: this.state.towerList[towerIndex]

        })
        this.modalRef.current.hide();
    }
    onCloseFloorModal = (name, _id, floorIndex) => {
        console.log("Floor", name, _id)
        this.setState({
            floorName: name,
            floor_id: _id,
            unitName: "",
            floorList: this.state.floorList[floorIndex]
        })
        this.modalRef.current.hide();
    }
    onCloseUnitModal = (name, _id) => {
        console.log("unit", name, _id)
        this.setState({
            unitName: name,
            unit_id: _id
        })
        this.modalRef.current.hide();
    }
    renderSearchModal = () => {
        return (
            <SearchModal

                list={this.state.list}
                closeModal={this.closeRenderModal}
                onCancelModalPress={this.onCancelModalPress}
            />
        );
    }
    renderDeveloperModal = (key) => {
        return (<DeveloperModal
            key={key}
            list={this.state.developerList}
            closeModal={this.closeRenderModal}
            onCancelModalPress={this.onCloseDeveloperModal}
        />)
    }
    renderProjectModal = () => {
        return (<ProjectModal

            list={this.state.projectList}
            closeModal={this.closeRenderModal}
            onCancelModalPress={this.onCloseProjectModal}
        />)
    }
    renderTowerModal = () => {
        return (<TowerModal

            list={this.state.towerList}
            closeModal={this.closeRenderModal}
            onCancelModalPress={this.onCloseTowerModal}
        />)
    }
    renderFloorModal = () => {
        return (<FloorModal
            list={this.state.floorList}
            closeModal={this.closeRenderModal}
            onCancelModalPress={this.onCloseFloorModal}
        />)
    }
    renderUnitModal = () => {

        return (<UnitModal

            list={this.state.unitList}
            closeModal={this.closeRenderModal}
            onCancelModalPress={this.onCloseUnitModal}
        />)
    }
    openFloorModal = () => {
        console.log("this.state.towerList.floorsList", this.state.towerList.floorsList)
        if (this.state.towerList.length != 0) {
            this.setState({
                isLoading: true,
                floorList: this.state.towerList.floorsList,
                isLoading: false,
            });

            if (this.state.towerList.floorsList.length > 0) {
                this.modalRef.current.setView(this.renderFloorModal);
                this.modalRef.current.show();
            } else {
                alert("No Floors data found")
            }

        } else {
            alert("No Floors data found")
        }

    }
    openUnitModal = () => {
        console.log("openUnitModal", this.state.floorList.unitsData)
        if (this.state.floorList.length != 0) {
            this.setState({
                isLoading: true,
                unitList: this.state.floorList.unitsData,
                isLoading: false,

            });
            if (this.state.floorList.unitsData.length > 0) {
                this.modalRef.current.setView(this.renderUnitModal);
                this.modalRef.current.show();
            } else {
                alert("No Unit data found")
            }

        } else {
            alert("No Unit data found")
        }

    }
    openCityModal = (list) => {

        this.cityLisApi()
    }
    openDeveloperModal = () => {
        this.developerListApi()
    }
    openProjectNamemodal = () => {
        this.projectNameApi()
    }
    openTowerModal = () => {

        this.hitProjectInfoApi()
    }
    renderLoader = () => {
        return (
            <Loader
                loading={this.state.isLoading} />
        );
    };
    renderView = () => {
        const { projectName, towerName, floorName, unitName, cityName, developerName } = this.state
        console.log("floorName", JSON.stringify(floorName))
        return <View style={{ marginHorizontal: 30, marginTop: 40 }}>
            <Text style={styles.propertDetailsText}>{res.strings.PROPERT_DETAILS}</Text>
            <Text style={styles.instructionText}>{res.strings.PROPERT_INSTRUCTION} </Text>
            <View style={{ marginTop: -20 }}>

                <ModalButton
                    labelName={"City"}
                    btnText={cityName}
                    onPress={() => this.openCityModal()}
                    btnStyle={{ backgroundColor: 'transparent' }}
                />
                <ModalButton
                    labelName={"Developer"}
                    btnText={developerName}
                    onPress={() => this.openDeveloperModal()}
                    btnStyle={{ backgroundColor: 'transparent' }}
                />
                <ModalButton
                    labelName={"Project"}
                    btnText={projectName}
                    onPress={() => this.openProjectNamemodal()}
                    btnStyle={{ backgroundColor: 'transparent' }}
                />
                <ModalButton
                    labelName={"Tower"}
                    btnText={towerName}
                    onPress={() => this.openTowerModal()}
                    btnStyle={{ backgroundColor: 'transparent' }}
                    btnStyle={{ backgroundColor: 'transparent' }}
                />
                <ModalButton
                    labelName={"Floor"}
                    btnText={floorName}
                    onPress={() => this.openFloorModal()}
                    btnStyle={{ backgroundColor: 'transparent' }}
                />
                <ModalButton
                    labelName={"Unit"}
                    btnText={unitName}
                    onPress={() => this.openUnitModal()}
                    btnStyle={{ backgroundColor: 'transparent' }}
                />

                <View style={{ marginVertical: 40 }}>
                    <PGButton

                        btnText={res.strings.SUBMIT}
                        onPress={() => this.submitPropertyDetails()}
                    />
                </View>
            </View>



        </View>

    }
    render() {
        const { isLoading } = this.state;
        return (
            <SafeAreaView style={[styles.mainContainer]}>
                {
                    this.isBack ?
                        <HeaderAndStatusBar
                            isDrawerIconVisible={false}
                            onBackClick={() => this.back()}
                        /> :
                        null
                }

                <View style={[styles.container]}>
                    <View style={styles.imageContainer}>
                        <ImageBackground
                            source={res.images.testImage}
                            style={styles.imageBack}
                        />
                    </View>
                    <View style={[styles.roundedContaier, styles.shadow]}>
                        <KeyboardAwareScrollView showsVerticalScrollIndicator={false}>

                            {this.renderView()}
                        </KeyboardAwareScrollView>
                        {/* <Text style={{width:'100%', textAlign:'center', height: 20,color:res.colors.appColor}}>New Delhi</Text> */}

                    </View>
                    <Modal ref={this.modalRef} show={false} />
                    {isLoading && this.renderLoader()}
                </View>


            </SafeAreaView>


        );
    }
}
const mapStateToProps = (state) => {
    return {};
};

function mapDispatchToProps(dispatch) {
    return bindActionCreators({

    }, dispatch);
}
let PropertyDetailsScreenContainer = connect(mapStateToProps, mapDispatchToProps)(PropertyDetailsScreen);
export default PropertyDetailsScreenContainer;

