import { StyleSheet,Platform ,StatusBar} from 'react-native';
import resources from '../../../res';
const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        backgroundColor: resources.colors.white,
        paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,

    }, inputCointainer: {
        flex: 1,
        borderTopLeftRadius: 30,
        borderTopRightRadius: 30,
        // backgroundColor:'red'
    },
    container: {
        flex: 1,
        justifyContent: 'center',
        // marginTop: Platform.OS == 'ios' ? 10 : 20,
        // alignItems: 'center'
    },
    content: {
        flexDirection: "column",
        justifyContent: "flex-start",
        alignItems: "center",
    }, inputCont: {
        // borderBottomWidth: 1,
        borderRadius: 4,
        borderColor: resources.colors.black,
        // paddingHorizontal: 20,

    }, errorUnderLine: {
        borderColor: "red",
    }, imageBack: {
        // marginTop:44,
        height: 216,
        width: '100%',
        top: -15
    }, inputFieldView: {

        borderTopLeftRadius: 22,
        borderTopRightRadius: 22,
        position: 'absolute',
        top: -30,
        left: 0,
        right: 0,
        bottom: 0,
        backgroundColor: resources.colors.white
    }, diffrenceField: {
        marginTop: 35
    }, forgotPassText: {
        fontFamily: resources.fonts.regular,
        fontSize: 12,
        color: resources.colors.btnBlue,
        marginVertical: 40,
        alignSelf: 'center',
        textAlign: 'center'
    }, instructionText: {
        fontFamily: resources.fonts.regular,
        fontSize: 14,
        color: resources.colors.warmGrey,
        marginTop:10
    }, propertDetailsText: {
        fontFamily: resources.fonts.semiBold,
        fontSize: 24,
        color: resources.colors.slate
    }, containerLoaderStyle: {
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        backgroundColor: "rgba(0,0,0,0.6)",
        justifyContent: 'center',
        alignItems: 'center'
    },shadow: {
        shadowColor: resources.colors.shadowColor,
        shadowOffset: {
            width: 0,
            height: -10,
        },
        shadowOpacity: 20.39,
        shadowRadius: 10.30,

        elevation: 13,
        zIndex: 10
    }, imageContainer: {
        height: 216,
        width: '100%',
        top: 10,
        backgroundColor: "white",
        justifyContent: "center",
        alignItems: "center",
    }, roundedContaier: {
        borderTopRightRadius: 25,
        borderTopLeftRadius: 25,
        marginTop: -18,
        flex:1,
        // alignItems: 'center',
        backgroundColor: resources.colors.butterscotch,
    },view: {
        justifyContent: 'flex-end',
        margin: 0,
      },

});

export default styles;
