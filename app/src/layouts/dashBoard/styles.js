import { StyleSheet, StatusBar } from 'react-native'
import res from '../../../res'
import { widthScale, heightScale } from '../../utility/Utils'
const styles = StyleSheet.create({
  fullScreen: {
    flex: 1,
    backgroundColor: res.colors.appColor,
    paddingTop: Platform.OS === "android" ? StatusBar.currentHeight + 10 : 0
  }, companyDetailstyle: {
    marginHorizontal: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 26,
    alignItems:'center'
  }, companyLogo: {
    // height: heightScale(83),
    width: '70%',
    borderRadius: 5,
    // alignItems:'center',
    // backgroundColor:'red'
  }, remaingText: {
    color: res.colors.white,
    textAlign: 'center',
    fontFamily: res.fonts.bold,
    fontSize: 10,
    marginTop:10
  }, squreFitView: {
    marginHorizontal: 22,
    backgroundColor: res.colors.cobalt,
    minHeight: 72,
    borderRadius: 8,
    marginTop: 27,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around'
  }, squreHeading: {
    fontFamily: res.fonts.semiBold,
    fontSize: 10,
    color: res.colors.pinkishTan,
    // marginLeft: 10,
    textAlign: "center"

  }, squreSubHeading: {
    fontFamily: res.fonts.semiBold,
    fontSize: 14,
    color: res.colors.white,
    textAlign: 'center',
    marginTop: 5

  }, carouselData: {
  }, dotStyle: {
    // width: 10,
    // height: 10,
    borderRadius: 5,
    // marginHorizontal: 8,
    backgroundColor: res.colors.pinkishTan
  }, dotColor: {
    width: 10,
    height: 10,
    borderRadius: 5,
    marginHorizontal: -5,
    backgroundColor: res.colors.pinkishTan
  }, inactiveDotStyle: {
    width: 10,
    height: 10,
    borderRadius: 5,
    marginHorizontal: -5,
    backgroundColor: res.colors.pinkishTan
  }, shadowHeader: {
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    backgroundColor: res.colors.butterscotch,
    shadowColor: res.colors.shadowColor,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 10.58,
    shadowRadius: 10.00,
    elevation: 24,
  }, corousalNameText: {
    fontWeight: 'bold',
    fontFamily: res.fonts.semiBold,
    fontSize: 18,
    letterSpacing: 0.3,
    textAlign: 'center',
    color: res.colors.greyishBrown,
    marginTop: 10
  }, percentageText: {
    fontFamily: res.fonts.semiBold,
    fontSize: 10,
    color: res.colors.white,
    textAlign: 'center'
  }, halfCircleHeaderText: {
    color: res.colors.toupe,
    fontFamily: res.fonts.semiBold,
    fontSize: 10,
    marginBottom: 11,
    textAlign: 'center'
  }, viewMoreText: {
    fontWeight: 'bold',
    color: res.colors.peacockBlue,
    fontFamily: res.fonts.semiBold,
    fontSize: 14
  }, projectNameText: {
    fontFamily: res.fonts.semiBold,
    fontSize: 14,
    color: res.colors.white,
    textAlign: 'center',
    marginTop: heightScale(5)
  },
})
export default styles