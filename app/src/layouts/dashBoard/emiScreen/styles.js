import { StyleSheet } from 'react-native';
import res from '../../../../res';
const style = StyleSheet.create({

    shadow: {
        shadowColor: res.colors.shadowColor,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.5,
        shadowRadius: 2.84,

        elevation: 3,
    }, UnitLevel: {
        minHeight: 80,
        marginHorizontal: 10,
        backgroundColor: res.colors.butterscotch,
        marginTop: 10,
        borderRadius: 5,
        // alignItems: 'center',
        justifyContent: 'center',
        bottom:10
    }, valueStyle: {
        fontFamily: res.fonts.medium,
        fontSize: 18,
        color: res.colors.peacockBlue
    },headingStyle:{
        fontFamily: res.fonts.medium,
        fontSize: 14,
        color: res.colors.greyishBrown
    }
})
export default style;