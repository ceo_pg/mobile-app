import React, { Component } from 'react'
import { SafeAreaView, View, Text, Image, Dimensions, FlatList } from 'react-native'
import res from '../../../../res'
import styles from './styles'
import moment from 'moment';
import ImageWithTitle from '../../../components/card/ImageWithTitle'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
const EmiScreen = (props) => {
  
    return <SafeAreaView style={{ flex: 1, marginTop: 10,overflow:'hidden' }}>
        <KeyboardAwareScrollView contentContainerStyle={{overflow:'hidden'}}>
            <ImageWithTitle
                isShadow={true}
                src={res.images.calender}
                Value={props.emi}
                Heading={"EMI"}
            />
            <ImageWithTitle
                isShadow={true}
                src={res.images.nextdrawdowndate}
                Value={moment(props.data.dateOfEmi ? props.data.dateOfEmi : "21 Days", "YYYYMMDD").fromNow()}
                Heading={"Remaining for Next EMI "}
            />

            <ImageWithTitle
                isShadow={true}
                src={res.images.principalamount}
                Value={props.data.outstandingPrinciple ? props.data.outstandingPrinciple : ""}
                Heading={"Outstanding Principal"}
                unitText={"Rs "}
            />
            <ImageWithTitle
                isShadow={true}
                src={res.images.interestrate}
                Value={props.data.outstandingInterest ? props.data.outstandingInterest : ""}
                Heading={"Outstanding Interest"}
                unitText={"Rs "}
            />
        </KeyboardAwareScrollView>



    </SafeAreaView>







}
export { EmiScreen }