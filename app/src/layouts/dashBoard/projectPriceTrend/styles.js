import { StyleSheet } from 'react-native';
import res from '../../../../res';
const style = StyleSheet.create({

    shadow: {
        shadowColor: res.colors.shadowColor,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.5,
        shadowRadius: 2.84,

        elevation: 3,
    }, projectText: {
        fontFamily: res.fonts.medium,
        fontSize: 16,
        color: res.colors.greyishBrown
    }, dateText: {
        fontFamily: res.fonts.semiBold,
        fontSize: 16,
        color: res.colors.peacockBlue
    }, instructionText: {
        fontFamily: res.fonts.regular,
        fontSize: 14,
        color: res.colors.greyishBrown
    }, priceText: {
        fontFamily: res.fonts.regular,
        fontSize: 12,
        color: res.colors.greyishBrown,
        marginVertical: 10,
        marginLeft: 20
    },hintText:{
        fontFamily:res.fonts.regular,
        fontSize:12,
        color:res.colors.charcoalGrey,
        marginLeft:15,
        textAlign:'left'
    },modalStyle: {
        flex: 1,
        top: Platform.OS == 'ios' ? 100 : 50,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        backgroundColor: res.colors.butterscotch,

    },shadowHeader: {
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        backgroundColor: res.colors.butterscotch,
        shadowColor: res.colors.shadowColor,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 10.58,
        shadowRadius: 10.00,
        elevation: 24,
    }, corousalNameText: {
        fontFamily: res.fonts.semiBold,
        fontSize: 18,
        letterSpacing: 0.3,
        textAlign: 'center',
        color: res.colors.greyishBrown,
        marginTop: 10
    }
})
export default style;