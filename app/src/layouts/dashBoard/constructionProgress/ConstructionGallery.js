import React, { Component } from 'react'
import { SafeAreaView, View, Text, Image, Dimensions, FlatList, TouchableOpacity,Linking } from 'react-native'
import Modal from 'react-native-modal'
import ImageZoom from 'react-native-image-pan-zoom';
import moment from 'moment';

import res from '../../../../res'
import styles from './styles';
import { heightScale } from '../../../utility/Utils';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

class ConstructionGallery extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isGalleryVisible: false
        }
    }

    showFullGallery = () => {
        const { isGalleryVisible } = this.state
        this.setState({
            isGalleryVisible: !isGalleryVisible
        })
    }
   
    viewGallery = ({ item, index }) => {

        return (
            <View style={{ flex: 1 }}>
                <View style={{ flex: 1, zIndex: 9999, overflow: 'hidden' }}>
                    <ImageZoom


                        cropWidth={Dimensions.get('window').width}
                        cropHeight={Dimensions.get('window').height}
                        imageWidth={Dimensions.get('window').width}
                        imageHeight={Dimensions.get('window').height}>

                        <Image source={item.image ? { uri: item.image } : res.images.parking} style={{ width: "100%", height: "100%" }} resizeMode={'center'} />

                    </ImageZoom>
                </View>

                <View style={{ position: 'absolute', bottom: heightScale(30), right: 20, left: 20, zIndex: 10, overflow: 'hidden' }}>
                    <Text style={styles.dateModalText}>{item.date ? moment(item.date).format("DD MMMM,YYYY") : moment(Date).format("DD MMMM,YYYY")}</Text>
                    <Text style={styles.instructionModalText} textBreakStrategy={'simple'} numberOfLines={4}>{item.description}</Text>
                </View>

                
            </View>





        )
    }
    modalView = () => {
        return (

            <View style={{ flex: 1 }}>
                <View style={styles.closeBtn}>
                    <TouchableOpacity onPress={() => this.showFullGallery()}>
                        <Image source={res.images.icnClose} style={{ width: 20, height: 20, tintColor: res.colors.black }} resizeMode={'center'} />
                    </TouchableOpacity>
                </View>
                <FlatList
                    keyExtractor={(item, index) => index.toString()}
                    bounces={true}
                    style={{ marginHorizontal: -20 }}
                    horizontal
                    pagingEnabled={true}
                    data={this.props.data}
                    renderItem={this.viewGallery}

                />
            </View>


        )
    }
    render() {
        console.log("ConstructionGallery", this.props)
        // const date = moment(this.props.data[0].date).format("DD MMMM,YYYY")
        const date = this.props && this.props.data[0] && this.props.data[0].date ? this.props.data[0].date : ""
        const description = this.props && this.props.data[0] && this.props.data[0].description ? this.props.data[0].description : ""
        const image = this.props && this.props.data[0] && this.props.data[0].image ? this.props.data[0].image : ""
        // console.log("ConstructionGallery", this.props.data.length)
        return (<View style={{ flex: 1, marginHorizontal: 10 }}>
            <KeyboardAwareScrollView contentContainerStyle={{ flex: 1 }}>
                <Modal
                    style={{ borderRadius: 10, zIndex: 30 }}
                    isVisible={this.state.isGalleryVisible}
                >

                    {this.modalView()}
                </Modal>
                <View style={styles.parkingView}>
                    <Image source={image ? { uri: image } : res.images.parking} resizeMode={'cover'} style={styles.parkingImages} />
                </View>
                <View style={{ marginHorizontal: 20, marginTop: 20, bottom: 10 }}>
                    <Text style={styles.dateText}>{moment(date).format("DD MMMM, YYYY")}</Text>
                    <Text style={styles.instructionText} textBreakStrategy={'simple'} numberOfLines={4}>{description}</Text>
                </View>
                {
                    this.props.data.length > 0 ?
                        <TouchableOpacity style={[{ alignItems: 'center', height: 30, justifyContent: 'center', width: '100%', marginHorizontal: -10 }]} onPress={() => this.showFullGallery()}>
                            <Text style={styles.viewMoreText}>View More Images</Text>
                            <View style={{ backgroundColor: res.colors.white, height: 2 }} />
                        </TouchableOpacity> : <View />
                }
            </KeyboardAwareScrollView>

        </View >)
    }








}
export default ConstructionGallery 