import React, { Component } from 'react'
import { SafeAreaView, View, Text, TouchableOpacity, Dimensions, ImageBackground, Animated, FlatList, Image,Linking } from 'react-native'
import ProgressCircle from 'react-native-progress-circle'
import Modal from 'react-native-modal'
import ImageZoom from 'react-native-image-pan-zoom';
import moment from 'moment';

import { heightScale, widthScale } from '../../../utility/Utils'
import res from '../../../../res'
import styles from './styles';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { ScrollView } from 'react-native-gesture-handler';

const screenWidth = Dimensions.get("window").width;

class ConstructionProgress extends Component {
    constructor(props) {
        super(props)
        this.state = {
            progressStatus: 0,
            projectLevel: "",
            isGalleryVisible: false
        }
    }
    anim = new Animated.Value(0);
    componentDidMount() {
        const projectLevel = (this.props.unitLevel + this.props.towerLevel + this.props.infraStructureLevel) / 3

        this.setState({
            projectLevel: projectLevel
        }, () => {
            this.onAnimate();
        })

    }
    onAnimate = () => {
        const projectLevel = (this.props.unitLevel + this.props.towerLevel + this.props.infraStructureLevel) / 3
        this.anim.addListener(({ value }) => {
            this.setState({ progressStatus: parseInt(value, 10) });
        });
        Animated.timing(this.anim, {
            toValue: this.props.projectLevel,
            duration: 500,
            useNativeDriver: false
        }).start();
    }
    ShareImage = (url) => {
        Linking.canOpenURL(url).then(supported => {
            if (supported) {
                Linking.openURL(url);
            } else {
                console.log("Don't know how to open URI: " + url);
            }
        });
    }
    viewGallery = ({ item, index }) => {

        return (
            <View style={{ flex: 1 }}>
                <View style={{ flex: 1, zIndex: 9999, overflow: 'hidden' }}>
                    <ImageZoom


                        cropWidth={Dimensions.get('window').width}
                        cropHeight={Dimensions.get('window').height}
                        imageWidth={Dimensions.get('window').width}
                        imageHeight={Dimensions.get('window').height}>

                        <Image source={item.image ? { uri: item.image } : res.images.parking} style={{ width: "100%", height: "100%" }} resizeMode={'center'} />

                    </ImageZoom>
                </View>

                <View style={{ position: 'absolute', bottom: heightScale(30), right: 20, left: 20, zIndex: 10, overflow: 'hidden' }}>
                    <Text style={styles.dateModalText}>{item.date ? moment(item.date).format("DD MMMM,YYYY") : moment(Date).format("DD MMMM,YYYY")}</Text>
                    <Text style={styles.instructionModalText} textBreakStrategy={'simple'} numberOfLines={4}>{item.description}</Text>
                </View>
                <TouchableOpacity style={styles.ShareBtn} onPress={() => this.ShareImage(item.image)}>
                    <View style={styles.shareView}>
                        <Image source={res.images.icn_share} />
                    </View>

                </TouchableOpacity>
            </View>





        )
    }
    modalView = () => {

        return (

            <View style={{ flex: 1 }}>
                <View style={styles.closeBtn}>
                    <TouchableOpacity onPress={() => this.showFullGallery()}>
                        <Image source={res.images.icnClose} style={{ width: 20, height: 20, tintColor: res.colors.black }} resizeMode={'center'} />
                    </TouchableOpacity>
                </View>
                <FlatList
                    keyExtractor={(item, index) => index.toString()}
                    bounces={true}
                    style={{ marginHorizontal: -20 }}
                    horizontal
                    pagingEnabled={true}
                    data={this.props.gallery}
                    renderItem={this.viewGallery}

                />
            </View>


        )
    }
    showFullGallery = () => {
        const { isGalleryVisible } = this.state

        if (!this.props.gallery) {
            return alert("No images Found")
        } else {
            this.setState({
                isGalleryVisible: !isGalleryVisible
            })
        }

    }
    render() {
        console.log("ConstructionProgress", this.props)

        const projectLevel = this.props.projectLevel
        const filledView = parseFloat(this.props.projectLevel) / 100

        return <SafeAreaView style={styles.SafeAreaView}>
            <Modal
                style={{ borderRadius: 10, zIndex: 30 }}
                isVisible={this.state.isGalleryVisible}
            >

                {this.modalView()}
            </Modal>
            <ScrollView style={{ flex: 1 }}>
                <View style={{ flexDirection: 'row', justifyContent: 'space-around', marginTop: 24 }}>
                    <View style={{ justifyContent: 'center', alignItems: 'center', marginLeft: 20 }}>
                        <Text style={styles.projectText}>Project Level</Text>
                        <Text style={[styles.projectText, { marginTop: 5 }]}>{Math.round(projectLevel) ? Math.round(projectLevel) : "0"}%</Text>
                    </View>
                    <View style={{ flex: 1, alignItems: 'center' }} >
                        <View
                            style={[
                                { width: 55, justifyContent: 'flex-end', backgroundColor: res.colors.btnBlue, position: 'absolute', bottom: 0, height: 130 * filledView, opacity: 1 },
                            ]}
                        />
                        <ImageBackground source={res.images.prjectLevel} style={{ width: 106, height: 130, alignItems: 'center' }} resizeMode={'cover'} >


                        </ImageBackground>
                    </View>
                </View>

                <View style={{ justifyContent: 'space-around', flexDirection: 'row', marginTop: 40, alignItems: 'center' }}>

                    <View>
                        <ProgressCircle
                            percent={this.props.infraStructureLevel ? this.props.infraStructureLevel : ""}
                            radius={screenWidth * 0.18}
                            borderWidth={6}
                            color={res.colors.peacockBlue}
                            shadowColor={res.colors.toupe}
                            bgColor={res.colors.butterscotch}
                        >

                            <View style={{ position: 'absolute', alignSelf: 'center' }}>
                                <ProgressCircle
                                    percent={this.props.towerLevel ? this.props.towerLevel : ""}
                                    radius={screenWidth * 0.13}
                                    borderWidth={6}
                                    color={res.colors.pastelRed}
                                    shadowColor={res.colors.toupe}
                                    bgColor={res.colors.butterscotch}
                                >
                                    <View style={{ position: 'absolute', alignSelf: 'center' }}>
                                        <ProgressCircle
                                            percent={this.props.unitLevel ? this.props.unitLevel : ""}
                                            radius={screenWidth * 0.08}
                                            borderWidth={6}
                                            color={res.colors.milkChocolate}
                                            shadowColor={res.colors.warmGreyThree}
                                            bgColor={res.colors.butterscotch}
                                        >
                                        </ProgressCircle>
                                    </View>
                                </ProgressCircle>
                            </View>
                        </ProgressCircle>

                    </View>
                    <View style={{ alignItems: 'flex-start', flexDirection: 'column', justifyContent: 'space-between', height: 100, width: screenWidth / 2.7 }}>
                        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
                            <View style={{ backgroundColor: res.colors.peacockBlue, width: 10, height: 10, borderRadius: 5 }} />
                            <View style={{ width: "30%" }}>
                                <Text style={styles.percentageText}>{Math.round(this.props.infraStructureLevel) ? Math.round(this.props.infraStructureLevel) : "0"}%</Text>
                            </View>
                            <View>
                                <Text style={[styles.percentageLabel, { paddingRight: 10 }]} numberOfLines={2}>{"Infrastructure \nLevel"}</Text>
                            </View>

                        </View>
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <View style={{ backgroundColor: res.colors.pastelRed, width: 10, height: 10, borderRadius: 5 }} />
                            <View style={{ width: "30%" }}>
                                <Text style={styles.percentageText}>{Math.round(this.props.towerLevel) ? Math.round(this.props.towerLevel) : "0"}%</Text>

                            </View>
                            <View>
                                <Text style={[styles.percentageLabel]}>Tower Level</Text>
                            </View>

                        </View>
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <View style={{ backgroundColor: res.colors.milkChocolate, width: 10, height: 10, borderRadius: 5 }} />
                            <View style={{ width: "30%" }}>
                                <Text style={styles.percentageText}>{Math.round(this.props.unitLevel) ? Math.round(this.props.unitLevel) : "0"}%</Text>
                            </View>
                            <View>
                                <Text style={[styles.percentageLabel]}>Unit</Text>
                            </View>

                        </View>
                    </View>

                </View>

                {this.props.gallery && this.props.gallery.length > 0 && <View>
                    <View style={styles.constructionView}>
                        <TouchableOpacity style={styles.constructionBtn} onPress={() => this.showFullGallery()}>
                            <Text style={styles.constructionText}>{"Construction Progress Gallery"}</Text>
                        </TouchableOpacity>
                    </View>

                </View>}
            </ScrollView>



        </SafeAreaView >






    }


}

export default ConstructionProgress 