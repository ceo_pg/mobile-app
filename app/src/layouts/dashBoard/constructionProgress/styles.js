import { StyleSheet } from 'react-native';
import res from '../../../../res';
import { heightScale } from '../../../utility/Utils';
const style = StyleSheet.create({

    shadow: {
        shadowColor: res.colors.shadowColor,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.5,
        shadowRadius: 2.84,

        elevation: 3,
    }, projectText: {
        fontFamily: res.fonts.medium,
        fontSize: 16,
        color: res.colors.greyishBrown
    }, dateText: {
        fontWeight: 'bold',
        fontFamily: res.fonts.semiBold,
        fontSize: 16,
        color: res.colors.peacockBlue
    }, instructionText: {
        fontFamily: res.fonts.regular,
        fontSize: 14,
        color: res.colors.greyishBrown
    }, percentageText: {
        fontWeight: 'bold',
        fontFamily: res.fonts.semiBold,
        fontSize: 12,
        color: res.colors.charcoalGrey,
        marginLeft: 10
    }, percentageLabel: {
        fontFamily: res.fonts.regular,
        fontSize: 12,
        color: res.colors.charcoalGrey,
        // flex: 1,
        paddingRight: 20,
        marginLeft: 5,
        textAlign: 'left'
    }, closeBtn: {
        position: "absolute",
        top: 30,
        right: 30,
        backgroundColor: res.colors.white,
        zIndex: 999,
        width: 30,
        height: 30,
        borderRadius: 15,
        alignItems: 'center',
        justifyContent: 'center'
    }, container: {
        // width: "100%",  
        height: 40,
        padding: 3,
        // borderColor: "#FAA",  
        // borderWidth: 3,  
        // borderRadius: 30,  
        marginTop: 200,
        justifyContent: 'flex-end',
    },
    inner: {
        flex: 1,

        width: 30,
        // borderRadius: 15,  
        backgroundColor: res.colors.btnBlue,

    },
    label: {
        fontSize: 23,
        color: "black",
        position: "absolute",
        zIndex: 1,
        alignSelf: "center",
    }, parkingImages: { flex: 1, borderBottomLeftRadius: 5, borderBottomRightRadius: 5 },
    parkingView: {
        flex: 1, borderRadius: 10,
    }, footerShadow: {
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20,
        backgroundColor: res.colors.butterscotch,
        shadowColor: res.colors.shadowColor,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 10.58,
        shadowRadius: 10.00,
        elevation: 24,
    }, headerView: {
        width: '100%',
        backgroundColor: res.colors.btnBlue,
        height: 40
    }, dateModalText: {
        fontWeight: 'bold',
        fontFamily: res.fonts.semiBold,
        fontSize: 16,
        color: res.colors.white
    }, instructionModalText: {
        fontFamily: res.fonts.regular,
        fontSize: 14,
        color: res.colors.white
    }, SafeAreaView: {
        flex: 1, flexDirection: 'column', justifyContent: 'space-between'
    }, viewMoreText: {
        fontWeight: 'bold',
        color: res.colors.peacockBlue,
        fontFamily: res.fonts.semiBold,
        fontSize: 14
    }, constructionBtn: {
        borderBottomWidth: 1,
        borderBottomColor: res.colors.appColor,

    },
    constructionText: {
        fontFamily: res.fonts.medium,
        fontSize: 16,
        color: res.colors.greyishBrown,

    }, constructionView: {
        marginTop: heightScale(30),
        marginBottom: 40,
        alignItems: 'center'
    }, ShareBtn: {
        position: 'absolute',
        bottom: 30,
        right: 30,
        backgroundColor:res.colors.black,
        borderRadius:25,
        zIndex: 9999999,
        width:50,
        height:50,
        overflow:'hidden',
        justifyContent:'center',
        alignItems:'center'
      },shareView:{
          backgroundColor:res.colors.white,
          borderRadius:15,
          width:30,
          height:30
      }
})
export default style;