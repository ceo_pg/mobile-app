import React, { Component } from 'react'
import { View, SafeAreaView, Text, Image, Dimensions, TouchableOpacity, ImageBackground } from 'react-native'
import Carousel, { Pagination } from 'react-native-snap-carousel';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { CommonActions } from '@react-navigation/native';
import AsyncStorage from '@react-native-community/async-storage'
import Modal from 'react-native-modal'

import ApiFetch from '../../apiManager/ApiFetch';
import DashBoardHeader from '../../components/header/DashBoardHeader'
import styles from './styles'
import res from '../../../res'
import { VastuMeter, HalfCircular, Loader, EmiScreenModal, PriceTrendModal, ConstructionProgressModal } from '../../components/index'
import ConstructionProgress from '../dashBoard/constructionProgress/ConstructionProgress';
import { EmiScreen } from './emiScreen/EmiScreen'
import ConstructionGallery from '../dashBoard/constructionProgress/ConstructionGallery';
import ProjectPriceTrend from './projectPriceTrend/ProjectPriceTrend'
import { DASHBOARD_ENDPOINTS } from '../../apiManager/ApiEndpoints';
import moment from 'moment';
import AppUser from '../../utility/AppUser'
import { heightScale } from '../../utility/Utils';

class DashBoard extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isModalVisible: false,
            dashBoardHeader: [{
                name: 'Construction Progress',
                id: '1'
            }, {
                name: 'Home Loan',
                id: '3'

            }, {
                name: 'Micro Market Price Trend',
                id: '4'

            }],
            emiData: [{
                heading: "Remaining for Next EMI",
                value: "Rs 50K",
                image: res.images.icnRoom

            }, {
                heading: "Remaining for Next EMI",
                value: "Rs 50K",
                image: res.images.icnRoom

            }, {
                heading: "Remaining for Next EMI",
                value: "Rs 50K",
                image: res.images.icnRoom

            }, {
                heading: "Remaining for Next EMI",
                value: "Rs 50K",
                image: res.images.icnRoom

            }],
            activeIndex: 0,
            projectLevel: 0,
            isLoading: false,
            vaastuScore: "",
            develperScore: "",
            nextInstallmentAmount: "",
            nextInstallMentdate: "",
            lifeStyleIndex: "",
            infraStructureLevel: "",
            towerLevel: "",
            unitLevel: "",
            loanStatement: {},
            emi: "",
            constructionGallery: [],
            projectPriceTrend: [],
            remainingDays: "",
            projectInfo: {},
            loading: true

        }
    }
    componentDidMount = async () => {
        setTimeout(() => {
            this.setState({ loading: false });
        }, 500);
        const projectId = res.AsyncStorageContants.PROJECTD;
        try {
            let projectID = await AsyncStorage.getItem(projectId)
            this.hitDashBoardApi(projectID)

        } catch (e) {
            console.log("Error removing data from async storage.", e);
        }
    }
    hitDashBoardApi = (projectId) => {
        console.log("hitDashBoardApi", projectId)
        const PROJECTID = projectId ? projectId : "5ef20fa4fe2625947b83f118"
        this.setState({
            isLoading: true
        })

        ApiFetch.fetchGet(DASHBOARD_ENDPOINTS + "/" + PROJECTID)
            .then((res) => {
                console.log("DASHBOARD RESPONCE", res, PROJECTID)
                this.setState({
                    nextInstallmentAmount: res.data && res.data.homeLoan && res.data.homeLoan.emi ? res.data.homeLoan.emi : "",
                    nextInstallMentdate: res.data && res.data.homeLoan && res.data.homeLoan.loanStatement && res.data.homeLoan.loanStatement.dateOfEmi ? res.data.homeLoan.loanStatement.dateOfEmi : "",
                    develperScore: res.data && res.data.developerScore ? res.data.developerScore : "",
                    vaastuScore: res.data && res.data.vastuScore ? res.data.vastuScore : "",
                    projectLevel: res.data && res.data.projectConstructionProgress && res.data.projectConstructionProgress.totalConstructionStatus ? res.data.projectConstructionProgress.totalConstructionStatus : "",
                    lifeStyleIndex: res.data && res.data.project && res.data.project.lifeStyleIndex && res.data.project.lifeStyleIndex.lifeStyleIndex ? res.data.project.lifeStyleIndex.lifeStyleIndex : "",
                    towerLevel: res.data && res.data.project && res.data.project.towerLevelConstructionProgress && res.data.project.towerLevelConstructionProgress.totalConstructionStatus ? res.data.project.towerLevelConstructionProgress.totalConstructionStatus : "",
                    unitLevel: res.data && res.data.project && res.data.project.unitLevelConstructionProgress && res.data.project.unitLevelConstructionProgress.totalConstructionStatus ? res.data.project.unitLevelConstructionProgress.totalConstructionStatus : "",
                    infraStructureLevel: res.data && res.data.project && res.data.project.projectConstructionProgress && res.data.project.projectConstructionProgress.totalConstructionStatus ? res.data.project.projectConstructionProgress.totalConstructionStatus : "",
                    loanStatement: res.data && res.data.homeLoan && res.data.homeLoan.loanStatement && res.data.homeLoan.loanStatement ? res.data.homeLoan.loanStatement : "",
                    emi: res.data && res.data.homeLoan && res.data.homeLoan.emi ? res.data.homeLoan.emi : "",
                    constructionGallery: res.data && res.data.project && res.data.project.gallery ? res.data.project.gallery : [],
                    projectPriceTrend: res.data && res.data.project && res.data.project.investmentProgressForm && res.data.project.investmentProgressForm.projectPriceTrend ? res.data.project.investmentProgressForm.projectPriceTrend : [],
                    remainingDays: res.data && res.data.project && res.data.project.remainingDays ? res.data.project.remainingDays : "",
                    projectInfo: res.data && res.data.project && res.data.project.projectInfo ? res.data.project.projectInfo : {},
                    isLoading: false

                })
                this.setState({
                    isLoading: false
                })

            }).catch((err) => {
                this.setState({
                    isLoading: false
                })
                console.log("Errror inside dashboard", err)
            })
    }
    renderLoader = () => {
        return (
            <Loader
                loading={this.state.isLoading} />
        );
    };
    renderCompanyDetails = () => {
        const { logo, projectName } = this.state.projectInfo
        console.log("logo", projectName)
        if (this.state.activeIndex < 0) {
            return <View style={styles.companyDetailstyle}>

            </View>
        } else {
            return <View style={styles.companyDetailstyle}>
                <View style={styles.companyLogo}>
                    <Image source={logo ? { uri: logo } : ""} style={{ width: "100%", height: heightScale(83), borderRadius: 5 }} resizeMode={'contain'} />
                    <Text numberOfLines={2} style={styles.projectNameText}>{projectName ? projectName : ""}</Text>

                </View>

                <View style={{ width: '30%', marginLeft: 10, alignItems: 'center', flex: 1, flexDirection: 'column' }}>
                    <VastuMeter
                        showText={this.state.remainingDays + "\ndays"}
                        percent={50}
                        radius={23}
                        bgRingWidth={5}
                        progressRingWidth={5}
                        ringColor={res.colors.pinkishTan}
                        ringBgColor={'grey'}
                        textFontSize={10}
                        textFontColor={res.colors.pinkishTan}
                        textFontWeight={'normal'}
                        clockwise={true}
                        bgColor={'white'}
                        startDegrees={0}
                    />
                    <Text style={styles.remaingText}>{"Remaining for Possession"}</Text>
                </View>
            </View>
        }

    }
    renderInstallment = () => {
        const { activeIndex, vaastuScore, develperScore, nextInstallmentAmount, nextInstallMentdate, lifeStyleIndex, infraStructureLevel } = this.state
        const installMentDate = nextInstallMentdate ? moment(nextInstallMentdate).format("DD MMMM,YYYY") : "NA"

        console.log("vaastuScore", nextInstallMentdate)
        if (activeIndex < 0) {
            return <View style={styles.squreFitView}>


                <View style={{ alignItems: 'center' }}>
                    <Text style={styles.halfCircleHeaderText}>Lifestyle Index</Text>
                    <HalfCircular
                        percentage={Math.round(lifeStyleIndex)}
                        progressColor={res.colors.butterscotch}
                        circleRadius={30}
                        interiorCircleColor={res.colors.cobalt}
                    >
                        <Text style={styles.percentageText}>{Math.round(lifeStyleIndex)}%</Text>
                    </HalfCircular>
                </View>
                <View style={{ alignItems: 'center' }}>
                    <Text style={styles.halfCircleHeaderText}>Vaastu Compliance</Text>
                    <HalfCircular
                        percentage={parseInt(vaastuScore)}
                        progressColor={res.colors.butterscotch}
                        circleRadius={30}
                        interiorCircleColor={res.colors.cobalt}
                    >
                        <Text style={styles.percentageText}>{vaastuScore}%</Text>
                    </HalfCircular>
                </View>
                <View style={{ alignItems: 'center' }}>
                    <Text style={styles.halfCircleHeaderText}>Developer Score</Text>
                    <HalfCircular
                        percentage={Math.round(develperScore)}
                        progressColor={res.colors.butterscotch}
                        circleRadius={30}
                        interiorCircleColor={res.colors.cobalt}
                    >
                        <Text style={styles.percentageText}>{Math.round(develperScore)}%</Text>
                    </HalfCircular>
                </View>
            </View>

        } else {
            return <View style={styles.squreFitView}>
                <View style={{ alignItems: 'center' }}>
                    <Text style={styles.squreHeading}>Next Installment Amount</Text>
                    <Text style={styles.squreSubHeading}>INR {nextInstallmentAmount ? nextInstallmentAmount.toFixed(2) : "0"}</Text>
                </View>
                <View style={{ alignItems: 'center' }}>
                    <Text style={styles.squreHeading}>Next Installment Date</Text>
                    {
                        installMentDate ?
                            <Text style={styles.squreSubHeading}>{installMentDate}</Text> :
                            <Text style={styles.squreSubHeading}>{"NA"}</Text>
                    }

                </View>

            </View >
        }

    }
    renderViewMore = () => {
        return <View style={{ alignItems: 'center', height: 31, justifyContent: 'center' }}>
            <TouchableOpacity onPress={() => this.toggleModal()} activeOpacity={1}>

                <Text style={styles.viewMoreText}>View More</Text>
            </TouchableOpacity>
        </View>
    }
    viewMore = (item) => {

        const id = item.id
        console.log("VIEWMOREID", id)
        if (id == 1) {
            return this.renderViewMore()
        }
        else if (id == 3) {
            return this.renderViewMore()
        }
        else if (id == 4) {
            return this.renderViewMore()
        }
        // switch (id) {
        //     case 1: return this.renderViewMore()
        //     case 2: return this.renderViewMore()
        //     case 3: return this.renderViewMore()
        //     case 4: return this.renderViewMore()
        // }
    }
    renderConstructionGallery = () => {
        const gallery = this.state.constructionGallery
        // console.log(" this.state.constructionGallery",  this.state.constructionGallery)
        if (!gallery || gallery.length == 0) {
            return (
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <Text>{res.strings.NO_DATA_FOUND}</Text>
                </View>
            )
        } else {
            return (
                <ConstructionGallery
                    toggleModal={this.toggleModal}
                    data={gallery}
                />

            )
        }
    }


    renderCorousalData = (item) => {
        const { towerLevel, unitLevel, infraStructureLevel, emi } = this.state
        const gallery = this.state.constructionGallery
        if (item.id == 1) {
            return <ConstructionProgress
                infraStructureLevel={infraStructureLevel}
                towerLevel={towerLevel}
                unitLevel={unitLevel}
                projectLevel={(infraStructureLevel * 0.2 + towerLevel * 0.3 + unitLevel * 0.5)}
                gallery={gallery}
            />


        } if (item.id == 3) {
            return <EmiScreen

                emi={emi}
                data={this.state.loanStatement}
            />
        } if (item.id == 4) {
            return <ProjectPriceTrend
                projectPriceTrend={this.state.projectPriceTrend}
            />


        }

    }

    renderData = ({ item, index }) => {
        console.log("renderData", this.state.activeIndex + 1)
        return <View style={{ flex: 1, backgroundColor: res.colors.butterscotch, borderRadius: 24, marginTop: 30, overflow: 'hidden' }} >

            <View style={[styles.shadowHeader, { height: 42 }]}>
                <Text style={styles.corousalNameText}>{item.name}</Text>
            </View>
            {this.renderCorousalData(item)}
            {this.viewMore(item)}

        </View>


    }
    renderCorusalView = () => {
        return <View style={{ flex: 0.95 }}>
            <Carousel
                data={this.state.dashBoardHeader}
                renderItem={this.renderData}
                sliderWidth={Dimensions.get('window').width}
                itemWidth={Dimensions.get('window').width - 80}
                layout={'default'}
                containerCustomStyle={styles.carouselData}
                removeClippedSubviews={true}
                onSnapToItem={index => this.setState({ activeIndex: index })}
                firstItem={this.state.activeIndex}
            />
            <View style={{ height: 20, width: "10%", alignSelf: 'center' }}>
                <Pagination
                    dotsLength={this.state.dashBoardHeader.length}
                    dotStyle={styles.dotStyle}
                    dotColor={styles.dotColor}
                    inactiveDotStyle={styles.inactiveDotStyle}
                    activeDotIndex={this.state.activeIndex}
                    inactiveDotOpacity={0.4}
                    inactiveDotScale={0.6}
                    activeOpacity={1}
                />
            </View>


        </View>
    }
    openDrawer = () => {
        this.props.navigation.openDrawer()
        const resetAction = CommonActions.reset({
            index: 0,
            routes: [{ name: 'DashBoard' }],
        });
        this.props.navigation.dispatch(resetAction);


    }
    openSupport = () => {
        this.props.navigation.navigate("SupportBaseScreen")
    }
    toggleModal = () => {
        this.setState({
            isModalVisible: !this.state.isModalVisible
        })
    }
    modalView = () => {

        const { towerLevel, unitLevel, infraStructureLevel, emi, activeIndex, constructionGallery } = this.state
        if (activeIndex == 0) {
            return <ConstructionProgressModal

                infraStructureLevel={infraStructureLevel}
                towerLevel={towerLevel}
                unitLevel={unitLevel}
                projectLevel={(infraStructureLevel * 0.2 + towerLevel * 0.3 + unitLevel * 0.5)}
                gallery={constructionGallery}
            />
        } if (activeIndex == 1) {
            return <EmiScreenModal
                emi={emi}
                data={this.state.loanStatement}
            />
        } if (activeIndex == 2) {
            return <PriceTrendModal
                projectPriceTrend={this.state.projectPriceTrend}
            />
        }

    }
    render() {
        const { loading, isLoading } = this.state
        if (loading) {
            return null;
        }
        return (
            <SafeAreaView style={styles.fullScreen}>
                <Modal isVisible={this.state.isModalVisible}
                    // swipeThreshold={50}
                    // onSwipeComplete={() => this.toggleModal()}
                    // swipeDirection={'down'}
                    onBackdropPress={() => this.toggleModal()}
                    style={styles.view}>
                    {this.modalView()}
                </Modal>

                <ImageBackground source={res.images.mainBackground} style={{ flex: 1 }}>
                    <DashBoardHeader
                        openDrawer={() => this.openDrawer()}
                        headerTitle={"Dashboard"}
                        settingVisible={false}
                        notificationVisible={false}
                        openSupport={() => this.openSupport()}
                    />
                    <View style={{ flex: 1 }}>
                        {/* <KeyboardAwareScrollView> */}
                        {this.renderCompanyDetails()}
                        {/* {this.renderInstallment()} */}
                        {this.renderCorusalView()}
                        {/* </KeyboardAwareScrollView> */}

                    </View>
                    {isLoading && this.renderLoader()}
                </ImageBackground>

            </SafeAreaView>
        )
    }
}
export default DashBoard