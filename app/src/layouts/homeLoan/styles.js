import { StyleSheet, Platform, StatusBar } from 'react-native'
import res from '../../../res'
import { heightScale, widthScale } from '../../utility/Utils'
const styles = StyleSheet.create({
    fullScreen: {
        flex: 1,
        backgroundColor: res.colors.appColor,
        paddingTop: Platform.OS === "android" ? StatusBar.currentHeight + 10 : 0
    }, boxView: {
        marginHorizontal: 20,
        // height: heightScale(234),
        // width: "100%",
        backgroundColor: res.colors.white,
        marginTop: heightScale(50),
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 12
    }, image: {
        margin: 25
    }, headerText: {
        fontWeight: 'bold',
        textAlign: 'center',
        fontFamily: res.fonts.semiBold,
        color: res.colors.formLabelGrey,
        fontSize: 16,
        letterSpacing: 0.77,
        lineHeight: heightScale(24)
    }, otherText: {
        // margin: 10,
        textAlign: 'center',
        fontFamily: res.fonts.regular,
        color: res.colors.formLabelGrey,
        fontSize: 14,
        letterSpacing: 0.67,
        lineHeight: heightScale(24)
    }, button: {
        height: heightScale(44),
        alignItems: 'center',
        backgroundColor: res.colors.butterscotch,
        width: '100%',
        borderBottomLeftRadius: 12,
        borderBottomRightRadius: 12,
        justifyContent: 'center'
    }, btnTextStyle: {
        textAlign: 'center',
        fontFamily: res.fonts.semiBold,
        color: res.colors.formLabelGrey,
        fontSize: 18,
        letterSpacing: 0.86,
        lineHeight: heightScale(24)
    },
    otpContainerView: {
        backgroundColor: res.colors.white,
        height: heightScale(150),
        marginTop: heightScale(150),
        borderRadius: widthScale(24),
        // justifyContent: "center",
        // alignItems: 'center'
        // marginHorizontal: widthScale(15)
    }, commingSoonText: {
        fontFamily: res.fonts.bold,
        fontSize: widthScale(20),
        color: res.colors.appColor,
        marginVertical:heightScale(30)
    }

})
export default styles