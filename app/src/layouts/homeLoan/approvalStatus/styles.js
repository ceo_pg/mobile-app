import { StyleSheet, Platform, StatusBar } from 'react-native'
import res from '../../../../res'
import { heightScale, widthScale, myHeight } from '../../../utility/Utils'
const styles = StyleSheet.create({
    approvalContainer: {
        margin: widthScale(15),
        borderRadius: widthScale(20),
        flex: 1,
        backgroundColor: res.colors.white,
        // height: myHeight,
        marginBottom: heightScale(200)
    }, fullScreen: {
        flex: 1,
        backgroundColor: res.colors.appColor,
        paddingTop: Platform.OS === "android" ? StatusBar.currentHeight + 10 : 0
    }, mainView: {
        // marginTop:heightScale(50),
        backgroundColor: res.colors.butterscotch,
        marginHorizontal: widthScale(20),
        borderRadius: 20,
        marginBottom: heightScale(70)

    }, listView: {
        backgroundColor: res.colors.white,
        margin: widthScale(15),
        borderRadius: 20,

    }, progresBox: {
        marginTop: 10,
        height: heightScale(44),
        flexDirection: 'row',
        alignItems: 'center',
        width: '95%',
        backgroundColor: res.colors.white,
        borderRadius: 5,
        marginHorizontal: 10,
        marginBottom: 10,

    }, shadow: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 4,
        },
        shadowOpacity: 0.30,
        shadowRadius: 4.65,

        elevation: 8,
    }, imageView: {
        width: '20%',
        alignItems: 'center',
        justifyContent: 'center'
    }, processText: {
        width: '60%',
        justifyContent: 'center',
        fontFamily: res.fonts.semiBold,
        color: res.colors.formLabelGrey,
        fontSize: 14,
        letterSpacing: 0.67,
        lineHeight: heightScale(24),
       
    }, loanStatusText: {
        textAlign: 'center',
        fontFamily: res.fonts.semiBold,
        color: res.colors.blackTwo,
        fontSize: 18,
        letterSpacing: 0.86,
        lineHeight: heightScale(24),
        marginVertical:heightScale(10)
    }
})
export default styles