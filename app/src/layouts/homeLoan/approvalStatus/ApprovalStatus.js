import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import { View, Text, FlatList, Image, SafeAreaView, TouchableOpacity,ImageBackground } from 'react-native';
import styles from './styles'

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import res from '../../../../res'
import DashBoardHeader from '../../../components/header/DashBoardHeader'
import { widthScale, heightScale } from '../../../utility/Utils';


class ApprovalStatus extends Component {
    constructor(props) {
        super(props);
        this.navigation = this.props.route.params && this.props.route.params.navigation;
        this.state = {
            ApprovalStatus: [{
                name: res.strings.Document_Submission,
                images: res.images.icn_Document_Submission,
                isChecked: true
            }, {
                name: res.strings.Processing_Fee_Payment,
                images: res.images.icn_Processing_Fee,
                isChecked: false
            }, {
                name: res.strings.Verification,
                images: res.images.icn_Verification,
                isChecked: false
            }, {
                name: res.strings.File_Login,
                images: res.images.icn_File_Login,
                isChecked: false
            }, {
                name: res.strings.Sanction_Letter,
                images: res.images.icn_Sanction,
                isChecked: false
            }, {
                name: res.strings.Legal_Documentation,
                images: res.images.icn_Legal_Doc,
                isChecked: false
            }, {
                name: res.strings.Disbursement,
                images: res.images.icn_Disbursement,
                isChecked: false
            }]
        }
    }
    componentDidMount() {

    }

    backPress = () => {
        const { changePage } = this.props;
        changePage(-1)
    }
    renderCheck = (item) => {
        if (item.isChecked) {
            return <Image source={res.images.icn_check} />
        } else {
            return <View />
        }
    }
    renderCustomizationItem = ((item, index) => {
        console.log("ApprovalStatus", item)
        return (<View style={[styles.progresBox, styles.shadow]}>
            <View style={styles.imageView}>
                <Image source={item.item.images} />
            </View>
            <View style={styles.processText}>
                <Text>{item.item.name}</Text>
            </View>
            <View>
                {this.renderCheck(item.item)}

            </View>
        </View>)
    })
    openDrawer = () => {
        this.props.navigation.openDrawer()
    }
    render() {
        const { ApprovalStatus } = this.state
        return (
            <SafeAreaView style={styles.fullScreen}>
                <ImageBackground source={res.images.mainBackground} style={{ flex: 1 }}>
                    <DashBoardHeader
                        openDrawer={() => this.openDrawer()}
                        headerTitle={res.strings.HOME_LOAN}
                        settingVisible={false}
                        notificationVisible={false}
                    />
                    <KeyboardAwareScrollView>
                        <View >
                            <TouchableOpacity>
                                <Image style={{ width: widthScale(44), height: heightScale(46), alignSelf: 'center', marginVertical: 20 }} source={res.images.icn_File_Login} />
                            </TouchableOpacity>

                        </View>
                        <View style={styles.mainView}>

                            <FlatList
                                ListHeaderComponent={() => {
                                    return <Text style={styles.loanStatusText}>
                                        {res.strings.LOAN_STATUS_APPROVE}
                                    </Text>
                                }}
                                style={styles.listView}
                                data={ApprovalStatus}
                                renderItem={this.renderCustomizationItem}
                            />
                        </View>
                    </KeyboardAwareScrollView>
                </ImageBackground>



            </SafeAreaView>

        )
    }


}
const mapStateToProps = (state) => {
    return {};
};

function mapDispatchToProps(dispatch) {
    return bindActionCreators({

    }, dispatch);
}
let ApprovalStatusContainer = connect(mapStateToProps, mapDispatchToProps)(ApprovalStatus);
export default ApprovalStatusContainer;

