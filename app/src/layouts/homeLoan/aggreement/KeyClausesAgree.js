import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import { View, Text, TouchableOpacity, ImageBackground, SafeAreaView } from 'react-native';
import styles from './styles'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import OutLinedInput from '../../../components/input/OutLinedInput'
import Toast from 'react-native-simple-toast'
import AsyncStorage from '@react-native-community/async-storage'

import res from '../../../../res';
import { CommonActions } from '@react-navigation/native';
import DashBoardHeader from '../../../components/header/DashBoardHeader'
import { Dropdown } from 'react-native-material-dropdown';
import { updateDocumentsAPI, postDocumentsAPI } from '../../../redux/actions/HomeLoan/HomeLoanAction'

class KeyClausesAgree extends Component {


    constructor(props) {
        super(props);
        this.hitBankList = this.props.route.params && this.props.route.params.hitBankList;
        this.USER_ID = this.props.route.params && this.props.route.params.USER_ID;
        this.bankName = this.props.route.params && this.props.route.params.bankName;
        this.state = {
            principalAmount: "",
            loanTenure: "",
            interestTypeValue: "",
            rateOfInterest: "",
            loanTransferCharges: "",
            prePaymentPenality: "",
            foreclosuresCharges: "",
            selectInterestType: null,
            interestType: [{
                value: 'Fixed',
            }, {
                value: 'Folded',
            },],
            USERID: ""
        }
    } 10
    componentDidMount = async () => {
        const userId = res.AsyncStorageContants.USERID;
        try {
            let USER_ID = await AsyncStorage.getItem(userId)
            this.hitPostBankLoanApi()
            this.setState({ USERID: USER_ID })
        } catch (e) {
            console.log("Error removing data from async storage.", e);
        }

    }
    hitPostBankLoanApi = () => {

        let postTempObj = {
            "bankName": "",
            "applicationFormDulyFilled": {
                "name": "",
                "base": ""
            },
            "photos": {
                "name": "",
                "base": ""
            },
            "addressProof": {
                "name": "",
                "base": ""
            },
            "idProof": {
                "name": "",
                "base": ""
            },
            "employerIDCard": {
                "name": "",
                "base": ""
            },
            "accountStatement": {
                "name": "",
                "base": ""
            },
            "previousLoan": {
                "name": "",
                "base": ""
            },
            "salarySlip": {
                "name": "",
                "base": ""
            },
            "form16": {
                "name": "",
                "base": ""
            },
            "propertyPapers": {
                "name": "",
                "base": ""
            },
            "paymentReceipts": {
                "name": "",
                "base": ""
            },
            "aggreement": {
                "name": "",
                "base": ""
            },
            "nocFromBuilder": {
                "name": "",
                "base": ""
            },
            "proofOfAssets": {
                "name": "",
                "base": ""
            },
            "companyProfile": {
                "name": "",
                "base": ""
            },
            "educationalCertificates": {
                "name": "",
                "base": ""
            },
            "employmentProof": {
                "name": "",
                "base": ""
            },
            "detailedCostEstimate": {
                "name": "",
                "base": ""
            },
            "sanctionLetter": {
                "name": "",
                "base": ""
            },
            "IRT": {
                "name": "",
                "base": ""
            },
            "ageProof": {
                "name": "",
                "base": ""
            },
            "panCard": {
                "name": "",
                "base": ""
            },
            "emi": "",
            "homeLoan": {
                "principalAmount": 0,
                "loanTenture": 234234,
                "interestType": "Fixed",
                "rateOfInterest": 234234,
                "loanTransferCharges": 0,
                "prepaymentPenality": 343,
                "foreClosureCharges": 545
            },
            "loanStatement": {
                "amountDisbursed": "",
                "principlePaid": "",
                "interestPaid": "",
                "outstandingPrinciple": "",
                "outstandingInterest": "",
                "preEmi": "",
                "dateOfEmi": ""
            },
            "documentSubmission": false,
            "processingFeePayment": false,
            "verification": false,
            "disbursement": false,
            "legalDocumentation": false,
            "sactionLetter": false,
            "fileLogin": false
        }
        this.setState({ isLoading: true })
        this.props.postDocumentsAPI(postTempObj).then((res => {

            this.setState({ isLoading: false })
            if (res.statusCode == 200 && res.status) {

                console.log("res===========>", res)
            } else {
                // Toast.show(res.message, Toast.LONG);
            }
        }))
            .catch(error => {
                this.setState({ isLoading: false })
                console.log(error, "error updateDocumentsAPI");
            })

    }
    backPress = () => {
        const { changePage } = this.props;
        changePage(-1)
    }
    validationBeforesubmit = () => {
        const { principalAmount, loanTenure, interestTypeValue, rateOfInterest, loanTransferCharges, prePaymentPenality, foreclosuresCharges } = this.state
        if (principalAmount.trim() == "") {
            alert(res.strings.Principle_Amount_Empty)
        }
        else if (loanTenure.trim() == "") {
            alert(res.strings.LoanTenure_Empty)
        }
        else if (interestTypeValue.trim() == "") {
            alert(res.strings.Interest_Type_Empty)
        }
        else if (rateOfInterest.trim() == "") {
            alert(res.strings.Rate_OF_Interest_Empty)
        }
        else if (loanTransferCharges.trim() == "") {
            alert(res.strings.Loan_Transfer_Empty)
        }
        else if (prePaymentPenality.trim() == "") {
            alert(res.strings.Pre_Payment_Empty)
        }
        else if (foreclosuresCharges.trim() == "") {
            alert(res.strings.Forclouser_Empty)
        } else {
            this.moveToHomeScreen()
        }

    }
    moveToHomeScreen = () => {

        const { principalAmount, loanTenure, interestTypeValue, rateOfInterest, loanTransferCharges, prePaymentPenality, foreclosuresCharges, USERID } = this.state
        const { HomeLoan } = this.props;
        let temDocObject =
        {
            "bankName": "Bank of Baroda",
            "applicationFormDulyFilled": {
                "name": "companycategory.png"
            },
            "photos": {
                "name": ""
            },
            "addressProof": {
                "name": ""
            },
            "idProof": {
                "name": ""
            },
            "employerIDCard": {
                "name": ""
            },
            "accountStatement": {
                "name": ""
            },
            "previousLoan": {
                "name": ""
            },
            "salarySlip": {
                "name": ""
            },
            "form16": {
                "name": ""
            },
            "propertyPapers": {
                "name": ""
            },
            "paymentReceipts": {
                "name": ""
            },
            "aggreement": {
                "name": ""
            },
            "nocFromBuilder": {
                "name": ""
            },
            "proofOfAssets": {
                "name": ""
            },
            "companyProfile": {
                "name": ""
            },
            "educationalCertificates": {
                "name": ""
            },
            "employmentProof": {
                "name": ""
            },
            "detailedCostEstimate": {
                "name": ""
            },
            "sanctionLetter": {
                "name": ""
            },
            "IRT": {
                "name": ""
            },
            "ageProof": {
                "name": ""
            },
            "panCard": {
                "name": ""
            },

            "homeLoan": {
                "principalAmount": parseFloat(principalAmount),
                "loanTenture": parseFloat(loanTenure),
                "interestType": interestTypeValue,
                "rateOfInterest": parseFloat(rateOfInterest),
                "loanTransferCharges": parseFloat(loanTransferCharges),
                "prepaymentPenality": parseFloat(prePaymentPenality),
                "foreClosureCharges": parseFloat(foreclosuresCharges)
            },
            "loanStatement": [
                {

                    "amountDisbursed": null,
                    "principlePaid": null,
                    "interestPaid": null,
                    "outstandingPrinciple": null,
                    "outstandingInterest": null,
                    "preEmi": null,
                    "dateOfEmi": null
                }
            ]

        }

        // let temDocObject = HomeLoan;
        const keyClausesObject = {
            principalAmount: parseFloat(principalAmount),
            loanTenture: parseFloat(loanTenure),
            interestType: interestTypeValue,
            rateOfInterest: parseFloat(rateOfInterest),
            loanTransferCharges: parseFloat(loanTransferCharges),
            prepaymentPenality: parseFloat(prePaymentPenality),
            foreClosureCharges: parseFloat(foreclosuresCharges)


        }
        temDocObject = {
            ...temDocObject,
            // homeLoan: keyClausesObject,
            bankName: this.bankName
            // customerId:this.state.USERID
        }
        console.log("keyClausesObject======>", USERID)
        this.setState({ isLoading: true })
        this.props.updateDocumentsAPI(temDocObject, USERID).then((res => {
            console.log("KEYCLAUSESS", res)
            this.setState({ isLoading: false })
            if (res.statusCode == 200 && res.status) {
                this.hitBankList
                setTimeout(() => {
                    Toast.show(res.message, Toast.LONG);
                    this.props.navigation.navigate("LoanStatement", {
                        hitBankList: this.hitBankList,
                        temDocObject: keyClausesObject,
                        USERID: this.state.USERID,
                        bankName: this.bankName
                    })
                }, 2000);
            } else {
                Toast.show(res.message, Toast.LONG);
            }
        }))
            .catch(error => {
                this.setState({ isLoading: false })
                console.log(error, "error updateDocumentsAPI");
            })




        // this.props.navigation.navigate("HomeLoanScreen")
    }
    openDrawer = () => {

        const resetAction = CommonActions.reset({
            index: 0,
            routes: [{ name: 'KeyClausesAgree' }],
        });
        this.props.navigation.dispatch(resetAction);
        this.props.navigation.openDrawer()
        // this.props.navigation.openDrawer()
    }
    onPrincipleAmountChange = (text) => {
        // console.log(text, this.state.principalAmount)
        this.setState({
            principalAmount: text
        })
    }
    onLoanTenureChange = (text) => {
        this.setState({
            loanTenure: text
        })
    }
    onRateOfInterestChange = (text) => {
        this.setState({
            rateOfInterest: text
        })
    }
    onLoanTransferChargesChange = (text) => {
        this.setState({
            loanTransferCharges: text
        })
    }
    onPrePaymentPenalityChange = (text) => {
        this.setState({
            prePaymentPenality: text
        })
    }
    onForeclosuresChargesChange = (text) => {
        this.setState({
            foreclosuresCharges: text
        })
    }

    onInterestTypeChange = (text) => {
        this.setState({ interestTypeValue: text })
    }
    render() {
        console.log("KEYCLOUSES", this.USER_ID)
        const { principalAmount, loanTenure, interestTypeValue, rateOfInterest, loanTransferCharges, prePaymentPenality, foreclosuresCharges, interestType } = this.state
        return (
            <SafeAreaView style={styles.fullScreen}>
                <DashBoardHeader
                    openDrawer={() => this.openDrawer()}
                    headerTitle={res.strings.HOME_LOAN}
                    settingVisible={false}
                    notificationVisible={false}
                />
                <ImageBackground source={res.images.mainBackground} style={{ flex: 1 }}>
                    <View style={styles.mainView}>
                        <KeyboardAwareScrollView >


                            <View style={styles.subView}>
                                <View style={styles.keyClausesContainer}>
                                    <Text style={styles.keyClausesText}>{res.strings.KEY_CLAUSE_AGREEMENT}</Text>
                                </View>

                                <View style={{ marginHorizontal: 10, marginBottom: 50 }}>
                                    <OutLinedInput
                                        value={principalAmount}
                                        label={res.strings.Principal_Amount}
                                        placeholder={res.strings.IN_INR}
                                        onChangeText={this.onPrincipleAmountChange}
                                        keyboardType={"decimal-pad"}
                                    />
                                    <OutLinedInput
                                        value={loanTenure}
                                        label={res.strings.Loan_Tenure}
                                        placeholder={res.strings.IN_MONTH}
                                        onChangeText={this.onLoanTenureChange}
                                        keyboardType={"decimal-pad"}
                                    />
                                    <View style={{ marginTop: 10 }}>
                                        <Dropdown
                                      
                                            useNativeDriver={false}
                                            pickerStyle={{  borderRadius: 10,alignItems:'center',right:0 }}
                                            animationDuration={1}
                                            rippleDuration={1}
                                            onChangeText={(val, i, d) => {
                                                this.setState({ selectInterestType: d[i], interestTypeValue: val })
                                            }}
                                            data={this.state.interestType.map(item => ({
                                                value: item.value, ...item
                                            }))}
                                            value={this.state && this.state.interestType.length > 0 ? "" : ""}
                                            dropdownPosition={-3}
                                            renderBase={(props) => (
                                                <OutLinedInput
                                                    label={res.strings.Interest_Type}
                                                    placeholder={res.strings.SELECT}
                                                    onChangeText={this.onInterestTypeChange}
                                                    isDropDownImageVisible={true}
                                                    value={this.state.interestTypeValue}
                                                    inputProps={{
                                                        autoCaptialize: 'none',
                                                        returnKeyType: 'next',
                                                    }}
                                                >
                                                </OutLinedInput>
                                            )}

                                        />
                                    </View>
                                    <OutLinedInput
                                        value={rateOfInterest}
                                        label={res.strings.Rate_of_Interest}
                                        placeholder={res.strings.InPercentage}
                                        onChangeText={this.onRateOfInterestChange}
                                        keyboardType={"decimal-pad"}
                                    />
                                    <OutLinedInput
                                        value={loanTransferCharges}
                                        label={res.strings.Loan_Transfer_Charges}
                                        placeholder={res.strings.IN_RS_LAKH}
                                        onChangeText={this.onLoanTransferChargesChange}
                                        keyboardType={"decimal-pad"}
                                    />
                                    <OutLinedInput
                                        value={prePaymentPenality}
                                        label={res.strings.Pre_Payment_Penality}
                                        placeholder={res.strings.AS_PERCENTAGE}
                                        onChangeText={this.onPrePaymentPenalityChange}
                                        keyboardType={"decimal-pad"}
                                    />
                                    <OutLinedInput
                                        value={foreclosuresCharges}
                                        label={res.strings.Foreclosures_Charges}
                                        placeholder={res.strings.AS_PERCENTAGE}
                                        onChangeText={this.onForeclosuresChargesChange}
                                        keyboardType={"decimal-pad"}
                                    />
                                </View>

                            </View>



                        </KeyboardAwareScrollView>
                        <TouchableOpacity style={styles.btnSubmit} onPress={() => this.validationBeforesubmit()}>
                            <Text style={styles.btnText}>{res.strings.SUBMIT}</Text>
                        </TouchableOpacity>
                    </View>



                </ImageBackground>

            </SafeAreaView>

        )
    }


}
const mapStateToProps = (state) => {
    return {
        HomeLoan: state.HomeLoanReducer.HomeLoan
    };
};

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        updateDocumentsAPI,
        postDocumentsAPI
    }, dispatch);
}
let KeyClausesAgreeContainer = connect(mapStateToProps, mapDispatchToProps)(KeyClausesAgree);
export default KeyClausesAgreeContainer;

