import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import { View, SafeAreaView, Text } from 'react-native';
import styles from './styles'
import strings from '../../../../res/constants/strings';
import DashBoardHeader from '../../../components/header/DashBoardHeader'
import ViewPager from '@react-native-community/viewpager';
import res from '../../../../res'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import LoanStatement from "./LoanStatement";
import KeyClausesAgree from './KeyClausesAgree'

class HomeLoanAgreement extends Component {
    constructor(props) {
        super(props);
        this.state = {
            pageIndex: 0,
            progressImg: res.images.customizationStep1
        }
        this.viewPager = React.createRef()

    }
    componentDidMount() {

    }
    openDrawer = () => {
        this.props.navigation.openDrawer()
    }

    changePageIndex = (index) => {

        if (index == -1) {
            // close the drwr
            return
        }
        this.setState({ pageIndex: index })
        this.viewPager.setPage(index)

    }

    pageChanged = (e) => {

        if (e && e.nativeEvent && e.nativeEvent) {
            this.setState({ pageIndex: e.nativeEvent.position });

        }
    }



    render() {
        const { CostSheetReducer } = this.props
        console.log(CostSheetReducer, "CostSheetReducer");

        return (
            <SafeAreaView style={styles.fullScreen}>
                <DashBoardHeader
                    openDrawer={() => this.openDrawer()}
                    headerTitle={strings.HOME_LOAN}
                    settingVisible={false}
                    notificationVisible={false}
                />
              
                   
                        <ViewPager style={[styles.viewPager]} initialPage={0}
                            ref={(viewPager) => { this.viewPager = viewPager }}
                            onPageSelected={(e) => this.pageChanged(e)}
                        >
                            <View key="1" >
                                <KeyClausesAgree changePage={this.changePageIndex} />
                            </View>
                            <View key="2">
                                <LoanStatement navigation={this.props.navigation} changePage={this.changePageIndex} />
                            </View>
                            
                        </ViewPager>
                   




            </SafeAreaView>
        )
    }


}
const mapStateToProps = (state) => {
    return {
        CostSheetReducer: state.CostSheetReducer
    };
};

function mapDispatchToProps(dispatch) {
    return bindActionCreators({

    }, dispatch);
}
let HomeLoanAgreementScreenContainer = connect(mapStateToProps, mapDispatchToProps)(HomeLoanAgreement);
export default HomeLoanAgreementScreenContainer;

