import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import { View, Text, TouchableOpacity, ImageBackground, SafeAreaView, Image } from 'react-native';
import styles from './styles'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import OutLinedInput from '../../../components/input/OutLinedInput'
import Toast from 'react-native-simple-toast'
import DatePicker from 'react-native-datepicker'

import res from '../../../../res';
import { CommonActions } from '@react-navigation/native';
import DashBoardHeader from '../../../components/header/DashBoardHeader'
import { updateDocumentsAPI } from '../../../redux/actions/HomeLoan/HomeLoanAction'
import { getParsedDate } from '../../../utility/Utils'
import moment from 'moment'
class LoanStatement extends Component {


    constructor(props) {
        super(props);
        this.hitBankList = this.props.route.params && this.props.route.params.hitBankList;
        this.temDocObject = this.props.route.params && this.props.route.params.temDocObject;
        this.USERID = this.props.route.params && this.props.route.params.USERID;
        this.bankName = this.props.route.params && this.props.route.params.bankName;
        this.state = {
            amountDisbursed: "",
            principlePaid: "",
            interestPaid: "",
            outstandingPrinciple: "",
            outstandingInterest: "",
            preEmi: "",
            dateOfEmi: "",
            showDate: ""

        }
    }
    componentDidMount() {

    }

    backPress = () => {
        const { changePage } = this.props;
        changePage(-1)
    }
    validationBeforesubmit = () => {
        const { amountDisbursed,
            principlePaid,
            interestPaid,
            outstandingPrinciple,
            outstandingInterest,
            preEmi,
            dateOfEmi } = this.state
        if (amountDisbursed.trim() == "") {
            alert(res.strings.Principle_Amount_Empty)
        }
        else if (principlePaid.trim() == "") {
            alert(res.strings.LoanTenure_Empty)
        }
        else if (outstandingPrinciple.trim() == "") {
            alert(res.strings.Interest_Type_Empty)
        }
        else if (outstandingInterest.trim() == "") {
            alert(res.strings.Rate_OF_Interest_Empty)
        }
        else if (preEmi.trim() == "") {
            alert(res.strings.Loan_Transfer_Empty)
        }
        else if (interestPaid.trim() == "") {
            alert(res.strings.Pre_Payment_Empty)
        }
        else {
            this.moveToHomeScreen()
        }

    }
    moveToHomeScreen = () => {

        const { amountDisbursed,
            principlePaid,
            interestPaid,
            outstandingPrinciple,
            outstandingInterest,
            preEmi,
            dateOfEmi } = this.state
        const { HomeLoan } = this.props;
        let temDocObject =
        {
            "bankName": "",
            "applicationFormDulyFilled": {
                "name": ""
            },
            "photos": {
                "name": ""
            },
            "addressProof": {
                "name": ""
            },
            "idProof": {
                "name": ""
            },
            "employerIDCard": {
                "name": ""
            },
            "accountStatement": {
                "name": ""
            },
            "previousLoan": {
                "name": ""
            },
            "salarySlip": {
                "name": ""
            },
            "form16": {
                "name": ""
            },
            "propertyPapers": {
                "name": ""
            },
            "paymentReceipts": {
                "name": ""
            },
            "aggreement": {
                "name": ""
            },
            "nocFromBuilder": {
                "name": ""
            },
            "proofOfAssets": {
                "name": ""
            },
            "companyProfile": {
                "name": ""
            },
            "educationalCertificates": {
                "name": ""
            },
            "employmentProof": {
                "name": ""
            },
            "detailedCostEstimate": {
                "name": ""
            },
            "sanctionLetter": {
                "name": ""
            },
            "IRT": {
                "name": ""
            },
            "ageProof": {
                "name": ""
            },
            "panCard": {
                "name": ""
            },

            "homeLoan": {
                "principalAmount": 0,
                "loanTenture": 0,
                "interestType": "",
                "rateOfInterest": 0,
                "loanTransferCharges": 0,
                "prepaymentPenality": 0,
                "foreClosureCharges": 0
            },
            "loanStatement": [
                {

                    "amountDisbursed": null,
                    "principlePaid": null,
                    "interestPaid": null,
                    "outstandingPrinciple": null,
                    "outstandingInterest": null,
                    "preEmi": null,
                    "dateOfEmi": null
                }
            ]

        }

        // let temDocObject = preObject
        const loanStatementObject = {
            amountDisbursed: parseFloat(amountDisbursed),
            principlePaid: parseFloat(principlePaid),
            interestPaid: parseFloat(interestPaid),
            outstandingPrinciple: parseFloat(outstandingPrinciple),
            outstandingInterest: parseFloat(outstandingInterest),
            preEmi: parseFloat(preEmi),
            dateOfEmi: dateOfEmi//"1970-01-01T00:00:00.454Z"


        }
        let loanStatementArr = []

        loanStatementArr.push(loanStatementObject)

        temDocObject = {
            ...temDocObject,
            loanStatement: loanStatementArr,
            homeLoan: this.temDocObject,
            bankName: this.bankName
            // customerId:this.state.USERID
        }
        console.log("temDocObject====>", temDocObject)
        this.setState({ isLoading: true })
        this.props.updateDocumentsAPI(temDocObject, this.USERID).then((res => {
            console.log("updateDocumentsAPI", res)
            this.setState({ isLoading: false })
            if (res.statusCode == 200 && res.status || res.statusCode == 400) {
                this.hitBankList
                setTimeout(() => {
                    Toast.show(res.message, Toast.LONG);
                    this.props.navigation.navigate("HomeLoanScreen",)
                }, 2000);
            } else {
                Toast.show(res.message, Toast.LONG);
            }
        }))
            .catch(error => {
                this.setState({ isLoading: false })
                console.log(error, "error updateDocumentsAPI");
            })




        // this.props.navigation.navigate("HomeLoanScreen")
    }
    openDrawer = () => {

        const resetAction = CommonActions.reset({
            index: 0,
            routes: [{ name: 'KeyClausesAgree' }],
        });
        this.props.navigation.dispatch(resetAction);
        this.props.navigation.openDrawer()
        // this.props.navigation.openDrawer()
    }
    onPrincipleAmountChange = (text) => {
        console.log(text, this.state.principalAmount)
        this.setState({
            principlePaid: text
        })
    }
    onInterestRateChange = (text) => {
        this.setState({
            interestPaid: text
        })
    }
    onDateOfEmiChange = (text) => {
        var dateString = text;
        var time2 = Date.parse(text);
        var newdate = moment(time2).format('DD MMMM yyy HH:MM UTCC')
        newdate = newdate.substring(1, newdate.length - 1);
        const event = new Date(newdate);
        const finalDate = event.toISOString()
        var showDate = moment(text).format('DD/MM/YYYY')
        console.log("finalDate", finalDate, new Date(finalDate))
        this.setState({
            dateOfEmi: new Date(finalDate),
            showDate: showDate
        })
    }
    onOutstandingPrincipleChange = (text) => {
        this.setState({
            outstandingPrinciple: text
        })
    }
    onPrincipleamoutbursedChange = (text) => {
        this.setState({
            amountDisbursed: text
        })
    }
    onOutstandingInterestChange = (text) => {
        this.setState({
            outstandingInterest: text
        })
    }
    onpreEmiChange = (text) => {
        this.setState({
            preEmi: text
        })
    }
    goBack = () => {
        this.props.navigation.goBack()
    }

    render() {
        console.log("KEYCLOUSES====>", this.temDocObject)
        const { amountDisbursed,
            principlePaid,
            interestPaid,
            outstandingPrinciple,
            outstandingInterest,
            preEmi,
            dateOfEmi,
            showDate } = this.state
        return (
            <SafeAreaView style={styles.fullScreen}>
                <DashBoardHeader
                    openDrawer={() => this.openDrawer()}
                    headerTitle={res.strings.HOME_LOAN}
                    settingVisible={false}
                    notificationVisible={false}
                />
                <ImageBackground source={res.images.mainBackground} style={{ flex: 1 }}>
                    <View style={styles.mainView}>
                        <KeyboardAwareScrollView >


                            <View style={styles.subView}>

                                <View style={styles.headerMainView}>
                                    <TouchableOpacity onPress={() => this.goBack()}>
                                        <Image source={res.images.icn_back} />
                                    </TouchableOpacity>
                                    <Text style={styles.keyClausesText}>{res.strings.Loan_Statement}</Text>
                                    <View />
                                </View>



                                <View style={{ marginHorizontal: 10, marginBottom: 50 }}>
                                    <OutLinedInput
                                        value={principlePaid}
                                        label={res.strings.Principal_Paid}
                                        placeholder={res.strings.RS}
                                        onChangeText={this.onPrincipleAmountChange}
                                        keyboardType={"decimal-pad"}
                                    />

                                    <OutLinedInput
                                        value={interestPaid}
                                        label={res.strings.Interest_Paid}
                                        placeholder={res.strings.RS}
                                        onChangeText={this.onInterestRateChange}
                                        keyboardType={"decimal-pad"}
                                    />

                                    <OutLinedInput
                                        value={outstandingPrinciple}
                                        label={res.strings.Outstanding_Principal}
                                        placeholder={res.strings.RS}
                                        onChangeText={this.onOutstandingPrincipleChange}
                                        keyboardType={"decimal-pad"}
                                    />
                                    <OutLinedInput
                                        value={outstandingInterest}
                                        label={res.strings.Outstanding_Interest}
                                        placeholder={res.strings.RS}
                                        onChangeText={this.onOutstandingInterestChange}
                                        keyboardType={"decimal-pad"}
                                    />
                                    <OutLinedInput
                                        value={amountDisbursed}
                                        label={res.strings.Principle_amout_disbursed}
                                        placeholder={res.strings.RS}
                                        onChangeText={this.onPrincipleamoutbursedChange}
                                        keyboardType={"decimal-pad"}
                                    />
                                    <OutLinedInput
                                        value={preEmi}
                                        label={res.strings.Pre_EMI}
                                        placeholder={res.strings.RS}
                                        onChangeText={this.onpreEmiChange}
                                        keyboardType={"decimal-pad"}
                                    />
                                    <View
                                        style={[
                                            styles.container,
                                            {
                                                borderColor: res.colors.appColor,
                                                marginTop: 20
                                            }

                                        ]}
                                    >
                                        <DatePicker

                                            label={res.strings.Date_of_Birth}
                                            style={{ ...styles.inputContainer, color: res.colors.lightGreyBlue }}
                                            date={showDate}
                                            mode="date"
                                            placeholder={"DD/MM/YYYY"}
                                            placeholderText={{ color: 'red' }}
                                            //  format={"DD/MM/YYYY"}
                                            // minDate="01-01-1970"
                                            confirmBtnText="Confirm"
                                            cancelBtnText="Cancel"
                                            showIcon={false}
                                            customStyles={{
                                                placeholderText: {
                                                    color: res.colors.peacockBlueLight
                                                },
                                                dateText: {
                                                    color: res.colors.peacockBlue
                                                },
                                                dateInput: {
                                                    height: 40,
                                                    justifyContent: 'center',
                                                    alignItems: 'flex-start',
                                                    backgroundColor: res.colors.white,
                                                    borderWidth: 0,
                                                    color: res.colors.peacockBlue

                                                }

                                            }}
                                            onDateChange={this.onDateOfEmiChange}
                                        />


                                        <View style={{
                                            position: "absolute",
                                            top: -10,
                                            // width: labelWidth,
                                            left: 20,
                                            backgroundColor: res.colors.white,
                                            minHeight: 10,
                                        }}>
                                            <Text style={styles.labelStyle}>{res.strings.Date_Of_Payment}</Text>
                                        </View>
                                    </View>
                                </View>
                            </View>



                        </KeyboardAwareScrollView>
                        <TouchableOpacity style={styles.btnSubmit} onPress={() => this.validationBeforesubmit()}>
                            <Text style={styles.btnText}>{res.strings.SUBMIT}</Text>
                        </TouchableOpacity>
                    </View>



                </ImageBackground>

            </SafeAreaView>

        )
    }


}
const mapStateToProps = (state) => {
    return {
        HomeLoan: state.HomeLoanReducer.HomeLoan
    };
};

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        updateDocumentsAPI
    }, dispatch);
}
let LoanStatementContainer = connect(mapStateToProps, mapDispatchToProps)(LoanStatement);
export default LoanStatementContainer;



