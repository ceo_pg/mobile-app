import { StyleSheet, StatusBar } from 'react-native'
import res from '../../../../res'
import { heightScale, widthScale } from '../../../utility/Utils'
const styles = StyleSheet.create({
    fullScreen: {
        flex: 1,
        backgroundColor: res.colors.appColor,
        paddingTop: Platform.OS === "android" ? StatusBar.currentHeight + 10 : 0
    },
    viewPager: {
        flex: 1,
        backgroundColor: res.colors.butterscotch,
        marginHorizontal: widthScale(20),
        borderRadius: widthScale(20),
        marginTop: heightScale(18)
    }, mainView: {
        // flex: 1,
        backgroundColor: res.colors.butterscotch,
        margin: widthScale(10),
        padding: widthScale(10),
        borderRadius: widthScale(20),
        marginTop: heightScale(25),
        marginBottom:heightScale(50),
        overflow:'hidden'
    }, subView: {
        flex: 1,
        backgroundColor: res.colors.white,
        // margin: widthScale(10),
        borderTopLeftRadius: widthScale(20),
        borderTopRightRadius: widthScale(20),
    }, keyClausesContainer: {
        marginVertical: heightScale(5),
        borderTopLeftRadius: widthScale(20),
        borderTopRightRadius: widthScale(20),
        flex: 1,
      padding:widthScale(20)

    },
    shadowViewContainer: {
        margin: widthScale(10),
        borderRadius: widthScale(10),
        backgroundColor: res.colors.white,
        flex: 1,
        justifyContent: 'center',
        // alignItems: 'center',
        shadowColor: "rgba(0,0,0, 0.1)",
        shadowOffset: {
            width: 0,
            height: 4,
        },
        shadowOpacity: 0.8,
        shadowRadius: 10,
        elevation: 10,
    }, headerTitle: {
        textAlign: 'center'
    }, headerView: {
        backgroundColor: res.colors.white,
        height: heightScale(50),
        flex: 1,
        // justifyContent: 'center',
        // alignItems: 'center',
        // shadowColor: "rgba(0,0,0, 0.1)",
        shadowOffset: {
            width: 0,
            height: 0,
        },
        shadowOpacity: 0,
        shadowRadius: 0,
        elevation: 0,
        borderTopLeftRadius: 0,
        borderTopRightRadius: 0
    }, loanView: {
        flex: 1
    }, btnSubmit: {
        width: '100%',
        height: 44,
        alignItems: 'center',
        justifyContent:'center'
    }, btnText: {
        fontWeight:'bold',
        fontFamily: res.fonts.semiBold,
        fontSize: 18,
        color: res.colors.formLabelGrey
    }, keyClausesText: {
        fontWeight: 'bold',
        fontFamily: res.fonts.semiBold,
        fontSize: widthScale(18),
        color: res.colors.blackTwo,
        textAlign: 'center'
    },inputContainer: {
        flex: 1,
        paddingHorizontal: 20,
        fontSize: 16
    }, container: {
        height: 45,
        borderWidth: 1,
        borderColor: res.colors.appColor,
        borderRadius: 5,
        backgroundColor: res.colors.white
    },  labelStyle: {
        fontFamily: res.fonts.medium,
        color: res.colors.peacockBlue,
        fontSize: 12,
        letterSpacing: 0.5,
        lineHeight: 16

    },headerMainView:{
        borderTopLeftRadius: widthScale(20),
        borderTopRightRadius: widthScale(20),
        flex: 1,
        paddingVertical: widthScale(20),
        justifyContent: 'space-around',
        flexDirection: 'row'
    }
})

export default styles