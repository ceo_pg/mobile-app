import React, { Component } from 'react'
import { View, Text, Linking, TouchableOpacity, Image, Dimensions, Platform } from 'react-native'
import ImagePicker from 'react-native-image-picker'
import DocumentPicker from 'react-native-document-picker'
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import AsyncStorage from '@react-native-community/async-storage'
import Toast from 'react-native-simple-toast'
import Modal from 'react-native-modal'
import PDFView from 'react-native-view-pdf';
import ImageZoom from 'react-native-image-pan-zoom';
import Input from '../../../components/input/OtpField'
import styles from '../../legalDocumentation/styles'
import res from '../../../../res'
import { heightScale, myWidth, checkIsImageOrPdfFileType, checkValidFileSize, validatePasscode } from '../../../utility/Utils'
import { DocItemComponent, Loader } from '../../../components/index'
import { PickerViewModal, CAMERA, DOCUMENT, IMAGE_PICK } from '../../../components/Picker/PickerViewModal'
import { fileUpload } from '../../../redux/actions/FileUploadAction'
import { updateDocumentsAPI } from '../../../redux/actions/HomeLoan/HomeLoanAction'
import DocumentItemComp from '../../../components/legalDoc/DocumentItemComp'
import Pdf from 'react-native-pdf';
import RNFS from 'react-native-fs'
const options = {

    title: 'Select Image',
    mediaType: 'photo',
    allowsEditing: false,
    quality: 1.0,
    maxWidth: 500,
    maxHeight: 400,
    progress: {},
    storageOptions: {
        skipBackup: true,
        path: 'images',
    },
}
class HomeLoanDoc extends Component {
    constructor(props) {
        super(props)

        this.modalRef = React.createRef();
        this.otpFirst = React.createRef();
        this.otpSecond = React.createRef();
        this.otpThird = React.createRef();
        this.otpFourth = React.createRef();
        this.state = {
            pickOptionsVisible: null,
            progress: {},
            isLoading: false,
            otp1: '',
            otp2: '',
            otp3: '',
            otp4: '',
            isModalVisible: false,
            isDataModal: false,
            isShare: false
        }

    }

    showPickerOptions = (uniqueID) => {
        setTimeout(() => {
            this.setState({
                pickOptionsVisible: 'bottom'
            })
        }, 1000);
        this.uniqueID = uniqueID;
    }
    onResultFromImagePicker = (response) => {
        console.log("onResultFromImagePicker", response)

        if (response.didCancel) {
            this.setState({
                pickOptionsVisible: null
            })
        } else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
            this.setState({
                pickOptionsVisible: null
            })
        } else {
            this.onResultFromPicker(response);
        }
    }
    onResultFromPicker = (response) => {
        if (!response) {
            Toast.show("Please try again")
            return
        }
        console.log("uri => " + response.uri + "\n " +
            "type => " + response.type + "\n Size => " +
            response.size ? response.size : response.fileSize)
        this.setState({
            pickOptionsVisible: null
        })
        if (checkIsImageOrPdfFileType(response.type)) {
            if (checkValidFileSize(response.size ? response.size : response.fileSize)) {
                console.log("checkValidFileSize", response.size)
            } else {
                return Toast.show("Size can not exceeds 10 MB")
            }
        } else {
            return Toast.show("Please select pdf,jpg,jpeg,png format documents only")
        }
        // UPLOAD File then hit update DOC API
        this.hitUploadAPI(response)

    }
    progressCallback = (uniqueID, progress) => {
        this.setState({ progress: { ...this.state.progress, [uniqueID]: (progress.loaded / (progress.total)) } }, () => {
            console.log(this.state.progress, "progress value")
        })
    }
    hitUploadAPI = async (fileDetails) => {
        console.log("Hithomedocapi", fileDetails)
        const userId = res.AsyncStorageContants.USERID;
        this.setState({
            isLoading: true
        })
        try {

            this.props.fileUpload(fileDetails, this.progressCallback.bind(this, this.uniqueID)).then((response => {
                console.log("file updaded response", response);
                if (response.statusCode == 200 && response.status) {
                    this.updateDocument(response.data.key);
                    this.setState({
                        isLoading: false
                    })
                } else {
                    Toast.show(response.message ? response.message : res.strings.PLEASE_TRY_AFTER_SOMETIME);
                    this.setState({
                        isLoading: false
                    })
                }

            }))
                .catch(error => {
                    console.log(error, "error");
                    this.setState({
                        isLoading: false
                    })
                })

        } catch (e) {
            console.log("Error in homeloan doc.", e);
            this.setState({
                isLoading: false
            })
        }
    }
    updateDocument = (fileName) => {
        console.log("HOMELOANDOC1", this.props, fileName)
        const { HomeLoan } = this.props;
        let homeLoanObject = HomeLoan;
        if (homeLoanObject.applicationFormDulyFilled) {
            delete homeLoanObject.applicationFormDulyFilled.path
        } if (homeLoanObject.salarySlip) {
            delete homeLoanObject.salarySlip.path
        } if (homeLoanObject.employmentProof) {
            delete homeLoanObject.employmentProof.path
        } if (homeLoanObject.idProof) {
            delete homeLoanObject.idProof.path
        } if (homeLoanObject.IRT) {
            delete homeLoanObject.IRT.path
        }

        switch (this.uniqueID) {
            case "Loan_Application":
                homeLoanObject.applicationFormDulyFilled.name = fileName
                break;
            case "Income_Proof":
                homeLoanObject.salarySlip.name = fileName
                break;
            case "Employment_Proof":
                homeLoanObject.employmentProof.name = fileName
                break
            case "ID_Proof":
                homeLoanObject.idProof.name = fileName
                break
            case "IRT":
                homeLoanObject.IRT.name = fileName
                break
            default:
            // code block
        }
        this.setState({ isLoading: true })
        this.props.updateDocumentsAPI(homeLoanObject, this.props.USER_ID).then((res => {
            console.log("updateDocumentsAPI", this.props.USER_ID)
            this.setState({ isLoading: false })
            if (res.statusCode == 200 && res.status) {
                this.props.getHomeLoanDetails(this.props.USER_ID)
                this.setState({
                    isLoading: false
                })

                setTimeout(() => {
                    Toast.show(res.message, Toast.LONG);
                }, 2000);
            } else {
                Toast.show(res.message, Toast.LONG);
                this.setState({
                    isLoading: false
                })
            }
        }))
            .catch(error => {
                this.setState({ isLoading: false })
                console.log(error, "error updateDocumentsAPI");
            })
    }
    openDocument = async () => {
        try {
            const res = await DocumentPicker.pick({
                type: [DocumentPicker.types.pdf, DocumentPicker.types.images],
            });
            console.log("Document", res)
            this.onResultFromPicker(res);
        } catch (err) {
            this.setState({
                pickOptionsVisible: null
            })
            if (DocumentPicker.isCancel(err)) {
                // User cancelled the picker, exit any dialogs or menus and move on
            } else {
                throw err;
            }
        }

    }
    onClickPickType = (type) => {
        this.setState({
            pickOptionsVisible: null
        })
        setTimeout(() => {
            switch (type) {
                case DOCUMENT:
                    // Open File picker
                    this.openDocument()
                    break;
                case IMAGE_PICK:
                    // Open Image Library:
                    ImagePicker.launchImageLibrary(options, this.onResultFromImagePicker);
                    break;
                case CAMERA:
                    // Launch Camera:
                    ImagePicker.launchCamera(options, this.onResultFromImagePicker);
                    break;
                default:
                    break;
            }
        }, 500)

    }
    onPressBackDrop = () => {
        this.setState({
            pickOptionsVisible: null
        })
    }
    renderLoader = () => {
        return (
            <Loader
                loading={this.state.isLoading} />
        );
    };
    onfocusFirstOtp = () => {

        this.otpFirst.current.focus();
    }
    onfocusSecondOtp = () => {

        this.otpSecond.current.focus();
    }
    onfocusThirdOtp = () => {
        this.otpThird.current.focus();
    }
    onfocusFourthOtp = () => {
        this.otpFourth.current.focus();
    }
    // onfocusFiveOtp = () => {
    //     this.otpFive.current.focus();
    // }
    // onfocusSixOtp = () => {
    //     this.otpSix.current.focus();
    // }
    onOtp1Change = (text) => {
        if (text.length == 1) {
            this.onfocusSecondOtp();
        }
        this.setState({
            otp1: text
        }, () => {
            this.validatePassCode();
        });
    }
    onOtp2Change = (text) => {
        if (text.length == 1) {
            this.onfocusThirdOtp();
        }
        this.setState({
            otp2: text
        }, () => {
            this.validatePassCode();
        });
    }
    onOtp3Change = (text) => {
        if (text.length == 1) {
            this.onfocusFourthOtp();
        }
        this.setState({
            otp3: text
        }, () => {
            this.validatePassCode();
        });
    }
    onOtp4Change = (text) => {
        // if (text.length == 1) {
        //     this.onfocusFiveOtp();
        // }
        this.setState({
            otp4: text
        }, () => {
            this.validatePassCode();
        });

    }

    validatePassCode = () => {
        // let appUsrObj = AppUser.getInstance();
        // let phoneNumber  = appUsrObj.phone;
        // var lastFourDigit = phoneNumber.substr(phoneNumber.length - 4)
        const { otp1, otp2, otp3, otp4, isShare } = this.state;
        console.log("ISSGARE", isShare)
        if (otp1.length > 0 && otp2.length > 0 && otp3.length > 0 && otp4.length > 0) {
            let passCode = otp1 + otp2 + otp3 + otp4;
            console.log(validatePasscode(passCode), "validatePasscode");
            if (validatePasscode(passCode)) {
                isShare ?
                    this.OpenWeb(this.selectedURL)
                    :
                    this.OpenURL(this.selectedURL);
                setTimeout(() => {
                    this.setState({ otp1: '', otp2: '', otp3: '', otp4: '' });
                }, 1000);
            } else {
                Toast.show("Passcode Dosen't matched", Toast.LONG);
            }
            // if(lastFourDigit == otp1 + otp2 + otp3 + otp4){
            //     this.OpenURL();
            // }
        }
    }
    OpenURL = (url) => {
        this.toggleModal()
        console.log("URL", url)
        if (!url) {
            return <Text>No data found</Text>
        } else {
            setTimeout(() => {
                this.toggleDataModal()
            }, 1000);

        }

    }
    OpenWeb = (url) => {
        this.toggleModal()
        Linking.canOpenURL(url).then(supported => {
            if (supported) {
                Linking.openURL(url);
            } else {
                console.log("Don't know how to open URI: " + url);
            }
        });
    }
    displayOtpModal = (isShare) => {
        this.toggleModal(isShare)
    }
    closeModal = () => {
        this.toggleModal()
    }
    viewDocument = (data) => {
        // this.closeModal();
        setTimeout(() => {
            this.displayOtpModal();
            this.selectedURL = data.path ? data.path : ''
        }, 1000);
    }
    shareDocument = (data, isShare) => {
        console.log("ISSHARETOGGLE",data, isShare)
        setTimeout(() => {
            this.displayOtpModal(isShare);
            this.selectedURL = data.path ? data.path : ''
        }, 1000);
    }
    onBackFromOtp2 = (event) => {
        if (event === 'Backspace') {
            this.setState({ otp2: '' })
            this.onfocusFirstOtp();
        }
    }
    onBackFromOtp3 = (event) => {
        if (event === 'Backspace') {
            this.setState({ otp3: '' })
            this.onfocusSecondOtp();
        }

    }
    onBackFromOtp4 = (event) => {
        if (event === 'Backspace') {
            this.setState({ otp4: '' })
            this.onfocusThirdOtp();
        }
    }
    renderOtpModal = () => {
        return (
            <View style={styles.otpContainerView}>
                <TouchableOpacity style={styles.closeBtnView} onPress={() => this.closeModal()}>
                    <Image source={res.images.icnClose} resizeMode={'contain'} style={{ height: 40, width: 40 }} />
                </TouchableOpacity>
                <Text style={styles.passcodeText}>{res.strings.SHARE_PASSCODE_SHARED_ON_YOUR_DEVICE}</Text>
                <View style={styles.iconsCOntainer}>
                    <Input
                        value={this.state.otp1}
                        onChangeText={this.onOtp1Change}
                        containerStyle={styles.textInputStyle}
                        inputProps={{
                            maxLength: 1,
                            keyboardType: "numeric",
                            secureTextEntry: true,
                            returnKeyType: "next",
                            ref: this.otpFirst,
                            textAlign: "center",
                        }}
                        inputStyles={styles.inputStyle}
                    />
                    <Input
                        value={this.state.otp2}
                        onBackKeyPress={this.onBackFromOtp2}
                        onChangeText={this.onOtp2Change}
                        containerStyle={styles.textInputStyle}
                        inputProps={{
                            keyboardType: 'numeric',
                            secureTextEntry: true,
                            maxLength: 1,
                            returnKeyType: "next",
                            ref: this.otpSecond,
                            textAlign: "center",
                        }}
                        inputStyles={styles.inputStyle}
                    />
                    <Input
                        value={this.state.otp3}
                        onBackKeyPress={this.onBackFromOtp3}
                        onChangeText={this.onOtp3Change}
                        containerStyle={styles.textInputStyle}
                        inputProps={{
                            keyboardType: 'numeric',
                            returnKeyType: "next",
                            maxLength: 1,
                            secureTextEntry: true,
                            ref: this.otpThird,
                            textAlign: "center",
                        }}
                        inputStyles={styles.inputStyle}
                    />
                    <Input
                        value={this.state.otp4}
                        onBackKeyPress={this.onBackFromOtp4}
                        onChangeText={this.onOtp4Change}
                        containerStyle={styles.textInputStyle}
                        inputProps={{
                            keyboardType: 'numeric',
                            maxLength: 1,
                            returnKeyType: "next",
                            secureTextEntry: true,
                            ref: this.otpFourth,
                            textAlign: "center",
                        }}
                        inputStyles={styles.inputStyle}
                    />
                </View>
            </View>
        )
    }
    renderDataModal = () => {
        const brochurePdf = this.selectedURL
        const resources = {
            file: Platform.OS === 'ios' ? 'downloadedDocument.pdf' : '/sdcard/Download/downloadedDocument.pdf',
            url: brochurePdf,
            base64: 'JVBERi0xLjMKJcfs...',
        }
        const resourceType = 'url';
        const source = { uri: brochurePdf, cache: true }
        console.log("renderDataModal====.", brochurePdf)
        if (!brochurePdf) {
            return <View />
        }
        return (
            brochurePdf.toLowerCase().includes(".pdf") ?
                <View style={{ borderRadius: 10, flex: 1 }}>

                    <View style={styles.closeBtn}>
                        <TouchableOpacity onPress={() => this.toggleDataModal()}>
                            <Image source={res.images.icnClose} style={{ width: 20, height: 20, tintColor: res.colors.white }} resizeMode={'center'} />
                        </TouchableOpacity>
                    </View>
                    {
                        Platform.OS == 'ios' ?
                            <Pdf
                                source={source}
                                style={{ flex: 1 }}
                            />
                            :
                            <PDFView
                                // fadeInDuration={250.0}
                                style={{ flex: 1, }}
                                resource={resources[resourceType]}
                                resourceType={resourceType}
                                onLoad={() => console.log(`PDF rendered from ${resourceType}`)}
                                onError={(error) => console.log('Cannot render PDF', error)}
                            />
                    }
                </View>
                :

                <View style={{ borderRadius: 10, flex: 1 }}>
                    <View style={styles.closeBtn}>
                        <TouchableOpacity onPress={() => this.toggleDataModal()}>
                            <Image source={res.images.icnClose} style={{ width: 20, height: 20, tintColor: res.colors.white }} resizeMode={'center'} />
                        </TouchableOpacity>
                    </View>
                    <View style={{ width: "100%", height: "100%", zIndex: 9999, overflow: 'hidden' }}>
                        <ImageZoom cropWidth={Dimensions.get('window').width}
                            cropHeight={Dimensions.get('window').height}
                            imageWidth={Dimensions.get('window').width}
                            imageHeight={Dimensions.get('window').height}>

                            <Image source={brochurePdf ? { uri: brochurePdf } : res.images.parking} style={{ flex: 1 }} resizeMode={'contain'} />

                        </ImageZoom>

                    </View>
                </View>



        )
    }
    toggleModal = (isShare) => {
       
        const { isModalVisible } = this.state
        isShare ?

            this.setState({ isModalVisible: !isModalVisible, isShare: true })
            :
            this.setState({ isModalVisible: !isModalVisible, isShare: false })
    }
    toggleDataModal = () => {
        const { isDataModal } = this.state
        console.log("toggleDataModal", isDataModal)
        this.setState({ isDataModal: !isDataModal })
    }

   

    render() {
        const { isLoading, isModalVisible } = this.state
        const { applicationFormDulyFilled, salarySlip, employmentProof, idProof, IRT } = this.props.HomeLoan;
        console.log("this.props.HomeLoan", this.props.HomeLoan)
        if (!applicationFormDulyFilled) {
            return <View />
        }
        return (


            <View style={{ flex: 1, marginBottom: 200 }}>
                <Modal isVisible={this.state.isModalVisible}
                    // onSwipeComplete={() => this.toggleModal()}
                    // swipeDirection={'down'}
                    onBackdropPress={() => this.toggleModal()}
                    style={styles.view}>
                    {this.props ? this.renderOtpModal() : <View />}
                </Modal>
                <Modal isVisible={this.state.isDataModal}
                    // onSwipeComplete={() => this.toggleModal()}
                    // swipeDirection={'down'}
                    onBackdropPress={() => this.toggleDataModal()}
                    style={{ zIndex: 99999 }}>
                    {this.renderDataModal()}
                </Modal>
                <DocumentItemComp label={res.strings.Loan_Application} data={applicationFormDulyFilled} viewDocument={this.viewDocument} shareDocument={this.shareDocument} onUpdateDoc={this.showPickerOptions} uniqueID="Loan_Application" />
                <DocumentItemComp label={res.strings.Income_Proof} data={salarySlip} viewDocument={this.viewDocument} shareDocument={this.shareDocument} onUpdateDoc={this.showPickerOptions} uniqueID="Income_Proof" />
                <DocumentItemComp label={res.strings.Employment_Proof} data={employmentProof} viewDocument={this.viewDocument} shareDocument={this.shareDocument} onUpdateDoc={this.showPickerOptions} uniqueID="Employment_Proof" />
                <DocumentItemComp label={res.strings.ID_Proof} data={idProof} viewDocument={this.viewDocument} shareDocument={this.shareDocument} onUpdateDoc={this.showPickerOptions} uniqueID="ID_Proof" />
                <DocumentItemComp label={res.strings.ITR} data={IRT} viewDocument={this.viewDocument} shareDocument={this.shareDocument} onUpdateDoc={this.showPickerOptions} uniqueID="IRT" />

                {this.state.pickOptionsVisible ?
                    <PickerViewModal
                        onClickPickType={this.onClickPickType}
                        visibleModal={this.state.pickOptionsVisible}
                        onPressBackDrop={this.onPressBackDrop} /> : <View />}
                {isLoading && this.renderLoader()}




            </View>


        )

    }
}
const mapStateToProps = (state) => {
    return {
        HomeLoan: state.HomeLoanReducer.HomeLoan
    };
};

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        fileUpload,
        updateDocumentsAPI
    }, dispatch);
}
let HomeLoanDocScreenContainer = connect(mapStateToProps, mapDispatchToProps)(HomeLoanDoc);
export default HomeLoanDocScreenContainer;
// export default HomeLoanDoc