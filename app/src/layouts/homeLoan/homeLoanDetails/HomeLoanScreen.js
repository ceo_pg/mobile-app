import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import { View, SafeAreaView, Text, Dimensions, Image, TouchableOpacity, ImageBackground } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { CommonActions } from '@react-navigation/native';
import Modal from 'react-native-modal'
import AsyncStorage from '@react-native-community/async-storage'
import Pie from 'react-native-pie'
import Carousel, { Pagination } from 'react-native-snap-carousel';

import styles from './styles'
import DashBoardHeader from '../../../components/header/DashBoardHeader'
import res from '../../../../res'
import FinacialParameter from './FinacialParameter'
import { FinacialParamModal, OtherTermsModal, HomeLoanDocModal, PieGraph, Loader } from '../../../components/index';
import OtherTerms from './OtherTerms';
import HomeLoanDoc from './HomeLoanDoc'
import { getHomeLoanDetails, saveHomeLoanDetails } from '../../../redux/actions/HomeLoan/HomeLoanAction'
import { widthScale, heightScale } from '../../../utility/Utils';

class HomeLoanScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            isLoading: false,
            finacialParameter: [],
            isModalVisible: false,
            OtherTerms: {},
            activeIndex: 0,
            homeLoanScreen: [
                {
                    "name": res.strings.Financial_Parameters,
                    "id": 0
                }, {
                    "name": res.strings.DOCUMENTS,
                    "id": 1
                }, {
                    "name": res.strings.Other_Terms,
                    "id": 2
                },
            ],
            userId: ""
        }
    }
    componentDidMount = async () => {
        setTimeout(() => {
            this.setState({ loading: false });
          }, 10);
        const userId = res.AsyncStorageContants.USERID;
        try {
            let USER_ID = await AsyncStorage.getItem(userId)
            this.setState({
                userId: USER_ID
            })
            this.hitHomeLoanDetailsApi(USER_ID)
        } catch (e) {
            console.log("Error removing data from async storage.", e);
        }
    }
    hitHomeLoanDetailsApi = (USER_ID) => {
        console.log("hitHomeLoanDetailsApi===>", USER_ID)
        this.setState({ isLoading: true })
        this.props.getHomeLoanDetails(USER_ID).then((res) => {
            console.log("RESPONCE OF HOMELOAN", res)
            this.setState({ isLoading: false })
            this.props.saveHomeLoanDetails(res.data)

        }).catch((err) => {
            this.setState({ isLoading: false })
        })
    }
    openDrawer = () => {
        const resetAction = CommonActions.reset({
            index: 0,
            routes: [{ name: 'HomeLoanScreen' }],
        });
        this.props.navigation.dispatch(resetAction);
        this.props.navigation.openDrawer()

    }
    renderFinacialParameter = () => {
        const { finacialParameter } = this.state
        console.log("finacialParameter", finacialParameter)
        return (
            <KeyboardAwareScrollView scrollEnabled={true}>
                <View style={{ flex: 1 }}>
                    <FinacialParameter
                    />
                </View>
            </KeyboardAwareScrollView>
        )
    }
    renderDocument = () => {

        return (
            <KeyboardAwareScrollView
                scrollEnabled={true}
            >
                <View style={{ flex: 1 }}>
                    <HomeLoanDoc
                        USER_ID={this.state.userId}
                        getHomeLoanDetails={this.hitHomeLoanDetailsApi}
                    />
                </View>
            </KeyboardAwareScrollView>
        )
    }
    renderOtherTerms = () => {
        return <KeyboardAwareScrollView
            scrollEnabled={true}
        >
            <View style={{ flex: 1 }}>
                <OtherTerms
                // OtherTerms={this.state.OtherTerms}
                />
            </View>
        </KeyboardAwareScrollView>
    }
    renderCorousalData = (item, index) => {
        const id = item.item.id
        console.log("FINACIAL", item)
        switch (id) {
            case 0:
                return this.renderFinacialParameter()
                break;
            case 1:
                return this.renderDocument()
                break;
            case 2:
                return this.renderOtherTerms()
                break;
            default:
                break;
        }
    }
    renderViewMore = () => {
        return <View style={{ alignItems: 'center', height: heightScale(31), justifyContent: 'center' }}>
            <TouchableOpacity onPress={() => this.toggleModal(false)} activeOpacity={1}>
                <Text style={styles.viewMoreText}>{res.strings.VIEW_MORE}</Text>
            </TouchableOpacity>
        </View>
    }
    viewMore = (item) => {
        const id = item.item.id
        switch (id) {
            case 0:
                return this.renderViewMore()
                break;
            case 1:
                return this.renderViewMore()
                break;
            case 2:
                return this.renderViewMore()
                break;
            default:
                break;
        }
    }
    renderData = ((item, index) => {
        return <View style={styles.corousalCard} >
            <View style={[{ height: heightScale(42) }]}>
                <Text style={styles.corousalNameText}>{item.item.name}</Text>
            </View>
            {this.renderCorousalData(item)}
            {this.viewMore(item)}
        </View>
    })
    modalView = () => {
        const { activeIndex, finacialParameter } = this.state
        if (activeIndex == 0) {
            return <FinacialParamModal
                finacialParameter={finacialParameter}
            />
        }
        if (activeIndex == 1) {
            return <HomeLoanDocModal
                hitHomeLoanDetailsApi={this.hitHomeLoanDetailsApi}
                USER_ID={this.state.userId}
            />
        } else if (activeIndex == 2) {
            return <OtherTermsModal

                OtherTerms={this.state.OtherTerms}
            />
        }
    }
    renderBankDetails = () => {
        const { bankName } = this.props.HomeLoan
        console.log("BANKNAME", bankName)
        if (!bankName) {
            return <View />
        }

        return <View style={styles.bankView}>
            <View style={styles.bankImageview}>
                {/* <Image source={res.images.icnRoom} /> */}
                <Text style={styles.bankNametext}>{bankName ? bankName : ""}</Text>
            </View>

        </View>
    }
    renderGraphDetails = () => {
        const loanStatement = this.props && this.props.HomeLoan && this.props.HomeLoan.loanStatement ? this.props.HomeLoan.loanStatement : []
        const { amountPaid } = this.props.HomeLoan

        if (!loanStatement) {
            return
        }
        return loanStatement.map((item, index) => {
            return <View style={styles.graphView}>
                <View style={styles.amountGraph}>
                    <Pie
                        radius={widthScale(25)}
                        sections={[
                            {
                                percentage: amountPaid ? amountPaid : "0",
                                color: res.colors.toupe,
                            },
                        ]}
                        backgroundColor={res.colors.pinkishGrey}
                        strokeCap={'butt'}
                    />
                    <View style={{ flexDirection: 'row' }}>
                        <Text style={styles.percentageText}>{amountPaid ? amountPaid : "0"}%</Text>
                    </View>

                    <Text style={styles.graphText}>{"Amount Due \nVs Amount Paid"}</Text>
                </View>
                <View style={styles.amountGraph}>
                    <Pie
                        radius={widthScale(25)}
                        sections={[
                            {
                                percentage: Math.round(item.outstandingPrinciplePercentage) ? Math.round(item.outstandingPrinciplePercentage) : "0",
                                color: res.colors.toupe,
                            },
                        ]}
                        backgroundColor={res.colors.pinkishGrey}
                        strokeCap={'butt'}
                    />
                    <View style={styles.outStandingText}>
                        <Text style={styles.percentageText}>{Math.round(item.outstandingPrinciplePercentage) ? Math.round(item.outstandingPrinciplePercentage) : "0"}%</Text>
                        <View style={styles.seprator} />
                        <Text style={styles.percentageText}>Rs.{item.outstandingPrinciple ? item.outstandingPrinciple : "0"}</Text>
                    </View>
                    <Text style={styles.graphText}>{"Outstanding \nPrincipal"}</Text>
                </View>
                <View style={styles.amountGraph}>
                    <Pie
                        radius={widthScale(25)}
                        sections={[
                            {
                                percentage: Math.round(item.interestPaidPercentage) ? Math.round(item.interestPaidPercentage) : "0",
                                color: res.colors.toupe,
                            },
                        ]}
                        backgroundColor={res.colors.pinkishGrey}
                        strokeCap={'butt'}
                    />
                    <View style={styles.interstTextView}>
                        <Text style={styles.percentageText}>{Math.round(item.interestPaidPercentage) ? Math.round(item.interestPaidPercentage) : "0"}%</Text>
                        <View style={styles.seprator} />
                        <Text style={styles.percentageText}>Rs.{item.interestPaid ? item.interestPaid : "0"}</Text>
                    </View>
                    <Text style={styles.graphText}>{"Interest Paid \ntill Date"}</Text>
                </View>
            </View>
        })

    }
    toggleModal = (isVastu) => {
        const { isModalVisible } = this.state
        this.setState({ isModalVisible: !isModalVisible })
    };
    renderLoader = () => {
        return (
            <Loader
                loading={this.state.isLoading} />
        );
    };
    openSupport = () => {
        this.props.navigation.navigate("SupportBaseScreen")
    }
    render() {
        console.log("HOMELOANSCREEN", this.props)
        const { isLoading } = this.state
        if(this.state.loading) {
            return null;
          }
        return (
            <SafeAreaView style={styles.fullScreen}>
                <ImageBackground source={res.images.mainBackground} style={{ flex: 1 }}>
                    <DashBoardHeader
                        openSupport={() => this.openSupport()}
                        openDrawer={() => this.openDrawer()}
                        headerTitle={res.strings.HOME_LOAN}
                        settingVisible={false}
                        notificationVisible={false}
                    />
                    <Modal isVisible={this.state.isModalVisible}
                        // onSwipeComplete={() => this.toggleModal()}
                        // swipeDirection={'down'}
                        onBackdropPress={() => this.toggleModal()}
                        style={styles.view}>
                        {this.props ? this.modalView() : <View />}
                    </Modal>
                    <View style={{ flex: 1 }}>
                        {this.renderBankDetails()}
                        {this.renderGraphDetails()}
                        <View style={{ flex: 1, marginTop: heightScale(-20) }}>
                            <Carousel
                                data={this.state.homeLoanScreen}
                                renderItem={this.renderData}
                                sliderWidth={Dimensions.get('window').width}
                                itemWidth={306}
                                layout={'default'}
                                containerCustomStyle={styles.carouselData}
                                removeClippedSubviews={true}
                                onSnapToItem={index => this.setState({ activeIndex: index })}
                                firstItem={this.state.activeIndex}
                                style={styles.carousel}
                            />
                            <View style={{ height: 20, justifyContent: 'center', width: "10%", alignSelf: 'center' }}>
                                <Pagination

                                    dotsLength={this.state.homeLoanScreen.length}
                                    dotStyle={styles.dotStyle}
                                    dotColor={styles.dotColor}
                                    inactiveDotStyle={styles.inactiveDotStyle}
                                    activeDotIndex={this.state.activeIndex}
                                    inactiveDotOpacity={0.4}
                                    inactiveDotScale={0.6}
                                    activeOpacity={1}
                                />
                            </View>

                        </View>
                    </View>
                </ImageBackground>
                {isLoading && this.renderLoader()}
            </SafeAreaView>
        )
    }
}
const mapStateToProps = (state) => {
    return {
        HomeLoan: state.HomeLoanReducer.HomeLoan
    };
};
function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        getHomeLoanDetails,
        saveHomeLoanDetails
    }, dispatch);
}
let HomeLoanScreenContainer = connect(mapStateToProps, mapDispatchToProps)(HomeLoanScreen);
export default HomeLoanScreenContainer;

