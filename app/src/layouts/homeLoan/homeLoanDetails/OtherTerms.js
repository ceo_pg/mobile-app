import React, { Component } from 'react'
import { View } from 'react-native'
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";


import res from '../../../../res'
import { heightScale,numberFormat } from '../../../utility/Utils';
import KeyClauseItemComp from '../../../components/legalDoc/KeyClauseItemComp'

class OtherTerms extends Component {
    constructor(props) {
        super(props)
        this.state = {

        }

    }


    render() {

        const { homeLoan } = this.props.HomeLoan
        console.log("HOMELOAN REDUCER", homeLoan)
        // const numberFormat = new Intl.NumberFormat('en-INR', {
        //     style: 'currency',
        //     currency: 'INR',
        //     minimumFractionDigits: 2,
        // });
        // return <View />
        // const { foreClosureCharges, interestType, loanTransferCharges, prepaymentPenality } = homeLoan
        if (!homeLoan) {
            return <View />
        }
        return (
            <View >


                <View style={{ flex: 1, marginBottom: 200 }}>
                    <View style={{ marginTop: heightScale(5) }}>
                        <KeyClauseItemComp icon={res.images.principalamount} value={(homeLoan && homeLoan.interestType) ? homeLoan.interestType : "NA"} label={""} img={res.images.proof_ic} subLabel={res.strings.Floating_Interest} />

                    </View>

                    <View style={{ marginTop: heightScale(5) }}>
                        <KeyClauseItemComp icon={res.images.principalamount}unitText={"Rs. "} value={(homeLoan && homeLoan.foreClosureCharges) ? numberFormat(homeLoan.foreClosureCharges) : "0"} label={""} img={res.images.proof_ic} subLabel={res.strings.Foreclosures_Charges} />
                    </View>
                    <View style={{ marginTop: heightScale(5) }}>
                        <KeyClauseItemComp icon={res.images.principalamount} unitText={"Rs. "} value={(homeLoan && homeLoan.loanTransferCharges) ? numberFormat(homeLoan.loanTransferCharges) : "0"} label={""} img={res.images.proof_ic} subLabel={res.strings.Transfer_Charges} />
                    </View>
                    <View style={{ marginTop: heightScale(5) }}>
                        <KeyClauseItemComp icon={res.images.principalamount}unitText={"Rs. "} value={(homeLoan && homeLoan.prepaymentPenality) ? numberFormat(homeLoan.prepaymentPenality) : "0"} label={""} img={res.images.proof_ic} subLabel={res.strings.prepaymentPenality} />

                    </View>

                </View>



            </View>
        )

    }
}
const mapStateToProps = (state) => {
    return {
        HomeLoan: state.HomeLoanReducer.HomeLoan
    };
};

function mapDispatchToProps(dispatch) {
    return bindActionCreators({

    }, dispatch);
}
let OtherTermsContainer = connect(mapStateToProps, mapDispatchToProps)(OtherTerms);
export default OtherTermsContainer;
