import { StyleSheet, StatusBar, Platform } from 'react-native'
import res from '../../../../res'
import { heightScale, widthScale, myHeight } from '../../../utility/Utils'
const styles = StyleSheet.create({
    fullScreen: {
        flex: 1,
        backgroundColor: res.colors.appColor,
        paddingTop: Platform.OS === "android" ? StatusBar.currentHeight + 10 : 0
    }, shadowHeader: {

        borderTopLeftRadius: widthScale(20),
        borderTopRightRadius: widthScale(20),
        backgroundColor: res.colors.butterscotch,
        shadowColor: res.colors.shadowColor,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 10.58,
        shadowRadius: 10.00,
        elevation: 24,
    }, corousalNameText: {
        // fontWeight: "bold",
        fontFamily: res.fonts.semiBold,
        fontSize: widthScale(18),
        letterSpacing: 0.3,
        textAlign: 'center',
        color: res.colors.black,
        marginTop: heightScale(10)
    }, carouselData: {

        // borderRadius: 100,
        // backgroundColor: 'red',
        zIndex: 99,
        // marginTop: 56
    }, dotStyle: {
        // width: 10,
        // height: 10,
        borderRadius: 5,
        // marginHorizontal: 8,
        backgroundColor: res.colors.pinkishTan
    }, dotColor: {
        width: widthScale(10),
        height: widthScale(10),
        borderRadius: 5,
        marginHorizontal: -5,
        backgroundColor: res.colors.pinkishTan
    }, inactiveDotStyle: {
        width: widthScale(10),
        height: widthScale(10),
        borderRadius: widthScale(5),
        marginHorizontal: widthScale(-5),
        backgroundColor: res.colors.pinkishTan
    }, bankView: {
        marginTop: heightScale(16),
        // width: '100%',
        // height: heightScale(114),
        backgroundColor: res.colors.white,
        marginHorizontal: widthScale(20),
        borderRadius: 12,
        flexDirection: 'column'
    }, graphView: {
        // flex: 1,
        marginTop: heightScale(10),
        //  width: '100%',
        // height: heightScale(123),
        backgroundColor: res.colors.cobalt,
        marginHorizontal: widthScale(20),
        borderRadius: 12,
        flexDirection: 'row',
        justifyContent: 'space-evenly'
    },
    buttomText: {
        // height: '30%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginHorizontal: 20,
        alignItems: 'center',
        marginVertical: heightScale(5)
    }, corousalMainView: {
        height: heightScale(365)
    }, percentageText: {
        marginTop: heightScale(5),
        fontFamily: res.fonts.semiBold,
        fontSize: widthScale(10),
        color: res.colors.toupe
    }, graphText: {
        marginTop: heightScale(5),
        fontFamily: res.fonts.regular,
        fontSize: widthScale(10),
        color: res.colors.pinkishGrey,
        textAlign: 'center'
    }, seprator: {
        // borderLeftWidth:1,
        // borderLeftColor:'red',
        width: 1,
        height: "70%",
        backgroundColor: res.colors.sepratorColor
        , justifyContent: 'center', marginHorizontal: 4,
        alignSelf: 'flex-end'
    }, container: {
        // flex: 1,
        // top: Platform.OS == 'ios' ? heightScale(100) : heightScale(100),
        // borderTopLeftRadius: widthScale(20),
        // borderTopRightRadius: widthScale(20),
        // backgroundColor: res.colors.butterscotch,

    },
    amountGraph: {
        alignItems: 'center',
        justifyContent: 'center',
        marginVertical: heightScale(20)
    },
    interstTextView: {
        flexDirection: 'row',
        justifyContent: 'center'
    }, outStandingText: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    loanStatusStyle: {
        fontFamily: res.fonts.semiBold,
        fontSize: widthScale(12),
        color: res.colors.blackTwo,
        marginLeft: widthScale(10)
    }, imageStatus: {
        width: widthScale(14),
        height: heightScale(14),
        tintColor: res.colors.black
    }, changeBankText: {
        fontFamily: res.fonts.semiBold,
        fontSize: widthScale(10),
        color: res.colors.blackTwo,

    }, bankImageview: {
        // height: '75%',
        alignItems: 'center',
        justifyContent: 'center',
        marginVertical: heightScale(20)
    }, sepratorView: {
        height: '1%',
        backgroundColor: res.colors.sepratorColor
    },
    loanView:
    {
        flexDirection: 'row',
        alignItems: 'center',

    }, noDataFound: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    corousalCard: {
        backgroundColor: res.colors.butterscotch,
        borderRadius: widthScale(24),
        marginTop: heightScale(30),
        width: 306,
        flex: 1,

    }, viewMoreText: {
        fontWeight:'bold',
        color: res.colors.peacockBlue,
        fontFamily: res.fonts.semiBold,
        fontSize: 14
    }, bankNametext: {
        fontWeight: 'bold',
        fontFamily: res.fonts.semiBold,
        fontSize: widthScale(16),
        letterSpacing: 0.77,
        color: res.colors.blackTwo
    }
})
export default styles
