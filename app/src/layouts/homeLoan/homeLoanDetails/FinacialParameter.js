import React, { Component } from 'react';
import { View, FlatList, SafeAreaView, Text } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import KeyClauseItemComp from '../../../components/legalDoc/KeyClauseItemComp'
import res from '../../../../res'
import { heightScale, numberFormat } from '../../../utility/Utils';
import styles from './styles'
import moment from 'moment'
class FinacialParameter extends Component {
    constructor(props) {
        super(props)
        this.state = {
            Name: [
                {
                    name: res.strings.Loan_Principal_Amount,
                    image: res.images.principalamount
                }, {
                    name: res.strings.Interest_Rate,
                    image: res.images.interestrate
                }, {
                    name: res.strings.Next_Drawdown_Date,
                    image: res.images.nextdrawdowndate
                }, {
                    name: res.strings.foreClosureCharges,
                    image: res.images.principalamount
                }, {
                    name: res.strings.Interest_Type,
                    image: res.images.nextdrawdowndate
                }, {
                    name: res.strings.loanTransferCharges,
                    image: res.images.nextdrawdowndate
                }, {
                    name: res.strings.prepaymentPenality,
                    image: res.images.nextdrawdowndate
                }
            ],
            isModalVisible: false,
        }
    }

    renderFinacialParameter = ((item, index) => {
        const { loanStatement,homeLoan } = this.props.HomeLoan
        var last = loanStatement[loanStatement.length - 1]

        return (
            <View style={{ marginTop: heightScale(10), justifyContent: 'space-between', flex: 1 }}>
                <View style={{ marginVertical: heightScale(15) }}>
                    <KeyClauseItemComp unitText={"Rs. "} icon={res.images.principalamount} value={numberFormat(homeLoan.principalAmount)} label={""} img={res.images.proof_ic} subLabel={res.strings.Loan_Principal_Amount} />

                </View>
                <View style={{ marginVertical: heightScale(10) }}>
                    <KeyClauseItemComp  icon={res.images.interestrate} value={(homeLoan.rateOfInterest).toFixed(2)} label={"%"} img={res.images.proof_ic} subLabel={res.strings.Interest_Rate} />
                </View>
                {/* <View style={{ marginVertical: heightScale(10) }}>

                    <KeyClauseItemComp icon={res.images.nextdrawdowndate} value={moment(last.dateOfEmi).format('Do MMM  YYYY')} label={""} img={res.images.proof_ic} subLabel={res.strings.Next_Drawdown_Date} />

                </View> */}
                <View style={{ marginVertical: heightScale(10) }}>
                    <KeyClauseItemComp unitText={"Rs. "} icon={res.images.total_intresttobepaid} value={numberFormat(last.interestPaid)} label={""} img={res.images.proof_ic} subLabel={res.strings.Total_Intrest_Paid} />
                </View>
            </View>
        )

    }
    )

    render() {

        const { loanStatement } = this.props.HomeLoan

        if (!loanStatement) {
            return <View style={styles.noDataFound}>
                <Text>{res.strings.NO_DATA_FOUND}</Text>
            </View>
        }
        return (
            <SafeAreaView style={{ flex: 1 }}>
                {
                    loanStatement && loanStatement.length > 0
                    &&
                    this.renderFinacialParameter()

                }

            </SafeAreaView>
        )
    }

}
const mapStateToProps = (state) => {
    return {
        HomeLoan: state.HomeLoanReducer.HomeLoan
    };
};

function mapDispatchToProps(dispatch) {
    return bindActionCreators({

    }, dispatch);
}
let FinacialParameterContainer = connect(mapStateToProps, mapDispatchToProps)(FinacialParameter);
export default FinacialParameterContainer;
// export default