import { StyleSheet, Platform, StatusBar } from 'react-native'
import res from '../../../../res'
import { heightScale, widthScale } from '../../../utility/Utils'
const styles = StyleSheet.create({
    fullScreen: {
        flex: 1,
        backgroundColor: res.colors.appColor,
        paddingTop: Platform.OS === "android" ? StatusBar.currentHeight + 10 : 0
    }, boxView: {
        marginHorizontal: 10,
        // height: heightScale(234),
        // width: "100%",
        backgroundColor: res.colors.white,
        // marginTop: heightScale(50),
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 12,
        marginBottom:heightScale(20)

    }, mainView: {
        marginTop:heightScale(50),
        backgroundColor: res.colors.butterscotch,
        marginHorizontal: widthScale(20),
        borderRadius: 20,
        marginBottom:heightScale(100)

    }, listView: {
        backgroundColor: res.colors.white,
        margin: widthScale(15),
        borderRadius: 20,

    }, shadow: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 4,
        },
        shadowOpacity: 0.30,
        shadowRadius: 4.65,

        elevation: 8,
    }, button: {
        height: heightScale(44),
        alignItems: 'center',
        backgroundColor: res.colors.butterscotch,
        width: '100%',
        borderBottomLeftRadius: 12,
        borderBottomRightRadius: 12,
        justifyContent: 'center'
    }, image: {
        margin: 25
    }, btnTextStyle: {
        textAlign: 'center',
        fontFamily: res.fonts.semiBold,
        color: res.colors.blackTwo,
        fontSize: 18,
        letterSpacing: 0.86,
        lineHeight: heightScale(24)
    }, headerText: {
        margin: 10,
        textAlign: 'center',
        fontFamily: res.fonts.semiBold,
        color: res.colors.formLabelGrey,
        fontSize: 16,
        letterSpacing: 0.77,
        lineHeight: heightScale(24)
    }, ProcessingFeeText: {
        marginVertical: heightScale(50),
        textAlign: 'center',
        fontFamily: res.fonts.semiBold,
        color: res.colors.blackTwo,
        fontSize: 18,
        letterSpacing: 0.86,
        lineHeight: heightScale(24)
    }
})
export default styles