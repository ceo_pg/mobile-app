import React, { Component } from 'react'
import { View, Text, SafeAreaView, FlatList, Image, TouchableOpacity,ImageBackground } from 'react-native'
import styles from './styles'
import res from '../../../../res'
import DashBoardHeader from '../../../components/header/DashBoardHeader'

class ProcessingFee extends Component {
    constructor(props) {
        super(props)
        this.state = {
            option: [{
                image: res.images.homeLoan,
                header: res.strings.Select_Bank,

                btnText: res.strings.Pay_Now

            }, {
                image: res.images.applyHomeLoan,
                header: res.strings.Select_Bank,

                btnText: res.strings.Pay_Now

            }]
        }

    }
    SelectBank = (index) => {
        console.log("SelectBankINDEX", index)
        if (index == 0) {
            this.props.navigation.navigate("HomeLoanScreen")
        } else {
            this.props.navigation.navigate("ApprovalStatus", {
                navigation: this.props.navigation
            })
        }



    }

    renderItem = ((item, index) => {
        console.log("HomeLoasn", item, item.index)
        return (<View style={[styles.boxView, styles.shadow]}>
            <Image source={item.item.image} style={styles.image} />
            <Text style={styles.headerText}>{item.item.header}</Text>

            <TouchableOpacity onPress={() => this.SelectBank(item.index)} style={styles.button}>
                <Text style={styles.btnTextStyle}>{item.item.btnText}</Text>
            </TouchableOpacity>
        </View>)
    })
    openDrawer = () => {
        this.props.navigation.openDrawer()
    }
    render() {
        console.log("HomeLoasn", this.props)
        const { option } = this.state
        return (
            <SafeAreaView style={styles.fullScreen}>
                <ImageBackground source={res.images.mainBackground} style={{ flex: 1 }}>
                    <DashBoardHeader
                        openDrawer={() => this.openDrawer()}
                        headerTitle={res.strings.HOME_LOAN}
                        settingVisible={false}
                        notificationVisible={false}
                    />
                    <View style={styles.mainView}>

                        <FlatList
                            ListHeaderComponent={(item, index) => {
                                return <Text style={styles.ProcessingFeeText}>{res.strings.Processing_Fee_Payment}</Text>
                            }}
                            style={styles.listView}
                            renderItem={this.renderItem}
                            data={option}
                        />
                    </View>
                </ImageBackground>


            </SafeAreaView>
        )

    }
}
export default ProcessingFee