import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import { View, SafeAreaView,ImageBackground } from 'react-native';
import styles from './styles'
import { CommonActions } from '@react-navigation/native';

import CircleProgress from '../../ConstructionProgress/Customization/CircleProgress'
import SelectBankStep1 from './SelectBankStep1'
import UploadBankDocument from './UploadBankDocument'
import DashBoardHeader from '../../../components/header/DashBoardHeader'

import ViewPager from '@react-native-community/viewpager';
import res from '../../../../res'


class SelectBank extends Component {
    constructor(props) {
        super(props);
        this.state = {
            pageIndex: 0,
            progressImg: res.images.customizationStep1
        }
        this.viewPager = React.createRef()
    }

    componentDidMount() {

    }



    changePageIndex = (index) => {
        if (index == -1) {
            // close the drwr
            return
        }
        this.setState({ pageIndex: index })
        this.viewPager.setPage(index);
        this.setProgressImage(index)
    }

    pageChanged = (e) => {
        if (e && e.nativeEvent && e.nativeEvent) {
            this.setState({ pageIndex: e.nativeEvent.position });
            this.setProgressImage(e.nativeEvent.position)
        }
    }

    setProgressImage = (index) => {
        switch (index) {
            case 0:
                // code block
                this.setState({ progressImg: res.images.customizationStep1 })
                break;
            case 1:
                // code block
                this.setState({ progressImg: res.images.customizationStep2 })
                break;

            default:
            // code block
        }
    }
    openDrawer = () => {
        const resetAction = CommonActions.reset({
            index: 0,
            routes: [{ name: 'SelectBank' }],
        });
        // this.props.navigation.dispatch(resetAction);
        this.props.navigation.openDrawer(resetAction)
    }

    render() {
        const { progressImg } = this.state
        console.log("SelectBank", this.props)
        return (
            <SafeAreaView style={styles.fullScreen}>
                <ImageBackground source={res.images.mainBackground} style={{ flex: 1 }}>
                    <DashBoardHeader
                        openDrawer={() => this.openDrawer()}
                        headerTitle={res.strings.HOME_LOAN}
                        settingVisible={false}
                        notificationVisible={false}
                    />
                    <View style={styles.progressContainer}>
                        <CircleProgress pageIndex={this.state.pageIndex} progressImg={progressImg} />
                    </View>
                    <ViewPager style={[styles.viewPager]} initialPage={0}
                        ref={(viewPager) => { this.viewPager = viewPager }}
                        onPageSelected={(e) => this.pageChanged(e)}
                    >
                        <View key="1" >
                            <SelectBankStep1 changePage={this.changePageIndex} />

                        </View>
                        <View key="2">
                            <UploadBankDocument
                                navigation={this.props.navigation}
                                changePage={this.changePageIndex} />
                        </View>

                    </ViewPager>
                </ImageBackground>

            </SafeAreaView>
        )
    }
}

const mapStateToProps = (state) => {
    return {
    };
};

function mapDispatchToProps(dispatch) {
    return bindActionCreators({

    }, dispatch);
}
let SelectBankScreenContainer = connect(mapStateToProps, mapDispatchToProps)(SelectBank);
export default SelectBankScreenContainer;

