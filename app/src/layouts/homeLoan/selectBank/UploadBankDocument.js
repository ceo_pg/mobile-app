import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import { View, Text, TouchableOpacity, ImageBackground, SafeAreaView, Image } from 'react-native';
import styles from './styles'
// import strings from '../../../../res/constants/strings';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import Header from '../../commercial/costSheet/Header'
import res from "../../../../res";
import FileUploadComp from '../../../components/fileUpload/FileUploadComp'
import Button from '../../../components/button/Button'

class UploadBankDocument extends Component {
    constructor(props) {
        super(props);
        this.state = {

        }
    }
    componentDidMount() {

    }

    backPress = () => {
        const { changePage } = this.props;
        changePage(-1)
    }
    moveToProcessingFee = () => {
        this.props.navigation.navigate("ProcessingFee")
    }
    render() {
        const { bankList } = this.state
        return (
            <View style={{ flex: 1 }}>
                <KeyboardAwareScrollView style={styles.headerView}>
                    <Header title={res.strings.Upload_Document} onBack={this.backPress} />
                    <View style={{ marginHorizontal: 10, marginBottom: 100 }}>
                        <FileUploadComp
                            label={res.strings.Loan_Application}
                        />
                        <TouchableOpacity>
                            <Text style={styles.downloadFormText}>{res.strings.DOWNLOAD_FORM}</Text>
                        </TouchableOpacity>
                        <FileUploadComp
                            label={res.strings.Income_Proof}
                        />
                        <FileUploadComp
                            label={res.strings.Employment_Proof}
                        />
                        <View style={[styles.contentView]}>

                            <Text style={styles.headingText}>{res.strings.ADD_PHOTO}</Text>
                            <View style={[{ flexDirection: 'row', justifyContent: 'space-evenly', }]}>
                                <FileUploadComp

                                    containerView={{ margin: 5 }}
                                    label={res.strings.UPLOAD}
                                />
                                <FileUploadComp
                                    containerView={{ margin: 5 }}
                                    label={res.strings.CAPTURE}
                                />
                            </View>

                        </View>
                        <View style={[styles.contentView]}>

                            <Text style={styles.headingText}>{res.strings.PAN_CARD}</Text>
                            <View style={[{ flexDirection: 'row', justifyContent: 'space-evenly', }]}>
                                <FileUploadComp

                                    containerView={{ margin: 5 }}
                                    label={res.strings.UPLOAD}
                                />
                                <FileUploadComp
                                    containerView={{ margin: 5 }}
                                    label={res.strings.CAPTURE}
                                />
                            </View>

                        </View>
                        <View style={[styles.contentView]}>

                            <Text style={styles.headingText}>{res.strings.AGE_PROOF}</Text>
                            <View style={[{ flexDirection: 'row', justifyContent: 'space-evenly', }]}>
                                <FileUploadComp

                                    containerView={{ margin: 5 }}
                                    label={res.strings.UPLOAD}
                                />
                                <FileUploadComp
                                    containerView={{ margin: 5 }}
                                    label={res.strings.CAPTURE}
                                />
                            </View>

                        </View>
                    </View>


                </KeyboardAwareScrollView>
                <View style={styles.btnView}>
                    <Button
                        onPress={() => this.moveToProcessingFee()}
                        btnStyle={{ backgroundColor: res.colors.butterscotch, marginHorizontal: 10, bottom: 10 }}
                        btnText={res.strings.Processing_Fee}
                    />
                </View>
            </View>

        )
    }


}
const mapStateToProps = (state) => {
    return {};
};

function mapDispatchToProps(dispatch) {
    return bindActionCreators({

    }, dispatch);
}
let UploadBankDocumentContainer = connect(mapStateToProps, mapDispatchToProps)(UploadBankDocument);
export default UploadBankDocumentContainer;

