import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import { View, Text, TouchableOpacity, ImageBackground, SafeAreaView, Image } from 'react-native';
import styles from './styles'
import { CommonActions } from '@react-navigation/native';

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import Header from '../../commercial/costSheet/Header'
import res from "../../../../res";
import { FlatList } from 'react-native-gesture-handler';
import CardWithTitle from '../../../components/card/CardWithTitle'
import ApiFetch from '../../../apiManager/ApiFetch'
import { BANK_LIST } from '../../../apiManager/ApiEndpoints'
import DashBoardHeader from '../../../components/header/DashBoardHeader'


class SelectBankStep1 extends Component {
    constructor(props) {
        super(props);
        this.USER_ID = this.props.route.params && this.props.route.params.USER_ID;
        this.state = {
            // bankList: [{
            //     id: 0,
            //     image: "",
            //     title: "Bank Name",
            //     isSelected: false
            // }, {
            //     id: 1,
            //     image: "",
            //     title: "Bank Name",
            //     isSelected: false
            // }, {
            //     id: 2,
            //     image: "",
            //     title: "Bank Name",
            //     isSelected: false
            // }
            // ],
            bankList: []
        }
    }
    componentDidMount() {
        this.hitBankListApi()
    }
    hitBankListApi = () => {
        ApiFetch.fetchGet(BANK_LIST).then((res) => {
            // console.log("BANkLIST", res)
            this.setState({
                bankList: res.data
            })
        })
    }
    backPress = () => {
        const { changePage } = this.props;
        changePage(-1)
    }
    SelectBank = (item, index) => {
        // console.log("BANkLIST", item)
        this.props.navigation.navigate("KeyClausesAgree", {
            hitBankList: this.hitBankListApi,
            bankName:item.item.name
        })
        // const { bankList } = this.state
        // let tempArray = bankList.map((value, id) => {
        //     if (value.id == item.item.id) {
        //         return { ...value, isSelected: !value.isSelected }
        //     } else {
        //         return value
        //     }
        // })
        // this.setState({
        //     bankList: tempArray,

        // })
    }
    renderBankList = ((item, index) => {
        // console.log("BANKLIST", item)
        return <TouchableOpacity onPress={() => this.SelectBank(item, index)} style={[styles.bankListView, styles.shadow]}>

            {/* <Image style={[{ width: 54, height: 40 }]} source={res.images.icnRoom} resizeMode={'contain'} /> */}

            <Text style={[styles.bankNameText]}>{item.item.name}</Text>

        </TouchableOpacity>
        // if (item.item.isSelected) {
        //     return <TouchableOpacity onPress={() => this.SelectBank(item, index)} style={[styles.bankListSelectedView, styles.shadow]}>

        //         <Image style={[{ width: 54, height: 40 }]} source={res.images.icnRoom} resizeMode={'contain'} />

        //         <Text style={[styles.bankNameText]}>{item.item.name}</Text>

        //     </TouchableOpacity>

        // } else {
        //     return <TouchableOpacity onPress={() => this.SelectBank(item, index)} style={[styles.bankListView, styles.shadow]}>

        //         <Image style={[{ width: 54, height: 40 }]} source={res.images.icnRoom} resizeMode={'contain'} />

        //         <Text style={[styles.bankNameText]}>{item.item.name}</Text>

        //     </TouchableOpacity>
        // }




    })
    openDrawer = () => {
        const resetAction = CommonActions.reset({
            index: 0,
            routes: [{ name: 'SelectBankStep1' }],
        });
        this.props.navigation.dispatch(resetAction);
        this.props.navigation.openDrawer()
    }
    render() {
        const { bankList } = this.state
        return (

            <SafeAreaView style={styles.fullScreen}>
                <ImageBackground source={res.images.mainBackground} style={{ flex: 1 }}>
                    <DashBoardHeader
                        openDrawer={() => this.openDrawer()}
                        headerTitle={res.strings.HOME_LOAN}
                        settingVisible={false}
                        notificationVisible={false}
                    />
                    <View style={styles.mainView}>
                        <View style={styles.subView}>
                            {/* <Text style={styles.morebankText}>{res.strings.Select_Bank}</Text> */}
                            <View style={styles.headerShadowView}>
                                <Text style={styles.morebankText}>{res.strings.Select_Bank}</Text>
                            </View>

                            <FlatList
                                data={bankList}
                                renderItem={this.renderBankList}
                                numColumns={2}
                            />
                        </View>

                    </View>

                </ImageBackground>
            </SafeAreaView>
        )
    }


}
const mapStateToProps = (state) => {
    return {};
};

function mapDispatchToProps(dispatch) {
    return bindActionCreators({

    }, dispatch);
}
let SelectBankStep1Container = connect(mapStateToProps, mapDispatchToProps)(SelectBankStep1);
export default SelectBankStep1Container;

