import { StyleSheet, StatusBar } from 'react-native'
import res from '../../../../res'
import { heightScale, widthScale } from '../../../utility/Utils'
const styles = StyleSheet.create({
    fullScreen: {
        flex: 1,
        backgroundColor: res.colors.appColor,
        paddingTop: Platform.OS === "android" ? StatusBar.currentHeight + 10 : 0
    },
    progressContainer: {
        marginTop: heightScale(17),
        alignItems: 'center'
    },
    viewPager: {
        flex: 1,
        backgroundColor: res.colors.butterscotch,
        marginHorizontal: widthScale(20),
        borderRadius: widthScale(20),
        marginTop: heightScale(18)
    },
    costSheetViewContainer: {
        margin: widthScale(15),
        borderRadius: widthScale(20),
        flex: 1,
        backgroundColor: res.colors.white
    },
    headerView: {
        margin: widthScale(10),
        borderRadius: widthScale(20),
        flex: 1,
        backgroundColor: res.colors.white,

    },
    headerTitle: {
        fontFamily: res.fonts.semiBold,
        fontSize: widthScale(18),
        color: res.colors.greyishBrown
    },
    headerLeftView: {
        position: 'absolute',
        left: widthScale(22),
        top: heightScale(18)
    },
    contentView: {
        marginTop: widthScale(10),
        borderRadius: widthScale(10),
        backgroundColor: res.colors.white,
        flex: 1,
        justifyContent: 'center',
        // alignItems: 'center',
        shadowColor: "rgba(0,0,0, 0.1)",
        shadowOffset: {
            width: 0,
            height: 4,
        },
        shadowOpacity: 0.8,
        shadowRadius: 10,
        elevation: 10,
    },
    subHeadingText: {
        textAlign: 'center',
        fontFamily: res.fonts.semiBold,
        fontSize: widthScale(16),
        color: res.colors.greyishBrown,
        paddingVertical: heightScale(20)
    }, morebankText: {
        fontWeight:'bold',
        textAlign: 'center',
        fontFamily: res.fonts.semiBold,
        fontSize: widthScale(16),
        color: res.colors.blackTwo,
        // marginVertical: heightScale(20)
    }, bankListView: {
        marginHorizontal: widthScale(5.5),
        marginVertical: widthScale(4.5),
        paddingVertical:widthScale(30),

        // minHeight: 84,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 5,
        flexDirection: 'column',
        width: '46%'
    }, shadow: {
        backgroundColor: res.colors.white,
        shadowColor: res.colors.shadowColor,
        shadowOffset: {
            width: 2,
            height: 2,
        },
        shadowOpacity: 5.25,
        shadowRadius: 3.84,
        elevation: 5,
    }, bankNameText: {
        fontWeight:'bold',
        fontFamily: res.fonts.medium,
        fontSize: 14,
        color: res.colors.greyishBrown,
        textAlign: 'center',
        // top: 10,

        paddingLeft: 10,
        paddingRight: 10
    }, downloadFormText: {
        fontFamily: res.fonts.regular,
        color: res.colors.appColor,
        fontSize: 12,
        textAlign: 'center',
        letterSpacing: 0.58
    }, btnView: {
        position: 'absolute',
        bottom: 0,
        right: 0,
        left: 0,
        backgroundColor: res.colors.dusk,
        zIndex: 999,

    }, bankListSelectedView: {
        borderWidth: 1,
        borderColor: res.colors.peacockBlue,
        marginHorizontal: widthScale(5.5),
        marginVertical: widthScale(4.5),
        minHeight: 84,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 5,
        flexDirection: 'column',
        width: '46%'
    }, headingText: {
        fontFamily: res.fonts.semiBold,
        fontSize: 16,
        color: res.colors.formLabelGrey,
        marginTop: 10,
        marginHorizontal: 10
    }, mainView: {
        flex: 1,
        backgroundColor: res.colors.butterscotch,
        marginHorizontal: widthScale(10),
        borderRadius: widthScale(20),
        marginTop: heightScale(25)
        // marginBottom:heightScale(20)
    }, subView: {
        flex: 1,
        backgroundColor: res.colors.white,
        margin: widthScale(10),
        borderRadius: widthScale(20),
    }, headerShadowView: {
        backgroundColor: res.colors.white,
         height: heightScale(50),
        // flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        shadowColor: "rgba(0,0,0, 0.1)",
        shadowOffset: {
            width: 0,
            height: 4,
        },
        shadowOpacity: 0.8,
        shadowRadius: 10,
        elevation: 10,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20

    }
})
export default styles