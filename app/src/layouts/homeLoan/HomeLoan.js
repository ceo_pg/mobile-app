import React, { Component } from 'react'
import { View, Text, SafeAreaView, FlatList, Image, TouchableOpacity, ImageBackground } from 'react-native'
import Modal from 'react-native-modal'
import styles from './styles'
import res from '../../../res'
import { CommonActions } from '@react-navigation/native';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import { getHomeLoanDetails, saveHomeLoanDetails } from '../../redux/actions/HomeLoan/HomeLoanAction'
import DashBoardHeader from '../../components/header/DashBoardHeader'
import AsyncStorage from '@react-native-community/async-storage'
import { Loader } from '../../components/index'
import { CommingSoonModal } from '../../components/index'

class HomeLoan extends Component {
    constructor(props) {
        super(props)
        this.state = {
            CommingSoonModal: false,
            USER_ID: "",
            isFormCompleted: false,
            isLoading: false,
            option: [{
                image: res.images.homeLoan,
                header: res.strings.Already_Home_Loan,
                otherText: res.strings.Upload_Information,
                btnText: res.strings.GET_STARTED

            }, {
                image: res.images.applyHomeLoan,
                header: res.strings.Apply_Home_Loan,
                otherText: res.strings.Home_Loan_Root,
                btnText: res.strings.Apply_Now

            }]
        }

    }
    SelectBank = (index) => {
        const { isFormCompleted } = this.state
        if (index == 0) {
            isFormCompleted ?

                //  this.props.navigation.navigate("HomeLoanScreen")
                this.props.navigation.navigate("HomeLoanScreen", {
                    USER_ID: this.state.USER_ID
                })
                :
                this.props.navigation.navigate("SelectBankStep1", {
                    USER_ID: this.state.USER_ID
                })
        } else {
            this.toggleCommingSoon()
            // this.props.navigation.navigate("SelectBank")
        }
    }
    componentDidMount = async () => {
        console.log("COMPONENT DID MOUND HOME LOAN")
        const userId = res.AsyncStorageContants.USERID;
        try {
            let USER_ID = await AsyncStorage.getItem(userId)

            this.hitHomeLoanDetailsApi(USER_ID)
        } catch (e) {
            console.log("Error removing data from async storage.", e);
        }

    }
    hitHomeLoanDetailsApi = (USER_ID) => {
        console.log("hitHomeLoanDetailsApi", USER_ID)
        this.setState({ isLoading: true })
        this.props.getHomeLoanDetails(USER_ID).then((res) => {
            console.log("RESPONCE OF HOMELOAN", res)
            if (res.data != null) {
                if (res.data && res.data.homeLoan && res.data.loanStatement && (res.data.homeLoan.principalAmount == null || (res.data.loanStatement.length > 0 && res.data.loanStatement[res.data.loanStatement.length - 1].amountDisbursed == null))) {
                    this.setState({ isFormCompleted: false, USER_ID: USER_ID })
                } else {
                    this.setState({ isFormCompleted: true })
                    this.props.navigation.navigate("HomeLoanScreen", {
                        USER_ID: this.state.USER_ID
                    })
                }
            } else {
                this.setState({ isFormCompleted: false })
                // this.props.navigation.navigate("HomeLoanScreen", {
                //     USER_ID: this.state.USER_ID
                // })

            }
            this.setState({ isLoading: false })
            // this.props.saveHomeLoanDetails(res.data)

        }).catch((err) => {
            this.setState({ isLoading: false })
        })
    }
    renderItem = ((item, index) => {
        console.log("HomeLoasn", item)
        return (<View style={[styles.boxView,]}>
            <Image source={item.item.image} style={styles.image} />
            <Text style={styles.headerText}>{item.item.header}</Text>
            <Text style={styles.otherText}>{item.item.otherText}</Text>
            <TouchableOpacity onPress={() => this.SelectBank(item.index)} style={styles.button}>
                <Text style={styles.btnTextStyle}>{item.item.btnText}</Text>
            </TouchableOpacity>
        </View>)
    })
    openDrawer = () => {

        const resetAction = CommonActions.reset({
            index: 0,
            routes: [{ name: 'HomeLoan' }],
        });
        this.props.navigation.dispatch(resetAction);
        this.props.navigation.openDrawer()
        // this.props.navigation.openDrawer()
    }
    renderLoader = () => {
        return (
            <Loader
                loading={this.state.isLoading} />
        );
    };
    openSupport = () => {
        this.props.navigation.navigate("SupportBaseScreen")
    }
    toggleCommingSoon = () => {
        const { CommingSoonModal } = this.state
        this.setState({
            CommingSoonModal: !CommingSoonModal
        })
    }
    render() {
        console.log("HomeLoasn", this.props)
        const { option, isLoading } = this.state
        return (
            <SafeAreaView style={styles.fullScreen}>
                <CommingSoonModal
                    toggleModal={this.toggleCommingSoon}
                    isToggle={this.state.CommingSoonModal}
                />
                <ImageBackground source={res.images.mainBackground} style={{ flex: 1 }}>
                    <DashBoardHeader
                        openSupport={() => this.openSupport()}
                        openDrawer={() => this.openDrawer()}
                        headerTitle={res.strings.HOME_LOAN}
                        settingVisible={false}
                        notificationVisible={false}
                    />
                    <FlatList
                        renderItem={this.renderItem}
                        data={option}
                    />
                </ImageBackground>
                {isLoading && this.renderLoader()}
            </SafeAreaView>
        )

    }
}
const mapStateToProps = (state) => {
    return {
        HomeLoan: state.HomeLoanReducer.HomeLoan
    };
};
function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        getHomeLoanDetails,
        saveHomeLoanDetails
    }, dispatch);
}
let HomeLoanContainer = connect(mapStateToProps, mapDispatchToProps)(HomeLoan);
export default HomeLoanContainer;
