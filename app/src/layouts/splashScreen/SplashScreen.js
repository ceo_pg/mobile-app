import React, { Component } from 'react';
import AsyncStorage from '@react-native-community/async-storage'
import { CommonActions } from '@react-navigation/native';

import { SIGNUP_ENDPOINTS } from '../../apiManager/ApiEndpoints'
import { View, Text, TouchableOpacity, ImageBackground } from 'react-native';
import styles from './styles';
import ApiFetch from '../../apiManager/ApiFetch'
import resources from '../../../res';
import AppUser from '../../utility/AppUser'
let appUsrObj = AppUser.getInstance()
const LAUNCH_DELAY = 1500;
class SplashScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {

        }
    }
    componentDidMount = async () => {
        let isLoggedIn = false;

        try {
            let keyArray = await AsyncStorage.getAllKeys();

            const result = await AsyncStorage.multiGet(keyArray);
            const mapObject = new Map(result)
            var token = mapObject.get(resources.AsyncStorageContants.TOKEN)
            var isUnitAssign = mapObject.get(resources.AsyncStorageContants.UNITASSIGN)
            var isSignup = mapObject.get(resources.AsyncStorageContants.SIGNUPTOKEN) 
            const profileUrl = mapObject.get(resources.AsyncStorageContants.PROFILEURL)
            const name = mapObject.get(resources.AsyncStorageContants.NAME)
            appUsrObj.profileurl = profileUrl
            appUsrObj.name = name
            // const name = await AsyncStorage.getItem(resources.AsyncStorageContants.NAME)
            // const token = await AsyncStorage.getItem(resources.AsyncStorageContants.TOKEN)

            console.log("profileUrl", profileUrl)
            if (token != null && token != "" && isSignup != 1) {
                console.log("TOKEN", token)
                let appUsrObj = AppUser.getInstance();
                appUsrObj.token = token;
                isLoggedIn = true;
                this.getUserDetails()
            } else {
                isLoggedIn = false;
            }
        } catch (error) {
            console.log("Error fetching user id", error);
        }

        setTimeout(() => {
            //  this.props.navigation.navigate("LoginScreen")
            this.resetStack(isLoggedIn, isUnitAssign, token);
        }, LAUNCH_DELAY)
    }
    /*
      * Rest app stack.
      */
    resetStack = (isLoggedIn, isUnitAssign, token) => {
        console.log("resetStack", isLoggedIn, isUnitAssign, token)
        if (isLoggedIn) {
            if (isUnitAssign) {
                console.log("DASHBOARD")
                const resetAction = CommonActions.reset({
                    index: 0,
                    routes: [{ name: 'DashBoard' }],
                });
                this.props.navigation.dispatch(resetAction);
            } else {
                console.log("isUnitAssign")
                const resetAction = CommonActions.reset({
                    index: 0,
                    routes: [{ name: 'PropertyDetailsScreen' }],
                });
                this.props.navigation.dispatch(resetAction);
            }
            // if (token != undefined && token != "") {
            // console.log("token")
            // if (isUnitAssign == "false") {
            //     console.log("isUnitAssign")
            //     const resetAction = CommonActions.reset({
            //         index: 0,
            //         routes: [{ name: 'PropertyDetailsScreen' }],
            //     });
            //     this.props.navigation.dispatch(resetAction);
            // } else {

            // }

            // }

        } else {
            const resetAction = CommonActions.reset({
                index: 0,
                routes: [{ name: 'LoginScreen' }],
            });
            this.props.navigation.dispatch(resetAction);
        }

    };

    getUserDetails = () => {
        AsyncStorage.multiGet([resources.AsyncStorageContants.PHONE, resources.AsyncStorageContants.USERID, resources.AsyncStorageContants.USERNAME]).then(response => {
            console.log(response[1][0], "AsyncStorage") // Key1
            console.log(response[1][1], "AsyncStorage") // Value1
            let appUsrObj = AppUser.getInstance();
            if (response[0][0] && response[0][1]) {
                appUsrObj.phone = response[0][1]
            }
            if (response[1][0] && response[1][1]) {
                appUsrObj.userId = response[1][1]
            }
            if (response[2][0] && response[2][1]) {
                appUsrObj.userName = response[2][1]
            }
        })
    }
    render() {
        return (

            <View style={styles.mainContainer}>
                <View style={styles.container}>
                    <ImageBackground
                        style={{ width: '100%', height: '100%' }}
                        source={resources.images.splashScreen}
                    />
                    {/* <Text>SplashScreen</Text>
                    <TouchableOpacity >
                        <Text>Hit login Api</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate("LoginScreen")}>
                        <Text>Hello</Text>
                    </TouchableOpacity> */}
                </View>

            </View>
        );
    }
}

export default SplashScreen;

