import { StyleSheet } from 'react-native';
import resources from '../../../res';
const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,

    },
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },

});

export default styles;
