import { StyleSheet } from 'react-native';
import resources from '../../../res';
const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        backgroundColor: resources.colors.white,

    }, inputCointainer: {
        flex: 1,
        borderTopLeftRadius: 30,
        borderTopRightRadius: 30,
        // backgroundColor:'red'
    },
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    content: {
        flexDirection: "column",
        justifyContent: "flex-start",
        alignItems: "center",
    }, inputCont: {
        // borderBottomWidth: 1,
        borderRadius: 4,
        borderColor: resources.colors.black,
        // paddingHorizontal: 20,

    }, errorUnderLine: {
        borderColor: "red",
    }, imageBack: {
        height: 261,
        width: '100%',
        top: 0
    }, inputFieldView: {

        borderTopLeftRadius: 22,
        borderTopRightRadius: 22,
        position: 'absolute',
        top: -20,
        left: 0,
        right: 0,
        bottom: 0,
        backgroundColor: resources.colors.white
    }, signText: {
        fontFamily: resources.fonts.semiBold,
        fontSize: 32,
        color: resources.colors.slate
    }, passwordField: {
        marginTop: 35
    }, forgotPassText: {
        fontFamily:resources.fonts.regular,
        fontSize:12,
        color:resources.colors.btnBlue,
        marginVertical: 40,
        alignSelf:'center',
        textAlign:'center'
    }

});

export default styles;
