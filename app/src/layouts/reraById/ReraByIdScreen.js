import React, { Component } from 'react';
import { View, Text, TouchableOpacity, ImageBackground, FlatList } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import AsyncStorage from '@react-native-community/async-storage'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

import styles from './styles';
import res from '../../../res';
import PGButton from '../../components/button/Button'
import { GET_BY_RERAID_ENDPOINTS } from '../../apiManager/ApiEndpoints';
import ApiFetch from '../../apiManager/ApiFetch';
import AppUser from '../../utility/AppUser'


class ReraByIdScreen extends Component {
    constructor(props) {
        super(props);
        this._id = this.props.route.params && this.props.route.params._id?this.props.route.params._id:"5ee896b7982b963bf96c199d";
        this.state = {
            reraListbyId: []
        }
    }
    componentDidMount() {
        this.reraByIdApi()
    }
    reraByIdApi = () => {
        ApiFetch.fetchGet(GET_BY_RERAID_ENDPOINTS+"/"+this._id)
            .then((data) => {
                console.log(data.data)
                this.setState({

                    reraListbyId: data.data
                })
            }).catch((err) => {
                console.log(err)
            })
    }
    reraListbyId = ({ item, index }) => {
        return <View>
            <Text>
                {item}
            </Text>
        </View>
    }
    renderView = () => {
        const { reraListbyId } = this.state
        return <View style={styles.inputFieldView}>


            <View style={{ marginHorizontal: 30, marginTop: 40 }}>
                <Text style={styles.signText}>RERA LIST BY ID </Text>

                <FlatList
                    data={reraListbyId}
                    renderItem={this.reraListbyId}
                />
                <View >
                    <PGButton
                        btnText={'RERAList'}
                        onPress={() => this.reraByIdApi()}
                    />
                </View>



            </View>

        </View>




    }
    render() {

        return (
            <View style={[styles.mainContainer]}>
                <ImageBackground
                    source={res.images.testImage}
                    style={styles.imageBack}
                />

                <View style={[styles.inputCointainer]}>

                    {this.renderView()}


                </View>

            </View>

        );
    }
}
const mapStateToProps = (state) => {
    return {};
};

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
    }, dispatch);
}
let ReraByIdScreenContainer = connect(mapStateToProps, mapDispatchToProps)(ReraByIdScreen);
export default ReraByIdScreenContainer;

