import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image, TextInput, KeyboardAvoidingView } from 'react-native';
import strings from '../../../res/constants/strings';
import res from '../../../res';
import { widthScale, heightScale } from '../../utility/Utils'
import OutLinedInput from '../../components/input/OutLinedInput'
import { Dropdown } from 'react-native-material-dropdown';
import { formFields } from '../../../res/constants/formFields'
import { PGButton } from '../../components/index'
import ApiFetch from '../../apiManager/ApiFetch';
import { CREATE_TICKET_ENDPOINTS } from '../../apiManager/ApiEndpoints';
import AppUser from '../../utility/AppUser'
import Toast from 'react-native-simple-toast'
import {Loader} from '../../components'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';


class CreateTicket extends Component {
    constructor(props) {
        super(props);
        this.state = {
            // options: [{ id: 1, value: "A" }, { id: 2, value: "B" }],
            selectedOption: {},
            descriptionText: '',
            isLoading:false
        }
    }
    componentDidMount() {

    }

    onSelectOption = (selectedOption) => {
        console.log(selectedOption, "selectedOption");
        this.setState({ selectedOption: selectedOption });
    }

    onSave = () =>{
        const {onSaveClick} = this.props;
        this.hitCreateTicketAPI()

    }

    onSaveAndAddNew = () =>{
        const {onSaveAndNewClick} = this.props;
 
    }

    isValidated = () =>{
        const { selectedOption, descriptionText } = this.state
        console.log(selectedOption, "selectedOption")

        if(!(selectedOption.hasOwnProperty('value'))){
            Toast.show("Please select subject", Toast.LONG);
            return false
        }
        else if(descriptionText.length == 0){
            Toast.show("Please enter description", Toast.LONG);
            return false
        }else{
            return true
        }
    }

    hitCreateTicketAPI = (isAddMore) =>{
        const { selectedOption, descriptionText } = this.state
        const { closeModal, onReferesh} = this.props;
       
        let parameters = {
            "subject": selectedOption.value,
            "subjectId": selectedOption.id,
            "description": descriptionText,
        }
        if(this.isValidated()){
            console.log(JSON.stringify(parameters), "parameters")
            this.setState({ isLoading: true });
            ApiFetch.fetchPost(CREATE_TICKET_ENDPOINTS, JSON.stringify(parameters))
            .then((data) => {
                console.log(data, "data")
                this.setState({ isLoading: false });
                if (data && data.status && data.statusCode == 200) {
                 if(isAddMore){
                   this.setState({selectedOption:'',descriptionText:''});
                 }else{
                    closeModal()
                 }
                 if(onReferesh){
                    onReferesh();
                 }

                }
                setTimeout(() => {
                    Toast.show(data.message, Toast.LONG); 
                }, 500);
    
    
            }).catch((error) => {
                this.setState({ isLoading: false });
                console.log("Error inside login", error)
            })
        }
        
    }

    renderLoader = () => {
        return (
            <Loader
                loading={this.state.isLoading} />
        );
    };

    render() {
        const { closeModal} = this.props
        const { selectedOption, isLoading } = this.state
        return (
            <View style={styles.containerView}>
                <View style={styles.whiteView}>
                    <View style={styles.headerStyle}>
                        <TouchableOpacity style={styles.closeBtnView} onPress={() => closeModal()}>
                            <Image source={res.images.cross_ic} resizeMode={'contain'} style={{ height: widthScale(20), width: widthScale(20) }} />
                        </TouchableOpacity>
                        <Text style={styles.headerTxt}>{res.strings.CREATE_TICKET}</Text>
                    </View>
                    <KeyboardAwareScrollView>
                        <Dropdown
                            animationDuration={1}
                            rippleDuration={1}
                            onChangeText={(val, i, d) => {
                                let choosedOption = { id: i + 1, value: val }
                                this.onSelectOption(choosedOption)
                            }}
                            data={formFields.subjectForTickets.map(item => ({
                                value: item.value, ...item
                            }))}
                            value={selectedOption.value}
                            containerStyle={styles.formView}
                            dropdownPosition={-3}
                            renderBase={(props) => (
                                <OutLinedInput
                                    placeholder={res.strings.SELECT_SUBJECT}
                                    placeholderTextColor={res.colors.liteBlack}
                                    onChangeText={this.onSelectOption}
                                    placeHolderStyle={styles.placeHolderText}
                                    isDropDownImageVisible={true}
                                    value={selectedOption.value}
                                    inputProps={{
                                        autoCaptialize: 'none',
                                        returnKeyType: 'next',
                                    }}
                                >
                                </OutLinedInput>
                            )}

                        />
                        <View style={styles.inputBoxCont}>
                            <TextInput
                                style={styles.textInputStyle}
                                onChangeText={text => this.setState({ descriptionText: text })}
                                value={this.state.descriptionText}
                                placeholder={'Enter your Comments here'}
                                placeholderTextColor={res.colors.black}
                                multiline={true}
                                autoCompleteType='off'
                                maxLength={150}

                            />
                        </View>
                        <View style={styles.bottomView}>
                            <View style={styles.saveBtn}>
                                <PGButton
                                    btnText={strings.SAVE}
                                    onPress={() => this.hitCreateTicketAPI(false)}
                                    btnStyle={styles.whiteBtn}
                                    textStyleOver={styles.btnTxt}
                                />
                            </View>
                            <View style={styles.saveAndAddNewBtn}>
                                <PGButton
                                    btnText={strings.SAVE_AND_ADDNEW}
                                    onPress={() => this.hitCreateTicketAPI(true)}
                                    btnStyle={styles.yellowBtn}
                                    textStyleOver={styles.btnTxt}
                                />
                            </View>


                        </View>
                        {isLoading && this.renderLoader()}
                    </KeyboardAwareScrollView>
                </View>

            </View>
        )
    }
}

const styles = StyleSheet.create({

    containerView: {
        backgroundColor: res.colors.butterscotch,
        height: heightScale(250),
        marginTop: heightScale(40),
        borderRadius: widthScale(24),
        marginHorizontal: widthScale(10),
        flex: 1
    },
    whiteView: {
        backgroundColor: res.colors.white,
        margin: widthScale(10),
        flex: 1,
        borderRadius: widthScale(24),
        overflow: 'hidden'
    },
    headerStyle: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingVertical: heightScale(10),
        // shadowColor: "rgba(0,0,0, 0.1)",
        // shadowOffset: {
        //     width: 0,
        //     height: 1,
        // },
        // shadowOpacity: 0.8,
        // shadowRadius: 10,
        // elevation: 10,
        borderTopRightRadius: widthScale(20),
        borderTopLeftRadius: widthScale(20),
        borderBottomWidth: 0.5,
        borderBottomColor: res.colors.shadowColor

    },
    headerTxt: {
        textAlign: 'center',
        fontFamily: res.fonts.bold,
        fontSize: widthScale(18),
        color: res.colors.greyishBrown,
        // marginTop:heightScale(10)
    },
    closeBtnView: {
        position: 'absolute',
        right: widthScale(10),
    },
    formView: {
        marginHorizontal: widthScale(10)
    },
    inputBoxCont: {
        marginHorizontal: widthScale(10),
        marginTop: heightScale(30)
    },
    textInputStyle: {
        height: heightScale(105),
        borderWidth: 1,
        borderColor: res.colors.appColor,
        justifyContent: 'flex-start',
        padding: widthScale(16),
        fontFamily: res.fonts.semiBold,
        fontSize: widthScale(16),
        color: res.colors.liteBlack,
        // textAlignVertical:'top',
    },
    placeHolderText: {
        fontFamily: res.fonts.semiBold,
        fontSize: widthScale(16),
        color: res.colors.liteBlack,
        marginRight: 5
    },

    saveBtn:{
        width:widthScale(122),
        marginRight:widthScale(10)
    },
     saveAndAddNewBtn:{
        width:widthScale(168),
    },
    whiteBtn:{
backgroundColor:res.colors.white,
borderColor:res.colors.butterscotch,
borderWidth:1
    },
    yellowBtn:{
        backgroundColor:res.colors.butterscotch,
       
    },
    bottomView: {
        marginTop: heightScale(30),
        marginHorizontal: widthScale(20),
        flexDirection: 'row',
    },
    btnTxt:{
        fontFamily: res.fonts.bold,
        fontSize: widthScale(16),
        color: res.colors.greyishBrown, 
    }
})


export default CreateTicket;

