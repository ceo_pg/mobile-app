import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image, TextInput, SafeAreaView, FlatList } from 'react-native';
import res from '../../../res';
import { widthScale, heightScale, myHeight } from '../../utility/Utils'
import moment from 'moment'
import TicketCell from './TicketCell'
import style from '../../components/button/styles';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import AppUser from '../../utility/AppUser'
import resources from '../../../res';
import SimpleToast from 'react-native-simple-toast';
import ApiFetch from '../../apiManager/ApiFetch';
import { UPDATE_TICKET_ENDPOINTS } from '../../apiManager/ApiEndpoints';
import { ScrollView } from 'react-native-gesture-handler';

class TicketDetailView extends Component {
    constructor(props) {
        super(props);
        this.userId = AppUser.getInstance().userId
        this.state = {
            comment: '',
            isLoading: false
        }
    }
    componentDidMount() {

    }

    postComment = () => {
        const { comment } = this.state;
        const { ticketDetails, getTicketDetail } = this.props;
        if (comment.length == 0) {
            SimpleToast.show('Please enter comment', SimpleToast.LONG);
            return
        }

        let appUser = AppUser.getInstance()
        let parameters = {
            "status": ticketDetails.status,
            "comment": {
                "text": comment,
                "authorName": appUser.userName,
                "authorId": appUser.userId,
                "date": new Date(),
            }
        }
        console.log(JSON.stringify(parameters), parameters, "parameters")
        this.setState({ isLoading: true });
        let endPoints = UPDATE_TICKET_ENDPOINTS + '/' + ticketDetails._id
        ApiFetch.fetchPut(endPoints, JSON.stringify(parameters))
            .then((data) => {
                console.log(data, "data")
                this.setState({ isLoading: false, comment: '' });
                if (data && data.status && data.statusCode == 200) {
                    // refresh UI
                    if (getTicketDetail) {
                        getTicketDetail(ticketDetails);
                    }

                } else {
                    setTimeout(() => {
                        SimpleToast.show(data.message, SimpleToast.LONG);
                    }, 500);

                }

            }).catch((error) => {
                this.setState({ isLoading: false });
                console.log("Error inside login", error)
            })

    }

    renderComments = ({ item, index }) => {
        let name = item.authorName ? item.authorName : 'NA'
        let time = item.time ? item.time : 'NA'
        let desc = item.text ? item.text : ''
        return (
            <TouchableOpacity style={styles.contentView}>
                <View style={styles.userImgView}>
                    <Image resizeMode={'cover'} source={res.images.user_default}></Image>
                </View>
                <View style={styles.nameView}>
                    <Text style={styles.nameTxt}>{name}</Text>
                    <Text style={styles.timeTxt}>{time}</Text>
                    <Text style={styles.descTxt}>{desc}</Text>

                </View>
            </TouchableOpacity>


        )
    }



    render() {
        const { ticketDetails, closeModal } = this.props;
        const isOpen = ticketDetails && ticketDetails.status == 'Open' ? true : false
        // console.log(ticketDetails, "ticketDetails");
        return (

            <KeyboardAwareScrollView scrollEnabled={false} contentContainerStyle={{ flex: 1 }}>
                <View style={{ flex: 1 }}>

                    {/* <KeyboardAwareScrollView> */}

                    {/* <SafeAreaView style={{ width: '100%', height: '100%' }}> */}
                    <View style={{ flex: 1 }}>


                        <View style={styles.headerView}>
                            <Text style={styles.headerText}>Ticket</Text>
                            <TouchableOpacity style={styles.closeBtnView} onPress={() => closeModal()}>
                                <Image source={res.images.cross_ic} resizeMode={'contain'} style={{ height: widthScale(20), width: widthScale(20) }} />
                            </TouchableOpacity>
                        </View>

                        <View style={styles.ticketCellOutView}>

                            <TicketCell ticket={ticketDetails} />

                        </View>


                        {ticketDetails && ticketDetails.comments && ticketDetails.comments.length > 0 &&
                            // <View style={{ flex: 1 }}>
                            //     <ScrollView>
                            //         {
                            //             ticketDetails.comments.map((item, index) => {
                            //                 return this.renderComments({ item, index })
                            //             })
                            //         }
                            //     </ScrollView>
                            // </View>

                            <FlatList

                                keyExtractor={(item, index) => index.toString()}
                                showsVerticalScrollIndicator={false}
                                data={ticketDetails.comments}
                                renderItem={this.renderComments}
                                scrollEnabled={true}
                                style={{ flex: 1, }}
                            />

                        }
                        {isOpen &&
                            <View style={styles.inputCont}>
                                <View style={styles.overLayView}></View>
                                <TextInput
                                    style={styles.textInputStyle}
                                    onChangeText={text => this.setState({ comment: text })}
                                    value={this.state.comment}
                                    placeholder={'New Message'}
                                    placeholderTextColor={res.colors.black}
                                    multiline={true}
                                    autoCompleteType='off'
                                    maxLength={150}

                                />
                                <TouchableOpacity style={styles.sendBtnView} onPress={() => this.postComment()}>
                                    <Image resizeMode={'stretch'} source={res.images.send_ic}></Image>

                                </TouchableOpacity>
                            </View>

                        }

                    </View>

                </View>
            </KeyboardAwareScrollView>



        )
    }
}

const styles = StyleSheet.create({

    containerView: {
        marginHorizontal: widthScale(10),
        backgroundColor: res.colors.butterscotch,
        borderRadius: widthScale(10),
        padding: widthScale(5),
        overflow: 'hidden',
        flex: 1,
        marginTop: heightScale(60)
    },
    ticketCellOutView: {
        marginTop: heightScale(10),
        // flex: 1,
    },
    headerView: {
        justifyContent: 'center',
        alignItems: 'center',
        shadowOffset: {
            width: 0,
            height: 0,
        },
        shadowOpacity: 0.4,
        shadowRadius: 3,
        elevation: 10,
        borderBottomLeftRadius: widthScale(10),
        borderBottomRightRadius: widthScale(10),
        backgroundColor: res.colors.butterscotch,
    },
    headerText: {
        fontFamily: res.fonts.bold,
        fontSize: widthScale(18),
        color: res.colors.greyishBrown,
        paddingVertical: 10
    },
    listView: {
        flex: 1,
        // backgroundColor: 'red'
    },
    contentView: {
        flexDirection: 'row',
        // alignItems: 'center',
        // marginTop: heightScale(10),
        backgroundColor: resources.colors.butterscotch,
        marginHorizontal: widthScale(10),
        // shadowOffset: {
        //     width: 0,
        //     height: 0,
        // },
        // shadowOpacity: 0.4,
        // shadowRadius: 3,
        // elevation: 10,
        padding: 10,
        // borderRadius:10
    },
    userImgView: {
        height: widthScale(30),
        width: widthScale(30),
        borderRadius: widthScale(15),
        shadowOffset: {
            width: 0,
            height: 0,
        },
        shadowOpacity: 0.4,
        shadowRadius: 3,
        elevation: 10,
        backgroundColor: res.colors.butterscotch,
        // marginLeft: widthScale(10),
        marginRight: 10
    },
    nameView: {
        marginRight: widthScale(10)
    },
    nameTxt: {
        fontFamily: res.fonts.regular,
        fontSize: widthScale(12),
        color: res.colors.black,
    },
    timeTxt: {
        fontFamily: res.fonts.regular,
        fontSize: widthScale(12),
        color: 'rgba(60, 60, 67, 0.6)',
        paddingTop: 2
    },
    descTxt: {
        fontFamily: res.fonts.regular,
        fontSize: widthScale(14),
        color: res.colors.black,
        paddingTop: 2
    },
    inputCont: {
        marginHorizontal: widthScale(10),
        flexDirection: 'row',
        height: heightScale(35),
        marginBottom: 20
        //    overflow:'hidden'

        // flex: 1
    },
    textInputStyle: {
        height: heightScale(50),
        width: '90%',
        paddingLeft: 10,
        fontFamily: res.fonts.regular,
        fontSize: widthScale(12),
        color: res.colors.black,
        paddingTop: heightScale(20)
    },
    sendBtnView: {
        width: '10%',
        height: heightScale(50),
        justifyContent: 'center',
        alignItems: 'center'
    },
    overLayView: {
        position: 'absolute',
        backgroundColor: 'rgba(0,0,0,0.1)',
        height: heightScale(50),
        width: '100%'
    },
    closeBtnView: {
        position: 'absolute',
        right: widthScale(10),
    },

})


export default TicketDetailView;

