import React, { Component } from 'react'
import { View, Text, SafeAreaView, ImageBackground, Image, Linking, FlatList } from 'react-native'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { CommonActions } from '@react-navigation/native';
import { bindActionCreators } from "redux"
import { connect } from 'react-redux';
import Toast from 'react-native-simple-toast'
import styles from './styles'
import HeaderWithTitle from '../../components/header/HeaderWithTitle'
import res from '../../../res'
import { TabView, Loader } from '../../components'
import SupportHeader from '../../components/header/SupportHeader'
import Modal from '../../components/modal/Modal'
import CreateTicket from './CreateTicket'
import { PGButton } from '../../components/index'
import { heightScale } from '../../utility/Utils';
import TicketCell from './TicketCell'
import TicketDetailView from './TicketDetailView'
import { getAllTickets, saveAllTickets, getTicketDetails } from '../../redux/actions/supportTickets/TicketAction'
import NoDataFoundComp from '../../components/noDataFound/NoDataFoundComp'
import { TouchableOpacity } from 'react-native-gesture-handler';
import { openInbox } from 'react-native-email-link'
import { openComposer } from 'react-native-email-link'


class SupportBaseScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isLoading: false,
            selectedIndex: 0,
            filteredIndex: 0,
            originalTickets: [],
            selectedTicket: {},
            cellList: [{
                image: res.images.icn_contract,
                number: "123",
                cell: res.strings.TICKETS
            }, {
                image: res.images.icn_contract,
                number: "123",
                cell: res.strings.OTHERS
            },
            ],

        }
        this.modalRef = React.createRef();

    }
    componentDidMount() {
        this.getAllTickets()
    }

    getAllTickets = () => {
        this.setState({ isLoading: true })
        this.props.getAllTickets().then((response => {
            this.setState({ isLoading: false });
            if (response.statusCode == 200 && response.status) {
                if (response.data && response.data.data) {
                    this.props.saveAllTickets(response.data.data)
                    this.setState({ originalTickets: response.data.data });
                }

            } else {
                Toast.show(response.message, Toast.LONG)
            }
        }))
            .catch(error => {
                this.setState({ isLoading: false })
                console.log(error, "error");
            })
    }


    selectedIndex = (index) => {
        this.setState({
            selectedIndex: index
        })
    }

    changeFilterIndex = async (index) => {
        const { originalTickets } = this.state
        console.log(originalTickets, "originalTickets");
        await this.filteredData(index);
        this.setState({ filteredIndex: index });
    }

    filteredData = (index) => {
        const { originalTickets } = this.state
        let tempFilterData = []
        if (index == 1) {
            tempFilterData = originalTickets.filter(ticket => ticket.status == 'Open')
            this.props.saveAllTickets(tempFilterData)

        } else if (index == 2) {
            tempFilterData = originalTickets.filter(ticket => ticket.status == "Closed")
            this.props.saveAllTickets(tempFilterData)

        } else {
            this.props.saveAllTickets(originalTickets)

        }
        console.log(tempFilterData, "tempFilterData");
        return tempFilterData

    }

    renderTabView = () => {
        const { cellList } = this.state
        return <TabView
            listDataItem={cellList}
            selectedIndex={this.selectedIndex}
            tabBarUnderlineStyle={{ backgroundColor: res.colors.pinkishTan, height: 2 }}
            tabBarInactiveTextColor={res.colors.white}
            tabBarActiveTextColor={res.colors.pinkishTan}

        />
    }

    renderContent = () => {
        if (this.state.selectedIndex == 0) {
            this.renderTocketsView();
        } else {
            this.renderOthers()
        }
    }

    renderOthers = () => {
        return (
            <View style={styles.otherViewContainer}>
                <KeyboardAwareScrollView>
                    <View style={{ flex: 1 }}>
                        <View style={styles.scheduleCallView}>
                            <Image source={res.images.ticket_support_ic} resizeMode={'contain'} style={styles.supportImg} />
                            <Text style={styles.scheduleCallTxt}>{res.strings.SCHEDULE_A_CALL_WITH_EXPERT}</Text>

                            <PGButton
                                btnText={res.strings.SCHEDULE_CALL}
                                onPress={() => this.onpenCalendyLink()}
                                btnStyle={styles.scheduleCallBtn}
                                textStyleOver={styles.scheduleBtnTxt}
                            />

                        </View>

                        <View style={[styles.scheduleCallView, { marginTop: heightScale(40) }]}>
                            <Image source={res.images.mail_support_ic} resizeMode={'contain'} style={styles.supportImg} />
                            <Text style={styles.scheduleCallTxt}>{res.strings.REACH_OUT_TO_US}</Text>

                            <PGButton
                                btnText={res.strings.SEND_EMAIL}
                                onPress={() => this.onSendEmail()}
                                btnStyle={styles.scheduleCallBtn}
                                textStyleOver={styles.scheduleBtnTxt}
                            />

                        </View>
                    </View>


                </KeyboardAwareScrollView>

            </View>
        )
    }

    renderTicketViewList = () => {
        const { allTickets } = this.props
        const { filteredIndex } = this.state
        // console.log(allTickets, "allTickets")
        // if (!allTickets || allTickets.length == 0) {
        //     return (
        //         <View style={styles.allTicketsContainer}>
        //             <NoDataFoundComp />
        //         </View>
        //     )
        // }
        return (
            <View style={styles.allTicketsContainer}>
                <View style={styles.filterHeaderView}>
                    <View style={styles.filterTabView}>
                        <TouchableOpacity style={filteredIndex == 0 ? styles.selectedFliter : styles.unSelectedFilter} onPress={() => this.changeFilterIndex(0)}>
                            <Text style={filteredIndex == 0 ? styles.selectedText : styles.unSelectedText}>All</Text>

                        </TouchableOpacity>

                    </View>
                    <View style={styles.filterTabView}>
                        <TouchableOpacity style={filteredIndex == 1 ? styles.selectedFliter : styles.unSelectedFilter} onPress={() => this.changeFilterIndex(1)}>
                            <Text style={filteredIndex == 1 ? styles.selectedText : styles.unSelectedText}>Open</Text>

                        </TouchableOpacity>

                    </View>
                    <View style={styles.filterTabView}>
                        <TouchableOpacity style={filteredIndex == 2 ? styles.selectedFliter : styles.unSelectedFilter} onPress={() => this.changeFilterIndex(2)}>
                            <Text style={filteredIndex == 2 ? styles.selectedText : styles.unSelectedText}>Closed</Text>

                        </TouchableOpacity>

                    </View>
                </View>
                {allTickets && allTickets.length > 0 &&

                    <FlatList
                        keyExtractor={(item, index) => item._id.toString()}
                        showsVerticalScrollIndicator={false}
                        data={allTickets}
                        renderItem={this.renderListItem}
                    />

                }
                {(!allTickets || allTickets.length == 0) &&
                    <View style={styles.allTicketsContainer}>
                        <NoDataFoundComp />
                    </View>

                }

            </View>
        )

    }

    renderListItem = ({ item, index }) => {
        return (
            <TicketCell ticket={item} onRowClicked={this.onRowClicked} />
        )
    }

    onRowClicked = (ticketDetails) => {
        console.log(ticketDetails, "ticketDetails")
        this.setState({ isLoading: true });
        this.props.getTicketDetails(ticketDetails._id).then((response => {
            console.log(response, "ticket detail")
            this.setState({ isLoading: false });
            if (response.statusCode == 200 && response.status) {
                let responseObj = response.data
                let tempResponse = {
                    ...responseObj,
                    comments: responseObj.comments && responseObj.comments.length > 0 ? responseObj.comments.reverse() : []

                }
                this.setState({ selectedTicket: tempResponse }, () => {
                    this.displayTicketDetailModal();
                })

            } else {
                Toast.show(response.message, Toast.LONG)
            }
        }))
            .catch(error => {
                this.setState({ isLoading: false })
                console.log(error, "error");
            })
    }

    hitTicketDetailAPI = (ticketDetails) => {
        this.setState({ isLoading: true });
        this.props.getTicketDetails(ticketDetails._id).then((response => {
            console.log(response, "ticket detail")
            this.setState({ isLoading: false });
            if (response.statusCode == 200 && response.status) {
                let responseObj = response.data
                let tempResponse = {
                    ...responseObj,
                    comments: responseObj.comments.reverse()

                }
                this.setState({ selectedTicket: tempResponse }, () => {
                })
                // this.getAllTickets()
            } else {
                Toast.show(response.message, Toast.LONG)
            }
        }))
            .catch(error => {
                this.setState({ isLoading: false })
                console.log(error, "error");
            })
    }

    displayTicketDetailModal = () => {
        this.modalRef.current.setView(this.renderTicketDetailModal);
        this.modalRef.current.show();
    }

    renderTicketDetailModal = () => {
        const { selectedTicket } = this.state
        return (
            <View style={styles.ticketDetail}>
                <TicketDetailView ticketDetails={selectedTicket} getTicketDetail={this.hitTicketDetailAPI} closeModal={this.closeModal} />

            </View>

        )
    }


    renderLoader = () => {
        return (
            <Loader
                loading={this.state.isLoading} />
        );
    };
    renderTab = () => {
        return <View style={{ overflow: 'hidden', paddingBottom: 5, marginTop: 10 }}>
            <View
                style={styles.headerShadow}
            >
                {this.renderTabView()}
            </View>
        </View>
    }

    onPlusClick = () => {
        this.displayTicketModal();
    }

    displayTicketModal = () => {
        this.modalRef.current.setView(this.renderTicketModal);
        this.modalRef.current.show();
    }

    renderTicketModal = () => {
        return (
            <View style={{ justifyContent: 'center', flex: 1 }}>
                <CreateTicket closeModal={this.closeModal} onReferesh={this.getAllTickets} />
            </View>
        )
    }

    closeModal = () => {
        this.modalRef.current.hide()
    }

    onpenCalendyLink = () => {
        let url = 'https://calendly.com/propguide_legal'
        Linking.canOpenURL(url).then(supported => {
            if (supported) {
                Linking.openURL(url);
            } else {
                console.log("Don't know how to open URI: " + url);
            }
        });
    }

    onSendEmail = () => {
        try {
            openComposer({
                to: 'support@propright.com',
                subject: 'Suppor',
                body: ''
            })
        }
        catch (e) {
            Toast.show("No App found", Toast.LONG)
        }

    }
    openDrawer = () => {
        this.props.navigation.openDrawer()
        const resetAction = CommonActions.reset({
            index: 0,
            routes: [{ name: 'SupportBaseScreen' }],
        });
        this.props.navigation.dispatch(resetAction);
    }
    render() {
        const { isLoading, selectedIndex } = this.state
        return (
            <View style={{flex: 1}}>
            <SafeAreaView style={styles.fullScreen}>
                <ImageBackground source={res.images.mainBackground} style={{ flex: 1 }}>
                    <HeaderWithTitle
                        styleHeaderContainer={{ backgroundColor: res.colors.appColor }}
                        headerTitle={res.strings.SUPPORT}
                        isBackIconVisible={false}
                        isSupport={true}
                        onPressRightBtn={this.onPlusClick}
                        openDrawer={() => this.openDrawer()}
                    />
                    {/* <SupportHeader title={"res.strings.OTHER_DOCUMENTS1"} onBack={this.backPress}/> */}
                    {this.renderTab()}

                    {selectedIndex == 0 &&
                        this.renderTicketViewList()
                    }
                    {selectedIndex == 1 &&
                        this.renderOthers()
                    }
                    {isLoading && this.renderLoader()}

                </ImageBackground>

            </SafeAreaView>
            <Modal ref={this.modalRef} isManualHide={true} />

            </View>
        )
    }

}
const mapStateToProps = (state) => {
    return {
        allTickets: state.TicketsReducer.allTickets

    };
};

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        getAllTickets,
        saveAllTickets,
        getTicketDetails
    }, dispatch);
}
let SupportBaseScreenContainer = connect(mapStateToProps, mapDispatchToProps)(SupportBaseScreen);
export default SupportBaseScreenContainer;