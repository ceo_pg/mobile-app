import { StyleSheet, Platform, StatusBar } from 'react-native'
import res from '../../../res'
import { heightScale, widthScale } from '../../utility/Utils'
const styles = StyleSheet.create({
    fullScreen: {
        flex: 1,
        backgroundColor: res.colors.appColor,
        paddingTop: Platform.OS === "android" ? StatusBar.currentHeight + 10 : 0
    }, headerShadow: {
        backgroundColor: res.colors.appColor,

        height: 50,
        shadowColor: '#000',
        shadowOffset: { width: 1, height: 1 },
        shadowOpacity: 0.4,
        shadowRadius: 3,
        elevation: 5,
    }, container: {
        flex: 1,
        backgroundColor: res.colors.appColor,
        overflow: 'hidden'
        // marginTop: Platform.OS == 'ios' ? 35 : 20
    },
    otherViewContainer: {
        flex: 1,
    },
    scheduleCallView: {
        marginTop: heightScale(25),
        marginHorizontal: widthScale(15),
        borderRadius: widthScale(10),
        backgroundColor: res.colors.white,
        justifyContent: 'center',
        alignItems: 'center'

    },
    supportImg: {
        height: heightScale(60),
        width: widthScale(46),
        marginTop: heightScale(10)
    },
    scheduleCallTxt: {
        fontFamily: res.fonts.bold,
        fontSize: widthScale(16),
        color: res.colors.greyishBrown,
    },
    scheduleBtnTxt: {
        fontFamily: res.fonts.bold,
        fontSize: widthScale(18),
        color: res.colors.greyishBrown,
    },
    scheduleCallBtn: {
        backgroundColor: res.colors.butterscotch,
        borderBottomLeftRadius: 0,
        borderTopRightRadius: 0,
        marginTop: heightScale(20)
    },
    allTicketsContainer: {
        backgroundColor: res.colors.butterscotch,
        flex: 1,
        marginHorizontal: widthScale(15),
        borderRadius: widthScale(15),
        marginTop: heightScale(10)
    },
    ticketDetail: {
        marginTop: heightScale(70),
        marginHorizontal: widthScale(15),
        flex: 1,
        backgroundColor: res.colors.butterscotch,
        borderRadius: widthScale(10)
    },
    filterHeaderView: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        height: heightScale(40),
        shadowOffset: {
            width: 0,
            height: 0,
        },
        shadowOpacity: 0.4,
        shadowRadius: 3,
        elevation: 10,
        borderBottomLeftRadius: widthScale(10),
        borderBottomRightRadius: widthScale(10),
        backgroundColor: res.colors.butterscotch,
        marginBottom: 10
    },
    filterTabView: {
        width: '33.33%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    selectedFliter: {
        backgroundColor: res.colors.appColor,
        borderRadius: 8,
        width:widthScale(85)  
    },
    unSelectedFilter:{
        backgroundColor: res.colors.peacockBlueLight,
        borderRadius: 8,  
        width:widthScale(85)
    },

    selectedText: {
        paddingVertical: 5,
        // paddingHorizontal: 10,
        fontFamily: res.fonts.bold,
        fontSize: widthScale(10),
        color: res.colors.white,
        textAlign:'center'
    },
    unSelectedText: {
        paddingVertical: 5,
        paddingHorizontal: 25,
        fontFamily: res.fonts.regular,
        fontSize: widthScale(10),
        color: res.colors.black,
        textAlign:'center'

    },

})
export default styles;
