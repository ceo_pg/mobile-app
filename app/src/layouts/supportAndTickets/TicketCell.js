import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image, TextInput, KeyboardAvoidingView } from 'react-native';
import res from '../../../res';
import { widthScale, heightScale } from '../../utility/Utils'
import moment from 'moment'


class TicketCell extends Component {
    constructor(props) {
        super(props);
        this.state = {

        }
    }
    componentDidMount() {

    }

    render() {
        const { ticket, onRowClicked } = this.props;
        if (!ticket || !(ticket.subject)) {
            return <View />
        }
        let commentCount = ticket.comments ? ticket.comments.length : 0;
        let createdAt = ticket.createdAt ? moment(ticket.createdAt).format('MMM DD YYYY hh:mm') : 'NA'

        return (
            <View style={styles.containerView}> 

  
            <TouchableOpacity disabled={onRowClicked ? false : true} style={{width:'100%'}} onPress={() => onRowClicked(ticket)}>
                <View style={styles.innerView}>
        <View style={styles.titleStatusView}>
                    <View style={styles.subjectView}>
                        <Text style={styles.ticketTitleTxt}>{ticket.subject}</Text>
                        <Text style={styles.dateTxt}>{createdAt}</Text>
                    </View>
                    <View style={styles.statusView}>
                        <Text style={styles.statusTxt}>{'Status:'} {ticket.status}</Text>
                        <Text style={styles.ticketNumberTxt}>{'Ticket No.'} {ticket.ticketNumber ? ticket.ticketNumber : 'NA'}</Text>
                    </View>
                </View>
                <View>
                    <View style={styles.descriptionView}>
                        <Text style={styles.dateTxt}>
                            {ticket.description ? ticket.description : 'NA'}
                        </Text>
                    </View>
                    <View style={styles.commentView}>
                        <View style={styles.iconView}>
                            <Image resizeMode={'stretch'} source={res.images.comment_ic}></Image>

                        </View>
                        <Text style={styles.commentCountTxt}>{commentCount} comments</Text>

                    </View>
                </View>
                </View>

            </TouchableOpacity>
       </View>
        )
    }
}

const styles = StyleSheet.create({

    containerView: {
        justifyContent:'center',
        alignItems:'center',
        overflow: 'hidden',
        width:'100%',
    },
    innerView:{
        shadowColor: 'black',
        shadowOffset: {
            width: 0,
            height: 0,
        },
        shadowOpacity: 0.4,
        shadowRadius: 3,
        elevation: 10,
        marginBottom:heightScale(10),
        marginHorizontal:'2%',
        width:'96%',
        // backgroundColor:'red',
        borderRadius: widthScale(10),
        backgroundColor:res.colors.butterscotch,
        padding:widthScale(10),
    },
    titleStatusView: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: heightScale(10),
        // flex: 1,
        // backgroundColor:'red'

    },
    ticketTitleTxt: {
        fontFamily: res.fonts.bold,
        fontSize: widthScale(14),
        color: res.colors.liteBlack,
    },
    dateTxt: {
        fontFamily: res.fonts.regular,
        fontSize: widthScale(10),
        color: res.colors.greyishBrown,
    },
    ticketNumberTxt: {
        fontFamily: res.fonts.regular,
        fontSize: widthScale(10),
        color: res.colors.greyishBrown,
        textAlign: 'right',
    },
    statusTxt: {
        fontFamily: res.fonts.bold,
        fontSize: widthScale(14),
        color: res.colors.peacockBlue,
        textAlign: 'right'
    },
    descriptionView: {
        justifyContent: 'center',
        marginTop: heightScale(5),
        overflow: 'hidden'
    },
    descriptionTxt: {
        fontFamily: res.fonts.regular,
        fontSize: widthScale(10),
        color: res.colors.greyishBrown,
    },
    commentView: {
        marginTop: heightScale(5),
        flexDirection: 'row',
        alignItems: 'center',
    },
    commentCountTxt: {
        fontFamily: res.fonts.regular,
        fontSize: widthScale(14),
        color: res.colors.greyishBrown,
    },
    iconView: {
        marginRight: 2,
        width: 20,
        height: 20,
        justifyContent: 'center'
    },
    statusView: {
        // flex: 1,
        width:'50%'
    },
    subjectView: {
        // flex: 1
        width:'50%'
    },
})


export default TicketCell;

