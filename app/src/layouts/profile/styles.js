import { StyleSheet, StatusBar } from 'react-native';
import resources from '../../../res';
import { widthScale, heightScale } from '../../utility/Utils';
const styles = StyleSheet.create({
    fullScreen: {
        flex: 1,
        backgroundColor: resources.colors.appColor,
        paddingTop: Platform.OS === "android" ? StatusBar.currentHeight + 10 : 0
    }, mainView: {
        borderRadius: widthScale(20),
        marginHorizontal: widthScale(17),
        // width: "100%",
        backgroundColor: resources.colors.butterscotch,
        marginTop:heightScale(10),
        marginBottom:heightScale(30)
    }, subView: {
        borderRadius: widthScale(20),
        margin: widthScale(10),
        backgroundColor: resources.colors.white
    }, profileImage: {
        width: widthScale(84),
        height: widthScale(84),
        borderRadius: widthScale(42),
        alignSelf: 'center',
        justifyContent:'center',
        // marginTop:widthScale(10),
        // backgroundColor: 'red'
    }, profileNameText: {
        fontFamily: resources.fonts.semiBold,
        fontSize: heightScale(16),
        color: resources.colors.greyishBrown,
        textAlign: 'center'
    }, icnCameraStyle: {
        position: 'absolute',
        // width: widthScale(20),
        // height: widthScale(20),
        // backgroundColor:'red',
        top: heightScale(22),
        alignSelf: 'center',

    },
    bottomBtnContainer: {
        flex: 1,
        justifyContent: 'space-between',
        flexDirection: 'row',
        marginHorizontal: widthScale(10),
        marginTop:heightScale(20)
    },
    flex1: {
        flex: 1,
        margin: 5
    },
    saveBtn: {

    },
    cancelBtn: {
        backgroundColor: resources.colors.white,
        borderWidth: 2,
        borderColor: resources.colors.appColor
    },
    saveBtnText:{
        fontFamily: resources.fonts.bold,
        fontSize: widthScale(18),
        color: resources.colors.white,
        letterSpacing:0.13,
        fontWeight:'bold'
    },
    cancelBtnText:{
        fontFamily: resources.fonts.bold,
        fontSize: widthScale(18),
        color: resources.colors.appColor,  
        letterSpacing:0.13,
        fontWeight:'bold'

    },
    changePassView:{
        marginTop:heightScale(25),
        justifyContent:'center',
        marginHorizontal:widthScale(30)
    },
    changePassText:{
        fontFamily: resources.fonts.bold,
        fontSize: widthScale(18),
        color: resources.colors.appColor,
        letterSpacing:0.13,
        fontWeight:'bold',
        textDecorationLine:'underline',        
        textAlign:'center'  
    }

});

export default styles;
