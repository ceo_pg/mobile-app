//remove addree line 2d
//view more
//includes
//change your property back
//sign without verfication
//profile issue sign up

import React, { Component } from 'react';
import { View, Text, TouchableOpacity, ImageBackground, Image, SafeAreaView } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { CommonActions } from '@react-navigation/native';
import AsyncStorage from '@react-native-community/async-storage'
import moment from 'moment'
import DashBoardHeader from '../../components/header/DashBoardHeader'
import styles from './styles';
import res from '../../../res';
import PGButton from '../../components/button/Button'
import { USER_DETAILS_ENDPOINTS } from '../../apiManager/ApiEndpoints';
import ApiFetch from '../../apiManager/ApiFetch';
import OutLinedInput from '../../components/input/OutLinedInput'
import { widthScale, heightScale, checkIsImageOrPdfFileType, checkValidFileSize } from '../../utility/Utils';
import { getProfile, updateProfile } from '../../redux/actions/Profile/ProfileAction'
import PGDatePicker from '../../components/input/DatePicker'
import ImagePicker from 'react-native-image-picker'
import DocumentPicker from 'react-native-document-picker'
import Toast from 'react-native-simple-toast'
import { PickerViewModal, CAMERA, DOCUMENT, IMAGE_PICK } from '../../components/Picker/PickerViewModal'
import { fileUpload } from '../../redux/actions/FileUploadAction'
import resources from '../../../res';
import AppUser from '../../utility/AppUser';
import { Loader } from '../../components/index'
let appUsrObj = AppUser.getInstance()
const options = {
    title: 'Select Image',
    mediaType: 'photo',
    allowsEditing: false,
    quality: 1.0,
    maxWidth: 500,
    maxHeight: 400,
    progress: {},
    storageOptions: {
        skipBackup: true,
        path: 'images',
    },
}

class ProfileScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            profile: {},
            email: "",
            dob: "",
            address1: "",
            address2: '',
            panNo: "",
            aadhar: "",
            gstIn: "",
            profileUrl: '',
            profileImageName: '',
            pickOptionsVisible: false,
            userName: '',
            phoneNumber: '',
            isLoading: false,
            dobParams: ''
        }
    }
    componentDidMount() {
        this.profileListApi()
    }
    profileListApi = () => {
        this.setState({ isLoading: true })
        this.props.getProfile().then((res) => {
            this.setState({ isLoading: false })
            if (!res || res.statusCode != 200 || !(res.status)) {
                Toast.show(res && res.message ? res.message : resources.strings.PLEASE_TRY_AFTER_SOMETIME);
                return
            }

            let dob = res.data.dob ? res.data.dob : null
            if (dob) {
                dob = moment(dob).format('DD-MM-YYYY');
            }
            this.setState({
                profile: res.data,
                profileUrl: res.data.profileImageUrl ? res.data.profileImageUrl : '',
                profileImageName: res.data.profileImage ? res.data.profileImage : appUsrObj.profileurl,
                userName: res.data.userName,
                email: res.data.email,
                aadhar: res.data.aadhaar ? res.data.aadhaar : '',
                panNo: res.data.panNo ? res.data.panNo : '',
                phoneNumber: res.data.phone ? res.data.phone : '',
                gstIn: res.data.gstIn ? res.data.gstIn : '',
                address1: res.data.address ? res.data.address : '',
                dob: dob,
                dobParams: res.data.dob ? res.data.dob : '',
                isLoading: false
            })
            this.saveToAsync(res.data.profileImageUrl, res.data.userName, res.data.phone)
        })
            .catch(error => {
                this.setState({ isLoading: false });
                Toast.show(res.strings.PLEASE_TRY_AFTER_SOMETIME, Toast.LONG);
                console.log(error, "error");
            })
    }
    saveToAsync = async (url, name, phone) => {
        console.log("PROFILE", url, name, phone)
        // let appUsrObj = AppUser.getInstance();
        appUsrObj.name = name
        appUsrObj.profileurl = url
        const namefiled = [res.AsyncStorageContants.NAME, name];
        const profileUrl = [res.AsyncStorageContants.PROFILEURL, url];
        const phonenumber = [res.AsyncStorageContants.PHONE, phone];
        try {
            await AsyncStorage.multiSet([namefiled, profileUrl, phonenumber])

        } catch (e) {
            console.log("Error saving user details", e);
        }
    }
    openDrawer = () => {

        const resetAction = CommonActions.reset({
            index: 0,
            routes: [{ name: 'ProfileScreen' }],
        });
        this.props.navigation.dispatch(resetAction);
        this.props.navigation.openDrawer()
        // this.props.navigation.openDrawer()
    }

    onCancel = () => {
        this.props.navigation.goBack()
    }

    onSave = () => {
        const { userName, phoneNumber, email, aadhar, address1, address2, dobParams, gstIn, panNo, profileImageName } = this.state
        console.log(userName, phoneNumber, email, aadhar, address1, address2, dobParams, gstIn, panNo, profileImageName, "valuesss")
        console.log(this.state.profileImageName, this.state.profileUrl, "on save");
        //   let appUser = AppUser.getInstance()
        let profileObject = {
            userName: userName,
            phone: phoneNumber,
            // email:email,
            aadhaar: aadhar,
            address: address1 + address2,
            dob: dobParams,
            gstIn: gstIn,
            panNo: panNo,
            profileImage: profileImageName
        }
        this.setState({ isLoading: true });
        this.props.updateProfile(profileObject).then((res) => {
            this.setState({ isLoading: false });
            if (res.statusCode == 200 && res.status) {
                this.profileListApi()
            }
            setTimeout(() => {
                Toast.show(res.message ? res.message : res.strings.PLEASE_TRY_AFTER_SOMETIME, Toast.LONG);

            }, 400);
        })
            .catch(error => {
                this.setState({ isLoading: false })
                Toast.show(res.strings.PLEASE_TRY_AFTER_SOMETIME, Toast.LONG)
                console.log(error, "error");
            })
    }

    // onChangePassword = () => {
    //     this.props.navigation.navigate("ChangePasswordScreen")
    // }
    onChangeProperty = () => {
        this.props.navigation.navigate("PropertyDetailsScreen", {
            isBack: true
        })
    }
    onClickPickType = (type) => {
        this.setState({
            pickOptionsVisible: null
        })
        setTimeout(() => {
            switch (type) {
                case DOCUMENT:
                    // Open File picker
                    this.openDocument()
                    break;
                case IMAGE_PICK:
                    // Open Image Library:
                    ImagePicker.launchImageLibrary(options, this.onResultFromImagePicker);
                    break;
                case CAMERA:
                    // Launch Camera:
                    ImagePicker.launchCamera(options, this.onResultFromImagePicker);
                    break;
                default:
                    break;
            }
        }, 500)

    }

    openDocument = async () => {
        try {
            const res = await DocumentPicker.pick({
                type: [DocumentPicker.types.pdf, DocumentPicker.types.images],
            });
            console.log("Document", res)
            this.onResultFromPicker(res);
        } catch (err) {
            this.setState({
                pickOptionsVisible: null
            })
            if (DocumentPicker.isCancel(err)) {
                // User cancelled the picker, exit any dialogs or menus and move on
            } else {
                throw err;
            }
        }

    }

    onResultFromPicker = (response) => {
        if (!response) {
            Toast.show("Please try again")
            return
        }
        console.log("uri => " + response.uri + "\n " +
            "type => " + response.type + "\n Size => " +
            response.size ? response.size : response.fileSize)
        this.setState({
            pickOptionsVisible: null
        })
        if (checkIsImageOrPdfFileType(response.type)) {
            if (checkValidFileSize(response.size ? response.size : response.fileSize)) {
                console.log("checkValidFileSize", response.size)
            } else {
                return Toast.show("Size can not exceeds 10 MB")
            }
        } else {
            return Toast.show("Please select pdf,jpg,jpeg,png format documents only")
        }
        // UPLOAD File then hit update DOC API
        this.hitUploadAPI(response)

    }

    hitUploadAPI = (fileDetails) => {
        this.props.fileUpload(fileDetails, this.progressCallback.bind(this, 'profileImage')).then((response => {
            console.log(response, "file updaded response");
            if (response.statusCode == 200 && response.status) {
                this.setState({ profileUrl: fileDetails.uri ? fileDetails.uri : '', profileImageName: response.data.key ? response.data.key : '' });
            } else {
                Toast.show(response.message ? response.message : resources.strings.PLEASE_TRY_AFTER_SOMETIME);
            }

        }))
            .catch(error => {
                console.log(error, "error");
            })

    }

    progressCallback = (uniqueID, progress) => {

    }

    onPressBackDrop = () => {
        this.setState({
            pickOptionsVisible: null
        })
    }
    showPickerOptions = () => {
        setTimeout(() => {
            this.setState({
                pickOptionsVisible: 'bottom'
            })
        }, 1000);
    }

    onResultFromImagePicker = (response) => {
        console.log("onResultFromImagePicker", response)

        if (response.didCancel) {
            this.setState({
                pickOptionsVisible: null
            })
        } else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
            this.setState({
                pickOptionsVisible: null
            })
        } else {
            this.onResultFromPicker(response);
        }
    }

    onChangeUserName = (text) => {
        this.setState({ userName: text });
    }

    onChangeEmail = (text) => {

    }
    onChangeDOB = (text) => {
        console.log(text, "text")
        var time2 = Date.parse(text);
        var newdate = moment(time2).format('DD MMMM yyy HH:MM UTCC')
        newdate = newdate.substring(1, newdate.length - 1);
        const event = new Date(newdate);
        const finalDate = event.toISOString()
        var showDate = moment(text).format('DD/MM/YYYY')
        console.log("showDate", showDate)
        this.setState({
            dobParams: finalDate,
            dob: showDate
        })
    }
    onChangePhone = (text) => {
        this.setState({ phoneNumber: text });
    }

    onChangeAddressLine1 = (text) => {
        this.setState({ address1: text });

    }

    onChangeAddressLine2 = (text) => {
        this.setState({ address2: text });

    }

    onChangePan = (text) => {
        this.setState({ panNo: text });

    }

    onChangeAdhar = (text) => {
        this.setState({ aadhar: text });

    }

    onChangeGSTIN = (text) => {
        this.setState({ gstIn: text });
    }
    renderLoader = () => {
        return (
            <Loader
                loading={this.state.isLoading} />
        );
    };


    renderView = () => {
        const { profile } = this.state
        console.log(profile)
        return <View style={styles.mainView}>
            <View style={styles.subView}>
                <KeyboardAwareScrollView>
                    <View style={{ marginHorizontal: widthScale(10), marginBottom: heightScale(100) }}>
                        <TouchableOpacity style={[styles.profileImage, { marginTop: heightScale(10) }]} onPress={this.showPickerOptions}>
                            <View>
                                {this.state.profileUrl ? <Image style={styles.profileImage} source={{
                                    uri: this.state.profileUrl,
                                }} resizeMode={'cover'} />
                                    :
                                    <Image style={styles.profileImage} source={res.images.icn_Profile} />

                                }


                                <View style={styles.icnCameraStyle}>

                                    <Image source={res.images.icn_Camera} resizeMode={'contain'} />


                                </View>
                            </View>

                        </TouchableOpacity>
                        <View style={{ marginVertical: heightScale(10) }}>
                            <Text style={styles.profileNameText}>{profile.userName ? profile.userName : ""}</Text>
                        </View>
                        <OutLinedInput
                            label={res.strings.Full_Name}
                            placeholder={res.strings.Full_Name}
                            value={this.state.userName}
                            onChangeText={this.onChangeUserName}

                        />
                        <OutLinedInput
                            label={res.strings.Email_ID}
                            placeholder={res.strings.Email_ID}
                            value={profile.email ? profile.email : ""}
                            isEdit={false}
                        />
                        <OutLinedInput
                            label={res.strings.Phone_Number}
                            placeholder={res.strings.Phone_Number}
                            value={this.state.phoneNumber}
                            onChangeText={this.onChangePhone}
                            keyboardType={"phone-pad"}
                            maxLength={10}
                        />
                        <PGDatePicker
                            maxDate={new Date()}
                            label={res.strings.Date_of_Birth}
                            placeholder={"dd/mm/yyyy"}
                            dob={this.state.dob ? this.state.dob : ""}
                            onDateChange={this.onChangeDOB}
                        />
                        {/* <OutLinedInput
                        label={res.strings.Date_of_Birth}
                        placeholder={res.strings.Date_of_Birth}
                        value={moment(profile.dob).format("DD MMMM,YYYY") ? moment(profile.dob).format("DD/MM/yyyy") : ""}
                    /> */}
                        <OutLinedInput
                            label={res.strings.Address_Line_1}
                            placeholder={res.strings.Address_Line_1}
                            value={this.state.address1}
                            onChangeText={this.onChangeAddressLine1}

                        />
                        {/* <OutLinedInput
                            label={res.strings.Address_Line_2}
                            placeholder={res.strings.Address_Line_2}
                            value={this.state.address2}
                            onChangeText={this.onChangeAddressLine2}

                        /> */}
                        <OutLinedInput
                            label={res.strings.PAN_No}
                            placeholder={res.strings.PAN_No}
                            value={this.state.panNo}
                            onChangeText={this.onChangePan}

                        />
                        <OutLinedInput
                            label={res.strings.Adhaar}
                            placeholder={res.strings.Adhaar}
                            onChangeText={this.onChangeAdhar}
                            value={this.state.aadhar}

                        />
                        <OutLinedInput
                            label={res.strings.GSTIN}
                            placeholder={res.strings.GSTIN}
                            value={this.state.gstIn}
                            onChangeText={this.onChangeGSTIN}

                        />
                        <View style={styles.bottomBtnContainer}>
                            <View style={styles.flex1}>
                                <PGButton
                                    btnText={res.strings.CANCEL}
                                    onPress={() => this.onCancel()}
                                    btnStyle={styles.cancelBtn}
                                    textStyleOver={styles.cancelBtnText}

                                />
                            </View>
                            <View style={styles.flex1}>
                                <PGButton
                                    btnText={res.strings.SAVE}
                                    onPress={() => this.onSave()}
                                    textStyleOver={styles.saveBtnText}
                                />

                            </View>

                        </View>
                        <TouchableOpacity style={styles.changePassView} onPress={this.onChangeProperty}>
                            <Text style={styles.changePassText}>{res.strings.CHANGE_PROPERTY}</Text>
                        </TouchableOpacity>
                        {/* <TouchableOpacity style={styles.changePassView} onPress={this.onChangePassword}>
                            <Text style={styles.changePassText}>{res.strings.CHANGE_PASSWORD}</Text>
                        </TouchableOpacity> */}
                    </View>

                </KeyboardAwareScrollView>

            </View>

        </View>
    }

    render() {

        return (
            <SafeAreaView style={styles.fullScreen}>
                <ImageBackground source={res.images.mainBackground} style={{ flex: 1 }}>
                    <DashBoardHeader
                        isShowSupport={true}
                        openDrawer={() => this.openDrawer()}
                        headerTitle={res.strings.Profile}
                        settingVisible={false}
                        notificationVisible={false}
                    />

                    {this.renderView()}

                    {this.state.pickOptionsVisible ?
                        <PickerViewModal
                            isProfile={true}
                            onClickPickType={this.onClickPickType}
                            visibleModal={this.state.pickOptionsVisible}
                            onPressBackDrop={this.onPressBackDrop} /> : <View />}
                </ImageBackground>
                {this.state.isLoading && this.renderLoader()}
            </SafeAreaView >


        );
    }
}
const mapStateToProps = (state) => {
    return {

    };
};
function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        getProfile,
        fileUpload,
        updateProfile
    }, dispatch);
}
let ProfileScreenContainer = connect(mapStateToProps, mapDispatchToProps)(ProfileScreen);
export default ProfileScreenContainer;
// export default ProfileScreen

