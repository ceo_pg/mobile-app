import React, { Component } from 'react';
import { View, Text, TouchableOpacity, SafeAreaView, Image } from 'react-native';
import styles from './styles'
import HeaderWithTitle from '../../components/header/HeaderWithTitle'
import res from '../../../res'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { PGButton } from '../../components/index'
import * as RNIap from 'react-native-iap';
import { isPlatformIOS } from '../../utility/Utils';
import Toast from 'react-native-simple-toast'

const iOSProductIds = ["com.propright.monthplan"]
const androidProductIds = ["com.propright.monthplan"]

class Subscription extends Component {
    constructor(props) {
        super(props);
        this.state = {
            receipt:'',
            isLoading:false

        }
    }
    async componentDidMount() {
        //this.getSubscriptionList();
        try {
             await RNIap.initConnection();
          //  await RNIap.prepare();
         //   this.loadData();
            this.purchaseUpdateSubscription = RNIap.purchaseUpdatedListener(this.purchaseUpdatedCallback);
            this.purchaseErrorSubscription = RNIap.purchaseErrorListener(this.purchaseErrorCallback);
            setTimeout(() => {
                this.getSubscriptionList()
  
            }, 500);

        } catch (error) {
            console.log("Error inside componentDidMount", error);
            Toast.show(error.message)
            this.setState({ isLoading: false, });
          //  this.fetchSubscriptionList();
        }
    }



    getSubscriptionList = async () => {
        let productId = ""
        if (isPlatformIOS) {
            productId = iOSProductIds
        } else {
            productId = androidProductIds
        }
        console.log(productId, "productId")
        return new Promise(async (resolve, reject) => {
            try {
                let products = await RNIap.getAvailablePurchases();
                let planIdObject = {};
                console.log(products, "planIdObject");
                // for (let i=0; i<products.length; i++) {
                //     const pId = products[i].productId
                //     planIdObject[pId] = products[i];
                // }
                // this.setState({ storeProduts: products,planIdObject }, () => {
                //     resolve();
                // });
                resolve();

            } catch (error) {
                console.log("Inside catch of getPurchases ", error);
                // Toast.show(error.message || error.msg);
                reject();
            }
        });
    }
    purchaseErrorCallback = (error) => {
        console.log('purchaseErrorListener', error);
        Toast.show(error.message || error.msg)
    };
    purchaseUpdatedCallback = (purchase) => {
        console.log('purchaseUpdatedListener', purchase);

        const receipt = purchase.transactionReceipt;
        if (receipt) {
         //   this.verifyReceipt(purchase);
            if (Platform.OS === 'ios') {
            //    RNIap.finishTransactionIOS(purchase.transactionId);
            } else if (Platform.OS === 'android') {
              //  RNIap.consumePurchaseAndroid(purchase.purchaseToken);
            }
        }
        this.setState({
            receipt: purchase.transactionReceipt
        });
    };
    openDrawer = () => {

    }

    purchaseSubscription = () => {

    }

    render() {
        return (

            <SafeAreaView style={styles.fullScreen}>
                <HeaderWithTitle
                    isBackIconVisible={false}
                    openDrawer={() => this.openDrawer()}
                    styleHeaderContainer={{ backgroundColor: res.colors.appColor }}
                    headerTitle={res.strings.SUBSCRIPTION}
                />
                <View style={styles.contentView}>
                    <View style={styles.innerWhiteView}>

                        <KeyboardAwareScrollView style={{ flex: 1 }}>
                            <View style={styles.headingView}>
                                <Text style={styles.headingText}>After subscription for Rs. 299/month you will have following benifits -</Text>
                            </View>
                            <View style={styles.cardView}>
                            <View style={styles.unlockImgView}>
                            <Image source={res.images.unlock_ic} resizeMode={'stretch'} />
                        </View>
                            <View style={styles.subscriptionDescView}>
                                <View style={{flex:1, marginHorizontal:5}}>
                                <Text style={styles.subscriptionDescText}>You will have access to view construction progress</Text>

                                </View>
                                <View style={{justifyContent:'flex-end', marginRight:5}}>
                                <Image source={res.images.checkBox} resizeMode={'contain'} />

                                </View>
                            </View>
                            <View style={styles.subscribeBtnView}>
                                <PGButton
                                    btnText={"Get it for RS. 399/ month"}
                                    onPress={this.purchaseSubscription}
                                    btnStyle={styles.purchaseBtnView}
                               textStyleOver={styles.getPurchaseText}

                                />
                            </View>
                            </View>
                       

                        </KeyboardAwareScrollView>
                    </View>
                </View>



            </SafeAreaView>
        );
    }
}

export default Subscription;

