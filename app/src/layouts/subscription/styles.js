import { StyleSheet,StatusBar } from 'react-native';
import res from '../../../res'
import { heightScale, widthScale } from '../../utility/Utils';
const styles = StyleSheet.create({
    fullScreen: {
        flex: 1,
        backgroundColor: res.colors.appColor,
        paddingTop: Platform.OS === "android" ? StatusBar.currentHeight + 10 : 0
    },
    contentView:{
        flex:1,
        marginTop: heightScale(60),
        justifyContent: 'center',
        backgroundColor: res.colors.butterscotch,
        marginHorizontal: widthScale(20),
        alignItems: 'center',
        borderRadius: widthScale(8)
    },
    headingView: {
        justifyContent: 'center',
        marginHorizontal: widthScale(10),
        alignItems: 'center',
        marginTop: heightScale(10),
    },
    headingText: {
        fontSize: widthScale(16),
        fontFamily: res.fonts.semiBold,
        color: res.colors.greyishBrown,
        textAlign: 'center'
    },
    subscriptionDescView:{
        marginTop:heightScale(20),
        marginHorizontal: widthScale(10),
        justifyContent:'center',
        flexDirection:'row',
        flex:1

    },
    subscriptionDescText:{
        fontSize: widthScale(16),
        fontFamily: res.fonts.regular,
        color: res.colors.greyishBrown,
         textAlign:'center'
    },
    subscribeBtnView:{
        marginTop:heightScale(40),
         marginHorizontal:widthScale(30),
        marginBottom:5
    },
    innerWhiteView:{
        margin:widthScale(8),
        backgroundColor:res.colors.white,
        borderRadius:widthScale(10),
        flex:1

    },
    unlockImgView: {
        justifyContent: 'center',
        alignItems:'center',
        marginTop:heightScale(20),
    },
    cardView:{
        marginTop:heightScale(20),
        shadowOffset: {
            width: 1,
            height: 1,
        },
        shadowOpacity:0.8,
        shadowRadius: 10,
      borderWidth:0.5,
      borderColor:res.colors.shadowColor,
        elevation: 9,
        marginHorizontal:widthScale(5)
    },
    getPurchaseText:{
        fontFamily: res.fonts.semiBold,
        fontSize: widthScale(16),
        color: res.colors.white,
        fontWeight:'bold'
    },
    purchaseBtnView:{
        backgroundColor:res.colors.butterscotch,
        borderTopRightRadius:0,
        borderTopLeftRadius:0
    }

});

export default styles;
