import { StyleSheet, Platform, StatusBar } from 'react-native';
import resources from '../../../res';
const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        backgroundColor: resources.colors.white,
        paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0

    },
    container: {
        flex: 1,
        // marginTop: Platform.OS == 'ios' ? 10 : 20,

    }, inputCont: {
        borderWidth: 1,
        borderRadius: 4,
        borderColor: resources.colors.black,
        paddingHorizontal: 20,

    }, imageBack: {
        height: 216,
        width: '100%',
        top: -15
    }, inputFieldView: {

        // borderTopLeftRadius: 22,
        // borderTopRightRadius: 22,
        // position: 'absolute',
        // // top: -20,
        // left: 0,
        // right: 0,
        // bottom: 0,
        // backgroundColor: resources.colors.white
    }, forgotText: {
        fontFamily: resources.fonts.semiBold,
        color: resources.colors.slate,
        fontSize: 24,
        color: resources.colors.slate
    }, inputCointainer: {
        flex: 1,
        borderTopLeftRadius: 30,
        borderTopRightRadius: 30,
        // backgroundColor:'red'
    }, shadow: {
        shadowColor: resources.colors.shadowColor,
        shadowOffset: {
            width: 0,
            height: 0,
        },
        shadowOpacity: 20.39,
        shadowRadius: 10.30,

        elevation: 13,
        zIndex: 10
    }, shadow: {
        shadowColor: resources.colors.shadowColor,
        shadowOffset: {
            width: 0,
            height: 0,
        },
        shadowOpacity: 20.39,
        shadowRadius: 10.30,

        elevation: 13,
        zIndex: 10
    }, shadow: {
        shadowColor: resources.colors.shadowColor,
        shadowOffset: {
            width: 0,
            height: -10,
        },
        shadowOpacity: 20.39,
        shadowRadius: 10.30,

        elevation: 13,
        zIndex: 10
    }, imageContainer: {
        height: 216,
        width: '100%',
        top: 10,
        justifyContent: "center",
        alignItems: "center",
    }, roundedContaier: {
        borderTopRightRadius: 25,
        borderTopLeftRadius: 25,
        marginTop: -18,
        flex:1,
        // alignItems: 'center',
        backgroundColor: resources.colors.butterscotch,
    },

});

export default styles;
