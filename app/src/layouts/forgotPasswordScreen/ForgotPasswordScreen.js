import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import { View, Text, TouchableOpacity, ImageBackground, SafeAreaView } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import SimpleToast from 'react-native-simple-toast'

import { MyStatusBar } from '../../components/header/Header'
import styles from './styles';
import ApiFetch from '../../apiManager/ApiFetch'
import PGInputField from '../../components/input/TextInput'
import PGButton from '../../components/button/Button'
import { FORGOT_PASSWORD_ENDPOINTS } from '../../apiManager/ApiEndpoints'
import res from '../../../res'
import { renderInputError, validateEmail } from '../../utility/Utils'
import HeaderAndStatusBar from '../../components/header/Header'
import style from '../../components/button/styles';


class ForgotPasswordScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: "",
            error: {}
        }
    }
    componentDidMount() {

    }

    onEmailEnter = (text) => {
        this.setState({
            email: text
        })
    }
    back = () => {
        this.props.navigation.goBack()
    }
    validateBeforeVerify = () => {
        const { email } = this.state
        let errorObject = {};
        if (email.trim() == "") {
            errorObject.email = res.strings.emailCannotBeEmpty;
        } else if (email.trim().length > 100) {
            errorObject.email = res.strings.email100length;
        } else if (!validateEmail(email.trim())) {
            errorObject.email = res.strings.enterValidEmail;
        }
        this.setState({ error: errorObject });
        return Object.keys(errorObject).length == 0;
    }
    hitForgotPasswordApi = () => {
        const { email } = this.state
        const isValid = this.validateBeforeVerify();
        if (!isValid) { return; }
        const parameters = JSON.stringify({
            email: email
        })
        ApiFetch.fetchPost(FORGOT_PASSWORD_ENDPOINTS, parameters)
            .then((data) => {

                if (data.statusCode == 200) {
                    SimpleToast.show(data.message)
                    this.moveToEmailVerificationScreen()
                } else {
                    SimpleToast.show(data.message)
                }


            }).catch((err) => {
                console.log(err)
            })
    }

    moveToEmailVerificationScreen = () => {
        const { email } = this.state
        this.props.navigation.navigate("EmailVarificationScreen", {
            data: email
        })
    }
    renderView = () => {
        return <View style={{ marginHorizontal: 30, marginTop: 40 }}>
            <Text style={styles.forgotText}>Forgot Password</Text>
            <PGInputField
                label={"Email"}
                onChangeText={this.onEmailEnter}
                value={this.state.email}
                error={renderInputError("email", this.state.error)}
                inputStyles={{ marginTop: 30, }}
                inputProps={{
                    maxLength: 50,
                    returnKeyType: "next",
                    keyboardType: "email-address",
                    autoCapitalize: "none",
                    autoCorrect: false,
                    autoFocus: false,
                    numberOfLines: 1,
                }}

            />
            <View style={{ marginTop: 59 }}>
                <PGButton

                    btnText={"Submit"}
                    onPress={() => this.hitForgotPasswordApi()}
                />
            </View>
            {/* <View style={{ marginTop: 60 }}>
                        <PGButton

                            btnText={"SUBMIT"}
                            onPress={() => this.props.navigation.navigate("EmailVarificationScreen")}
                        />
                    </View> */}
            <Text style={style.forgotPasswprdInstructiom}>
                {'"' + res.strings.FORGOT_PASSWORD_INSTRUCTION + '"'}
            </Text>

        </View>


    }
    render() {
        return (
            <SafeAreaView style={[styles.mainContainer]}>
                <HeaderAndStatusBar
                    onBackClick={() => this.back()}
                />
                <View style={[styles.container]}>
                        <View style={styles.imageContainer}>
                            <ImageBackground
                                source={res.images.testImage}
                                style={styles.imageBack}
                            />
                        </View>
                        <View style={[styles.roundedContaier, styles.shadow]}>
                        <KeyboardAwareScrollView showsVerticalScrollIndicator={false}>

                            {this.renderView()}
                            </KeyboardAwareScrollView>

                        </View>
                </View>


            </SafeAreaView>

        );
    }
}
const mapStateToProps = (state) => {
    return {};
};

function mapDispatchToProps(dispatch) {
    return bindActionCreators({

    }, dispatch);
}
let ForgotPasswordScreenContainer = connect(mapStateToProps, mapDispatchToProps)(ForgotPasswordScreen);
export default ForgotPasswordScreenContainer;
