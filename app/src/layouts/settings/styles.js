import { StyleSheet ,Platform,StatusBar} from 'react-native';
import resources from '../../../res';
const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        backgroundColor: resources.colors.appColor,
        paddingTop: Platform.OS === "android" ? StatusBar.currentHeight + 10 : 0

    }, inputCointainer: {
        flex: 1,
        backgroundColor: resources.colors.white
        // backgroundColor:'red'
    },
    container: {
        flex: 1,
        // marginTop: Platform.OS == 'ios' ? 35 : 20,
    },
    content: {
        flexDirection: "column",
        justifyContent: "flex-start",
        alignItems: "center",
    }, inputCont: {
        // borderBottomWidth: 1,
        borderRadius: 4,
        borderColor: resources.colors.black,
        // paddingHorizontal: 20,

    }, errorUnderLine: {
        borderColor: "red",
    }, imageBack: {
        height: 200,
        width: '100%',
        top: -15
    }, inputFieldView: {

        // borderTopLeftRadius: 22,
        // borderTopRightRadius: 22,
        // position: 'absolute',
        // top: -20,
        // left: 0,
        // right: 0,
        // bottom: 0,
        backgroundColor: resources.colors.white
    }, signText: {
        fontFamily: resources.fonts.semiBold,
        fontSize: 32,
        color: resources.colors.slate
    }, passwordField: {
        marginTop: 35
    }, forgotPassText: {
        fontFamily:resources.fonts.regular,
        fontSize:12,
        color:resources.colors.btnBlue,
        marginVertical: 40,
        alignSelf:'center',
        textAlign:'center'
    },shadow: {
        shadowColor: resources.colors.shadowColor,
        shadowOffset: {
            width: 0,
            height: 0,
        },
        shadowOpacity: 20.39,
        shadowRadius: 10.30,

        elevation: 13,
        zIndex: 10
    }, imageContainer: {
        height: 216,
        width: '100%',
        // borderRadius: widthScale(50),
        backgroundColor: "white",
        justifyContent: "center",
        alignItems: "center",
    },
     roundedContaier: {
        borderTopRightRadius: 25,
        borderTopLeftRadius: 25,
        marginTop: -18,
        // alignItems: 'center',
        backgroundColor: resources.colors.white,
    },
});

export default styles;
