import React, { Component } from 'react';
import { View, Text, TouchableOpacity, ImageBackground, FlatList, BackHandler, SafeAreaView } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import AsyncStorage from '@react-native-community/async-storage'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { CommonActions } from '@react-navigation/native'

import styles from './styles';
import res from '../../../res';
import PGButton from '../../components/button/Button'
import { DEVELOPER_LIST_ENDPOINTS, SEND_LINK_FOR_BUILDER_ENDPOINTS, CONSTRUCTION_EMAIL_ENDPOINTS } from '../../apiManager/ApiEndpoints';
import ApiFetch from '../../apiManager/ApiFetch';
import AppUser from '../../utility/AppUser'
import DashBoardHeader from '../../components/header/DashBoardHeader'


class DeveloperListScreen extends Component {
    constructor(props) {
        super(props);
        this.navigation = this.props.route.params && this.props.route.params.navigation ? this.props.route.params.navigation : null
        this.state = {
            developerList: [],
            projectName: "Aparna Sarovar Zenith",
            email: "ritesh.pratap@algoworks.com",

        }
    }
    componentDidMount() {
        // this.developerListApi()
    }
    developerListApi = () => {
        ApiFetch.fetchGet(DEVELOPER_LIST_ENDPOINTS + "?" + "city" + "=" + "Hyderabad")
            .then((data) => {
                console.log(data.data)
                this.setState({
                    developerList: data.data
                })
            }).catch((err) => {
                console.log(err)
            })
    }
    sendLinkForBuilder = () => {
        const { email, projectName } = this.state
        const parameter = JSON.stringify({
            email: email,
            projectName: projectName
        })
        ApiFetch.fetchPost(SEND_LINK_FOR_BUILDER_ENDPOINTS, parameter)
            .then((res) => {
                alert(res.message)
            }).catch((err) => {
                console.log(err)
            })
    }
    sendConstructionEmail = () => {
        const { email, projectName } = this.state
        const parameter = JSON.stringify({
            constructionProgressEmail: email,
            projectName: projectName
        })
        ApiFetch.fetchPost(CONSTRUCTION_EMAIL_ENDPOINTS, parameter)
            .then((res) => {
                alert(res.message)
            }).catch((err) => {
                console.log(err)
            })
    }
    MoveToProfile = () => {
        this.props.navigation.navigate("ProfileScreen")
    }
    renderDeveloperList = ({ item, index }) => {
        return <View>
            <Text>
                {item}
            </Text>
        </View>
    }
    reraList = () => {
        this.props.navigation.navigate("GetAllReraScreen")
    }
    back = () => {
        this.props.navigation.goBack()
    }
    knowYourProperty = () => {
        this.props.navigation.navigate("KnowYourProperty", { navigation: this.navigation })

    }
    moveToDashBoard = () => {
        this.props.navigation.navigate("DashBoard")
    }
    onPressChangeProperty = () => {
        this.props.navigation.navigate("PropertyDetailsScreen")
    }
    onPressLogout = async () => {
        try {
            const keys = [res.AsyncStorageContants.TOKEN,
            res.AsyncStorageContants.USERID,
            res.AsyncStorageContants.DATE_OF_BIRTH,
            res.AsyncStorageContants.AADHAR,
            res.AsyncStorageContants.ADDRESS,
            res.AsyncStorageContants.DEVELOPERID,
            res.AsyncStorageContants.EMAILID,
            res.AsyncStorageContants.FLOORID,
            res.AsyncStorageContants.GSTIN,
            res.AsyncStorageContants.PANNO,
            res.AsyncStorageContants.PROJECTD,
            res.AsyncStorageContants.TOWERID,
            res.AsyncStorageContants.UNITID

            ];

            try {
                await AsyncStorage.multiRemove(keys)
            } catch (e) {
                console.log("Error removing data from async storage.", e);
            }
            // let appUsrObj = AppUser.getInstance();
            // appUsrObj.token = "";
            // appUsrObj.userId = "";


            const resetAction = CommonActions.reset({
                index: 0,
                routes: [{ name: 'LoginScreen' }],
            });
            this.props.navigation.dispatch(resetAction);


        } catch (error) {
            console.log("Error while logging out", error)
        }
    }
    renderView = () => {
        const { developerList } = this.state
        return <View style={[styles.inputFieldView]}>
            <View style={{ marginHorizontal: 30, marginTop: 40 }}>
                <View >
                    <PGButton
                        btnText={'log out'}
                        onPress={() => this.onPressLogout()}
                    />
                </View>
                <View >
                    <PGButton
                        btnText={'Change Your Property'}
                        onPress={() => this.onPressChangeProperty()}
                    />
                </View>
                {/* <View >
                    <PGButton
                        btnText={'knowYourProperty'}
                        onPress={() => this.knowYourProperty()}
                    />
                </View>
                <View >
                    <PGButton
                        btnText={'DashBoard'}
                        onPress={() => this.moveToDashBoard()}
                    />
                </View> */}
            </View>


            {/* <View style={{ marginHorizontal: 30, marginTop: 40 }}>
                <Text style={styles.signText}>Developer LIST </Text>

                <FlatList
                    data={developerList}
                    renderItem={this.renderDeveloperList}
                />
                
                <View >
                    <PGButton
                        btnText={'send Link For Builder'}
                        onPress={() => this.sendLinkForBuilder()}
                    />
                </View>
                <View >
                    <PGButton
                        btnText={'Send Construction progress email'}
                        onPress={() => this.sendConstructionEmail()}
                    />
                </View>
                <View >
                    <PGButton
                        btnText={'Profile'}
                        onPress={() => this.MoveToProfile()}
                    />
                </View>
                <View >
                    <PGButton
                        btnText={'Rera List'}
                        onPress={() => this.reraList()}
                    />
                </View>


            </View> */}

        </View>




    }
    openDrawer = () => {
        const resetAction = CommonActions.reset({
            index: 0,
            routes: [{ name: 'SettingsScreen' }],
        });
        this.props.navigation.dispatch(resetAction);
        this.props.navigation.openDrawer()

    }
    render() {
        
        return (
            <SafeAreaView style={[styles.mainContainer]}>
                <DashBoardHeader
                    openDrawer={() => this.openDrawer()}
                    headerTitle={res.strings.Setting}
                    settingVisible={false}
                    notificationVisible={false}
                />
               

                <View style={[styles.inputCointainer]}>

                    {this.renderView()}


                </View>

            </SafeAreaView>

        );
    }
}
const mapStateToProps = (state) => {
    return {};
};

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
    }, dispatch);
}
let DeveloperListScreenContainer = connect(mapStateToProps, mapDispatchToProps)(DeveloperListScreen);
export default DeveloperListScreenContainer;

