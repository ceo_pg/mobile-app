import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import AsyncStorage from '@react-native-community/async-storage'
import { CommonActions } from '@react-navigation/native'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

import { View, Text, ImageBackground, FlatList, Image, TouchableOpacity, SafeAreaView } from 'react-native';
import AppUser from '../../utility/AppUser'
import styles from './styles';
import res from '../../../res';
import HeaderAndStatusBar from '../../components/header/Header'
class HomeScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {

            data: [{
                key: 0,
                image: res.images.icnHomeAddress,
                instruction: res.strings.HOME_ADDRESS_INSTRUCTION,
                checked: true
            }, {
                key: 1,
                image: res.images.icnSearchHome,
                instruction: res.strings.HOME_LOCATION_INSTRUCTION,
                checked: false
            }]
        }
    }
    componentDidMount() {

    }
    onPressCityList = () => {
        this.props.navigation.navigate("CityListScreen")
    }
    selectBox = (index) => {
        const { data } = this.state
        let propertyArr = data.filter(item => {
            item.key == index ? item.checked = true : item.checked = false
            return item
        })
        this.setState({
            data: propertyArr,
        })
        if (index == 0) {
            this.props.navigation.navigate("PropertyDetailsScreen", { navigation: this.props })
        } else {
            this.props.navigation.navigate("DeveloperListScreen", { navigation: this.props })
        }
    }
    renderItem = ({ item, index }) => {
        console.log("ITEM", item)
        return <TouchableOpacity style={[styles.boxStyle, styles.shadowBox]} onPress={() => this.selectBox(index)}>
            {
                item.checked ? <Image style={{ width: 30, height: 30, alignSelf: 'flex-end', marginRight: 5 }} source={res.images.checkBox} /> : <View style={{ width: 30, height: 30, alignItems: 'flex-start', justifyContent: 'flex-start' }} />
            }
            <Image style={{ width: 95, height: 95 }} source={item.image} />
            <Text style={styles.instructionText}>{item.instruction}</Text>
        </TouchableOpacity>
    }

    renderView = () => {
        const { data } = this.state
        return <View style={[{ marginVertical: 10 }, styles.shadowBox]}>
            <FlatList
                data={data}
                renderItem={this.renderItem}
                keyExtractor={(item, index) => index.toString()}
                showsVerticalScrollIndicator={false}
            />
        </View>


    }
    openDrawer = () => {
        // this.props.navigation.toggleDrawer()
    }
    render() {
        console.log("HOME PROPS", this.props)
        return (

            <SafeAreaView style={[styles.mainContainer]}>
                {/* <HeaderAndStatusBar


                    onBackClick={() => this.openDrawer()}
                /> */}
                <View style={[styles.container]}>
                    <KeyboardAwareScrollView showsVerticalScrollIndicator={false}>
                        <View style={styles.imageContainer}>
                            <ImageBackground
                                source={res.images.testImage}
                                style={styles.imageBack}
                            />
                        </View>
                        <View style={[styles.roundedContaier, styles.shadow]}>
                            {this.renderView()}
                        </View>
                    </KeyboardAwareScrollView>
                </View>


            </SafeAreaView>
        );
    }
}
const mapStateToProps = (state) => {
    return {};
};


let HomeScreenContainer = connect(mapStateToProps, undefined, null, { forwardRef: true })(HomeScreen);
export default HomeScreenContainer;
