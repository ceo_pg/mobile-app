import { StyleSheet, Platform, StatusBar } from 'react-native';
import resources from '../../../res';
const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        backgroundColor: resources.colors.white,
        paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0
    },
    container: {
        flex: 1,
        justifyContent: 'center',
        // marginTop: Platform.OS == 'ios' ? 0 : 20,
        // marginHorizontal: 30
        // alignItems: 'center'
    }, inputCont: {
        borderWidth: 1,
        borderRadius: 4,
        borderColor: resources.colors.black,
        paddingHorizontal: 20,

    }, imageBack: {
        height: 216,
        width: '100%',
        top: -15
    }, inputFieldView: {

        borderTopLeftRadius: 22,
        borderTopRightRadius: 22,
        position: 'absolute',
        top: -20,
        left: 0,
        right: 0,
        bottom: 0,
        backgroundColor: resources.colors.white
    }, inputCointainer: {
        flex: 1,
        borderTopLeftRadius: 30,
        borderTopRightRadius: 30,
    }, inputFieldView: {

        borderTopLeftRadius: 22,
        borderTopRightRadius: 22,
        position: 'absolute',
        top: -20,
        left: 0,
        right: 0,
        bottom: 0,
        backgroundColor: resources.colors.white
    }, boxStyle: {
        marginHorizontal: 22,
        marginVertical: 20,
        height: 218,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: resources.colors.paleGrey,
        borderRadius: 20
    }, shadowBox: {
        shadowColor: resources.colors.shadowColor,
        shadowOffset: {
            width: 0,
            height: 0,
        },
        shadowOpacity: 10.39,
        shadowRadius: 5.30,

        elevation: 2,
        zIndex: 13
    }, instructionText: {
        fontFamily: resources.fonts.regular,
        fontSize: 18,
        color: resources.colors.peacockBlue,
        textAlign: 'center',
        marginVertical: 12
    }, shadow: {
        shadowColor: resources.colors.shadowColor,
        shadowOffset: {
            width: 0,
            height: -10,
        },
        shadowOpacity: 20.39,
        shadowRadius: 10.30,

        elevation: 13,
        zIndex: 10
    }, imageContainer: {
        height: 216,
        width: '100%',
        top: 10,
        backgroundColor: "white",
        justifyContent: "center",
        alignItems: "center",
    }, roundedContaier: {
        borderTopRightRadius: 25,
        borderTopLeftRadius: 25,
        marginTop: -18,
        // alignItems: 'center',
        backgroundColor: resources.colors.white,
    },

});

export default styles;
