import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import { View, Text, TouchableOpacity, ImageBackground, SafeAreaView } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import SimpleToast from "react-native-simple-toast";

import styles from './styles';
import ApiFetch from '../../apiManager/ApiFetch'
import PGInputField from '../../components/input/TextInput'
import PGButton from '../../components/button/Button'
import { RESET_PASSWORD_ENDPOINTS } from '../../apiManager/ApiEndpoints'
import res from '../../../res'
import AppUser from '../../utility/AppUser'
import MyStatusBar from '../../components/header/Header'
import { renderInputError, } from '../../utility/Utils'
import HeaderAndStatusBar from '../../components/header/Header';

class ResetPasswordScreen extends Component {
    constructor(props) {
        super(props);
        this.oldPasswordInputRef = React.createRef();
        this.newPasswordInputRef = React.createRef();
        this.confirmPasswordInputRef = React.createRef();
        this.state = {
            oldPassword: "",
            newPassword: "",
            confirmPassword: "",
            error: {}
        }
    }
    componentDidMount() {

    }
    back = () => {
        this.props.navigation.goBack()
    }
    focusNewPasswordInput = () => {
        this.newPasswordInputRef.current.focus();
    };
    focusConfirmPasswordInput = () => {
        this.confirmPasswordInputRef.current.focus();
    };
    onOldPasswordEnter = (text) => {
        this.setState({
            oldPassword: text
        })
    }
    onNewPasswordEnter = (text) => {
        this.setState({
            newPassword: text
        })
    }
    onConfirmPasswordEnter = (text) => {
        this.setState({
            confirmPassword: text
        })
    }
    moveToChangePassword = () => {
        this.props.navigation.navigate("ChangePasswordScreen")
    }
    moveToLogin = () => {
        this.props.navigation.navigate("LoginScreen")
    }
    validateBeforeReset = () => {
        const { newPassword, confirmPassword } = this.state
        let errorObject = {};

        if (newPassword.trim() == "") {
            errorObject.NewPass = res.strings.passwordCannotBeEmpty;
        } else if (newPassword.length < 6) {
            errorObject.NewPass = res.strings.password6length;
        }
        else if (newPassword.length > 16) {
            errorObject.NewPass = res.strings.password16length;
        } if (confirmPassword.trim() == "") {
            errorObject.OldPass = res.strings.confirmPasswordCannotBeEmpty;
        } else if (confirmPassword.length < 6) {
            errorObject.OldPass = res.strings.password6length;
        }
        else if (confirmPassword.length > 16) {
            errorObject.OldPass = res.strings.password16length;
        } else if (newPassword != confirmPassword) {
            alert("Password and Confirm password must be same")
        }
        this.setState({ error: errorObject });
        return Object.keys(errorObject).length == 0;
    }
    hitResetPasswordApi = () => {
        const { newPassword, confirmPassword } = this.state
        const isValid = this.validateBeforeReset();
        if (!isValid) { return; }
        let appUsrObj = AppUser.getInstance();
        let TOKEN = appUsrObj.token;

        const parameters = JSON.stringify({
            password: newPassword,
            confirmPassword: confirmPassword
        })
        ApiFetch.fetchPut(RESET_PASSWORD_ENDPOINTS + "/" + TOKEN, parameters)
            .then((data) => {
                if (data.statusCode == 200) {
                    SimpleToast.show(data.message)
                    this.moveToLogin()
                } else {
                    alert("Something went wrong", data.message)
                }

                //  console.log(data)
            }).catch((err) => {
                alert("Something went wrong ")
                console.log(err)
            })
    }
    renderView = () => {
        const { oldPassword, newPassword, confirmPassword, error } = this.state
        return <View style={{ marginHorizontal: 27, marginTop: 39, }}>
            <Text style={styles.forgotText}>Create New Password</Text>

            <PGInputField
                label={"New Password"}
                onChangeText={this.onNewPasswordEnter}
                value={newPassword}
                inputStyles={{ marginTop: 39 }}
                error={renderInputError("NewPass", error)}
                inputProps={{
                    secureTextEntry: true,
                    ref: this.newPasswordInputRef,
                    maxLength: 50,
                    returnKeyType: "next",
                    keyboardType: "email-address",
                    onSubmitEditing: this.onConfirmPasswordEnter,
                    autoCapitalize: "none",
                    autoCorrect: false,
                    autoFocus: false,
                    numberOfLines: 1,
                }}

            />
            <PGInputField
                label={"Confirm Password"}
                onChangeText={this.onConfirmPasswordEnter}
                value={confirmPassword}
                inputStyles={{ marginTop: 0 }}
                error={renderInputError("OldPass", error)}
                refrence={this.confirmPasswordInputRef}
                inputProps={{
                    secureTextEntry: true,
                    ref: this.confirmPasswordInputRef,
                    maxLength: 50,
                    returnKeyType: "next",
                    keyboardType: "email-address",
                    autoCapitalize: "none",
                    autoCorrect: false,
                    autoFocus: false,
                    numberOfLines: 1,
                }}

            />
            <View style={{ marginTop: 48 }}>
                <PGButton

                    btnText={res.strings.SUBMIT}
                    onPress={() => this.hitResetPasswordApi()}
                />
            </View>

        </View>



    }
    render() {
        return (
            <SafeAreaView style={[styles.mainContainer]}>
                <HeaderAndStatusBar
                    isBackIconVisible={false}
                // onBackClick={()=>this.back()}
                />
                <View style={[styles.container]}>
                    <KeyboardAwareScrollView showsVerticalScrollIndicator={false}>
                        <View style={styles.imageContainer}>
                            <ImageBackground
                                source={res.images.testImage}
                                style={styles.imageBack}
                            />
                        </View>
                        <View style={[styles.roundedContaier, styles.shadow]}>
                            {this.renderView()}
                        </View>
                    </KeyboardAwareScrollView>
                </View>


            </SafeAreaView>

        );
    }
}
const mapStateToProps = (state) => {
    return {};
};

function mapDispatchToProps(dispatch) {
    return bindActionCreators({

    }, dispatch);
}
let ResetPasswordScreenContainer = connect(mapStateToProps, mapDispatchToProps)(ResetPasswordScreen);
export default ResetPasswordScreenContainer;
