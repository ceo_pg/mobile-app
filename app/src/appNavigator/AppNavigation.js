
import React, { Component, useState } from 'react'
import { StyleSheet, View, Text, Image, TouchableOpacity, ImageBackground } from 'react-native'
import {
    DrawerItem,
    createDrawerNavigator,
    DrawerContentScrollView,
} from '@react-navigation/drawer';
import AsyncStorage from '@react-native-community/async-storage'

import { CommonActions } from '@react-navigation/native';
import Animated from 'react-native-reanimated';
import { createStackNavigator } from '@react-navigation/stack';
import res from '../../res'
import SplashScreen from '../layouts/splashScreen/SplashScreen';
import LoginScreen from "../layouts/loginScreen/LoginScreen";
import SignUpScreen from '../layouts/signUpScreen/SignUpScreen';
import EmailVarificationScreen from '../layouts/emailVarificationScreen/EmailVarificationScreen'
import ForgotPasswordScreen from '../layouts/forgotPasswordScreen/ForgotPasswordScreen'
import ResetPasswordScreen from '../layouts/resetPassword/ResetPasswordScreen'
import HomeScreen from '../layouts/homeScreen/HomeScreen'
import ChangePasswordScreen from '../layouts/changePassword/ChangePasswordScreen'
import PropertyDetailsScreen from '../layouts/propertyDetails/PropertyDetailsScreen'
import SettingsScreen from '../layouts/settings/SettingsScreen'
import ReraByIdScreen from '../layouts/reraById/ReraByIdScreen'
import ProfileScreen from '../layouts/profile/ProfileScreen'
import KnowYourProperty from '../layouts/knowYourProperty/KnowYourProperty'
import DashBoard from '../layouts/dashBoard/DashBoard'
import InitialScreen from '../layouts/initialScreen/InitialScreen'
import WebViewDisplay from '../components/webView/WebViewDisplay'
import ConstructionProgress from '../layouts/ConstructionProgress/ConstructionProgress'
import CostSheetBaseScreen from '../layouts/commercial/costSheet/CostSheetBaseScreen'
import LegalDocBaseScreen from '../layouts/legalDocumentation/LegalDocBaseScreen'
import HomeLoan from '../layouts/homeLoan/HomeLoan'
import SelectBank from '../layouts/homeLoan/selectBank/SelectBank';
import ProcessingFee from '../layouts/homeLoan/processingFee/ProcessingFee';
import ApprovalStatus from '../layouts/homeLoan/approvalStatus/ApprovalStatus';
import HomeLoanAgreement from '../layouts/homeLoan/aggreement/HomeLoanAgreement';
import HomeLoanScreen from '../layouts/homeLoan/homeLoanDetails/HomeLoanScreen'
import resources from '../../res';
import SelectBankStep1 from '../layouts/homeLoan/selectBank/SelectBankStep1'

import { heightScale, widthScale } from '../utility/Utils';
import Subscription from '../layouts/subscription/Subscription'
import KeyClausesAgree from '../layouts/homeLoan/aggreement/KeyClausesAgree'
import LoanStatement from '../layouts/homeLoan/aggreement/LoanStatement'
import SupportBaseScreen from '../layouts/supportAndTickets/SupportBaseScreen'
import AppUser from '../utility/AppUser'
import { CommingSoonModal } from '../components/index'

const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();
let appObj = AppUser.getInstance()
console.log("DRAWERVALUE", appObj)
export const gotoLoginScreen = async (props) => {
    try {
        const keys = [res.AsyncStorageContants.TOKEN,
        res.AsyncStorageContants.USERID,
        res.AsyncStorageContants.DATE_OF_BIRTH,
        res.AsyncStorageContants.AADHAR,
        res.AsyncStorageContants.ADDRESS,
        res.AsyncStorageContants.DEVELOPERID,
        // res.AsyncStorageContants.EMAILID,
        res.AsyncStorageContants.FLOORID,
        res.AsyncStorageContants.GSTIN,
        res.AsyncStorageContants.PANNO,
        res.AsyncStorageContants.PROJECTD,
        res.AsyncStorageContants.TOWERID,
        res.AsyncStorageContants.UNITID

        ];

        try {
            await AsyncStorage.multiRemove(keys)
            props.navigation.closeDrawer()
            const resetAction = CommonActions.reset({
                index: 0,
                routes: [{ name: 'LoginScreen' }],
            });

            props.navigation.dispatch(resetAction);
        } catch (e) {
            console.log("Error removing data from async storage.", e);
        }
        // let appUsrObj = AppUser.getInstance();
        // appUsrObj.token = "";
        // appUsrObj.userId = "";


        const resetAction = CommonActions.reset({
            index: 0,
            routes: [{ name: 'LoginScreen' }],
        });
        this.props.navigation.dispatch(resetAction);


    } catch (error) {
        console.log("Error while logging out", error)
    }

}





const Screens = ({ navigation, style }) => {
    return (<Animated.View style={StyleSheet.flatten([styles.stack, style])}>
        <Stack.Navigator
            // initialRouteName="SplashScreen"
            screenOptions={{
                headerShown: false,
            }}>
            <Stack.Screen name={'SplashScreen'} component={SplashScreen} />
            {/* <Stack.Screen name={'InitialScreen'} component={InitialScreen} /> */}
            <Stack.Screen name={'LoginScreen'} component={LoginScreen} />
            <Stack.Screen name={'SignUpScreen'} component={SignUpScreen} />
            <Stack.Screen name={'ForgotPasswordScreen'} component={ForgotPasswordScreen} />
            <Stack.Screen name={'EmailVarificationScreen'} component={EmailVarificationScreen} />
            <Stack.Screen name={'ResetPasswordScreen'} component={ResetPasswordScreen} />
            <Stack.Screen name={'HomeScreen'} component={HomeScreen} />
            <Stack.Screen name={'ChangePasswordScreen'} component={ChangePasswordScreen} />
            <Stack.Screen name={'PropertyDetailsScreen'} component={PropertyDetailsScreen} />
            <Stack.Screen name={'SettingsScreen'} component={SettingsScreen} />
            <Stack.Screen name={'ReraByIdScreen'} component={ReraByIdScreen} />
            <Stack.Screen name={'ProfileScreen'} component={ProfileScreen} />
            <Stack.Screen name={'KnowYourProperty'} component={KnowYourProperty} />
            <Stack.Screen name={'DashBoard'} component={DashBoard} />
            <Stack.Screen name={'WebViewDisplay'} component={WebViewDisplay} />
            <Stack.Screen name={'CostSheetBaseScreen'} component={CostSheetBaseScreen} />
            <Stack.Screen name={'ConstructionProgress'} component={ConstructionProgress} />
            <Stack.Screen name={'HomeLoan'} component={HomeLoan} />
            <Stack.Screen name={'SelectBank'} component={SelectBank} />
            <Stack.Screen name={'LegalDocBaseScreen'} component={LegalDocBaseScreen} />
            <Stack.Screen name={'ProcessingFee'} component={ProcessingFee} />
            <Stack.Screen name={'ApprovalStatus'} component={ApprovalStatus} />
            <Stack.Screen name={'HomeLoanAgreement'} component={HomeLoanAgreement} />
            <Stack.Screen name={'HomeLoanScreen'} component={HomeLoanScreen} />
            <Stack.Screen name={'SelectBankStep1'} component={SelectBankStep1} />
            <Stack.Screen name={'Subscription'} component={Subscription} />
            <Stack.Screen name={'KeyClausesAgree'} component={KeyClausesAgree} />
            <Stack.Screen name={'LoanStatement'} component={LoanStatement} />
            <Stack.Screen name={'SupportBaseScreen'} component={SupportBaseScreen} />


        </Stack.Navigator>
    </Animated.View>


    );
};
const Modal = (props) => {
    alert(res.strings.Comming_Soon)
}
const DrawerContent = props => {

    return (
        <DrawerContentScrollView {...props} scrollEnabled={false} contentContainerStyle={{ flex: 1, alignItems: 'center' }}>
            <ImageBackground source={res.images.mainBackground} style={{ flex: 1 }}>

                <TouchableOpacity onPress={() => props.navigation.navigate('ProfileScreen')} style={{ marginLeft: 20, flexDirection: 'row', marginTop: heightScale(30) }}>


                    <View style={{ width: widthScale(39), height: heightScale(39), borderRadius: widthScale(19.5), justifyContent: 'center', alignItems: 'center' }}>
                        {appObj.profileurl ? <Image
                            source={{ uri: appObj.profileurl }}
                            resizeMode="cover"
                            style={styles.avatar}
                        /> :
                            <Image
                                source={resources.images.icn_Profile}
                                resizeMode="center"
                                style={styles.avatar}
                            />}

                    </View>



                    <View style={{ marginLeft: 10 }}>
                        <Text style={styles.nameText}>
                            {appObj.name}
                        </Text>
                        <TouchableOpacity onPress={() => gotoLoginScreen(props)} hitSlop={{ top: 5, right: 5, left: 5, bottom: 5 }}>
                            <Text style={styles.emailText}>
                                {res.strings.LOGOUT}
                            </Text>
                        </TouchableOpacity>

                    </View>

                </TouchableOpacity>
                <View style={{ marginTop: heightScale(10) }}>
                    <DrawerItem
                        label={resources.strings.DASHBOARD}
                        labelStyle={styles.drawerLabel}
                        style={styles.drawerItemStyle}
                        onPress={() => props.navigation.navigate('DashBoard')}
                        icon={() => <Image source={resources.images.icn_dashboard} style={styles.imageStyle} />}
                    />


                    <DrawerItem
                        label={resources.strings.KNOW_YOUR_PROPERTY}
                        labelStyle={styles.drawerLabel}
                        style={styles.drawerItemStyle}
                        onPress={() => props.navigation.navigate('KnowYourProperty')}
                        icon={() => <Image source={resources.images.icn_knowYourProperty} />}
                    />
                    <DrawerItem
                        label={resources.strings.CONSRUCTION_PROGRESS}

                        labelStyle={styles.drawerLabel}
                        style={styles.drawerItemStyle}
                        onPress={() => props.navigation.navigate('ConstructionProgress')}
                        icon={() => <Image source={resources.images.icn_Construction} />}
                    />
                    <DrawerItem
                        label="Legal & Documentation"
                        labelStyle={styles.drawerLabel}
                        style={styles.drawerItemStyle}
                        onPress={() => props.navigation.navigate('LegalDocBaseScreen')}
                        icon={() => <Image source={resources.images.icn_Legal} />}
                    />
                    <DrawerItem
                        label={resources.strings.HomeLoan}
                        labelStyle={styles.drawerLabel}
                        style={styles.drawerItemStyle}
                        onPress={() => props.navigation.navigate('HomeLoan')}
                        icon={() => <Image source={resources.images.icn_HomeLoan} />}
                    />
                    <DrawerItem
                        label={resources.strings.COMMERCIAL}
                        labelStyle={styles.drawerLabel}
                        style={styles.drawerItemStyle}
                        onPress={() => Modal()}
                        icon={() => <Image source={resources.images.icn_knowYourProperty} />
                        }
                    />

                    <DrawerItem
                        label={res.strings.Investment_Progress}
                        labelStyle={styles.drawerLabel}
                        style={styles.drawerItemStyle}
                        onPress={() => Modal()}
                        icon={() => <Image source={resources.images.icn_Investment} />}
                    />
                    <DrawerItem
                        label={res.strings.Handover}
                        labelStyle={styles.drawerLabel}
                        style={styles.drawerItemStyle}
                          onPress={() => Modal()}
                        icon={() => <Image source={resources.images.icn_Investment} />}
                    />
                    <DrawerItem
                        label={resources.strings.SUPPORT}
                        labelStyle={styles.drawerLabel}
                        style={styles.drawerItemStyle}
                        onPress={() => props.navigation.navigate('SupportBaseScreen')}
                        icon={() => <Image style={{ width: 25, height: 25, tintColor: res.colors.appColor,opacity:0.5 }} source={resources.images.support} />}
                    />

                </View>
            </ImageBackground>


        </DrawerContentScrollView>
    );
};

export default () => {
    const [progress, setProgress] = React.useState(new Animated.Value(0));
    const scale = Animated.interpolate(progress, {

        inputRange: [0, 1],
        outputRange: [1, 0.8],

    });
    const borderRadius = Animated.interpolate(progress, {
        inputRange: [0, 1],
        outputRange: [0, 16],
    });

    const animatedStyle = { borderRadius, transform: [{ scale }] };

    return (
        <Drawer.Navigator
            // hideStatusBar
            drawerType="slide"
            overlayColor="transparent"
            drawerStyle={styles.drawerStyles}
            contentContainerStyle={{ flex: 1 }}
            drawerContentOptions={{
                activeBackgroundColor: 'transparent',
                activeTintColor: 'white',
                inactiveTintColor: 'white',
            }}
            sceneContainerStyle={{ backgroundColor: resources.colors.white }}
            drawerContent={props => {
                setProgress(props.progress);
                return <DrawerContent {...props} />;
            }}>
            <Drawer.Screen name="Screens">
                {props => <Screens {...props} style={animatedStyle} />
                }
            </Drawer.Screen>
        </Drawer.Navigator>

    );
};

const styles = StyleSheet.create({
    stack: {
        flex: 1,
        shadowColor: '#FFF',
        shadowOffset: {
            width: 0,
            height: 8,
        },
        shadowOpacity: 0.44,
        shadowRadius: 10.32,
        elevation: 5,
        // borderRadius:30
        // overflow: 'scroll',
        // borderWidth: 1,
    },
    drawerStyles: {
        flex: 1,
        width: '65%',
        backgroundColor: resources.colors.white
    },
    drawerItem: {
        alignItems: 'flex-start',
        marginVertical: 0
    },
    drawerLabel: {
        color: resources.colors.black,
        fontFamily: resources.fonts.medium,
        fontSize: 14,
        letterSpacing: 0.5,
        lineHeight: 25,
        marginLeft: widthScale(-20)

    },
    avatar: {
        width: 39, height: 39, borderRadius: 19.5
    }, nameText: {
        color: resources.colors.greyishBrown,
        fontFamily: resources.fonts.semiBold,
        fontSize: 16,
        lineHeight: 25,

    }, emailText: {
        color: resources.colors.greyishBrown,
        fontFamily: resources.fonts.regular,
        fontSize: 12,
        lineHeight: 25,
    }, drawerItemStyle: {
        alignItems: 'flex-start', marginVertical: 0, marginLeft: 20
    }, imageStyle: {

    }
});
