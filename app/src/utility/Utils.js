'use strict';
import { Platform, Dimensions, StatusBar, Alert } from 'react-native';
import AppUser from '../utility/AppUser'
import RNFS from 'react-native-fs'
import res from '../../res'

export const myWidth = Dimensions.get('window').width;
export const myHeight = Dimensions.get('window').height;
// const myHeight = Dimensions.get('window').height;
export const isPlatformIOS = Platform.OS == 'ios';
const standardWidth = 375.0;
const isiPhoneX = isPlatformIOS && myHeight > 800;
const standardHeight = 667.0;
const statusBarHeight = (() => {
  if (Platform.OS === 'android') {
    return StatusBar.currentHeight || 20;
    // return 30;
  }
  if (isiPhoneX) {
    return 45;
  }
  return 20;
})();
var validateEmail = email => {
  let regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return regex.test(email);
};
var validateMobileNumber = phone => {
  let regex = /^[0-9]{10}$/;
  return regex.test(phone);
};
export function focusTo(ref) {
  if (ref && ref.current) {
    ref.current.focus();
  }
}
export function camelize(str) {
  console.log("camelize", str)
  return str.replace(/(?:^\w|[A-Z]|\b\w|\s+)/g, function (match, index) {
    if (+match === 0) return ""; // or if (/\s+/.test(match)) for white spaces
    return index === 0 ? match.toLowerCase() : match.toUpperCase();
  });
}
export function fieldHasError(key, error) {
  if (!error) { return null; }
  if (!error) { return false; }
  const errorKeys = Object.keys(error);
  if (errorKeys == 0) { return false; }
  return errorKeys.includes(key);
};

var renderInputError = (key, error) => {
  if (!error) { return null; }
  const errorKeys = Object.keys(error);
  if (errorKeys == 0) { return null; }
  if (!errorKeys.includes(key)) { return null; }
  return error[key]
};
const color = [res.colors.btnBlue, res.colors.greyishBrown, res.colors.dustyOrange, res.colors.bluePurple, res.colors.charcoalGrey, res.colors.cobalt, res.colors.toupe,]
export function getRandomColor(index) {
  return color[index]

}
export function getHeader() {
  let appUsrObj = AppUser.getInstance();
  console.log("getHeader", appUsrObj)
  let headers = {};
  let token = appUsrObj.token;
  console.log("TOKEN", token)
  if (token && token.length > 0) {
    headers = {
      "x-access-token": token,

      Accept: "application/json",
      "Content-Type": "application/json"
    };
  } else {
    headers = {

      Accept: "application/json",
      "Content-Type": "application/json"
    };
  }
  return headers;
}

export function widthScale(dimension) {
  return (dimension / standardWidth) * myWidth;
}

export function heightScale(dimension) {
  return (dimension / standardHeight) * myHeight;
}
function getExtension(url) {
  return url.split('/').pop();
}
const MAX_SIZE = 10 * 1024 * 1024; // 10 MB
export function checkIsImageOrPdfFileType(url) {
  var ext = getExtension(url);
  switch (ext) {
    case 'jpg':
    case 'jpeg':
    case 'png':
    case 'pdf':
      return true;
  }
  return false;

}
export function checkValidFileSize(size) {
  console.log(size, "file size")
  if (size <= MAX_SIZE) {
    return true;
  } return false
}
export function getParsedDate(date) {
  date = String(date).split(' ');
  var days = String(date[0]).split('-');
  var hours = String(date[1]).split(':');
  return [parseInt(days[0]), parseInt(days[1]) - 1, parseInt(days[2]), parseInt(hours[0]), parseInt(hours[1]), parseInt(hours[2])];
}

export function validatePasscode(passcode) {

  let appUsrObj = AppUser.getInstance();
  console.log("UTILSUSEROBJ", appUsrObj);
  let phoneNumber = appUsrObj.phone;
  console.log(phoneNumber, "phoneNumber");

  var lastFourDigit = phoneNumber.substr(phoneNumber.length - 4);
  return passcode == lastFourDigit
}

export function convertFileToBase64(path) {
  return new Promise(async (resolve, reject) => {
    try {
      const image = await RNFS.readFile(path, "base64");
      resolve(image);
    } catch (error) {
      console.log("Error occured inside readFile", error);
      reject(error);
    }
  });
}

export const numberFormat = (value) => {
  return (value)
}

export function getPastProject(pastArr) {
  let pastData = ""
  for (let index = 0; index < pastArr.length; index++) {

    pastData = pastData + " " + pastArr[index].name;

  }
  return pastData
}

export {
  validateEmail,
  renderInputError,
  statusBarHeight,
  validateMobileNumber,
  isiPhoneX,


};














