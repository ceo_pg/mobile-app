import { combineReducers } from 'redux';
import LoginReducer from '../reducers/LoginReducer'
import modalReducer from './ModalReducer'
import CostSheetReducer from './commercial/CostSheetReducer'
import ConstructionReducer from './Construction/ConstructionReducer'
import LegalDocReducer from './LegalDocReducer'
import SharePostReducer from './SharePostReducer'
import HomeLoanReducer from './HomeLoan/HomeLoanReducer'
import TicketsReducer from './supportTickets/TicketsReducer'
const appReducer = combineReducers({
    LoginReducer: LoginReducer,
    modalReducer: modalReducer,
    CostSheetReducer: CostSheetReducer,
    ConstructionReducer: ConstructionReducer,
    LegalDocReducer: LegalDocReducer,
    SharePostReducer: SharePostReducer,
    HomeLoanReducer: HomeLoanReducer,
    TicketsReducer:TicketsReducer
});

let rootReducer = (state, action) => {

    return appReducer(state, action)
}

export default rootReducer;     