
import {
    LEGAL_DOCUMENTS,
    LEGAL_DOCUMENTS_FORM_DATA
} from '../actions/Type'
const initialState = {
legalDocuments:{},
legalDocumentsFormData:{}
};

const LegalDocReducer = (state = initialState, action) => {
    console.log(action.payload, "LegalDocReducer");
    switch (action.type) {
        case LEGAL_DOCUMENTS:
            return {
                ...state,
                legalDocuments: action.payload
            }
            break;
            case LEGAL_DOCUMENTS_FORM_DATA:
                return {
                    ...state,
                    legalDocumentsFormData: action.payload
                }
                break;
            default:
            return state
    }
}


export default LegalDocReducer
