
import {
    CHANGE_PAGE_INDEX
} from '../../actions/Type'
const initialState = {
pageIndex:0
};

const CostSheetReducer = (state = initialState, action) => {
    switch (action.type) {
        case CHANGE_PAGE_INDEX:
            return {
                ...state,
                pageIndex: action.payload
            }
            break;
            default:
            return state
    }
}


export default CostSheetReducer
