import {
    OPEN_SHARE_MODAL,

} from "../actions/Type";
import R from "../../../res";


const INITIAL_STATE = {
    showSharePostModal: false,

    sharePostModalVisible: false,
};

function SharePostReducer(state = INITIAL_STATE, action = {}) {
    switch (action.type) {
        case OPEN_SHARE_MODAL:
            {
                const { isVisibleModal } = action.payload;
                return {
                    ...state,
                    showSharePostModal: isVisibleModal,

                };
            }

        default:
            return state;
    }
}


export default SharePostReducer;