import {
    CHANGE_PAGE_INDEX, GALLERY_DATA, PROGRESS_DATA
} from '../../actions/Type'
const initialState = {
    pageIndex: 0,
    galleryData:[],
    progressData:{}
};

const ConstructionReducer = (state = initialState, action) => {
    // console.log("ConstructionReducer", action)
    switch (action.type) {
        case CHANGE_PAGE_INDEX:
            return {
                ...state,
                pageIndex: action.payload
            }
            break;
        case GALLERY_DATA:
            return {
                ...state,
                galleryData: action.payload
            }
            break;
        case PROGRESS_DATA:
            return {
                ...state,
                progressData: action.payload
            }
            break;
        default:
            return state
    }
}


export default ConstructionReducer