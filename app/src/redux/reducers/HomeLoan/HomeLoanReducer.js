import {
    HOME_LOAN_DETAILS
} from '../../actions/Type'
const initialState = {
    HomeLoan: {}
};

const HomeLoanReducer = (state = initialState, action) => {
     console.log("HomeLoanReducer", action)
    switch (action.type) {
        case HOME_LOAN_DETAILS:
            return {
                ...state,
                HomeLoan: action.payload
            }
            break;

        default:
            return state
    }
}


export default HomeLoanReducer