import { GET_LEGAL_DOCUMENT_ENDPOINTS,UPDATE_DCOUMENTES_ENDPOINTS } from '../../../apiManager/ApiEndpoints'
import ApiFetch from '../../../apiManager/ApiFetch'
import {LEGAL_DOCUMENTS, LEGAL_DOCUMENTS_FORM_DATA} from '../Type'


export const getLegalDocuments = () =>
    (dispatch) => {
    return  ApiFetch.fetchGet(GET_LEGAL_DOCUMENT_ENDPOINTS)
            .then((response) => {
                console.log(response, "fetchGet");
                return response
            }).catch((err) => {
                console.log("errror", err)
            })
    };

    export const updateDocumentsAPI = (params) =>
    (dispatch) => {
        return  ApiFetch.fetchPut(UPDATE_DCOUMENTES_ENDPOINTS,JSON.stringify(params))
                .then((response) => {
                    console.log(response, "updateDocumentsAPI");
                    return response
                }).catch((err) => {
                    console.log("errror", err)
                })
        };



    export const saveLegalDocuments = data => {
        console.log(data, "saveLegalDocuments")
        return {
            type: LEGAL_DOCUMENTS,
            payload: data
        }
    }

    export const saveLegalDocumentsFromData = data => {

        console.log(data, "LEGAL_DOCUMENTS_FORM_DATA")
        return {
            type: LEGAL_DOCUMENTS_FORM_DATA,
            payload: data
        }
    }
