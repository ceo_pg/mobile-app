import { SIGNUP_ENDPOINTS } from '../../apiManager/ApiEndpoints'
import ApiFetch from '../../apiManager/ApiFetch'

export const hitCustomerRegistrationApi = (userName, email, phone, password) =>
    (dispatch) => {
        
        const parameters = JSON.stringify({
            userName: userName,
            email: email,
            phone: phone,
            password: password
        })
        ApiFetch.fetchPost(SIGNUP_ENDPOINTS, parameters)
            .then((response) => {
                return response
            }).catch((err) => {
                console.log("errror", err)
            })
    };
