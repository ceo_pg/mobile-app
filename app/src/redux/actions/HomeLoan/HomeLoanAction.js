import { HOME_LOAN_DETAILS_ENDPOINTS, HOME_LOAN_FILE_UPLOADENDPOINTS,HOME_LOAN_CREATE_ENDPOINTS } from '../../../apiManager/ApiEndpoints'
import ApiFetch from '../../../apiManager/ApiFetch'
import { HOME_LOAN_DETAILS } from '../Type'

export const getHomeLoanDetails = (USER_ID) =>
    (dispatch) => {
        return ApiFetch.fetchGet(HOME_LOAN_DETAILS_ENDPOINTS + "/" + USER_ID)
            .then((response) => {
                console.log(response, "fetchGet");
                return response
            }).catch((err) => {
                console.log("errror", err)
            })
    };

export const saveHomeLoanDetails = data => {
    // console.log(data, "saveLegalDocuments",data)
    return {
        type: HOME_LOAN_DETAILS,
        payload: data
    }
}

export const updateDocumentsAPI = (params, USER_ID) =>

    (dispatch) => {
        console.log("updateDocumentsAPI", JSON.stringify(params), USER_ID)
        return ApiFetch.fetchPut(HOME_LOAN_FILE_UPLOADENDPOINTS + "/" + USER_ID, JSON.stringify(params))
            .then((response) => {
                console.log(response, "updateDocumentsAPI");
                return response
            }).catch((err) => {
                console.log("errror updateDocumentsAPI Action", err)
            })
    };

export const postDocumentsAPI = (params) =>

    (dispatch) => {
        console.log("postDocumentsAPI", JSON.stringify(params))
        return ApiFetch.fetchPost(HOME_LOAN_CREATE_ENDPOINTS, JSON.stringify(params))
            .then((response) => {
                console.log(response, "updateDocumentsAPI");
                return response
            }).catch((err) => {
                console.log("errror updateDocumentsAPI Action", err)
            })
    };