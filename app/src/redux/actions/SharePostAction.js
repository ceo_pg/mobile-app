import {
    OPEN_SHARE_MODAL
} from "./Type";



export function openShareModal(isVisibleModal) {
    return {
        type: OPEN_SHARE_MODAL,
        payload: {
            isVisibleModal,

        }
    }
}