import { GET_ALL_TICKET_ENDPOINTS, GET_TICKET_DETAIL_ENDPOINTS } from '../../../apiManager/ApiEndpoints'
import ApiFetch from '../../../apiManager/ApiFetch'
import {ALL_TICKETS, TICKETS_DETAIL} from '../Type'


export const getAllTickets = () =>
    (dispatch) => {
        return ApiFetch.fetchGet(GET_ALL_TICKET_ENDPOINTS)
            .then((response) => {
                console.log(response, "get all tickets");
                return response
            }).catch((err) => {
                console.log("errror", err)
            })
    };

    export const saveAllTickets = data => {
        console.log(data, "saveallTickets")
        return {
            type: ALL_TICKETS,
            payload: data
        }
    }

    export const getTicketDetails = (ticketId) =>
    (dispatch) => {
        let endPoints = GET_TICKET_DETAIL_ENDPOINTS + '/' + ticketId
        return ApiFetch.fetchGet(endPoints)
            .then((response) => {
                console.log(response, "get all tickets");
                return response
            }).catch((err) => {
                console.log("errror", err)
            })
    };