// Commercial
export const CHANGE_PAGE_INDEX = 'CHANGE_PAGE_INDEX';

// Legal Doc

export const LEGAL_DOCUMENTS = 'LEGAL_DOCUMENTS';
export const LEGAL_DOCUMENTS_FORM_DATA = 'LEGAL_DOCUMENTS_FORM_DATA';

//COnstructionProgress
export const GALLERY_DATA = "GALLERY_DATA";
export const PROGRESS_DATA = "PROGRESS_DATA";

//
export const OPEN_SHARE_MODAL = 'OPEN_SHARE_MODAL';
export const FILE_UPLOAD = 'FILE_UPLOAD'
//
export const HOME_LOAN_DETAILS = 'HOME_LOAN_DETAILS';

// Support tickets
export const ALL_TICKETS = 'ALL_TICKETS'
export const TICKETS_DETAIL = 'TICKETS_DETAIL'

