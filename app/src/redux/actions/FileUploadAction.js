import { FILE_UPLOAD_ENDPOINTS,BASE64_FILE_UPLOAD_ENDPOINTS } from '../../apiManager/ApiEndpoints'
import ApiFetch from '../../apiManager/ApiFetch'
import {getHeader, convertFileToBase64} from '../../utility/Utils'

// import {FILE_UPLOAD} from './Type'


export const fileUpload = (fileDetails, progressCallback) =>
    async(dispatch) => {
        // var data = new FormData();
        var dtata=new Date().getTime().toString();
        let fileName = ''
        console.log(fileDetails, "fileDetails")
        if(fileDetails.fileName){
            fileName =  fileDetails.fileName
        }else if(fileDetails.name){
            fileName =  fileDetails.name 
        }else{
            fileName = dtata + '.jpeg'
        }
        let baseFile = await convertFileToBase64(fileDetails.uri);

        var data = {
            name:fileName,
            base:  baseFile
        }  
        let headers = getHeader();
console.log("upload params","JSON.stringify(data)====", data);
    return  ApiFetch.futch(BASE64_FILE_UPLOAD_ENDPOINTS, {
        method: 'post',
        headers:headers,
        body: JSON.stringify(data)
    },progressCallback)
            .then((res) => {
                const response = JSON.parse(res.response)
                console.log(response, "upload");
                return response
            }).catch((err) => {
                console.log("errror", JSON.stringify(err))
            })
    };

