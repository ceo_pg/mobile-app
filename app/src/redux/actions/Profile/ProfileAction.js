import { USER_DETAILS_ENDPOINTS, UPDATE_USER_ENDPOINTS } from '../../../apiManager/ApiEndpoints'
import ApiFetch from '../../../apiManager/ApiFetch'


export const getProfile = () =>
    (dispatch) => {
        return ApiFetch.fetchGet(USER_DETAILS_ENDPOINTS)
            .then((response) => {
                console.log(response, "fetchGet");
                return response
            }).catch((err) => {
                console.log("errror", err)
            })
    };

    export const updateProfile = (params) =>
    (dispatch) => {
        return  ApiFetch.fetchPut(UPDATE_USER_ENDPOINTS,JSON.stringify(params))
        .then((response) => {
            console.log(response, "updateProfileAPI");
            return response
        }).catch((err) => {
            console.log("errror", err)
        })
};