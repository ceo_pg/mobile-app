import { CONSTRUCTION_PROGRESS_ENDPOINT } from '../../../apiManager/ApiEndpoints'
import ApiFetch from '../../../apiManager/ApiFetch'
import {GALLERY_DATA,PROGRESS_DATA} from '../Type'

export const getConstructionProgress = () =>
    (dispatch) => {
        return ApiFetch.fetchGet(CONSTRUCTION_PROGRESS_ENDPOINT)
            .then((response) => {
                console.log(response, "fetchGet");
                return response
            }).catch((err) => {
                console.log("errror", err)
            })
    };
export const saveGallery = data => {
    // console.log(data, "saveLegalDocuments",data)
    return {
        type: GALLERY_DATA,
        payload: data
    }
}
export const getProgress = data => {
    // console.log("getProgress",data)
    return {
        type: PROGRESS_DATA,
        payload: data
    }
}