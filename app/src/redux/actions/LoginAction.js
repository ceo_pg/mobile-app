import { LOGIN_API, LOGIN_ENDPOINT } from '../../apiManager/ApiEndpoints'
import ApiFetch from '../../apiManager/ApiFetch'

export const hitUserLoginApi = ( email, password) =>
    (dispatch) => {
        
        const parameters = JSON.stringify({
            email: email,
            password: password
        })
        console.log("PARA",parameters)
        ApiFetch.fetchPost(LOGIN_ENDPOINT, parameters)
            .then((response) => {
                return response
            }).catch((err) => {
                console.log("errror", err)
            })
    };
