import {CHANGE_PAGE_INDEX} from '../Type'

export const changePageIndex = index => {
    return {
        type: CHANGE_PAGE_INDEX,
        payload: index
    }
}