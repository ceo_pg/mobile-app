export const BASE_URL = "https://customer.prop.guide/api/v1.0";
// export const BASE_URL = "http://3.6.19.108:3001/api/v1.0";
// Dev environment node URL: http://3.6.102.115:3001
// Staging environment node URL: http://3.6.19.108:3001
// use these credentials,  er.amandeep740@gmail.com / Aman@3982

//USER
export const LOGIN_ENDPOINT = "/user/login"
export const FORGOT_PASSWORD_ENDPOINTS = "/user/forgotPassword"
export const RESET_PASSWORD_ENDPOINTS = "/user/reset-password"
export const SIGNUP_ENDPOINTS = "/user/customerRegister"
export const EMAIL_VERIFY_ENDPOINTS = "/user/verifyEmail"
export const INVALID_USER_ENDPOINTS = "/user/inviteUser"
export const ALL_ENDPOINTS = "/user/all"
export const RESEND_OTP_ENDPOINTS = "/user/resendOtp"
export const CHANGE_PASSSWORD_ENDPOINTS = "/user/changePassword"
export const VERIFY_OTP_FORGOT_PASS_ENDPOINTS = "/user/verifyOtpForgotPwd"
export const UPDATE_ENDPOINTS = "/user/update"
export const UPDATE_STATUS_ENDPOINTS = "/user/updateStatus"
export const USER_DETAILS_ENDPOINTS = "/user/logged-userDetails"
export const UPDATE_USER_ENDPOINTS = "/user/update"



//PROJECTSINFO
export const IMPORT_PROJECT_ENDPOINTS = "/project-info/import-projects"
export const SEND_LINK_FOR_BUILDER_ENDPOINTS = "/project-info/create"
export const UPDATE_PROJECT_ENDPOINTS = "/project-info/update"
export const GET_ALL_PROJINFO_ENDPOINTS = "/project-info/getAll"
export const LIST_DEVELOPER_ID_ENDPOINTS = "/project-info/by-developer"
export const DASHBOARD_LIST_ENDPOINTS = "/project-info/getDashboardList"
export const CONSTRUCTION_EMAIL_ENDPOINTS = "/project-info/sendConstructionProgressEmail"
export const PROJECT_BY_ID_ENDPOINTS = "/project-info/get"
export const UPDATE_MANY_ENDPOINTS = "/project-info/updateMany"
export const FILE_UPLOAD_ENDPOINTS = "/project-info/file-upload"
export const BASE64_FILE_UPLOAD_ENDPOINTS = "/project-info/file-uploadBase"

export const MULTI_FILE_UPLOAD_ENDPOINTS = "/project-info/multi-file-upload"
export const DASHBOARD_ENDPOINTS = "/project-info/dashboard"
export const CONSTRUCTION_PROGRESS = "/project-info/construction-progress"
//RERA DATA
export const IMPORT_RERA_ENDPOINTS = "/rera/import-reraData"
export const GET_ALL_RERA_ENDPOINTS = "/rera/all"
export const GET_BY_RERAID_ENDPOINTS = "/rera"
//DEVELOPER
export const IMPORT_DEVELOPER_ENDPOINTS = "/developer/import-developers"
export const DEVELOPER_LIST_ENDPOINTS = "/developer/all"
export const CITY_LIST_ENDPOINTS = "/developer/city-list"
export const LIST_BY_CITY_ENDPOINTS = "/developer/by-city"
export const DEVELOPER_BY_ID_ENDPOINTS = "/developer"
//UNIT DETAILS
export const UNIT_DETAIL_CREATE_ENDPOINTS = "/unit-details/create"
export const MY_UNIT_ENDPOINTS = "/unit-details/my-unit"
export const CAR_PARKING_ENDPOINTS = "/unit-details/car-parking"
export const CONSTRUCTION_PROGRESS_ENDPOINT = "/unit-details/construction-progress"
export const CUSTUMIZATION_ENDPOINTS = "/unit-details/update"
// LEGAL DOCUMENTS
export const GET_LEGAL_DOCUMENT_ENDPOINTS = "/unit-details/get-legal-documents"
export const UPDATE_DCOUMENTES_ENDPOINTS = "/unit-details/legal-documents"


//Home Loan
export const BANK_LIST = "/home-loan/bank-list"
export const HOME_LOAN_DETAILS_ENDPOINTS = "/home-loan"
export const HOME_LOAN_FILE_UPLOADENDPOINTS = "/home-loan/update"
export const HOME_LOAN_CREATE_ENDPOINTS = "/home-loan/create"

// Support ticket
export const CREATE_TICKET_ENDPOINTS = "/support-ticket/create-ticket"
export const GET_ALL_TICKET_ENDPOINTS = "/support-ticket/all"
export const GET_TICKET_DETAIL_ENDPOINTS = "/support-ticket/get-ticket"
export const UPDATE_TICKET_ENDPOINTS = "/support-ticket/update-ticket"

























// export default function ApiEndpoint(type) {
//     switch (type) {
//         case LOGIN_API:
//             return `${BASE_URL}${LOGIN_ENDPOINT}`;
//         case SIGNUP_API:
//             return `${BASE_URL}${SIGNUP_ENDPOINTS}`;
//         case EMAIL_VERIFY_API:
//             return `${BASE_URL}${EMAIL_VERIFY_ENDPOINTS}`;
//         case FORGOT_PASSWORD_API:
//             return `${BASE_URL}${FORGOT_PASSWORD_ENDPOINTS}`;
//         case RESET_PASSWORD_API:
//             return `${BASE_URL}${RESET_PASSWORD_ENDPOINTS}`;
//         case INVALID_USER_API:
//             return `${BASE_URL}${INVALID_USER_ENDPOINTS}`;
//         case ALL_API:
//             return `${BASE_URL}${ALL_ENDPOINTS}`;
//         case RESEND_OTP_API:
//             return `${BASE_URL}${RESEND_OTP_ENDPOINTS}`;
//         case CHANGE_PASSSWORD_API:
//             return `${BASE_URL}${CHANGE_PASSSWORD_ENDPOINTS}`;
//         case VERIFY_OTP_FORGOT_PASS_API:
//             return `${BASE_URL}${VERIFY_OTP_FORGOT_PASS_ENDPOINTS}`;
//         case VERIFY_OTP_FORGOT_PASS_API:
//             return `${BASE_URL}${VERIFY_OTP_FORGOT_PASS_ENDPOINTS}`;
//         default:
//             return "";
//     }
// }

// export const LOGIN_API = "LOGIN_API";
// export const SIGNUP_API = "SIGNUP_API"
// export const EMAIL_VERIFY_API = "EMAIL_VERIFY_API"
// export const FORGOT_PASSWORD_API = "FORGOT_PASSWORD_API"
// export const RESET_PASSWORD_API = "RESET_PASSWORD_API"
// export const INVALID_USER_API = "INVALID_USER_API"
// export const ALL_API = "ALL_API"
// export const RESEND_OTP_API = "RESEND_OTP_API"
// export const CHANGE_PASSSWORD_API = "RESEND_OTP_API"
// export const VERIFY_OTP_FORGOT_PASS_API = "VERIFY_OTP_FORGOT_PASS_API"
