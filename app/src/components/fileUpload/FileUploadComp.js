import React, { Component } from 'react';
import { View, Text, TouchableOpacity, ImageBackground, Image, StyleSheet } from 'react-native';
import res from '../../../res'
import { widthScale, heightScale, checkIsImageOrPdfFileType, checkValidFileSize } from '../../utility/Utils'
import strings from '../../../res/constants/strings'
import * as Progress from 'react-native-progress';
import { PickerViewModal, CAMERA, DOCUMENT, IMAGE_PICK } from '../Picker/PickerViewModal'
import ImagePicker from 'react-native-image-picker'
import DocumentPicker from 'react-native-document-picker'
import SimpleToast from 'react-native-simple-toast'


const options = {
    title: 'Select Image',
    mediaType: 'photo',
    allowsEditing: false,
    quality: 1.0,
    maxWidth: 500,
    maxHeight: 400,
    storageOptions: {
        skipBackup: true,
        path: 'images',
    },
}

class FileUploadComp extends Component {
    constructor(props) {
        super(props);
        this.state = {
            pickOptionsVisible: false
        }

    }
    onResultFromPicker = (response) => {
        console.log(response, "img res")
        if (!response) {
            SimpleToast.show("Please try again")
            return
        }
        console.log("uri => " + response.uri + "\n " +
            "type => " + response.type + "\n Size => " +
            response.size ? response.size : response.fileSize)
        this.setState({
            pickOptionsVisible: null
        })
        if (checkIsImageOrPdfFileType(response.type)) {
            if (checkValidFileSize(response.size ? response.size : response.fileSize)) {
                console.log("checkValidFileSize", response.size)
            } else {
                return SimpleToast.show("Size can not exceeds 10 MB")
            }
        } else {
            return SimpleToast.show("Please select pdf,jpg,jpeg,png format documents only")
        }
        if (this.props.fileSelectCallack) {
            this.props.fileSelectCallack(response, this.props.uniqueID);
        }

    }

    onResultFromImagePicker = (response) => {
        if (response.didCancel) {
            this.setState({
                pickOptionsVisible: null
            })
        } else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
            this.setState({
                pickOptionsVisible: null
            })
        } else {
            this.onResultFromPicker(response);
        }
    }
    openDocument = async () => {
        try {
            const res = await DocumentPicker.pick({
                type: [DocumentPicker.types.pdf, DocumentPicker.types.images],
            });
            console.log("Document", res)
            this.onResultFromPicker(res);
        } catch (err) {
            this.setState({
                pickOptionsVisible: null
            })
            if (DocumentPicker.isCancel(err)) {
                // User cancelled the picker, exit any dialogs or menus and move on
            } else {
                throw err;
            }
        }

    }

    onPressBackDrop = () => {
        this.setState({
            pickOptionsVisible: null
        })
    }
    onClickPickType = (type) => {
        this.setState({
            pickOptionsVisible: null
        })
        setTimeout(() => {
            switch (type) {
                case DOCUMENT:
                    // Open File picker
                    this.openDocument()
                    break;
                case IMAGE_PICK:
                    // Open Image Library:
                    ImagePicker.launchImageLibrary(options, this.onResultFromImagePicker);
                    break;
                case CAMERA:
                    // Launch Camera:
                    ImagePicker.launchCamera(options, this.onResultFromImagePicker);
                    break;
                default:
                    break;
            }
        }, 500)

    }
    
    showPickerOptions = () => {
        this.setState({
            pickOptionsVisible: 'bottom'
        })
    }
    deleteFile = (fileName,uniqueID) =>{
        const {onDeleteFile} = this.props;

    console.log(fileName,uniqueID, "deleteFile");
    if(onDeleteFile){
        onDeleteFile(fileName,uniqueID);
    }
    }
    render() {
        const { label, containerView, uploadIconView, uploadView, progress, uniqueID, fileName} = this.props;
        let showDeleteBtn = false
  if(progress && progress ==1){
    showDeleteBtn = true
  }
        return (
            <View style={[styles.containerView, containerView]}>
                <View style={[styles.uploadView, uploadView]}>
                    <View >
                        <Text style={styles.label}>{label ? label : ''}</Text>
                    </View>
                    <TouchableOpacity onPress={() => showDeleteBtn ? this.deleteFile(fileName,uniqueID) : this.showPickerOptions()} style={[styles.uploadIconView, uploadIconView]} hitSlop={{ top: 10, bottom: 10, left: 10, right: 10 }}>
                        <Image source={showDeleteBtn ? res.images.delete_ic : res.images.upload_ic} resizeMode={'stretch'} />
                    </TouchableOpacity>
                </View>
               {progress &&  
               <View style={[styles.uploadView, { borderWidth: 0.5, borderColor: 'rgba(59,74,116,0.19)', alignItems: 'center', justifyContent: 'flex-start' }]}>
               <View>
                   <Text style={styles.uploadingText}>{showDeleteBtn ? strings.UPLOADING_SUCCESSFULLY : strings.UPLOADING}</Text>
               </View>
               <View style={{ marginLeft: widthScale(6), flex: 1 }}>
                   {(progress < 1 && progress != 0) ?
                       <Progress.Bar progress={progress} style={{ borderRadius: 5 }} height={heightScale(6)} color={'rgb(0,255,43)'} unfilledColor={'rgba(0,255,43, 0.24)'} />
                       :
                       <Image source={res.images.uploaded_ic} resizeMode={'stretch'} />

                   }
               </View>
           </View>
               }
               
                {this.state.pickOptionsVisible ?
                    <PickerViewModal
                        onClickPickType={this.onClickPickType}
                        visibleModal={this.state.pickOptionsVisible}
                        onPressBackDrop={this.onPressBackDrop} /> : <View />}
            </View>
        )
    }
}

const styles = StyleSheet.create({

    containerView: {
        borderColor: res.colors.appColor,
        borderWidth: 1,
        borderRadius: widthScale(5),
        marginVertical: heightScale(8),
        flex: 1,
    },
    uploadView: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        padding: widthScale(12),
    },
    label: {
        fontFamily: res.fonts.semiBold,
        fontSize: widthScale(16),
        color: res.colors.formLabelGrey
    },
    uploadIconView: {
        flex: 1,
        alignItems: 'flex-end',
        justifyContent: 'center'
    },
    uploadingText: {
        fontFamily: res.fonts.regular,
        fontSize: widthScale(14),
        color: res.colors.lightGrey,
        letterSpacing: 0.67,
    }
})

export default FileUploadComp;

