import React, { Component } from 'react'
import { View, Image, TouchableOpacity, StyleSheet, Text } from "react-native";
import res from '../../../res'
import { widthScale, heightScale } from '../../utility/Utils';

class CheckBox extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isChecked: false,

        }
    }
    // markCheck = () => {
    //     const { isChecked } = this.state
    //     this.setState({
    //         isChecked: !isChecked
    //     })
    // }
    render() {
        const { isChecked } = this.state
        const { label,markCheck } = this.props
        return (
            <View style={styles.checkBoxView}>
                <TouchableOpacity onPress={() => markCheck(isChecked)}>
                    {
                        isChecked ?
                            <View style={styles.squreBox}>
                                <Image source={res.images.icn_BlueCheck} />
                            </View> :
                            <View style={styles.squreBox}>

                            </View>
                    }
                </TouchableOpacity>
                {

                    isChecked ?
                        <Text style={styles.checkLabel}>{label ? label : ""}</Text> :
                        <Text style={styles.unChecklabel}>{label ? label : ""}</Text>

                }
            </View >
        )
    }
}
const styles = StyleSheet.create({
    unChecklabel: {
        marginLeft: widthScale(10),
        fontFamily: res.fonts.semiBold,
        fontSize: heightScale(16),
        color: res.colors.peacockBlueLight
    },
    checkLabel: {
        marginLeft: widthScale(10),
        fontFamily: res.fonts.semiBold,
        fontSize: heightScale(16),
        color: res.colors.peacockBlue
    },
    checkBoxView: {
        flexDirection: 'row',
        marginVertical:heightScale(15),
        alignItems:'center'
    },
    squreBox: {
        width: widthScale(20),
        height: widthScale(20),
        borderRadius: 4,
        borderWidth: 2,
        borderColor: res.colors.peacockBlue,
        alignItems: 'center',
        justifyContent: 'center'
    }
})
export default CheckBox