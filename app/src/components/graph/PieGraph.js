import React from 'react'
import { View, Text, Image, SafeAreaView } from 'react-native'
import Pie from 'react-native-pie'

const PieGraph = (props) => {
    const { radius,percentage ,imageStyle,backgroundColor,filledColor } = props
    return (

        <Pie
            radius={radius}
            sections={[
                {
                    percentage: percentage,
                    color: filledColor,
                },


            ]}
            backgroundColor={backgroundColor}
            strokeCap={'butt'}
        />


    )
}
export default PieGraph