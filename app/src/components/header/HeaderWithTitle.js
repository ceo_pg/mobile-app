import React from 'react'
import PropTypes from "prop-types";
import { statusBarHeight, widthScale } from '../../utility/Utils'
import { View, Text, StyleSheet, Image, TouchableOpacity, StatusBar } from 'react-native'
import res from '../../../res'


const HeaderWithTitle = props => {
    const { customStyle, styleHeaderContainer, isDrawerIconVisible, hamStyle, isBackIconVisible, onPressRightBtn, isSupportVisibe,openSupport } = props;
    const renderBackIcon = () => {
        if (isBackIconVisible) {
            return (
                <TouchableOpacity onPress={props.onBackClick} style={[styles.backBtnCont,
                { width: '15%' }]}
                    hitSlop={{ top: 10, left: 20, right: 20, bottom: 10 }}>
                    <Image style={[styles.backIconStyle, { tintColor: 'white' }]} source={res.images.icn_back} resizeMode={'contain'}></Image>
                </TouchableOpacity>

            );
        }
    }
   
    const renderHambergerIcon = () => {

        return (
            <TouchableOpacity onPress={props.openDrawer}
                style={[styles.backBtnCont, { width: '15%', }]}
                hitSlop={{ top: 10, left: 20, right: 20, bottom: 10 }}>
                <Image style={styles.crossIconStyle, hamStyle} source={res.images.icn_Hamberger} resizeMode={'contain'}></Image>
            </TouchableOpacity>
        );

    }
    const renderSupportIcon = () => {
        return (
            <TouchableOpacity style={styles.rightView} onPress={props.openSupport}>
                <Image style={styles.settings} source={res.images.support} resizeMode={'contain'}></Image>
            </TouchableOpacity>
        )
    }
    return (

        <View style={[styles.headerContainer]}>
            {isBackIconVisible ? renderBackIcon() : renderHambergerIcon()}

            <View style={{ flexDirection: 'row', alignItems: 'center', }}>

                <Text numberOfLines={1} ellipsizeMode={'tail'}
                    style={[styles.textHeaderStyle, customStyle ? customStyle : {}]} >{props.headerTitle}</Text>
            </View>
            {
                isSupportVisibe ?
                    renderSupportIcon() :
                    onPressRightBtn &&
                    <TouchableOpacity style={styles.rightView} onPress={() => onPressRightBtn()}>
                        <Image source={res.images.plus_ic} resizeMode={'center'}></Image>

                    </TouchableOpacity>

            }


        </View>



    )
}

export const MyStatusBar = ({ barStyle, backgroundColor, ...props }) => (
    <View style={[styles.statusBar, { backgroundColor }]}>
        <StatusBar translucent backgroundColor={backgroundColor} {...props} barStyle={barStyle} />
    </View>
);


const styles = StyleSheet.create({

    headerContainer: {
        marginHorizontal: 20,
        backgroundColor: 'transparent',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',


    },
    backIconStyle: {
        justifyContent: 'flex-start',
    },
    textHeaderStyle: {

        // flex: 1,
        borderWidth: 0,
        color: res.colors.white,
        textAlign: 'center',
        fontSize: 18,
        fontFamily: res.fonts.semiBold
    },
    textRightOptionStyle: {
        borderWidth: 0,
        color: res.colors.white,
        textAlign: 'center',
        fontSize: 16
    },
    backBtnCont: {
        // alignSelf: "stretch",
        // justifyContent: "center",
        position: 'absolute',
        left: widthScale(8),
        top: 0
    },
    navigationCont: {
        alignSelf: "center",
        marginRight: 9
    },
    statusBar: {
        height: statusBarHeight,
    },
    resetStyle: {
        justifyContent: 'flex-start',
        color: "rgb(45,109,154)",
        fontSize: 16
    },
    resetCont: {
        alignSelf: "stretch",
        justifyContent: "center",
        alignItems: 'flex-end',
        width: '15%',
    },
    crossIconStyle: {
        justifyContent: 'flex-start',
        width: 17,
        height: 17,
        tintColor: res.colors.appColor
    },
    navigationIconStyle: {
        width: 14,
        height: 18
    },
    rightView: {
        position: 'absolute',
        right: widthScale(8),
    }, styleNotif: {
        alignSelf: 'flex-start',
       // justifyContent: "center",
       // marginLeft: 25
       marginRight: 20
   }, settings: {
    width: 30,
    height: 30
},
})

HeaderWithTitle.propTypes = {
    isBackIconVisible: PropTypes.bool
};

HeaderWithTitle.defaultProps = {
    isBackIconVisible: true
};


export default HeaderWithTitle