import React from 'react'
import PropTypes from "prop-types";
import { statusBarHeight, widthScale } from '../../utility/Utils'
import { View, Text, StyleSheet, Image, TouchableOpacity, StatusBar } from 'react-native'
import res from '../../../res'


const DashBoardHeader = props => {
    const { customStyle, settingVisible, notificationVisible, isShowSupport, isShowShadow } = props;


    const renderHambergerIcon = () => {

        return (
            <TouchableOpacity onPress={props.openDrawer}
                style={[styles.backBtnCont,]}
                hitSlop={{ top: 10, left: 20, right: 20, bottom: 10 }}>
                <Image style={styles.crossIconStyle} source={res.images.icn_Hamberger} resizeMode={'contain'}></Image>
            </TouchableOpacity>
        );

    }
    const renderSettingsIcon = () => {
        return (
            <TouchableOpacity onPress={props.openSetting}
                style={[styles.settingStyle, { width: '15%', }]}
                hitSlop={{ top: 10, left: 20, right: 20, bottom: 10 }}>
                <Image style={styles.settings} source={res.images.icnSetings} resizeMode={'contain'}></Image>
            </TouchableOpacity>
        )
    }
    const renderSupportIcon = () => {
        return (
            <TouchableOpacity onPress={props.openSupport}
                style={[styles.styleNotif]}
                hitSlop={{ top: 10, left: 20, right: 20, bottom: 10 }}>
                <Image style={styles.settings} source={res.images.support} resizeMode={'contain'}></Image>
            </TouchableOpacity>
        )
    }
    return (
        <View style={{ overflow: 'hidden', paddingBottom: 5 }}>
            <View
                style={isShowShadow ? {} : styles.shadow}
            >
                <View style={[styles.headerContainer]}>
                    <View style={{ width: "20%" }}>
                        {renderHambergerIcon()}
                    </View>


                    <View style={settingVisible ? styles.headerView : styles.settingHeaderView}>

                        <Text numberOfLines={1} ellipsizeMode={'tail'}
                            style={[styles.textHeaderStyle, customStyle ? customStyle : {}]} >{props.headerTitle ? props.headerTitle : null}</Text>
                    </View>

                    <View style={{ flexDirection: 'row', width: "20%", justifyContent: 'flex-end', alignItems: 'flex-end' }}>
                        {/* {renderSettingsIcon()}
                        {renderNotificationIcon()} */}
                        {settingVisible ? renderSettingsIcon() : <View />}
                        {isShowSupport ? <View /> : renderSupportIcon()}

                    </View>
                </View>
            </View>
        </View>




    )
}

export const MyStatusBar = ({ barStyle, backgroundColor, ...props }) => (
    <View style={[styles.statusBar, { backgroundColor }]}>
        <StatusBar translucent backgroundColor={backgroundColor} {...props} barStyle={barStyle} />
    </View>
);


const styles = StyleSheet.create({

    headerContainer: {

        // marginHorizontal: 20,
        // backgroundColor: 'red',
        alignItems: 'center',
        justifyContent: 'space-between',
        flexDirection: 'row',


    },
    backIconStyle: {
        justifyContent: 'flex-start',
    },
    textHeaderStyle: {
        borderWidth: 0,

        // marginLeft: 20,
        color: res.colors.white,
        textAlign: 'center',
        fontSize: 18,
        fontFamily: res.fonts.SsemiBold
    },
    textRightOptionStyle: {
        borderWidth: 0,
        color: res.colors.white,
        textAlign: 'center',
        fontSize: 16
    },
    backBtnCont: {
        marginLeft: widthScale(20)
        // alignSelf: "stretch",
        // justifyContent: 'flex-start',

    },
    statusBar: {
        height: statusBarHeight,
    },
    resetCont: {
        alignSelf: "stretch",
        justifyContent: "center",
        alignItems: 'flex-end',
        width: '15%',
    },
    navigationIconStyle: {
        width: 14,
        height: 18
    }, settings: {
        width: 30,
        height: 30
    }, styleNotif: {
        alignSelf: 'flex-start',
        // justifyContent: "center",
        // marginLeft: 25
        marginRight: 20
    }, settingStyle: {
        marginRight: 15
    }, shadow: {
        shadowColor: '#000',
        shadowOffset: { width: 1, height: 1 },
        shadowOpacity: 0.4,
        shadowRadius: 3,
        elevation: 5, shadowColor: '#000',

    }, headerView: {

        // width: "33.33%",
        justifyContent: 'center',
        alignItems: 'center',
        // backgroundColor:'red'
        // backgroundColor: 'red'
        // alignItems: 'center',
        // justifyContent: 'center',
        // marginRight: -70,
        // backgroundColor: 'red'
    }, settingHeaderView: {
        // backgroundColor:'red',
        width: "60%",
        justifyContent: 'center',
        alignItems: 'center',
        // backgroundColor:'red'
        // backgroundColor: 'red'
        // alignItems: 'center',
        // justifyContent: 'center',
        // marginRight: 50,
        //  backgroundColor: 'red'
    }, shadow: {
        backgroundColor: res.colors.appColor,

        height: 50,
        shadowColor: '#000',
        shadowOffset: { width: 1, height: 1 },
        shadowOpacity: 0.4,
        shadowRadius: 3,
        elevation: 5,
    }

})

DashBoardHeader.propTypes = {
    isBackIconVisible: PropTypes.bool
};

DashBoardHeader.defaultProps = {
    isBackIconVisible: true
};


export default DashBoardHeader