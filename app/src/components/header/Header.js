import React from 'react'
import PropTypes from "prop-types";
import { statusBarHeight } from '../../utility/Utils'
import { View, Text, StyleSheet, Image, TouchableOpacity, StatusBar } from 'react-native'
import res from '../../../res'


const HeaderAndStatusBar = props => {
    const { isBackIconVisible, customStyle, styleHeaderContainer, onResetPress, isDrawerIconVisible, isResetVisible, navigationVisible } = props;
    const renderBackIcon = () => {
        if (isBackIconVisible) {
            return (
                <TouchableOpacity onPress={props.onBackClick} style={[styles.backBtnCont,
                { width: '15%', }]}
                    hitSlop={{ top: 10, left: 20, right: 20, bottom: 10 }}>
                    <Image style={styles.backIconStyle} source={res.images.icn_back} resizeMode={'contain'}></Image>
                </TouchableOpacity>

            );
        }
    }

    const renderHambergerIcon = () => {
        if (isDrawerIconVisible) {
            return (
                <TouchableOpacity onPress={props.onDrawerClick}
                    style={[styles.backBtnCont, { width: '15%', }]}
                    hitSlop={{ top: 10, left: 20, right: 20, bottom: 10 }}>
                    <Image style={styles.crossIconStyle} source={res.images.icn_Hamberger} resizeMode={'contain'}></Image>
                </TouchableOpacity>
            );
        }
    }

    const renderLeftEmptyViewToBalanceFlex = () => {
        if (!isBackIconVisible && !isDrawerIconVisible) {
            return (
                <View style={{ width: '15%', height: 20, borderWidth: 0 }} />
            );
        }
    }
    return (
        <View style={isDrawerIconVisible ?styles.headerContainer:styles.headerStyle}>
            <View style={styleHeaderContainer ? styleHeaderContainer : {}}>
                <MyStatusBar
                    backgroundColor={res.colors.btnBlue}
                    barStyle="light-content" />
                <View style={[styles.headerContainer]}>
                    {isDrawerIconVisible ? renderHambergerIcon() : renderBackIcon()}
                    {/* {renderLeftEmptyViewToBalanceFlex()} */}
                    <View style={{ flexDirection: 'row', alignItems: 'center', }}>

                        <Text numberOfLines={1} ellipsizeMode={'tail'}
                            style={[styles.textHeaderStyle, customStyle ? customStyle : {}]} >{props.headerTitle}</Text>
                    </View>

                    <View></View>
                </View>
            </View >
        </View>


    )
}

export const MyStatusBar = ({ barStyle, backgroundColor, ...props }) => (
    <View style={[styles.statusBar, { backgroundColor }]}>
        <StatusBar translucent backgroundColor={backgroundColor} {...props} barStyle={barStyle} />
    </View>
);


const styles = StyleSheet.create({

    headerContainer: {
        height: 55,
         paddingHorizontal: 10,
         alignItems: 'center',
         justifyContent: 'space-between',
        flexDirection: 'row',
        // borderWidth: 0,
        // backgroundColor: res.colors.trasparents
    },
    backIconStyle: {
        justifyContent: 'flex-start',
    },
    textHeaderStyle: {

        // flex: 1,
        borderWidth: 0,
        color: res.colors.white,
        textAlign: 'center',
        fontSize: 18,
        fontFamily: res.fonts.SsemiBold
    },
    textRightOptionStyle: {
        borderWidth: 0,
        color: res.colors.white,
        textAlign: 'center',
        fontSize: 16
    },
    backBtnCont: {
        alignSelf: "stretch",
        justifyContent: "center",
    },
    navigationCont: {
        alignSelf: "center",
        marginRight: 9
    },
    statusBar: {
        height: statusBarHeight,
    },
    resetStyle: {
        justifyContent: 'flex-start',
        color: "rgb(45,109,154)",
        fontSize: 16
    },
    resetCont: {
        alignSelf: "stretch",
        justifyContent: "center",
        alignItems: 'flex-end',
        width: '15%',
    },
    crossIconStyle: {
        justifyContent: 'flex-start',
        width: 17,
        height: 17
    },
    navigationIconStyle: {
        width: 14,
        height: 18
    }, headerStyle: {
        position: 'absolute',
        backgroundColor: 'transparent',
        zIndex: 10,
        top: 0,
        left: 0,
        right: 0,
        elevation: 0,



    }
})

HeaderAndStatusBar.propTypes = {
    isBackIconVisible: PropTypes.bool
};

HeaderAndStatusBar.defaultProps = {
    isBackIconVisible: true
};


export default HeaderAndStatusBar