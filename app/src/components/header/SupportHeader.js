import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image, StyleSheet } from 'react-native';
import res from '../../../res'
import {heightScale, widthScale} from '../../utility/Utils'


class SupportHeader extends Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }
    renderBackIcon = () => {
        const {  onBack } = this.props
        return <TouchableOpacity style={styles.headerLeftView} onPress={() => onBack()}>
            <Image source={res.images.icn_back} resizeMode={'contain'} />
        </TouchableOpacity>
    }
    render() {
        const { title, headerTitle, headerView, isBackIconVisisble } = this.props
        return (
            <View>
                <View style={[styles.headerView, headerView]}>
                    <Text style={[styles.headerTitle, headerTitle]}>{title ? title : ''}</Text>
                </View>
                {isBackIconVisisble ? this.renderBackIcon() : <View />}
            </View>

        )
    }


}

const styles = StyleSheet.create({

    headerView: {
        backgroundColor: res.colors.white,
        height: heightScale(50),
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        shadowColor: "rgba(0,0,0, 0.1)",
        shadowOffset: {
          width: 0,
          height: 4,
        },
        shadowOpacity: 0.8,
        shadowRadius: 10,
        elevation: 10,
        borderTopLeftRadius:20,
        borderTopRightRadius:20
    
      },
      headerTitle: {
        fontWeight:'bold',
        fontFamily: res.fonts.semiBold,
        fontSize: widthScale(18),
        color: res.colors.greyishBrown
      },
      headerLeftView:{
        position:'absolute',
        left:widthScale(22),
        top:heightScale(18)
      },
})

export default SupportHeader;

