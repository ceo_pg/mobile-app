import Modal from './modal/Modal'
import Header from './header/Header'
import ModalButton from './button/ModalButton'
import PGButton from './button/Button'
import SearchModal from './modal/SearchModal'
import DeveloperModal from './modal/DeveloperModal'
import ProjectModal from './modal/ProjectModal'
import TowerModal from './modal/TowerModal'
import FloorModal from './modal/FloorModal'
import UnitModal from './modal/UnitModal'
import Loader from './Indicator/Loader'
import TabView from './tabView/TabView'
import CustomModal from './modal/CustomModal';
import VastuMeter from './vastuMeter/VastuMeter'
import UnitFloorModal from './modal/KnowYourProperty/Unit/UnitFloorModal'
import KnowYourUnitModal from './modal/KnowYourProperty/Unit/KnowYourUnitModal'
import UnitLevelSpecificationModal from './modal/KnowYourProperty/Unit/UnitLevelSpecificationModal'
import AmenitiesModal from './modal/KnowYourProperty/Project/AmenitiesModal'
import ProjectSpecificationModal from './modal/KnowYourProperty/Project/ProjectSpecificationModal'
import ConsultantsModal from './modal/KnowYourProperty/Project/ConsultantsModal'
import ParkingSlotModal from './modal/KnowYourProperty/Unit/ParkingSlotModal'
import HalfCircular from './vastuMeter/HalfCircular'
import BroucherModal from './modal/KnowYourProperty/Project/BroucherModal'
import IndexOfChargesModal from './modal/KnowYourProperty/Developer/IndexOfChargesModal';
import VastuModal from "./modal/KnowYourProperty/Unit/VastuModal";
import AmenitiesProgressModal from './modal/ConstructionProgress/AmenitiesProgressModal';
import InfraModal from './modal/ConstructionProgress/InfraModal';
import TowerProgressModal from './modal/ConstructionProgress/TowerProgressModal'
import UnitProgressModal from './modal/ConstructionProgress/UnitProgressModal';
import FinacialParamModal from './modal/HomeLoan/FinacialParamModal';
import OtherTermsModal from './modal/HomeLoan/OtherTermsModal';
import DocItemComponent from './card/DocItemComponent';
import HomeLoanDocModal from './modal/HomeLoan/HomeLoanDocModal'
import PieGraph from './graph/PieGraph'
import ComplianceModal from './modal/KnowYourProperty/Project/ComplianceModal'
import HomeLoanApprovedByModal from './modal/KnowYourProperty/Project/HomeLoanApprovedByModal'
import ProjectOwnerShipModal from './modal/KnowYourProperty/Project/ProjectOwnerShipModal'
import PercentageCircle from './vastuMeter/PercentageCircle'
import PartnersAndLocationsModal from './modal/KnowYourProperty/Developer/PartnersAndLocationsModal'
import AssociationMemberModal from './modal/KnowYourProperty/Developer/AssociationMemberModal'
import OnGoingProjectModal from './modal/KnowYourProperty/Developer/OnGoingProjectModal'
import DeveloperScoreModal from './modal/KnowYourProperty/Developer/DeveloperScoreModal'
import ConstructionProgressModal from './modal/DashBoard/ConstructionProgressModal'
import EmiScreenModal from './modal/DashBoard/EmiScreenModal'
import PriceTrendModal from './modal/DashBoard/PriceTrendModal'
import CommingSoonModal from './modal/CommingSoonModal'

export {
    PriceTrendModal,
    EmiScreenModal,
    Modal,
    Header,
    ModalButton,
    PGButton,
    SearchModal,
    DeveloperModal,
    ProjectModal,
    TowerModal,
    FloorModal,
    UnitModal,
    Loader,
    TabView,
    CustomModal,
    VastuMeter,
    UnitFloorModal,
    KnowYourUnitModal,
    UnitLevelSpecificationModal,
    AmenitiesModal,
    ProjectSpecificationModal,
    ConsultantsModal,
    ParkingSlotModal,
    HalfCircular,
    BroucherModal,
    IndexOfChargesModal,
    VastuModal,
    AmenitiesProgressModal,
    InfraModal,
    TowerProgressModal,
    UnitProgressModal,
    FinacialParamModal,
    OtherTermsModal,
    DocItemComponent,
    HomeLoanDocModal,
    PieGraph,
    ComplianceModal,
    HomeLoanApprovedByModal,
    ProjectOwnerShipModal,
    PercentageCircle,
    PartnersAndLocationsModal,
    AssociationMemberModal,
    OnGoingProjectModal,
    DeveloperScoreModal,
    ConstructionProgressModal,
    CommingSoonModal



}