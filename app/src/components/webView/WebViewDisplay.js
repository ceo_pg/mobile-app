
import React, { Component } from 'react'
import {
  SafeAreaView,
  StyleSheet,
  ActivityIndicator
} from 'react-native'
// import WebView from 'react-native-webview'

import HeaderWithTitle from '../header/HeaderWithTitle'
import resources from '../../../res';
class WebViewDisplay extends Component {
  constructor(props) {
    super(props)
    this.url = this.props.route.params && this.props.route.params.url;
    this.state = {

    }
  }

  render() {
    console.log("this.url ", this.url)
    return (
      <SafeAreaView style={{ flex: 1, }}>
        <HeaderWithTitle

          onBackClick={() => this.props.navigation.goBack()}
        />
        {/* <WebView
          source={{ uri: this.url }}
          style={{ marginTop: 20, flex: 1, }}
          startInLoadingState={true}
          renderLoading={() => (
            <ActivityIndicator
              color='black'
              size='large'
              style={{ flex: 1, backgroundColor: resources.colors.white, justifyContent: 'flex-start' }}
            />
          )}
        /> */}
      </SafeAreaView>



    );
  }
}

const styles = StyleSheet.create({
  flexContainer: {
    flex: 1
  },
  tabBarContainer: {
    padding: 20,
    flexDirection: 'row',
    justifyContent: 'space-around',
    backgroundColor: '#b43757'
  },
  button: {
    color: 'white',
    fontSize: 24
  }
})

export default WebViewDisplay
