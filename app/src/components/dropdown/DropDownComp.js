import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image, StyleSheet } from 'react-native';
import res from '../../../res'
import { widthScale, heightScale } from '../../utility/Utils'
// import { Dropdown } from 'react-native-material-dropdown-v2';
import { Dropdown } from 'react-native-material-dropdown';
import OutLinedInput from '../input/OutLinedInput'



class DropDownComp extends Component {
    constructor(props) {
        super(props);
        this.state = {
            userTypeValue: "",

        }
    }

    render() {
        const { label, options, placeholder, onChangeText, value, dropDownValue, userType } = this.props
        return (
            <View style={{ marginTop: 10 }}>
                <Dropdown
                    animationDuration={1}
                    rippleDuration={1}
                    onChangeText={(val, i, d) => {
                        this.setState({ selectUserType: d[i], userTypeValue: val })
                    }}
                    data={userType.map(item => ({
                        value: item.value, ...item
                    }))}
                    value={userType && userType.length > 0 ? userType : ""}
                    dropdownPosition={-3}
                    renderBase={(props) => (
                        <OutLinedInput
                            isDropDownImageVisible={true}
                            label={label}
                            placeholder={placeholder}
                            onChangeText={onChangeText}
                            isDropDownImageVisible={true}
                            value={this.state.userTypeValue}
                            inputProps={{
                                autoCaptialize: 'none',
                                returnKeyType: 'next',
                            }}
                        >
                        </OutLinedInput>
                    )}

                />
            </View>

        )
    }
}

const styles = StyleSheet.create({

    containerView: {
        borderColor: res.colors.appColor,
        borderWidth: 1,
        borderRadius: widthScale(5),
        marginVertical: heightScale(8),
        flex: 1,
    },
    uploadView: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        padding: widthScale(12),
    },
    label: {
        fontFamily: res.fonts.semiBold,
        fontSize: widthScale(16),
        color: res.colors.formLabelGrey
    },
    uploadIconView: {
        flex: 1,
        alignItems: 'flex-end',
        justifyContent: 'center'
    },
})

export default DropDownComp;

