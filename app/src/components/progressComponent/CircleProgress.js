import React, { Component } from 'react';
import { View, Text, TouchableOpacity, ImageBackground, SafeAreaView, Image } from 'react-native';
import res from '../../../res/images'
import {widthScale, heightScale} from  '../../utility/Utils'


class CircleProgress extends Component {
    constructor(props) {
        super(props);
       
    }
    componentDidMount() {

    }

    render(){
        const {progressImg} = this.props
        return(
            <View style={{flexDirection:'row', width:widthScale(140), height:heightScale(24), alignItems:'center', justifyContent:'center'}}>
                <Image source={progressImg ? progressImg : ''} resizeMode={'cover'}/>
            </View>
        )
    }
}

export default CircleProgress;

