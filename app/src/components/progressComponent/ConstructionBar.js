
import React, { Component } from 'react'
import { View, Text, Image, StyleSheet } from 'react-native'
import * as Progress from 'react-native-progress';
import res from '../../../res'
import { heightScale, myWidth, widthScale } from '../../utility/Utils';
const ConstructionBar = (props) => {
    // console.log("ConstructionBar", props.percentage)
    const { header, src, uri, value, percentage, imagesView, imageStyle, percentageText } = props
    const progressPercentage = Math.round(percentage * 0.01)
    return (
        <View style={[styles.ConstructionBView, styles.shadow]}>
            <View style={styles.firstView}>

                <View style={[styles.imagesView, imagesView]}>
                    {(src || uri) ?
                        <Image style={[{ width: widthScale(40), height: widthScale(40) }, imageStyle]} source={src ? src : uri ? { uri: uri } : res.images.icnRoom} resizeMode={'contain'} /> :
                        <View></View>
                    }

                </View>
                <View style={{ width: "66%" }}>
                    <View>
                        <Text style={styles.headerText}>{header ? header : ""}</Text>
                    </View>
                    {/* {
                        value ?
                            <View>

                                <Text numberOfLines={2} style={styles.otherText}>{value ? "(" + value + ")" : ""}</Text>
                            </View> :
                            <View />
                    } */}

                </View>

                <View style={styles.percentageView}>

                    <Text>{Math.round(percentageText) ? Math.round(percentageText) : "0"}%</Text>
                </View>
            </View>
            {progressBar(percentageText, percentage)}
          

        </View>
    )

}
const progressBar = (percentageText, percentage ) => {
   
    console.log("progressBar", percentage)
    if (percentage < 0.3) {
        return <Progress.Bar
            width={null}
            height={8}
            borderWidth={0}
            progress={percentage}
            borderRadius={10}
            // width={myWidth}
            color={'red'}
            unfilledColor={res.colors.lightLavender}
        />
    } else if (percentage < 0.6) {
        return <Progress.Bar
            width={null}
            height={8}
            borderWidth={0}
            progress={percentage}
            borderRadius={10}
            // width={myWidth}
            color={res.colors.dustyOrange}
            unfilledColor={res.colors.lightLavender}
        />
    } else if (percentage <= 1) {
        return <Progress.Bar
            width={null}
            height={8}
            borderWidth={0}
            progress={percentage}
            borderRadius={10}
            // width={myWidth}
            color={res.colors.kelleyGreen}
            unfilledColor={res.colors.lightLavender}
        />
    }

}
const styles = StyleSheet.create({

    ConstructionBView: {
        // minHeight: 93,
        marginHorizontal: 10,
        backgroundColor: res.colors.butterscotch,
        marginTop: 10,
        borderRadius: 5,
        justifyContent: 'center',
        // top: heightScale(10),
    },
    shadow: {
        backgroundColor: res.colors.butterscotch,
        shadowColor: res.colors.shadowColor,
        shadowOffset: {
            width: 2,
            height: 2,
        },
        shadowOpacity: 5.25,
        shadowRadius: 3.84,
        elevation: 5,
    }, firstView: {
        // backgroundColor: 'red',
        flexDirection: 'row',
        marginVertical: 10,
        justifyContent: 'center',
        alignItems: 'center'
    }, imagesView: {
        width: "20%",
        alignItems: 'center',
        // backgroundColor:'blue'
    }, headerText: {
        fontWeight: 'bold',
        fontFamily: res.fonts.bold,
        fontSize: 14,
        color: res.colors.greyishBrown
    }, percentageView: {
        width: '15%',
        // backgroundColor: 'white',
        justifyContent: 'flex-end',
        // alignItems:'flex-end',
        right: 0,
        alignSelf: 'flex-end'
    }, otherText: {
        marginVertical: heightScale(5),
        fontFamily: res.fonts.regular,
        fontSize: 12,
        color: res.colors.liteBlack,
        letterSpacing: 0.13
    }, percentageText: {
        marginVertical: heightScale(5),
        fontFamily: res.fonts.Sregular,
        fontSize: 14,
        color: res.colors.black
    }
})
export default ConstructionBar