import React, { Component } from "react";
import { View, Text, Image, TouchableOpacity, TextInput, StyleSheet, FlatList,Keyboard } from "react-native";

import res from '../../../res'
import styles from './styles'



class TowerModal extends Component {

    constructor(props) {
        super(props);

        this.state = {
            searchResult: "",
            data: []
        };
    }
    componentDidMount() {
        this.setState({
            data: this.props.list
        })


    }
    searchResult = (index) => {
        const { data } = this.state
        let towerName = data[index].nameOfTower;
        let tower_id = data[index]._id;
        console.log("TOWER LIST", towerName, index, tower_id)
        this.setState({ searchResult: data });
        this.props.onCancelModalPress(towerName, tower_id,index)
    };

    onOkBtnPress = () => {
       Keyboard.dismiss()
    };
    renderSearchBtn = ({ item, index }) => {
        console.log("Tower ITEM", item)
        return (
            <View style={[styles.shadow,styles.reasonViewCon]}>
                <TouchableOpacity style={[styles.reasonBtn]} onPress={() => this.searchResult(index)}>
                    <Text style={styles.reasonTextStyle}>{item.nameOfTower}</Text>
                </TouchableOpacity>
            </View>
        )
    }
    newData = () => {

    }
    searchData = (text) => {
        const { data } = this.state

        const newData = this.props.list.filter(item => {
            const itemData = `${item.nameOfTower.toUpperCase()} ${item.nameOfTower.toUpperCase()}`;
            const textData = text.toUpperCase();
            return itemData.includes(textData); // this will return true if our itemData contains the textData
        });
        if(text!=''){
            this.setState({
                data: newData,
                searchResult: text
            });
        }else{
            this.setState({
                data: this.props.list,
                searchResult: text
            });
        }
    };
    FlatListItemSeparator = () => {
        return (
            <View
                style={{
                    height: 1,
                    width: "100%",
                    backgroundColor: res.colors.underlineColor,
                }}
            />
        );
    }
    render() {
        // console.log("DEVELOPER PROPS", this.props.list)

        const { searchResult, error, data } = this.state;
        console.log("Tower=====>", data)


        const { closeModal } = this.props;
        return (
            <View style={styles.container}>
                <TextInput
                    placeholder={"Search"}
                    placeholderTextColor={res.colors.black}
                    style={styles.inputField}
                    value={searchResult}
                    onChangeText={this.searchData}
                    inputProps={{
                        maxLength: 50,
                        returnKeyType: "next",
                        keyboardType: "email-address",
                        autoCapitalize: "none",
                        autoCorrect: false,
                        autoFocus: false,
                        numberOfLines: 1,
                    }}
                    onSubmitEditing={this.onOkBtnPress}
                />
                {error ? (<Text style={styles.errorTextFild}>{error}</Text>) : <View />}
                {
                    data && data.length > 0 &&
                    <FlatList
                        keyExtractor={(item, index) => index.toString()}
                        showsVerticalScrollIndicator={false}
                        data={data}
                        renderItem={this.renderSearchBtn}
                    // ItemSeparatorComponent={this.FlatListItemSeparator}
                    />
                }


                <TouchableOpacity style={styles.closeBtn} onPress={closeModal}>
                    <Image source={res.images.icnClose} style={{ width: 20, height: 20, tintColor: res.colors.white }} />
                </TouchableOpacity>
            </View>
        );
    }
}


export default TowerModal;