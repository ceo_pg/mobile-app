import React, { Component } from "react";
import { View, Text, Image, TouchableOpacity, TextInput, StyleSheet, FlatList, Keyboard, Dimensions, SafeAreaView, ScrollView } from "react-native";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

import res from '../../../../../res'
import styles from './styles'
import { VaastuData, VaastuScore, TotalScore } from "../../../../components/vaastu/VaastuView";
import { camelize, heightScale } from '../../../../utility/Utils'
class VastuModal extends Component {

    constructor(props) {
        super(props);
        this.modalRef = React.createRef();
        this.state = {
            isInfo: false,
            suggestion: [
                {
                    name: "Excellent",
                    value: 4,
                    backgroundColor: res.colors.kelleyGreen

                }, {
                    name: "Good",
                    value: 2,
                    backgroundColor: res.colors.kelleyGreen
                }, {
                    name: "Neutral",
                    value: 0,
                    backgroundColor: res.colors.hazel
                }, {
                    name: "Poor",
                    value: -2,
                    backgroundColor: res.colors.peacockBlue
                }, {
                    name: "Bad",
                    value: -4,
                    backgroundColor: res.colors.tomato
                }, {
                    name: "",
                    value: "",
                    backgroundColor: ""
                },
            ]
        };
    }
    componentDidMount() {

    }

    searchScore = (key1, key2) => {
        let vastuDetails = this.props.vastuDetails
        let keys = Object.keys(vastuDetails)
        let allScores = vastuDetails[key1]
        return allScores[key2] ? allScores[key2] : "0"
    }
    openInfoScreen = () => {
        this.setState({
            isInfo: true
        })
    }
    goBack = () => {
        this.setState({
            isInfo: false
        })
    }
    infoScreen = (vastuDetails) => {
        if (!vastuDetails) {
            return <View />
        }

        return <View style={{ marginBottom: heightScale(150) }}>
            <VaastuScore
                header={res.strings.Main_Entrance}
                north={vastuDetails.mainEntrance.north}
                east={vastuDetails.mainEntrance.east}
                west={vastuDetails.mainEntrance.west}
                south={vastuDetails.mainEntrance.south}
                northEast={vastuDetails.mainEntrance.northEast}
                northWest={vastuDetails.mainEntrance.northWest}
                southEast={vastuDetails.mainEntrance.southEast}
                southWest={vastuDetails.mainEntrance.southWest}
            />
            <VaastuScore
                header={res.strings.Master_Bedroom}

                north={vastuDetails.masterBedroom.north}
                east={vastuDetails.masterBedroom.east}
                west={vastuDetails.masterBedroom.west}
                south={vastuDetails.masterBedroom.south}
                northEast={vastuDetails.masterBedroom.northEast}
                northWest={vastuDetails.masterBedroom.northWest}
                southEast={vastuDetails.masterBedroom.southEast}
                southWest={vastuDetails.masterBedroom.southWest}
            />
            <VaastuScore
                header={res.strings.Kitchen_Location}

                north={vastuDetails.kitchen.north}
                east={vastuDetails.kitchen.east}
                west={vastuDetails.kitchen.west}
                south={vastuDetails.kitchen.south}
                northEast={vastuDetails.kitchen.northEast}
                northWest={vastuDetails.kitchen.northWest}
                southEast={vastuDetails.kitchen.southEast}
                southWest={vastuDetails.kitchen.southWest}
            />
            <VaastuScore
                header={res.strings.Location_of_Balcony}

                north={vastuDetails.balcony.north}
                east={vastuDetails.balcony.east}
                west={vastuDetails.balcony.west}
                south={vastuDetails.balcony.south}
                northEast={vastuDetails.balcony.northEast}
                northWest={vastuDetails.balcony.northWest}
                southEast={vastuDetails.balcony.southEast}
                southWest={vastuDetails.balcony.southWest}
            />
            <View style={styles.isDarkView}>
                <View style={{ width: "45%" }}>
                    <Text style={styles.parameterText}>{res.strings.Other_Parameters}</Text>

                </View>
                <View>
                    <Text style={styles.parameterText}>{res.strings.Same}</Text>

                </View>
                <View>
                    <Text style={styles.parameterText}>{res.strings.Parallel}</Text>

                </View>
                <View style={{ width: "20%" }}>
                    <Text style={styles.parameterText}>{res.strings.Perpen_dicular}</Text>

                </View>
                <View style={{ width: "10%" }}>
                    <Text style={styles.parameterText}>{res.strings.Gap}</Text>

                </View>
            </View>
            <View style={styles.isSimpleView}>
                <View style={{ width: "45%", borderWidth: 1, minHeight: 48, justifyContent: 'center', borderColor: 'rgb(242,177,63)' }}>
                    <Text style={styles.parameterText}>{res.strings.CooKing_Stove}</Text>

                </View>
                <View style={{ width: "10%", borderWidth: 1, minHeight: 48, justifyContent: 'center', borderColor: 'rgb(242,177,63)', alignItems: 'center' }}>
                    <Text style={styles.parameterText}>{vastuDetails.positionSink.same}</Text>

                </View>
                <View style={{ width: "20%", borderWidth: 1, minHeight: 48, justifyContent: 'center', borderColor: 'rgb(242,177,63)', alignItems: 'center' }}>
                    <Text style={styles.parameterText}>{vastuDetails.positionSink.parallel}</Text>

                </View>
                <View style={{ width: "15%", borderWidth: 1, minHeight: 48, justifyContent: 'center', borderColor: 'rgb(242,177,63)', alignItems: 'center' }}>
                    <Text style={styles.parameterText}>{vastuDetails.positionSink.perpendicular}</Text>

                </View>
                <View style={{ width: "10%", borderWidth: 1, minHeight: 48, justifyContent: 'center', borderColor: 'rgb(242,177,63)', alignItems: 'center' }}>
                    <Text style={styles.parameterText}>{vastuDetails.positionSink.gap}</Text>

                </View>
            </View>
            <View style={[styles.isDarkView, { marginTop: 10, marginHorizontal: 10 }]}>
                <View style={{ width: "50%", }}>
                    <Text style={styles.parameterText}>{res.strings.Other_Parameters}</Text>

                </View>
                <View style={{ alignItems: 'center', width: "25%", }}>
                    <Text style={styles.parameterText}>{"Yes"}</Text>

                </View>
                <View style={{ alignItems: 'center', width: "25%", }}>
                    <Text style={styles.parameterText}>{"No"}</Text>

                </View>

            </View>
            <View style={styles.isSimpleView}>
                <View style={{ width: "50%", borderWidth: 1, minHeight: 48, justifyContent: 'center', borderColor: 'rgb(242,177,63)' }}>
                    <Text style={styles.parameterDetailsText}>{res.strings.IS_THE_NORTH_EAST_RIGHT}</Text>

                </View>
                <View style={{ width: "25%", borderWidth: 1, minHeight: 48, justifyContent: 'center', borderColor: 'rgb(242,177,63)', alignItems: 'center' }}>
                    <Text style={styles.parameterDetailsText}>{vastuDetails.cutOff.yes}</Text>

                </View>
                <View style={{ width: "25%", borderWidth: 1, minHeight: 48, justifyContent: 'center', borderColor: 'rgb(242,177,63)', alignItems: 'center' }}>
                    <Text style={styles.parameterDetailsText}>{vastuDetails.cutOff.no}</Text>
                </View>

            </View>
            <View style={styles.isSimpleView}>
                <View style={{ width: "50%", borderWidth: 1, minHeight: 48, justifyContent: 'center', borderColor: 'rgb(242,177,63)' }}>
                    <Text style={styles.parameterDetailsText}>{res.strings.IS_THE_NORTH_EAST_NOT_RIGHT}</Text>

                </View>
                <View style={{ width: "25%", borderWidth: 1, minHeight: 48, justifyContent: 'center', borderColor: 'rgb(242,177,63)', alignItems: 'center' }}>
                    <Text style={styles.parameterDetailsText}>{vastuDetails.extended.yes}</Text>

                </View>
                <View style={{ width: "25%", borderWidth: 1, minHeight: 48, justifyContent: 'center', borderColor: 'rgb(242,177,63)', alignItems: 'center' }}>
                    <Text style={styles.parameterDetailsText}>{vastuDetails.extended.no}</Text>
                </View>

            </View>

            <View style={styles.isSimpleView}>
                <View style={{ width: "50%", borderWidth: 1, minHeight: 48, justifyContent: 'center', borderColor: 'rgb(242,177,63)' }}>
                    <Text style={styles.parameterDetailsText}>{res.strings.IS_STAIRCASE_LIFT}</Text>

                </View>
                <View style={{ width: "25%", borderWidth: 1, minHeight: 48, justifyContent: 'center', borderColor: 'rgb(242,177,63)', alignItems: 'center' }}>
                    <Text style={styles.parameterDetailsText}>{vastuDetails.staircase.yes}</Text>

                </View>
                <View style={{ width: "25%", borderWidth: 1, minHeight: 48, justifyContent: 'center', borderColor: 'rgb(242,177,63)', alignItems: 'center' }}>
                    <Text style={styles.parameterDetailsText}>{vastuDetails.staircase.no}</Text>
                </View>

            </View>
        </View>
    }
    headerView = () => {
        return <View style={styles.headerText}>
            <View>
                <Text style={[styles.headerTextStyle, { marginLeft: 10 }]}>{res.strings.S_NO}</Text>
            </View>
            <View style={{ alignItems: 'flex-start' }}>
                <Text style={[styles.headerTextStyle, { textAlign: 'left' }]}>{res.strings.PARAMETER}</Text>
            </View>
            <View>
                <Text style={[styles.headerTextStyle, { marginRight: 10 }]}>{res.strings.SCORE}</Text>
            </View>
        </View>
    }
    vastuScreen = (vastu) => {
        if (!vastu) {
            return <View />
        }
        const { vastuMinScore, vastuMaxScore, vastuRange } = this.props
        let total = this.props.vastuScore ? this.props.vastuScore : 0
        const totalValue = Math.round((total / 100) * 60) - 32
        return <View style={{ marginHorizontal: 10, top: 10, borderTopLeftRadius: 10, borderTopRightRadius: 10, marginBottom: heightScale(150) }}>
            {this.headerView()}
            <VaastuData
                S_No={"1"}
                header={res.strings.DIRECTION_MAIN_ENTRANCE}
                value={vastu.mainEntrance}
                score={this.searchScore("mainEntrance", vastu.mainEntrance ? camelize(vastu.mainEntrance) : "0")}
            />
            <VaastuData
                isDark={true}
                S_No={"2"}
                header={res.strings.DIRECTION_MASTER_BEDROOM}
                value={vastu.masterBedroom}
                score={this.searchScore("masterBedroom", vastu.masterBedroom ? camelize(vastu.masterBedroom) : "0")}
            />
            <VaastuData
                S_No={"3"}
                header={res.strings.DIRECTION_KITCHEN}
                value={vastu.kitchen}
                score={this.searchScore("kitchen", vastu.kitchen ? camelize(vastu.kitchen) : "0")}
            />
            <VaastuData
                isDark={true}
                S_No={"4"}
                header={res.strings.DIRECTION_BALCONY}
                value={vastu.balcony}
                score={this.searchScore("balcony", vastu.balcony ? camelize(vastu.balcony) : "0")}
            />
            <VaastuData
                S_No={"5"}
                header={res.strings.IS_STAIRCASE}
                value={vastu.staircase}
                score={this.searchScore("staircase", vastu.staircase ? camelize(vastu.staircase) : "0")}
            />
            <VaastuData
                isDark={true}
                S_No={"6"}
                header={res.strings.LOCATION_ATTACHED}
                value={vastu.locationStaircase}
                score={this.searchScore("locationStaircase", vastu.staircase ? camelize(vastu.locationStaircase) : "0")}
            />
            <VaastuData
                S_No={"7"}
                header={res.strings.POSITION_OF_COOKING}
                value={vastu.positionSink}
                score={this.searchScore("positionSink", vastu.positionSink ? camelize(vastu.positionSink) : "0")}
            />
            <VaastuData
                isDark={true}
                S_No={"8"}
                header={res.strings.NO_OF_DOORS}

                value={vastu.noOfDoors}
                score={vastu.noOfDoors == 7 ? "-4" : "4"}
            />
            <VaastuData
                S_No={"9"}
                header={res.strings.IS_THE_NORTH_EAST}
                value={vastu.cutOff}
                score={this.searchScore("cutOff", vastu.cutOff ? camelize(vastu.cutOff) : "0")}
            />
            <VaastuData
                isDark={true}
                S_No={"10"}
                header={res.strings.IS_THE_NORTH_EAST_EXTENDED}
                value={vastu.extended}
                score={this.searchScore("extended", vastu.extended ? camelize(vastu.extended) : "0")}
            />
            <View>
                <TotalScore
                    isDark={false}
                    header={"Total"}
                    value={totalValue}
                />
                <TotalScore
                    vaastuTotalScoreStyle={{ marginTop: -5 }}
                    isDark={true}
                    header={"Max Score"}
                    value={vastuMaxScore ? vastuMaxScore : 0}
                />
                <TotalScore
                    isDark={true}
                    header={"Min Score "}
                    value={vastuMinScore ? vastuMinScore : 0}
                />
                <TotalScore
                    isDark={true}
                    header={"Range"}
                    value={vastuRange ? vastuRange : "0"}
                />
                <TotalScore
                    isDark={true}
                    header={"Vaastu Compliance Score"}
                    value={this.props.vastuScore}
                />
                {/* <TotalScore
                    isDark={false}
                    header={"Total"}
                    value={"-16"}
                /> */}
                <View style={{ minHeight: 50 }}>
                    <FlatList
                        scrollEnabled={false}
                        data={this.state.suggestion}
                        numColumns={3}
                        keyExtractor={(item, index) => index.toString()}
                        renderItem={({ item, index }) => {
                            console.log("Excellent", item)
                            return (<View style={{ flexDirection: 'row', flexWrap: 'wrap', marginTop: 10, alignItems: 'center', flex: 1 }}>

                                <View style={{ width: 18, height: 18, backgroundColor: item.backgroundColor, borderRadius: 9, alignItems: 'center' }} >
                                    <Text style={styles.valueText}>{item.value}</Text>
                                </View>
                                <Text style={styles.suggestionText}>{item.name}</Text>
                            </View>)
                        }}
                    />

                </View>

            </View>
        </View>
    }
    render() {
        const { isInfo } = this.state
        console.log("VASTU MODAL", this.props)
        return (
            <View style={styles.container}>
                <View style={[styles.shadowHeader, { height: 42, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }]}>
                    {
                        isInfo ? <TouchableOpacity style={{ width: 20, height: 20, left: 20, alignItems: 'center', justifyContent: 'center' }} onPress={() => this.goBack()}>
                            <Image source={res.images.icn_back} style={{ flex: 1 }} />

                        </TouchableOpacity> : <View style={{ width: 30, height: 30 }} />
                    }


                    <Text style={styles.shadowVastuHeader}>{res.strings.VASTU_SCORE}</Text>

                    {
                        isInfo ? <View /> :
                            <TouchableOpacity onPress={() => this.openInfoScreen()} style={{ width: 30, height: 30, right: 30, alignItems: 'center', justifyContent: 'center' }} >
                                <Image source={res.images.icn_info} />
                            </TouchableOpacity>
                    }

                </View>
                <ScrollView>


                    {this.state.isInfo ?
                        (this.props.vastuDetails ? this.infoScreen(this.props.vastuDetails) : <View />) :
                        (this.props.vastu ? this.vastuScreen(this.props.vastu) : <View />)
                    }


                </ScrollView>
            </View>
        );
    }
}

export default VastuModal;