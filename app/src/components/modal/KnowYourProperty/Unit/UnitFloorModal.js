import React, { Component } from "react";
import { View, Text, Image, TouchableOpacity, FlatList, Linking, Dimensions } from "react-native";
import ImageZoom from 'react-native-image-pan-zoom';

import res from '../../../../../res'
import styles from './styles'
import Modal from 'react-native-modal'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { heightScale } from "../../../../utility/Utils";

class UnitFloorModal extends Component {

    constructor(props) {
        super(props);
        this.modalRef = React.createRef();
        this.state = {
            searchResult: "",
            data: [],
            isShowImage: false,
            image: this.props.image,
            isSecondImage: false

        };
    }
    componentDidMount() {
        this.setState({
            data: this.props.list
        })


    }

    renderUnitPlanView = ({ item, index }) => {
        console.log("renderUnitPlanView", item)
        const breathFeet = item && item.breadth && item.breadth.feet ? item.breadth.feet : ""
        const breathInches = item && item.breadth && item.breadth.inches ? item.breadth.inches : ""

        const areaUom = item && item.area && item.area.uom ? item.area.uom : ""
        const areaValue = item && item.area && item.area.value ? item.area.value : ""

        const lengthFeet = item && item.length && item.length.feet ? item.length.feet : ""
        const lengthInches = item && item.length && item.length.inches ? item.length.inches : ""
        return (
            <View style={styles.modalStyel}>
                {(index % 2 == 0) ?
                    <View style={[styles.unitFloorData]}>
                        <View style={styles.separator}>
                            <Text style={styles.unitFloorText}>{item.nameOfSpace ? item.nameOfSpace : ""}</Text>
                        </View>
                        <View style={styles.separator}>
                            <Text style={styles.unitFloorText}>{breathFeet + "'" + breathInches + '"' + "*" + lengthFeet + "'" + lengthInches + '"'}</Text>
                        </View>
                        <View style={styles.separator}>
                            <Text style={styles.unitFloorText}>{areaValue}</Text>
                            <Text style={styles.unitAreaDimentionText}>{areaUom + "  sq ft"}</Text>
                        </View>
                    </View> :
                    <View style={[styles.unitOddFloorData]}>
                        <View style={styles.separatorShadow}>
                            <Text style={styles.unitFloorText}>{item.nameOfSpace ? item.nameOfSpace : ""}</Text>
                        </View>
                        <View style={styles.separatorShadow}>
                            <Text style={styles.unitFloorText}>{breathFeet + "'" + breathInches + '"' + "*" + lengthFeet + "'" + lengthInches + '"'}</Text>
                        </View>
                        <View style={styles.separatorShadow}>
                            <Text style={styles.unitFloorText}>{areaValue}</Text>
                            <Text style={styles.unitAreaDimentionText}>{areaUom + "  sq ft"}</Text>
                        </View>
                    </View>}
            </View>)

    }

    closemodal = () => {
        this.modalRef.current.hide();
    }
    ShareImage = (url) => {
        Linking.canOpenURL(url).then(supported => {
            if (supported) {
                Linking.openURL(url);
            } else {
                console.log("Don't know how to open URI: " + url);
            }
        });
    }
    modalView = () => {
        const height = 140
        const step = 1 / (2 * height);
        return <View style={{ flex: 1, width: Dimensions.get('window').width, height: Dimensions.get('window').height, justifyContent: 'center', alignSelf: 'center' }}>

            <View style={styles.closeBtn}>
                <TouchableOpacity onPress={() => this.toggle()}>
                    <Image source={res.images.icnClose} style={{ width: 20, height: 20, tintColor: res.colors.white }} resizeMode={'cover'} />
                </TouchableOpacity>
            </View>
            <View >
                <ImageZoom cropWidth={Dimensions.get('window').width}
                    cropHeight={Dimensions.get('window').height}
                    imageWidth={Dimensions.get('window').width}
                    imageHeight={Dimensions.get('window').height}
                >
                    <Image source={this.state.image ? { uri: this.state.image } : res.images.parking} style={{ flex: 1, borderRadius: 5 }} resizeMode={'contain'} />
                </ImageZoom>
            </View>
            <TouchableOpacity style={styles.ShareBtn} onPress={() => this.ShareImage(this.state.image)}>

                <View style={styles.shareView}>
                    <Image source={res.images.icn_share} />
                </View>

            </TouchableOpacity>



        </View >



    }
    openZoomMode = () => {
        const { isShowImage } = this.state
        this.setState({
            isShowImage: !isShowImage
        })
    }
    toggle = () => {
        const { isShowImage } = this.state
        this.setState({
            isShowImage: !isShowImage
        })

    }
    renderHeader = () => {

        return (
            <View>
                <TouchableOpacity onPress={() => this.toggle()} style={{ width: 282, height: 275, marginHorizontal: 20, alignSelf: 'center' }} >
                    <Image source={this.props.image ? { uri: this.props.image } : res.images.parking} style={{ flex: 1, borderRadius: 5 }} resizeMode={'cover'} />
                </TouchableOpacity>
                <View style={[styles.unitHeaderData]}>
                    <Text style={styles.unitHeaderText}>{res.strings.Space}</Text>
                    <Text style={styles.unitHeaderText}>{res.strings.Dimension}</Text>
                    <Text style={styles.unitAreaText}>{res.strings.Area}</Text>
                </View>
            </View>

        )

    }
    render() {
        const { error, data } = this.state;

        const sumArea = this.props.list ? this.props.list : []
        const sum = sumArea
            .map(item => item.area.value)
            .reduce((prev, curr) => prev + curr, 0);
        console.log("TOTAL AREA===>2", this.props.image)
        // if (!sum) {
        //     return  <View style={[styles.container,{alignItems:'center',justifyContent:'center'}]}>
        //         <Text>{res.strings.NO_DATA_FOUND}</Text>
        //     </View>
        // }

        return (
            <View style={styles.container}>
                <View style={[styles.shadowHeader, { height: 42 }]}>
                    <Text style={styles.corousalNameText}>Unit Floor Plan</Text>
                </View>
                <KeyboardAwareScrollView
                    enableOnAndroid={true}
                >
                    <View style={{ marginBottom: heightScale(150) }}>
                        <Modal
                            style={{ borderRadius: 10 }}
                            isVisible={this.state.isShowImage}
                        >

                            {this.modalView()}
                        </Modal>

                        {error ? (<Text style={styles.errorTextFild}>{error}</Text>) : <View />}

                        {this.renderHeader()}
                        {
                            data && data.length > 0 &&
                            <FlatList
                                // ListHeaderComponent={
                                //     this.renderHeader
                                // }
                                keyExtractor={(item, index) => index.toString()}
                                showsVerticalScrollIndicator={false}
                                data={data}
                                renderItem={this.renderUnitPlanView}
                                ListFooterComponent={({ item }) => {
                                    return <View style={[styles.unitFloorData]}>
                                        <View style={styles.totalArea}>
                                            <Text style={styles.totalAreaText}>Total Area</Text>
                                        </View>
                                        <View style={styles.blankArea}>

                                        </View>
                                        <View style={styles.totalAreaValue}>
                                            <Text style={styles.totalAreaText}>{sum ? Math.round(sum) : 0}</Text>
                                            <Text style={styles.totalAreaText}>{"Sq ft"}</Text>
                                        </View>

                                    </View>
                                }}
                            />
                        }
                    </View>

                    <View />
                </KeyboardAwareScrollView>

                <Modal ref={this.modalRef} show={false} />
            </View>
        );
    }
}

export default UnitFloorModal;