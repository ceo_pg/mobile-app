import React, { Component } from "react";
import { View, Text, Image, ScrollView, FlatList, Keyboard } from "react-native";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

import res from '../../../../../res'
import styles from './styles'
import UnitLevel from '../../../card/UnitLevel'


class UnitLevelSpecificationModal extends Component {

    constructor(props) {
        super(props);

    }

    imageMapping = () => {
        let key = {}

    }

    renderHeader = ({ item, index }) => {
        console.log("FloorHeader", this.props.image)
        return <View style={{ height: 275, backgroundColor: res.colors.butterscotch, borderRadius: 5, marginTop: 5, marginHorizontal: 10 }}>
            <Image source={this.props.image ? { uri: this.props.image } : res.images.parking} style={{ flex: 1, borderRadius: 5 }} resizeMode={'stretch'} />
        </View>
    }
    render() {

        const wallFinish = this.props.data && this.props.data.wallFinish ? this.props.data.wallFinish : {}
        const doors = this.props.data && this.props.data.doors ? this.props.data.doors : {}
        const electrical = this.props.data && this.props.data.electrical ? this.props.data.electrical : {}
        const plumbing = this.props.data && this.props.data.plumbing ? this.props.data.plumbing : {}
        const dado = this.props.data && this.props.data.dado ? this.props.data.dado : {}
        const counterTops = this.props.data && this.props.data.counterTops ? this.props.data.counterTops : {}
        const kitchen = this.props.data && this.props.data.kitchen ? this.props.data.kitchen : {}
        const liftsMake = this.props.data && this.props.data.liftsMake ? this.props.data.liftsMake : ""
        const cpFitting = this.props.data && this.props.data.cpFitting ? this.props.data.cpFitting : ""
        const wC = this.props.data && this.props.data.wC ? this.props.data.wC : ""
        const windows = this.props.data && this.props.data.windows ? this.props.data.windows : ""
        const railings = this.props.data && this.props.data.railings ? this.props.data.railings : ""
        const bathroomsTilesHeight = this.props.data && this.props.data.bathroomsTilesHeight ? this.props.data.bathroomsTilesHeight : ""
        const balconyRailings = this.props.data && this.props.data.balconyRailings ? this.props.data.balconyRailings : ""
        const flooring = this.props.data && this.props.data.flooring ? this.props.data.flooring : {}

        return (<View style={[styles.container,]}>
            <View style={[styles.shadowHeader, { height: 42 }]}>
                <Text style={styles.corousalNameText}>Unit Level Specification</Text>
            </View>
            <KeyboardAwareScrollView
               
            >

                <View >
                    <Text style={styles.headerSection}>
                        Flooring
                </Text>
                    <UnitLevel
                        title={"Master Bedroom"}
                        source={res.images.masterbedroom}
                        name={flooring.masterBedroom.name}
                        brand={flooring.masterBedroom.brand}
                        dimensions={flooring.masterBedroom.dimensions}
                    />

                </View>
                <UnitLevel
                    title={"Bedroom 2"}
                    source={res.images.masterbedroom}
                    name={flooring.bedroom2.name}
                    brand={flooring.bedroom2.brand}
                    dimensions={flooring.bedroom2.dimensions}
                />
                <UnitLevel
                    title={"Bedroom 3"}
                    source={res.images.masterbedroom}
                    name={flooring.bedroom3.name}
                    brand={flooring.bedroom3.brand}
                    dimensions={flooring.bedroom3.dimensions}
                />

                <UnitLevel
                    title={"Bedroom 4"}
                    source={res.images.masterbedroom}
                    name={flooring.bedroom4.name}
                    brand={flooring.bedroom4.brand}
                    dimensions={flooring.bedroom4.dimensions}
                />
                <UnitLevel
                    title={"Living Area"}
                    source={res.images.living_Room}
                    name={flooring.livingArea.name}
                    brand={flooring.livingArea.brand}
                    dimensions={flooring.livingArea.dimensions}
                    

                />
                <UnitLevel
                    title={"Dining Area"}
                    source={res.images.dinning_room}
                    name={flooring.diningArea.name}
                    brand={flooring.diningArea.brand}
                    dimensions={flooring.diningArea.dimensions}
                />

                <UnitLevel
                    title={"Kitchen"}
                    source={res.images.kitchen}
                    name={flooring.kitchen.name}
                    brand={flooring.kitchen.brand}
                    dimensions={flooring.kitchen.dimensions}
                />
                <UnitLevel
                    title={"Balcony"}
                    source={res.images.balcony}
                    name={flooring.balcony.name}
                    brand={flooring.balcony.brand}
                    dimensions={flooring.balcony.dimensions}
                />
                <UnitLevel
                    title={"Bathrooms"}
                    source={res.images.bathroom}
                    name={flooring.bathrooms.name}
                    brand={flooring.bathrooms.brand}
                    dimensions={flooring.bathrooms.dimensions}
                />

                <View style={{marginBottom:200}}>
                    <Text style={styles.headerSection}>
                        WallFinish
                </Text>
                    {/* <UnitLevel
                        title={"InternalwallThickness"}
                        source={res.images.internalwallthickness}
                        name={wallFinish.internalwallThickness}

                    />
                    <UnitLevel
                        title={"ExternalwallThickness"}
                        source={res.images.extrenalwallthickness}
                        name={wallFinish.externalwallThickness} */}

                    {/* /> */}
                    <UnitLevel
                        title={"MasterBedroom"}
                        source={res.images.masterbedroom}
                        name={wallFinish.masterBedroom}

                    />
                    <UnitLevel
                        title={"OtherBedrooms"}
                        source={res.images.living_Room}
                        name={wallFinish.otherBedrooms}

                    />
                    <UnitLevel
                        title={"LivingArea"}
                        source={res.images.living_Room}
                        name={wallFinish.livingArea}
                     

                    />
                    <UnitLevel
                        title={"DiningArea"}
                        source={res.images.dinning_room}
                        name={wallFinish.diningArea}

                    />


                    <Text style={styles.headerSection}>
                        Maindoor
                </Text>
                    <UnitLevel
                        title={"Maindoor"}
                        source={res.images.maindoor}
                        name={doors.maindoor}

                    />
                    <UnitLevel
                        title={"Bedroom"}
                        source={res.images.masterbedroom}
                        name={doors.bedroom}

                    />
                    <UnitLevel
                        title={"Toilet"}
                        source={res.images.toilet}
                        name={doors.toilet}

                    />
                    {/* <UnitLevel
                        title={"CeilingHeight"}
                        source={res.images.ceiling_height}
                        name={doors.ceilingHeight.value}
                        meter={doors.ceilingHeight.uom}
                    /> */}
                    <UnitLevel
                        title={"MainDoorHeight"}
                        source={res.images.main_door_height}
                        name={doors.mainDoorHeight.value}
                        meter={doors.mainDoorHeight.uom}
                    />
                    <UnitLevel
                        title={"OtherRoomsDoorHeight"}
                        source={res.images._Otherroomdoorheight}
                        name={doors.otherRoomsDoorHeight.value}
                        meter={doors.otherRoomsDoorHeight.uom}
                    />


                    <Text style={styles.headerSection}>
                        Electrical
                </Text>
                    <UnitLevel
                        title={"AirConditioning"}
                        source={res.images.airConditioner}
                        name={electrical.airConditioning}

                    />
                    <UnitLevel
                        title={"Fans"}
                        source={res.images.fan}
                        name={electrical.fans}

                    />
                    <UnitLevel
                        title={"LightFixtures"}
                        source={res.images.lightfixtures}
                        name={electrical.lightFixtures}

                    />
                    <UnitLevel
                        title={"Switchboards"}
                        source={res.images.switch}
                        name={electrical.switchboards}

                    />

                    <Text style={styles.headerSection}>
                        Plumbing
                </Text>
                    <UnitLevel
                        title={"Faucets"}
                        source={res.images.faucets}
                        name={plumbing.faucets}

                    />
                    <UnitLevel
                        title={"Pipes"}
                        source={res.images.pipes}
                        name={plumbing.pipes}

                    />
                    <UnitLevel
                        title={"Bathtub"}
                        source={res.images.bathroom}
                        name={plumbing.bathtub}

                    />
                    <UnitLevel
                        title={"WaterCloset"}
                        source={res.images.WC}
                        name={plumbing.waterCloset.name}
                        brand={plumbing.waterCloset.brand}
                    />

                    <Text style={styles.headerSection}>
                        Dado
                </Text>
                    <UnitLevel
                        title={"Kitchen"}
                        source={res.images.kitchen}
                        name={dado.kitchen}

                    />
                    <UnitLevel
                        title={"Utility"}
                        source={res.images.utilities}
                        name={dado.utility}

                    />
                    <UnitLevel
                        title={"Toilet"}
                        source={res.images.toilet}
                        name={dado.toilet}

                    />


                    <Text style={styles.headerSection}>
                        CounterTops
                </Text>
                    <UnitLevel
                        title={"Toilet"}
                        source={res.images.toilet}
                        name={counterTops.toilet}

                    />
                    <UnitLevel
                        title={"Kitchen"}
                        source={res.images.kitchen}
                        name={counterTops.kitchen}

                    />

                    <Text style={styles.headerSection}>
                        Kitchen
                </Text>
                    <UnitLevel
                        title={"WashBasin"}
                        source={res.images.Washbasin}
                        name={kitchen.washBasin.name}
                        brand={kitchen.washBasin.brand}

                    />
                    <UnitLevel
                        title={"KitchenSinkSpec"}
                        source={res.images.sink}
                        name={kitchen.kitchenSinkSpec.name}
                        brand={kitchen.kitchenSinkSpec.brand}

                    />
                    <UnitLevel
                        title={"KitchenFurnishing"}
                        source={res.images.kitchen}
                        name={kitchen.kitchenFurnishing}


                    />
                    {/* <UnitLevel
                        title={"LiftsMake"}
                        source={res.images.liftmake}
                        name={liftsMake}


                    /> */}
                    <UnitLevel
                        title={"CpFitting"}
                        source={res.images.CpFitting}
                        name={cpFitting}


                    />
                    <UnitLevel
                        title={"WC"}
                        source={res.images.WC}
                        name={wC}


                    />
                    {/* <UnitLevel
                        title={"Windows"}
                        source={res.images.windows}
                        name={windows}


                    /> */}
                    <UnitLevel
                        title={"Railings"}
                        source={res.images.balcony}
                        name={railings}


                    />
                    <UnitLevel
                        title={"Bathrooms Tiles Height"}
                        source={res.images.bathroomtilesheight}
                        name={bathroomsTilesHeight}


                    />
                    <UnitLevel
                        title={"Balcony Railings"}
                        source={res.images.balcony}
                        name={balconyRailings}


                    />


                </View>

            </KeyboardAwareScrollView>

        </View >
        );
    }
}

export default UnitLevelSpecificationModal;