import React, { Component } from 'react'
import { View, Text, Image, TouchableOpacity, SafeAreaView, TextInput, ScrollView } from 'react-native'
import styles from './styles'
import res from '../../../../../res'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import ApiFetch from '../../../../apiManager/ApiFetch'
import { CAR_PARKING_ENDPOINTS } from '../../../../apiManager/ApiEndpoints';
import { FlatList } from 'react-native-gesture-handler'
import { heightScale } from '../../../../utility/Utils'
import SimpleToast from 'react-native-simple-toast'
import strings from '../../../../../res/constants/strings'
import ImageWithTitle from '../../../card/ImageWithTitle'
class KnowYourUnitModal extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isChecked: false,
            addCarParking: "",
            parkingArray: [{
                id: 1,
                value: "",
                isChecked: false
            }, {
                id: 2,
                value: "",
                isChecked: false
            }]
        }
    }

    onChangeCarParking = (text) => {

        this.setState({
            addCarParking: text
        })
    }
    markCheck = (item, index) => {
        const { parkingArray } = this.state
        console.log(item, index, "markCheckmarkCheck")
        // let checkParking = parkingArray.filter((item) => {
        //     console.log("markCheck", item)
        //     item.id == index + 1 ? item.isChecked = true 
        //     return item
        // })
        let tempArray = parkingArray.map((value, id) => {
            if (value.id == item.id) {
                return { ...value, isChecked: !value.isChecked }
            } else {
                return value
            }
        })
        this.setState({
            parkingArray: tempArray,

        })

    }
    saveCarParking = (parking) => {
        let slotNumber = []
        const { parkingArray } = this.state;
        const { toggleModal } = this.props
        console.log("parkingArray", toggleModal)
        for (let count = 0; count < parkingArray.length; count++) {
            if (parkingArray[count].isChecked && parkingArray[count].value != '') {
                slotNumber.push(parkingArray[count].value)
            }
        }
        // console.log("SLOTE NUMBER", parseInt(slotNumber));
        // if (slotNumber.length == 0) {
        //     alert("Please enter slot value");
        //     return
        // }
        const parameter = JSON.stringify({
            noOfSlots: parking,
            slotNumbers: slotNumber
        })
        ApiFetch.fetchPut(CAR_PARKING_ENDPOINTS, parameter)

            .then((res) => {
                console.log("PARKING SLOT RESPONCSE", res)
                if (res && res.statusCode == 200) {
                    SimpleToast.show(res.message, SimpleToast.LONG);

                    if (toggleModal) {

                        this.props.refreshData()
                        toggleModal()
                    }
                } else {
                    SimpleToast.show(res.message, SimpleToast.LONG);
                }

            }).catch((err) => {
                console.log("Error inside car parking", err)
            })
    }

    renderTextinput = ({ item, index }) => {
        console.log("renderTextinput", item)
        return <View style={{ flexDirection: 'row', width: '100%', alignItems: 'center', marginHorizontal: 15, marginTop: 13 }}>
            <TouchableOpacity onPress={() => this.markCheck(item, index)}>
                {item.isChecked ? <View style={{ width: 18, height: 18, alignItems: 'center', justifyContent: 'center', borderRadius: 3 }}><Image source={res.images.isChecked} /></View> : <View style={{ width: 18, height: 18, borderColor: res.colors.greyBlack, borderWidth: 1, borderRadius: 3 }}></View>}
            </TouchableOpacity>
            <Text style={{ marginLeft: 10, color: res.colors.greyBlack }}>0{item.id}</Text>
            <TextInput
                value={item.value}
                placeholder={res.strings.ADD_CAR_PARKING}
                onChangeText={(newText) => {

                    let parkingObject = this.state.parkingArray[index]
                    parkingObject.value = newText
                    let newParkingArray = this.state.parkingArray
                    newParkingArray[index] = parkingObject
                    this.setState({
                        parkingArray: newParkingArray
                    })
                    console.log("NEW TEXT", newParkingArray)
                }}
                style={styles.textInputCheckStyle}
                inputProps={{
                    keyboardType: 'default',
                    maxLength: 20,
                    returnKeyType: "next",
                    textAlign: "center",
                }}
                inputStyles={{ borderBottomWidth: 0, width: 40, paddingVertical: 5, fontSize: 18, }}
            />
        </View>

    }
    addMore = () => {
        let parkingArray = this.state.parkingArray
        let parkingObject = {
            id: this.state.parkingArray.length + 1,
            value: "",
            isChecked: false
        }
        parkingArray.push(parkingObject)
        this.setState({
            parkingArray: parkingArray
        })


    }

    render() {
        console.log("KNOw Your Unit Modal", this.props.parkingSlotValue)
        const unitType = this.props.data.floorsList && this.props.data.floorsList.unitsData && this.props.data.floorsList.unitsData.unitType ? this.props.data.floorsList.unitsData.unitType : ""
        const facing = this.props.data.floorsList && this.props.data.floorsList.unitsData && this.props.data.floorsList.unitsData.unitDirectionFacing ? this.props.data.floorsList.unitsData.unitDirectionFacing : ""
        const parking = this.props.data.floorsList && this.props.data.floorsList.unitsData && this.props.data.floorsList.unitsData.carParking && this.props.data.floorsList.unitsData.carParking.noOfSlots ? this.props.data.floorsList.unitsData.carParking.noOfSlots : 0
        const unitName = this.props.data.floorsList && this.props.data.floorsList.unitsData && this.props.data.floorsList.unitsData.unitName ? this.props.data.floorsList.unitsData.unitName : ""
        const nameOfTower = this.props.data.nameOfTower ? this.props.data.nameOfTower : ""
        const floor = this.props.data.floorsList && this.props.data.floorsList.nameOfFloor && this.props.data.floorsList.nameOfFloor ? this.props.data.floorsList.nameOfFloor : ""


        return (< View style={styles.container} >
            <KeyboardAwareScrollView
                enableOnAndroid={true}
                enableAutoAutomaticScroll={(Platform.OS === 'ios')} extraHeight={130} extraScrollHeight={130}

            >
                <View style={[styles.shadowHeader, { height: 42 }]}>
                    <Text style={styles.corousalNameText}>Know your Unit</Text>
                </View>


                <View style={{
                    flexDirection: 'row',
                    flexWrap: 'wrap',
                    width: '100%',
                    justifyContent: 'space-evenly'
                }}>

                    <ImageWithTitle
                        isShadow={true}
                        isdoubleCard={true}
                        src={res.images.icnContract}
                        Value={unitName ? unitName : "0"}
                        Heading={"Unit No"}
                        valueStyle={styles.unitNumber}
                        headingStyle={styles.unitText}
                    />
                    <ImageWithTitle
                        isShadow={true}
                        isdoubleCard={true}
                        src={res.images.icnTower}
                        Value={nameOfTower ? nameOfTower : "0"}
                        Heading={"Tower Number"}
                        valueStyle={styles.unitNumber}
                        headingStyle={styles.unitText}
                    />

                    <ImageWithTitle
                        isShadow={true}
                        isdoubleCard={true}
                        src={res.images.icnHome}
                        Value={floor ? floor : "0"}
                        Heading={"Floor"}
                        valueStyle={styles.unitNumber}
                        headingStyle={styles.unitText}
                    />
                    <ImageWithTitle
                        isShadow={true}
                        isdoubleCard={true}
                        src={res.images.icnRealEstate}
                        Value={unitType ? unitType : "0"}
                        Heading={"Unit Type"}
                        valueStyle={styles.facingText}
                        headingStyle={styles.unitText}
                    />

                    <ImageWithTitle
                        isShadow={true}
                        isdoubleCard={true}
                        src={res.images.icnUrban}
                        Value={this.props.noOfBalconies ? this.props.noOfBalconies : "0"}
                        Heading={"Balconies"}
                        valueStyle={styles.unitNumber}
                        headingStyle={styles.unitText}
                    />
                    <ImageWithTitle
                        isShadow={true}
                        isdoubleCard={true}
                        src={res.images.icnCompass}
                        Value={facing ? facing : ""}
                        Heading={"Direction Facing"}
                        valueStyle={styles.facingText}
                        headingStyle={styles.unitText}
                    />

                    <ImageWithTitle
                        isShadow={true}
                        isdoubleCard={true}
                        src={res.images.icnLift}
                        Value={this.props.data.noOfLifts ? this.props.data.noOfLifts : "0"}
                        Heading={"No. of Lifts"}
                        valueStyle={styles.unitNumber}
                        headingStyle={styles.unitText}
                    />

                    <ImageWithTitle
                        isShadow={true}
                        isdoubleCard={true}
                        src={res.images.icnCar}
                        Value={(this.props.parkingSlotValue && this.props.parkingSlotValue.length) ? this.props.parkingSlotValue.length : 0}
                        Heading={"Car Parking"}
                        valueStyle={styles.unitNumber}
                        headingStyle={styles.unitText}
                    />
                    <View />
                </View>
                <KeyboardAwareScrollView
                    enableOnAndroid={true}

                >
                    <View style={[{ flex: 1, marginBottom: heightScale(100) ,marginTop:heightScale(10)}]}>
                        <View style={[styles.footerBox, { flex: 1 }]}>
                            <FlatList
                                ListHeaderComponent={(item, index) => {
                                    return <Text style={styles.addCarText}>{res.strings.ADD_CAR_PARKING}</Text>
                                }}
                                data={this.state.parkingArray}
                                renderItem={this.renderTextinput}
                                ListFooterComponent={(item, index) => {
                                    return <View style={[styles.bottomBtn]}>
                                        <View style={styles.addMoreBtn} >
                                            <TouchableOpacity onPress={() => this.addMore()}>
                                                <Text style={styles.addMoreText}>{res.strings.ADD_MORE}</Text>
                                            </TouchableOpacity>
                                        </View>
                                        <View style={styles.saveBtn} >
                                            <TouchableOpacity onPress={() => this.saveCarParking(parking)}>
                                                <Text style={styles.addMoreText}>{res.strings.SAVE}</Text>
                                            </TouchableOpacity>
                                        </View>


                                    </View>
                                }}
                            />
                        </View>
                    </View>
                </KeyboardAwareScrollView>



            </KeyboardAwareScrollView>


        </View >)


    }
}
export default KnowYourUnitModal