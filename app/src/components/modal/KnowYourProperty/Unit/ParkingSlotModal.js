import React, { Component } from "react";
import { View, Text, Image, TouchableOpacity, Dimensions, SafeAreaView } from "react-native";

import res from '../../../../../res'
import styles from './styles'

import ImageZoom from 'react-native-image-pan-zoom';

const { width } = Dimensions.get('window')



class ParkingSlotModal extends Component {

    constructor(props) {
        super(props);

        this.state = {
            searchResult: "",
            data: []
        };
    }


    render() {

        return (
            <SafeAreaView style={{ flex: 1, top: 100, }}>
                <TouchableOpacity onPress={() => this.props.toggleModal()}>
                    <Text style={{ color: res.colors.white }}>Close</Text>
                </TouchableOpacity>
                <ImageZoom cropWidth={Dimensions.get('window').width}
                    cropHeight={Dimensions.get('window').height}
                    imageHeight={Dimensions.get('window').height}
                    imageWidth={Dimensions.get('window').width}
                >
                    <Image source={this.props.parkingImage ? { uri: this.props.parkingImage } : res.images.parking} style={{ height: Dimensions.get('window').height, width: Dimensions.get('window').width*0.9, borderRadius: 5, }} resizeMode={'contain'} />
                </ImageZoom> 

            </SafeAreaView>


        );
    }
}

export default ParkingSlotModal;