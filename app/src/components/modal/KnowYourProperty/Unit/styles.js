import { StyleSheet, Platform } from 'react-native'
import res from '../../../../../res'
const styles = StyleSheet.create({
  modalContainer: {
    flex: 1,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    backgroundColor: res.colors.butterscotch
  },
  container: {
    flex: 1,
    top: Platform.OS == 'ios' ? 100 : 100,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    backgroundColor: res.colors.butterscotch
  },
  image: {
    marginTop: 150,
    marginBottom: 10,
    width: '100%',
    height: 350,
  },
  text: {
    fontSize: 24,
    marginBottom: 30,
    padding: 40,
  },
  closeText: {
    fontSize: 24,
    color: '#00479e',
    textAlign: 'center',
  }, view: {
    justifyContent: 'flex-end',
    // marginHorizontal: 20,

  }, titleTextBox: {
    fontSize: 15,
    fontFamily: res.fonts.regular,
    color: res.colors.black,
    letterSpacing: 0.86
  }, modalHeaderText: {
    marginTop: 10,
    fontFamily: res.fonts.semiBold,
    fontSize: 18,
    letterSpacing: 0.86,
    textAlign: 'center',
    color: res.colors.greyishBrown,
    // marginTop: 18
  }, boxShadow: {
    backgroundColor: "white",
    shadowColor: "#000000",
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowOpacity: 0.25,
    shadowRadius: 1.84,
    elevation: 5,
  }, addCarText: {
    marginHorizontal: 14,
    fontFamily: res.fonts.regular,
    marginTop: 10,
    fontSize: 12,
    color: res.colors.black
  }, unitNumber: {
    fontFamily: res.fonts.regular,
    fontSize: 22,
    color: res.colors.liteBlack
  }, unitText: {
    fontFamily: res.fonts.regular,
    fontSize: 12,
    color: res.colors.liteBlack
  }, bottomBtn: {
    marginTop: 18,
    height: 30,
    flexDirection: 'row',
    justifyContent: 'space-between',
    // backgroundColor: 'red'

  }, addMoreBtn: {
    borderTopWidth: 1, borderTopColor: res.colors.black, flex: 1, alignItems: 'center', justifyContent: 'center',
    borderBottomLeftRadius: 12, borderBottomWidth: 1, borderBottomColor: res.colors.black,
  }, saveBtn: {
    borderBottomWidth: 1, borderBottomColor: res.colors.black, flex: 1, alignItems: 'center', justifyContent: 'center',
    borderBottomRightRadius: 12, borderTopWidth: 1, borderTopColor: res.colors.black, borderLeftWidth: 1, borderLeftColor: res.colors.black,
  }, addMoreText: {
    fontFamily: res.fonts.bold,
    fontSize: 12,
    color: res.colors.peacockBlue,

  }, textInputStyle: {
    paddingLeft: 10,
    width: '70%',
    marginLeft: 45,
    height: 27,
    borderWidth: 1.5,
    borderColor: res.colors.sepratorColor,
    borderRadius: 3
  }, textInputCheckStyle: {
    paddingTop: 0,
    paddingBottom: 0,
    paddingLeft: 10,
    width: '60%',
    marginLeft: 45,
    height: 27,
    borderWidth: 1,
    borderColor: res.colors.inputBorderColor,
    // borderRadius: 3
  }, squreHeading: {
    fontFamily: res.fonts.semiBold,
    fontSize: 10,
    color: res.colors.pinkishTan,
    marginLeft: 10
  }, squreSubHeading: {
    fontFamily: res.fonts.regular,
    fontSize: 10,
    color: res.colors.pinkishGrey,
    marginLeft: 10
  }, corousalNameText: {
    fontFamily: res.fonts.semiBold,
    fontSize: 18,
    letterSpacing: 0.3,
    textAlign: 'center',
    color: res.colors.greyishBrown,
    marginTop: 10
  }, footerBox: {
    borderRadius: 12,
    marginHorizontal: 20,
    minHeight: 150,
    borderTopColor: res.colors.black,
    borderTopWidth: 1,
    borderLeftColor: res.colors.black,
    borderLeftWidth: 1,
    borderRightColor: res.colors.black,
    borderRightWidth: 1,

  }, dotStyle: {
    // width: 10,
    // height: 10,
    borderRadius: 5,
    // marginHorizontal: 8,
    backgroundColor: res.colors.pinkishTan
  }, dotColor: {
    width: 10,
    height: 10,
    borderRadius: 5,
    marginHorizontal: -5,
    backgroundColor: res.colors.pinkishTan
  }, inactiveDotStyle: {
    width: 10,
    height: 10,
    borderRadius: 5,
    marginHorizontal: -5,
    backgroundColor: res.colors.pinkishTan
  },
  shadowHeader: {
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    backgroundColor: res.colors.butterscotch,
    shadowColor: res.colors.shadowColor,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 10.58,
    shadowRadius: 10.00,
    elevation: 24,
  },
  unitHeaderData: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',

    marginHorizontal: 10,

    backgroundColor: res.colors.shadowColor,

    marginTop: 10,
    height: 22,
    borderTopRightRadius: 5,
    borderTopLeftRadius: 5,


  }, unitHeaderText: {
    fontFamily: res.fonts.medium,
    fontSize: 12.2,
    color: res.colors.purpleBrown,
  }, unitFloorData: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',

    marginHorizontal: 10,

  }, unitFloorText: {
    fontFamily: res.fonts.regular,
    fontSize: 8.7,
    color: res.colors.black,
    textAlign: 'left',
    marginLeft: 10
  }, unitOddFloorData: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    height: 22,
    backgroundColor: res.colors.shadowColor,
    marginHorizontal: 10,
  }, separator: {
    flex: 1,
    flexDirection: 'row',
    borderWidth: 1,
    borderColor: res.colors.shadowColor,
    alignItems: 'center',
    height: 22,
    justifyContent: 'flex-start'
  }, separatorShadow: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    height: 22,
    justifyContent: 'flex-start'
  }, shadowTab: {
    shadowColor: res.colors.shadowColor,
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowOpacity: 0.0,
    shadowRadius: 0.0,

    elevation: 2,
    zIndex: 10
  }, UnitLevel: {
    height: 110,
    marginHorizontal: 10,
    backgroundColor: res.colors.butterscotch,
    marginTop: 10,
    borderRadius: 5,
    bottom: 10

  }, modalStyel: {
    flex: 1, backgroundColor: res.colors.butterscotch,
  }, nonModalStyle: {
    marginHorizontal: 15, backgroundColor: res.colors.butterscotch, zIndex: 99, borderTopLeftRadius: 30
  }, slotNumberText: {
    marginTop: 31,
    fontFamily: res.fonts.medium,
    fontSize: 14,
    color: res.colors.greyishBrown,
  }, slotValueText: {
    fontFamily: res.fonts.bold,
    fontSize: 24,
    color: res.colors.peacockBlue,
  }, shadow: {
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowOpacity: 0.3,
    shadowRadius: 4.11,
    zIndex: 99,
    elevation: 14,
  }, WallFinishLevel: {
    minHeight: 110,
    marginHorizontal: 10,
    backgroundColor: res.colors.butterscotch,
    marginTop: 10,
    borderRadius: 5,
    bottom: 10,
    // justifyContent: 'center'
  }, closeBtn: {
    position: "absolute",
    top: 30,
    right: 30,
    backgroundColor: res.colors.black,
    zIndex: 999,
    width: 30,
    height: 30,
    borderRadius: 15,
    alignItems: 'center',
    justifyContent: 'center'
  }, totalAreaText: {
    fontWeight: "bold",
    fontFamily: "Montserrat-Bold",
    fontSize: 8.7,
    color: res.colors.purpleBrown,
    textAlign: 'left',
    marginLeft: 10
  }, totalArea: {

    flex: 1,
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderLeftWidth: 1,
    borderColor: res.colors.shadowColor,
    // alignItems: 'center',
    height: 22,
    justifyContent: 'center'
  }, totalAreaValue: {
    flex: 1,
    flexDirection: 'row',
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderLeftWidth: 1,
    borderRightWidth: 1,
    borderColor: res.colors.shadowColor,
    alignItems: 'center',
    height: 22,
    justifyContent: 'flex-start'
  }, blankArea: {

    flex: 1,

    borderBottomWidth: 1,

    borderColor: res.colors.shadowColor,
    // alignItems: 'center',
    height: 22,
    justifyContent: 'center'
  }, unitAreaText: {
    fontFamily: res.fonts.medium,
    fontSize: 12.2,
    color: res.colors.purpleBrown,
    marginRight: 30
  }, unitAreaDimentionText: {
    fontFamily: res.fonts.regular,
    fontSize: 8.7,
    color: res.colors.black,
    textAlign: 'left',
    // marginLeft: 10
  }, resultText: {
    fontFamily: res.fonts.regular,
    fontSize: 12,
    color: res.colors.liteBlack
  }, headerSection: {
    fontWeight: 'bold',
    marginLeft: 10, marginTop: 13, color: res.colors.greyishBrown, marginVertical: 10, fontFamily: res.fonts.bold
  }, facingText: {
    fontFamily: res.fonts.regular,
    fontSize: 16,
    color: res.colors.liteBlack
  }, shadowVastuHeader: {
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    backgroundColor: res.colors.butterscotch,
    fontFamily: res.fonts.semiBold,
    fontSize: 18,
    letterSpacing: 0.3,
    textAlign: 'center',
    color: res.colors.greyishBrown,

  },
  headerText: {
    borderTopRightRadius: 10, borderTopLeftRadius: 10,
    height: 40,
    flexDirection: 'row',
    justifyContent: 'space-between',
    //  marginHorizontal: 10,
    alignItems: 'center',
    backgroundColor: 'rgb(242,177,63)'
  }, headerTextStyle: {
    fontFamily: res.fonts.medium,
    fontSize: 11,
    color: res.colors.purpleBrown
  }, valueText: {
    color: res.colors.white,
    fontFamily: res.fonts.regular,
    fontSize: 12,
    textAlign: 'center'
  }, suggestionText: {
    color: res.colors.black,
    fontFamily: res.fonts.regular,
    fontSize: 12,
    textAlign: 'center',
    marginLeft: 5
  }, isDarkView: {
    top: 20,
    borderTopRightRadius: 10, borderTopLeftRadius: 10,
    minHeight: 37,
    flexDirection: 'row',
    justifyContent: 'space-between',
    //  marginHorizontal: 10,
    alignItems: 'center',
    backgroundColor: 'rgb(242,177,63)',
    marginHorizontal: 10
  }, parameterText: {
    color: res.colors.purpleBrown,
    fontFamily: res.fonts.medium,
    fontSize: 11,
    // textAlign: 'center',
    marginLeft: 5
  }, isSimpleView: {
    top: 20,
    borderTopRightRadius: 10, borderTopLeftRadius: 10,
    minHeight: 48,
    flexDirection: 'row',
    justifyContent: 'space-between',
    //  marginHorizontal: 10,
    alignItems: 'center',
    // backgroundColor: 'rgb(242,177,63)',
    marginHorizontal: 10,

  }, parameterDetailsText: {
    color: res.colors.black,
    fontFamily: res.fonts.regular,
    fontSize: 12,
    marginLeft: 5

  }, ShareBtn: {
    position: 'absolute',
    bottom: 30,
    right: 30,
    backgroundColor:res.colors.black,
    borderRadius:25,
    zIndex: 9999999,
    width:50,
    height:50,
    overflow:'hidden',
    justifyContent:'center',
    alignItems:'center'
  },shareView:{
    backgroundColor:res.colors.white,
    borderRadius:15,
    width:30,
  }


})
export default styles