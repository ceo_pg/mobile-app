
import { StyleSheet } from 'react-native'
import res from '../../../../../res'
import { heightScale, widthScale } from '../../../../utility/Utils';
const styles = StyleSheet.create({
    modalStyle: {
        flex: 1,
        top: Platform.OS == 'ios' ? 100 : 50,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        backgroundColor: res.colors.butterscotch,

    },
    shadowHeader: {
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        backgroundColor: res.colors.butterscotch,
        shadowColor: res.colors.shadowColor,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 10.58,
        shadowRadius: 10.00,
        elevation: 24,
    },
    corousalNameText: {
        fontFamily: res.fonts.semiBold,
        fontSize: 18,
        letterSpacing: 0.3,
        textAlign: 'center',
        color: res.colors.greyishBrown,
        marginTop: 10
    }, shadow: {
        shadowColor: res.colors.shadowColor,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 2,
        backgroundColor: res.colors.butterscotch,

    }, projectsText: {
        fontWeight:'bold',
        marginVertical:heightScale(10),
        marginHorizontal:widthScale(20),
        fontSize: 15,
        fontFamily: res.fonts.bold,
        color: res.colors.greyishBrown
    },SafeAreaView:{ flex: 1, flexDirection: 'column', justifyContent: 'space-between' 
},projectText: {
    fontFamily: res.fonts.medium,
    fontSize: 16,
    color: res.colors.greyishBrown
}, percentageLabel: {
    fontFamily: res.fonts.regular,
    fontSize: 12,
    color: res.colors.charcoalGrey,
    // flex: 1,
    paddingRight: 20,
    marginLeft: 5,
    textAlign: 'left'
}, percentageText: {
    fontWeight:'bold',
    fontFamily: res.fonts.semiBold,
    fontSize: 12,
    color: res.colors.charcoalGrey,
    marginLeft: 10
},
constructionText: {
    fontFamily: res.fonts.medium,
    fontSize: 16,
    color: res.colors.greyishBrown,

}, constructionView: {
    marginTop: heightScale(30),
    marginBottom: 40,
    alignItems: 'center'
},
constructionBtn: {
    borderBottomWidth: 1,
    borderBottomColor: res.colors.appColor,

},
closeBtn: {
    position: "absolute",
    top: 30,
    right: 30,
    backgroundColor: res.colors.white,
    zIndex: 999,
    width: 30,
    height: 30,
    borderRadius: 15,
    alignItems: 'center',
    justifyContent: 'center'
}, ShareBtn: {
    position: 'absolute',
    bottom: 30,
    right: 30,
    backgroundColor:res.colors.black,
    borderRadius:25,
    zIndex: 9999999,
    width:50,
    height:50,
    overflow:'hidden',
    justifyContent:'center',
    alignItems:'center'
  },shareView:{
      backgroundColor:res.colors.white,
      borderRadius:15,
      width:30,
      height:30
  }
});
export default styles