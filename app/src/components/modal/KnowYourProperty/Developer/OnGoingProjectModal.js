import React, { Component } from "react";
import { View, Text, FlatList } from "react-native";
import res from '../../../../../res'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import CardWithTitle from '../../../../components/card/CardWithTitle'
import styles from './styles'
import { heightScale } from "../../../../utility/Utils";


class OnGoingProjectModal extends Component {

    constructor(props) {
        super(props);

    }




    render() {
        console.log("PartnersAndLocationsModal", this.props)
        return (
            < View style={styles.modalStyle} >

              
                    <View style={[styles.shadowHeader, { height: 42 }]}>
                        <Text style={styles.corousalNameText}>{res.strings.PROJECTS}</Text>
                    </View>
                    <KeyboardAwareScrollView >
                        <Text style={styles.projectsText}>Ongoing Projects</Text>
                        <View style={{ flexDirection: 'row', flexWrap: 'wrap', width: '100%', justifyContent: 'space-evenly' }}>

                            {this.props.onGoingProject.map((item, index) => {
                                return (

                                    <CardWithTitle uri={item.logo} imageStyle={{ resizeMode: 'stretch' }} />)
                            })}
                            {/* <CardWithTitle isShodow={true} isProjectSpecs={false} /> */}
                        </View>
                        <Text style={styles.projectsText}>Delivered Projects </Text>
                        <View style={{ flexDirection: 'row', flexWrap: 'wrap', width: '100%', justifyContent: 'space-evenly',marginBottom:heightScale(200) }}>

                            {this.props.deliveredProjects.map((item, index) => {
                                return (

                                    <CardWithTitle uri={item.logo} imageStyle={{ resizeMode: 'stretch' }} />)
                            })}
                            {/* <CardWithTitle isShodow={true} isProjectSpecs={false} /> */}
                        </View>
                    </KeyboardAwareScrollView>
             

            </ View>
        )
    }
}
export default OnGoingProjectModal