import React, { Component } from "react";
import { View, Text, StyleSheet, Platform, ScrollView } from "react-native";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

import res from '../../../../../res'
import { VaastuData, DeveloperScore, TotalScore } from "../../../../components/vaastu/VaastuView";
import { camelize, heightScale } from '../../../../utility/Utils'
class DeveloperScoreModal extends Component {

    constructor(props) {
        super(props);
        this.modalRef = React.createRef();
        this.state = {

        };
    }
    DeveloperScore = () => {
        const { developerDetailedScore, developerDetailedScoreParameters, developerDetailedWeight } = this.props
        if (!developerDetailedScore || !developerDetailedScoreParameters || !developerDetailedWeight) {
            return <View style={{ justifyContent: 'center', alignItems: 'center', flex: 1 }}>
                <Text>{res.strings.NO_DATA_FOUND}</Text>
            </View>
        }
        return <View style={{ marginHorizontal: 10, top: 10, borderTopLeftRadius: 10, borderTopRightRadius: 10, marginBottom: heightScale(150) }}>
            {this.headerView()}
            <DeveloperScore
                S_No={"1"}
                header={res.strings.Number_of_year_in_business}
                score={developerDetailedScore.noOfYrsInBusinessScore ? parseFloat((developerDetailedScore.noOfYrsInBusinessScore).toFixed(1)) : 0}
                weight={developerDetailedWeight.noOfYrsInBusinessWeight ?  parseFloat((developerDetailedWeight.noOfYrsInBusinessWeight).toFixed(1)) : ""}
                scoreParam={developerDetailedScoreParameters.noOfYrsInBusiness ? parseFloat((developerDetailedScoreParameters.noOfYrsInBusiness).toFixed(1)) : ""}
            />
            <DeveloperScore
                isDark={true}
                S_No={"2"}
                header={res.strings.Number_of_project_Delivered}
                score={developerDetailedScore.deliveredProjectsScore ? parseFloat((developerDetailedScore.deliveredProjectsScore).toFixed(1)) : ""}
                weight={developerDetailedWeight.deliveredProjectsWeight ? parseFloat((developerDetailedWeight.deliveredProjectsWeight).toFixed(1)) : ""}
                scoreParam={developerDetailedScoreParameters.deliveredProjects ? parseFloat((developerDetailedScoreParameters.deliveredProjects).toFixed(1)) : ""}
            />
            <DeveloperScore
                S_No={"3"}
                header={res.strings.Number_of_unit_delivered}
                score={developerDetailedScore.unitsDeliveredScore ? parseFloat((developerDetailedScore.unitsDeliveredScore).toFixed(1)) : ""}
                weight={developerDetailedWeight.unitsDeliveredWeight ? parseFloat((developerDetailedWeight.unitsDeliveredWeight).toFixed(1)) : ""}
                scoreParam={developerDetailedScoreParameters.unitsDelivered ? parseFloat((developerDetailedScoreParameters.unitsDelivered).toFixed(1)) : ""}
            />
            <DeveloperScore
                S_No={"4"}
                isDark={true}
                header={res.strings.Geographical_Presence}
                score={developerDetailedScore.geographicalPresenceScore ? parseFloat((developerDetailedScore.geographicalPresenceScore).toFixed(1)) : ""}
                weight={developerDetailedWeight.geographicalPresenceWeight ? parseFloat((developerDetailedWeight.geographicalPresenceWeight).toFixed(1)) : ""}
                scoreParam={developerDetailedScoreParameters.geographicalPresence ? developerDetailedScoreParameters.geographicalPresence : ""}
            />
            <DeveloperScore
                S_No={"5"}
                header={res.strings.RERA_COMPLIANCE}
                score={developerDetailedScore.reraComplainceScore ? parseFloat((developerDetailedScore.reraComplainceScore).toFixed(1)) : ""}
                weight={developerDetailedWeight.reraComplainceWeight ? parseFloat((developerDetailedWeight.reraComplainceWeight).toFixed(1)) : ""}
                scoreParam={developerDetailedScoreParameters.reraComplaince ? parseFloat((developerDetailedScoreParameters.reraComplaince).toFixed(1)) : ""}
            />
            <DeveloperScore
                S_No={"6"}
                isDark={true}
                header={res.strings.Number_of_complain}
                score={developerDetailedScore.reraComplaintsScore ? parseFloat((developerDetailedScore.reraComplaintsScore).toFixed(1)) : ""}
                weight={developerDetailedWeight.reraComplaintsWeight ? parseFloat((developerDetailedWeight.reraComplaintsWeight).toFixed(1)) : ""}
                scoreParam={developerDetailedScoreParameters.reraComplaints ? parseFloat((developerDetailedScoreParameters.reraComplaints).toFixed(1)) : ""}
            />
            <DeveloperScore
                S_No={"7"}
                header={res.strings.Number_of_award}
                score={developerDetailedScore.awardsScore ? parseFloat((developerDetailedScore.awardsScore).toFixed(1)) : ""}
                weight={developerDetailedWeight.awardsWeight ? parseFloat((developerDetailedWeight.awardsWeight).toFixed(1)) : ""}
                scoreParam={developerDetailedScoreParameters.awards ? parseFloat((developerDetailedScoreParameters.awards).toFixed(1)) : ""}
            />
            <DeveloperScore
                S_No={"8"}
                isDark={true}
                header={res.strings.Diversification_of_asset_classes}
                score={developerDetailedScore.classesScore ? parseFloat((developerDetailedScore.classesScore).toFixed(1)) : ""}
                weight={developerDetailedWeight.classesWeight ? parseFloat((developerDetailedWeight.classesWeight).toFixed(1)) : ""}
                scoreParam={developerDetailedScoreParameters.classes ? developerDetailedScoreParameters.classes : ""}
            />
            <DeveloperScore
                S_No={"9"}
                header={res.strings.RERA_Registration}
                score={developerDetailedScore.reraRegistrationScore ? parseFloat((developerDetailedScore.reraRegistrationScore).toFixed(1)) : ""}
                weight={developerDetailedWeight.reraRegistrationWeight ? parseFloat((developerDetailedWeight.reraRegistrationWeight).toFixed(1)) : ""}
                scoreParam={developerDetailedScoreParameters.reraRegistration ? developerDetailedScoreParameters.reraRegistration : ""}
            />
            <DeveloperScore
                S_No={"10"}
                isDark={true}
                header={res.strings.Customer_satisfacti_on_survey_score}
                score={developerDetailedScore.custSurveyScore ? parseFloat((developerDetailedScore.custSurveyScore).toFixed(1)) : ""}
                weight={developerDetailedWeight.custSurveyWeigh ? parseFloat((developerDetailedWeight.custSurveyWeight).toFixed(1)) : ""}
                scoreParam={developerDetailedScoreParameters.custSurvey ? parseFloat((developerDetailedScoreParameters.custSurvey).toFixed(1)) : ""}
            />

            <View style={{ marginTop: heightScale(30) }}>
                <TotalScore
                    isDark={true}
                    header={res.strings.Max_Score}
                    value={parseFloat((this.props.maxScore).toFixed(1))}
                />
                <TotalScore

                    isDark={true}
                    header={res.strings.Weighted_Average}
                    value={parseFloat((this.props.weightedAvrg).toFixed(1))}
                />


            </View>
        </View>
    }

    headerView = () => {
        return <View style={styles.headerText}>
            <View>
                <Text style={[styles.headerTextStyle, { marginLeft: 10 }]}>{res.strings.S_NO}</Text>
            </View>
            <View style={{ alignItems: 'flex-start' }}>
                <Text style={[styles.headerTextStyle, { textAlign: 'left' }]}>{res.strings.PARAMETER}</Text>
            </View>
            <View>
                <Text style={[styles.headerTextStyle, { marginRight: 10 }]}>{res.strings.SCORE}</Text>
            </View>
            <View>
                <Text style={[styles.headerTextStyle, { marginRight: 10 }]}>{res.strings.Weight}</Text>
            </View>
            <View>
                <Text style={[styles.headerTextStyle, { marginRight: 10 }]}>{res.strings.SCORE}</Text>
                <Text style={[styles.headerLowerTextStyle, { marginRight: 10 }]}>{res.strings.Paremeter}</Text>
            </View>
        </View>
    }

    render() {

        console.log("DEVELOPER MODAL", this.props)
        return (
            <View style={styles.container}>
                <View style={[styles.shadowHeader, { height: 42, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }]}>
                    <Text style={styles.shadowVastuHeader}>{res.strings.DEVELOPER_SCORE}</Text>
                </View>
                <ScrollView>

                    {this.DeveloperScore()}



                </ScrollView>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        top: Platform.OS == 'ios' ? 100 : 100,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        backgroundColor: res.colors.butterscotch
    }, headerText: {
        borderTopRightRadius: 10, borderTopLeftRadius: 10,
        height: 40,
        flexDirection: 'row',
        justifyContent: 'space-between',
        //  marginHorizontal: 10,
        alignItems: 'center',
        backgroundColor: 'rgb(242,177,63)'
    }, headerTextStyle: {
        fontFamily: res.fonts.medium,
        fontSize: 11,
        color: res.colors.purpleBrown,
        textAlign: 'center'
    }, headerLowerTextStyle: {
        fontFamily: res.fonts.medium,
        fontSize: 8,
        color: res.colors.purpleBrown,
        textAlign: 'center'
    },
    shadowHeader: {
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        backgroundColor: res.colors.butterscotch,
        shadowColor: res.colors.shadowColor,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 10.58,
        shadowRadius: 10.00,
        elevation: 24,
    }, shadowVastuHeader: {
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        backgroundColor: res.colors.butterscotch,
        fontFamily: res.fonts.semiBold,
        fontSize: 18,
        letterSpacing: 0.3,
        textAlign: 'center',
        color: res.colors.greyishBrown,

    },
})
export default DeveloperScoreModal;