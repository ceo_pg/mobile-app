import React, { Component } from "react";
import { View, Text, FlatList } from "react-native";
import res from '../../../../../res'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import TitleWithValue from '../../../../components/card/TitleWithValue'
import styles from './styles'
import moment from 'moment';
import { widthScale, heightScale } from '../../../../utility/Utils';


class IndexOfChargesModal extends Component {

    constructor(props) {
        super(props);

        this.state = {
            Name: [
                {
                    name: "SRN"
                }, {
                    name: "Charge ID"
                }, {
                    name: "Holder Name"
                }, {
                    name: "Date of Creation"
                }, {
                    name: "Date of Modification"
                }, {
                    name: "Amount"
                }, {
                    name: "Address"
                }
            ]
        };
    }

    renderIndexOfCharge = ({ item, index }) => {
        const value = item
        delete value['_id']
        delete value['dateOfSatisfaction']
        const chargesValue = Object.values(value)


        return (
            <View style={[{ flex: 1,marginBottom:50 ,flexDirection: 'row', flexWrap: 'wrap', width: '95%', marginHorizontal: widthScale(10),  borderRadius: 5 }, styles.shadow]}>

                {this.state.Name.map((item, index) => {

                    return (<TitleWithValue
                        indexOfChangesView={{ marginBottom: 15 }}
                        indexOfChangesHeader={{ top: 0 }}
                        indexOfChangesValue={{ top: 10 }}
                        title={item.name}
                        Value={chargesValue[index]

                        }
                    />)
                })}

            </View>
        )
    }


    render() {
        const { indexOfCharges } = this.props;


        return (
            < View style={styles.modalStyle} >


                <View style={[styles.shadowHeader, { height: 42 }]}>
                    <Text style={styles.corousalNameText}>{res.strings.INDEX_OF_CHARGES}</Text>
                </View>
                {
                    indexOfCharges && indexOfCharges.length > 0
                    && <FlatList
                        style={{marginTop:10}}
                        keyExtractor={(item, index) => index.toString()}
                        data={indexOfCharges}
                        renderItem={this.renderIndexOfCharge}

                    />

                }
            </ View>
        )
    }
}
export default IndexOfChargesModal