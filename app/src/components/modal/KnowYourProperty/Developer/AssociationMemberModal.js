import React, { Component } from "react";
import { View, Text, SafeAreaView } from "react-native";
import res from '../../../../../res'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import CardWithTitle from '../../../../components/card/CardWithTitle'
import styles from './styles'


class AssociationMemberModal extends Component {

    constructor(props) {
        super(props);

    }




    render() {
        console.log("PartnersAndLocationsModal", this.props)
        return (
            < View style={styles.modalStyle} >


                <View style={[styles.shadowHeader, { height: 42 }]}>
                    <Text style={styles.corousalNameText}>{"Association Memberships"}</Text>
                </View>
                <KeyboardAwareScrollView style={{ flex: 1, marginTop: 10, alignSelf: 'center' }}>
                    <View style={{ flexDirection: 'row', flexWrap: 'wrap', width: '100%', justifyContent: 'space-evenly' }}>

                        {this.props.memberShip.map((item, index) => {
                            return (

                                <CardWithTitle uri={item.url} imageStyle={{ resizeMode: 'stretch' }} />)
                        })}
                        <CardWithTitle isShodow={true} uri={""} imageStyle={{ resizeMode: 'stretch' }} title={""} />
                    </View>

                </KeyboardAwareScrollView>
            </ View>
        )
    }
}
export default AssociationMemberModal