import React, { Component } from "react";
import { View, Text, FlatList } from "react-native";
import res from '../../../../../res'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import CardWithHeader from '../../../../components/card/CardWithHeader'
import styles from './styles'


class PartnersAndLocationsModal extends Component {

    constructor(props) {
        super(props);

    }




    render() {
        console.log("PartnersAndLocationsModal", this.props)
        return (
            < View style={styles.modalStyle} >


                <View style={[styles.shadowHeader, { height: 42 }]}>
                    <Text style={styles.corousalNameText}>{res.strings.PARTNERS_AND_LOCATION}</Text>
                </View>
                <KeyboardAwareScrollView>
                    <View>
                        <CardWithHeader title={res.strings.PARTNERS} Value={this.props.nameOfPartners ? this.props.nameOfPartners : ""} src={res.images.partners} titleStyle={{ textAlign: 'left' }} />
                    </View>
                    <View>
                        <CardWithHeader title={res.strings.COROPORATE_ADDRESS} Value={this.props.corporateAdress ? this.props.corporateAdress : ""} src={res.images.address} titleStyle={{ textAlign: 'left' }} />
                    </View>
                </KeyboardAwareScrollView>
            </ View>
        )
    }
}
export default PartnersAndLocationsModal