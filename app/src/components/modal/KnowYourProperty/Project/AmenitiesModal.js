import React, { Component } from "react";
import { View, Text, Image, TouchableOpacity, TextInput, StyleSheet, FlatList, Keyboard } from "react-native";

import res from '../../../../../res'
import styles from './styles'
import { widthScale } from "../../../../utility/Utils";



class AmenitiesModal extends Component {

    constructor(props) {
        super(props);

        this.state = {
            searchResult: "",
            data: []
        };
    }
    componentDidMount() {
        this.setState({
            data: this.props.list
        })


    }



    render() {
        
        return (
            <View style={styles.container}>
                <View style={[styles.shadowHeader, { height: 42 }]}>
                    <Text style={styles.corousalNameText}>Amenities</Text>
                </View>
                <FlatList
                    style={{ marginHorizontal: 20 }}
                    numColumns={2}
                    data={this.props.data}
                    keyExtractor={(item, index) => index.toString()}
                    renderItem={({ item, index }) => (

                        <View style={[styles.amenitiesList, styles.shadow]}>
                            
                            <Image style={{ width: widthScale(45), height:widthScale(45) }} source={ item.url?{ uri: item.url }:{}} resizeMode={'contain'} />
                            <Text style={styles.amenitiesText}>{item.name}</Text>
                        </View>
                    )}

                />
            </View>
        );
    }
}

export default AmenitiesModal;