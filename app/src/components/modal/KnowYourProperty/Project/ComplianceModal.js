import React, { Component } from "react";
import { View, Text, Image, Clipboard, TouchableOpacity, FlatList, Linking } from "react-native";
import Modal from 'react-native-modal'
import ImageZoom from 'react-native-image-pan-zoom';
import PDFView from 'react-native-view-pdf';
import res from '../../../../../res'
import styles from './styles'
import SimpleToast from 'react-native-simple-toast'
import { widthScale } from '../../../../utility/Utils'
import Pdf from 'react-native-pdf';

class ComplianceModal extends Component {

    constructor(props) {
        super(props);

        this.state = {
            cuponCode: "HGT5855DR56",
            isModalVisible: false,
            currentIndex: 0
        };
    }

    copyCuponText = (item) => {
        const { cuponCode } = this.state;
        Clipboard.setString(item)
        SimpleToast.show(res.strings.COPIED_CODE_MSG)
    }
    markCheck = (item, index) => {
        // console.log("SELECTED ITEM", item, index)
    }
    toggleModal = (index, useItem) => {
        const { isModalVisible } = this.state
        if (!useItem.key) {
            alert("No Document Found")
        } else {

            this.setState({
                isModalVisible: !isModalVisible, currentIndex: index
            })

        }


    }
    renderComplience = ((item, index) => {

        const useItem = item.item

        return < TouchableOpacity onPress={() => this.toggleModal(item.index, useItem)} style={[styles.complianceView, styles.shadow]}>
            <View style={{ marginHorizontal: 10 }}>
                <View style={styles.CheckView}>
                    <Text style={styles.reraRegisterText}>{useItem.name}</Text>
                    <TouchableOpacity onPress={() => this.markCheck(item, index)} style={{ width: widthScale(20), height: widthScale(20) }}>
                        <Image source={res.images.round_liteblue_check} style={{ flex: 1 }} resizeMode={'contain'} />
                    </TouchableOpacity>
                </View>

                <View style={{ flexDirection: 'row', paddingTop: 20 }}>

                    <View style={{ width: 40, height: 50 }}>
                        <Image source={res.images.icn_pdf} resizeMode={'contain'} />
                    </View>

                    <View style={{ paddingLeft: 10, flexDirection: 'column', width: "80%" }}>
                        <View>
                            <Text style={styles.reraProjectText}>{useItem.name + " of \nThe project"}</Text>
                            <Text style={styles.reraNumberText}>{useItem.docNumber}</Text>
                        </View>
                        <View>
                            {/* <TouchableOpacity onPress={() => this.copyCuponText(useItem.docNumber)} style={styles.clipBoardView}> */}
                            {/* <Text style={styles.couponText}>{useItem.docNumber}</Text> */}
                            {/* <Text style={styles.copyText}>{"Copy"}</Text> */}
                            {/* </TouchableOpacity> */}
                        </View>
                    </View>

                </View>
            </View>



        </TouchableOpacity>


    })
    closeModal = () => {
        this.setState({
            isModalVisible: !this.state.isModalVisible
        })
    }
    ShareImage = (url) => {
        Linking.canOpenURL(url).then(supported => {
            if (supported) {
                Linking.openURL(url);
            } else {
                console.log("Don't know how to open URI: " + url);
            }
        });
    }
    renderDoc = (index) => {

        let modalDoc = this.props.documents[this.state.currentIndex]
        if (!modalDoc) {
            return
        }
        const resources = {
            file: Platform.OS === 'ios' ? 'downloadedDocument.pdf' : '/sdcard/Download/downloadedDocument.pdf',
            url: modalDoc.key,
            base64: 'JVBERi0xLjMKJcfs...',
        }
        const source = { uri: modalDoc.key, cache: true }
        const resourceType = 'url';
        if (!modalDoc.key) {
            return <View>
                <Text>No Document Found</Text>
            </View>
        }
        return <View style={{ flex: 1 }}>
            <View style={styles.closeBtn}>
                <TouchableOpacity onPress={() => this.closeModal()}>
                    <Image source={res.images.icnClose} style={{ width: 20, height: 20, tintColor: res.colors.white }} resizeMode={'center'} />
                </TouchableOpacity>
            </View>
            {
                modalDoc.key ?

                    modalDoc.key.includes('.pdf') ? <View style={{ borderRadius: 10, flex: 1 }}>

                        {
                            Platform.OS == 'ios' ?
                                <Pdf
                                    source={source}
                                    style={{ flex: 1 }}
                                />
                                :
                                <PDFView
                                    // fadeInDuration={250.0}
                                    style={{ flex: 1, }}
                                    resource={resources[resourceType]}
                                    resourceType={resourceType}
                                    onLoad={() => console.log(`PDF rendered from ${resourceType}`)}
                                    onError={(error) => console.log('Cannot render PDF', error)}
                                />
                        }
                    </View> :
                        <ImageZoom cropWidth={Dimensions.get('window').width}
                            cropHeight={Dimensions.get('window').height}
                            imageWidth={Dimensions.get('window').width}
                            imageHeight={Dimensions.get('window').height}>

                            <Image source={{ uri: modalDoc.key }} style={{ flex: 1 }} resizeMode={'cover'} />

                        </ImageZoom>
                    :
                    <View />


            }
             {
                modalDoc.key ?
                    <TouchableOpacity style={styles.ShareBtn} onPress={() => this.ShareImage(modalDoc.key)}>
                        <View style={styles.shareView}>
                            <Image source={res.images.icn_share} />
                        </View>
                    </TouchableOpacity>
                    : null
            }


        </View>
    }
    modalView = () => {
        const { currentIndex } = this.state

        return (
            <View style={{ flex: 1 }}>

                <View style={{ width: "100%", height: "100%", zIndex: 9999, overflow: 'hidden' }}>

                    {this.renderDoc(currentIndex)}

                </View>

            </View>



        )
    }


    render() {

        let modalDoc = this.props.documents[this.state.currentIndex]
        return (
            <View style={styles.container}>
                {
                    modalDoc.key ?
                        <Modal
                            style={{ borderRadius: 10 }}
                            isVisible={this.state.isModalVisible}
                        >
                            {this.modalView()}
                        </Modal> : null
                    // alert("No Document Found")
                }

                <View style={[styles.shadowHeader, { height: 42 }]}>
                    <Text style={styles.corousalNameText}>Compliance</Text>
                </View>
                <FlatList
                    data={this.props.documents}
                    keyExtractor={(item, index) => index.toString()}
                    renderItem={this.renderComplience}
                />

            </View >
        );
    }
}

export default ComplianceModal;