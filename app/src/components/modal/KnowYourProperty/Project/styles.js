import { StyleSheet, Platform } from 'react-native'
import res from '../../../../../res'
import { heightScale, widthScale } from '../../../../utility/Utils'

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: heightScale(100),
        // borderWidth: 1,
        // borderColor: "#696969",
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        backgroundColor: res.colors.butterscotch,
        overflow: 'hidden'
        // marginHorizontal: 16,
        // paddingHorizontal: 16,
        // paddingVertical: 16,
        // backgroundColor: res.colors.btnBlue
    }, amenitiesList: {
        flex: 1,
        margin: 5,
        backgroundColor: res.colors.butterscotch,
        height: 104,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 5,
        flexDirection: 'column',
        padding: 5
    }, shadow: {
        backgroundColor: res.colors.butterscotch,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,

    }, amenitiesText: {
        paddingHorizontal: 5,
        marginTop: 10,
        fontFamily: res.fonts.medium,
        fontSize: 14,
        color: res.colors.greyishBrown,
        textAlign: 'center'
    }, modalStyel: {
        flex: 1, backgroundColor: res.colors.butterscotch, borderTopLeftRadius: 30, borderTopRightRadius: 30, top: 100,
    }, specificationText: {
        fontFamily: res.fonts.medium,
        fontSize: 14,
        color: res.colors.greyishBrown,
        textAlign: 'center',
        top: 10,
        marginHorizontal: 10
    }, UnitLevel: {
        flex: 1,
        minHeight: 110,
        marginHorizontal: 10,
        backgroundColor: res.colors.butterscotch,
        marginTop: 10,
        borderRadius: 5
    }, consultentModal: {
        flex: 1, backgroundColor: res.colors.butterscotch, borderRadius: 30, marginHorizontal: 10
    }, specificationvalueText: {
        fontFamily: res.fonts.medium,
        fontSize: 11,
        color: res.colors.greyishBrown,
        textAlign: 'center',
        top: 10,

        paddingLeft: 10,
        paddingRight: 10
    }, shadowHeader: {
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        backgroundColor: res.colors.butterscotch,
        shadowColor: res.colors.shadowColor,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 10.58,
        shadowRadius: 10.00,
        elevation: 24,
    }, corousalNameText: {
        fontFamily: res.fonts.semiBold,
        fontSize: 18,
        letterSpacing: 0.3,
        textAlign: 'center',
        color: res.colors.greyishBrown,
        marginTop: 10
    }, consultentTextStyle: {
        marginLeft: 12, marginTop: 10, color: res.colors.liteBlack, fontFamily: res.fonts.regular
    }, consultentOtherTextStyle: {
        marginLeft: 12, color: res.colors.liteBlack, fontFamily: res.fonts.regular
    }, consultentWebTextStyle: {
        marginLeft: 12, color: 'rgba(29,29,29,0.5)', fontFamily: res.fonts.regular
    }, projectModalStyel: {
        flex: 1, backgroundColor: res.colors.butterscotch, borderTopLeftRadius: 30, borderTopRightRadius: 30, top: 100
    }, dataContainer: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        width: '100%',
        justifyContent: 'space-evenly'
    }, complianceView: {

        minHeight: 135,
        marginHorizontal: 10,
        backgroundColor: res.colors.trasparents,
        // marginTop: 10,
        borderRadius: 5,
        marginBottom: heightScale(10)
    },
    reraRegisterText: {
        fontFamily: res.fonts.medium,
        fontSize: 14,
        color: res.colors.greyishBrown,
        marginTop: 10,

    },
    reraProjectText: {
        fontFamily: res.fonts.regular,
        fontSize: 12,
        color: res.colors.liteBlack,
        letterSpacing: 0.13

    },
    reraNumberText: {
        fontFamily: res.fonts.regular,
        fontSize: 12,
        color: 'rgba(29,29,29,0.5)',
        letterSpacing: 0.13,
        marginTop: 5
    }, clipBoardView: {
        flexDirection: 'row',
        height: 30,
        borderWidth: 1,
        borderColor: res.colors.shadowColor,
        borderRadius: 5,
        alignItems: 'center',
        justifyContent: 'space-between',
        padding: 5,
        marginVertical: 5,
        // width:"70%"
    }, copyText: {
        fontFamily: res.fonts.medium,
        fontSize: 12,
        color: res.colors.peacockBlue,
        letterSpacing: 0.13,
        marginLeft: widthScale(30)
    }, couponText: {
        fontFamily: res.fonts.regular,
        fontSize: 12,
        color: 'rgba(29,29,29,0.5)',
        letterSpacing: 0.13
    }, cusultentText: {
        marginLeft: 10,
        marginVertical: heightScale(13),
        color: res.colors.greyishBrown
    }, CheckView: {
        flexDirection: 'row',
        justifyContent: 'space-between',

        alignItems: 'center',
        marginTop: heightScale(10)
    }, closeBtn: {
        position: "absolute",
        top: 30,
        right: 30,
        backgroundColor: res.colors.black,
        zIndex: 999,
        width: 30,
        height: 30,
        borderRadius: 15,
        alignItems: 'center',
        justifyContent: 'center'
    }, ShareBtn: {
        position: 'absolute',
        bottom: 30,
        right: 30,
        backgroundColor:res.colors.white,
        borderRadius:10,
        zIndex: 9999999,
      }, ShareBtn: {
        position: 'absolute',
        bottom: 30,
        right: 30,
        backgroundColor:res.colors.black,
        borderRadius:25,
        zIndex: 9999999,
        width:50,
        height:50,
        overflow:'hidden',
        justifyContent:'center',
        alignItems:'center'
      },shareView:{
          backgroundColor:res.colors.white,
          borderRadius:15,
          width:30,
          height:30
      }
})
export default styles