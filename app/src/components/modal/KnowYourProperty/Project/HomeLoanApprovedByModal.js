import React, { Component } from "react";
import { View, Text, Image, FlatList } from "react-native";

import CardWithImages from '../../../../components/card/CardWithImages'

import styles from './styles'
import resources from "../../../../../res";



class HomeLoanApprovedByModal extends Component {

    constructor(props) {
        super(props);

    }




    render() {
        console.log("PROJECT APPROVE", this.props.data)
        if (!this.props.data||this.props.data.length==0) {
            return <View style={[styles.container,{justifyContent:'center',alignItems:'center'}]}>
                <Text>{resources.strings.NO_DATA_FOUND}</Text>
                </View>

        }
        return (
            <View style={styles.container}>
                <View style={[styles.shadowHeader, { height: 42 }]}>
                    <Text style={styles.corousalNameText}>Project Approved By</Text>
                </View>
                <FlatList
                    style={{ marginHorizontal: 20 }}
                    numColumns={2}
                    data={this.props.data}
                    keyExtractor={(item, index) => index.toString()}
                    renderItem={({ item, index }) => (

                        <CardWithImages
                            src={item.url ? { uri: item.url } : ""}
                        />
                    )}

                />
            </View>
        );
    }
}

export default HomeLoanApprovedByModal;