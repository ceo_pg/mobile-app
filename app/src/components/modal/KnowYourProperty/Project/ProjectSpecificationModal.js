import React, { Component } from "react";
import { View, Text, Image, TouchableOpacity, TextInput, StyleSheet, FlatList, Keyboard, SafeAreaView } from "react-native";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

import res from '../../../../../res'
import styles from './styles'
import CardWithTitle from '../../../card/CardWithTitle'


class ProjectSpecificationModal extends Component {

    constructor(props) {
        super(props);

        this.state = {

        };
    }





    render() {
        const projectSpecification = this.props && this.props.data ? this.props.data : {}
        console.log("projectSpecification", projectSpecification);

        console.log(projectSpecification);

        return (< SafeAreaView style={styles.projectModalStyel} >
            <View style={[styles.shadowHeader, { height: 42 }]}>
                <Text style={styles.corousalNameText}>Project Specifications</Text>
            </View>
            <KeyboardAwareScrollView
                style={{ marginBottom: 100 }}
                enableOnAndroid={true}
            >

                <View style={styles.dataContainer}>
                    {
                        projectSpecification.pipedGas == true ?
                            <CardWithTitle
                                isProjectSpecs={true}
                                src={res.images.pipeline}
                                title={"PipedGas"}
                            />
                            :

                            <View />
                    }
                    <CardWithTitle
                        isProjectSpecs={true}
                        src={res.images.battery}
                        title={"Power Back up"}
                        Value={projectSpecification.powerBackupCapacity}
                    />




                    {
                        projectSpecification.pipedGas == true ?
                            <CardWithTitle
                                isProjectSpecs={true}
                                src={res.images.blueprint}
                                title={"Structure"}
                                Value={projectSpecification.structure}
                            /> :

                            <View />
                    }
                    <CardWithTitle
                        isProjectSpecs={true}
                        src={res.images.fireExtinguisher}
                        title={"Fire Fighting"}
                        Value={projectSpecification.fireFighting}
                    />



                    {
                        projectSpecification.waterSoftener == true ?
                            <CardWithTitle
                                isProjectSpecs={true}
                                src={res.images.plumber}
                                title={"Water Softner"}
                            />
                            : <View />
                    }


                    {
                        projectSpecification.solarPower == true ?
                            <CardWithTitle
                                isProjectSpecs={true}
                                src={res.images.solarPanel}
                                title={"Solar Power"}
                            />
                            :
                            <View />
                    }




                    <CardWithTitle
                        isProjectSpecs={true}
                        src={res.images.facade}
                        title={"Facade"}
                        Value={projectSpecification.facade}
                    />

                    <CardWithTitle
                        imageStyle={{ opacity: 0.7 }}
                        isProjectSpecs={true}
                        src={res.images.water}
                        title={"Water Source"}
                        Value={projectSpecification.waterSource}
                    />
                    <CardWithTitle
                        isShodow={true}
                        isProjectSpecs={true}
                    // src={res.images.water}
                    // title={"Water Source"}
                    // Value={projectSpecification.waterSource}
                    />
                </View>



            </KeyboardAwareScrollView>


        </SafeAreaView >
        );
    }
}

export default ProjectSpecificationModal;