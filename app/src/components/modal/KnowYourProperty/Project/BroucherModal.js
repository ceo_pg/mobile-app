import React, { Component } from "react";
import { View, Text, Image, Dimensions, TouchableOpacity, FlatList } from "react-native";
import ImageZoom from 'react-native-image-pan-zoom';
import Modal from 'react-native-modal'
import res from '../../../../../res'
import styles from './styles'



class BroucherModal extends Component {

    constructor(props) {
        super(props);

        this.state = {

        };
    }
    renderModal = ((item, index) => {
        const height = 100
        const step = 1 / (2 * height);
        return (
                    <ImageZoom cropWidth={Dimensions.get('window').width}
                        cropHeight={Dimensions.get('window').height}
                        imageWidth={Dimensions.get('window').width}
                        imageHeight={Dimensions.get('window').height}>

                        <Image source={item.fileName ? { uri: item.fileName } : ""} style={{ flex: 1 }} resizeMode={'cover'} />

                    </ImageZoom>
              

        );
    })
    render() {
        console.log("BROUCHER MODAL", this.props)
        return (
            <View style={{ borderRadius: 10 }}>
                <View style={styles.closeBtn}>
                    <TouchableOpacity onPress={() => this.props.toggleModal}>
                        <Image source={res.images.icnClose} style={{ width: 20, height: 20, tintColor: res.colors.white }} resizeMode={'center'} />
                    </TouchableOpacity>
                </View>
                <FlatList
                    style={{ marginHorizontal: -20 }}
                    bounces={false}
                    horizontal={true}
                    data={this.props.brochure}
                    renderItem={this.renderSlotsImages}
                />
            </View>



        )

    }
}

export default BroucherModal;