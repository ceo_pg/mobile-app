import React, { Component } from "react";
import { View, Text, Image, TouchableOpacity, TextInput, StyleSheet, FlatList, Keyboard, Linking } from "react-native";

import res from '../../../../../res'
import styles from './styles'
import { heightScale, getPastProject } from "../../../../utility/Utils";



class ConsultantsModal extends Component {

    constructor(props) {
        super(props);

        this.state = {
            searchResult: "",
            data: []
        };
    }
    componentDidMount() {
        this.setState({
            data: this.props.list
        })


    }



    openWebSite = (url) => {
        Linking.canOpenURL(url).then(supported => {
            if (supported) {
                Linking.openURL(url);
            } else {
                console.log("Don't know how to open URI: " + url);
            }
        });
        // this.props.navigation.navigate("WebViewDisplay", {
        //     url: url
        // })
    }
    render() {
        // console.log("DEVELOPER PROPS", this.props.list)

        const { searchResult, error, data } = this.state;
        console.log("=====>", this.props)


        const { closeModal } = this.props;
        return (
            <View style={styles.container}>
                <View style={[styles.shadowHeader, { height: 42 }]}>
                    <Text style={styles.corousalNameText}>Consultants</Text>
                </View>

                <FlatList
                    data={this.props.data}
                    keyExtractor={(item, index) => index.toString()}
                    renderItem={({ item, index }) => (
                        console.log("Consultatent modal", item),
                        < View style={[styles.UnitLevel, styles.shadow]}>
                            {
                                item.consultantType ?
                                    <Text style={styles.cusultentText}>{item.consultantType}</Text>
                                    : null
                            }

                            <View style={{ flexDirection: 'row', marginHorizontal: 10, }}>
                                <View style={{ justifyContent: 'center' }}>
                                    <Image style={{ width: 42, height: 42 }} source={item.url ? { uri: item.url } : ""} resizeMode={'center'} />
                                </View>
                                <View style={{ flexDirection: 'column', width: '80%', marginLeft: 20, justifyContent: 'center', marginVertical: 20 }}>
                                    {
                                        item.consultantName ?
                                            <View>
                                                <Text style={styles.consultentTextStyle}>{item.consultantName}</Text>
                                            </View> : null
                                    }

                                    <View>
                                        {
                                            item.website ?
                                                <TouchableOpacity onPress={() => this.openWebSite(item.website)}>
                                                    <Text style={styles.consultentWebTextStyle}>{item.website}</Text>
                                                </TouchableOpacity> : null
                                        }

                                    </View>
                                    <View style={{ overflow: 'hidden' }}>
                                        {
                                            item.pastProjects.length > 0 &&
                                            <Text style={styles.consultentOtherTextStyle}>{getPastProject(item.pastProjects)}</Text>

                                        }

                                    </View>


                                </View>

                            </View>

                        </View>
                    )}

                />
            </View >
        );
    }
}

export default ConsultantsModal;