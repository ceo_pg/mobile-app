import React, { Component } from "react";
import { View, Text, Image, FlatList } from "react-native";

import CardWithImages from '../../../../components/card/CardWithImages'

import styles from './styles'
import resources from "../../../../../res";



class ProjectOwnerShipModal extends Component {

    constructor(props) {
        super(props);

    }




    render() {
        const projectOwnershipStructure = this.props.projectOwnershipStructure
        console.log("projectOwnershipStructure", projectOwnershipStructure)
        if (!projectOwnershipStructure||projectOwnershipStructure.developer) {
            return  <View style={[styles.container,{alignItems:'center',justifyContent:'center'}]}>
                <Text>{resources.strings.NO_DATA_FOUND}</Text>
                </View>
        }
        return (
            <View style={styles.container}>
                <View style={[styles.shadowHeader, { height: 42 }]}>
                    <Text style={styles.corousalNameText}>Project Ownership Structure</Text>
                </View>
                 <FlatList
                    style={{ marginHorizontal: 20 }}
                    numColumns={2}
                    data={this.props.projectOwnershipStructure}
                    keyExtractor={(item, index) => index.toString()}
                    renderItem={({ item, index }) => (

                        <CardWithImages
                            src={item.url ? { uri: item.url } : ""}
                        />
                    )}

                /> 
            </View>
        );
    }
}

export default ProjectOwnerShipModal;