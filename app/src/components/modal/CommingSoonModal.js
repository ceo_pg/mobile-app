import React, { Component } from 'react'
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native'
import Modal from 'react-native-modal'
import res from '../../../res'
import { heightScale, widthScale } from '../../utility/Utils'
class CommingSoonModal extends Component {
    constructor(props) {
        super(props)
        this.state = {

            CommingSoonModal: false,
        }
    }
    renderCommingSoon = () => {
        return <View style={[styles.boxView,]}>

            <Text style={styles.commingSoonText}>{res.strings.Comming_Soon}</Text>

            <TouchableOpacity onPress={() => this.props.toggleModal()} style={styles.button}>
                <Text style={styles.btnTextStyle}>OK</Text>
            </TouchableOpacity>
        </View>
    }

    render() {
        return <Modal isVisible={this.props.isToggle}
            onSwipeComplete={() => this.props.toggleModal()}
            swipeDirection={'down'}
            onBackdropPress={() => this.props.toggleModal()}
            style={{ zIndex: 99999 }}>
            {this.renderCommingSoon()}
        </Modal>
    }
}
const styles = StyleSheet.create({

    otpContainerView: {
        backgroundColor: res.colors.white,
        height: heightScale(150),
        marginTop: heightScale(150),
        borderRadius: widthScale(24),
        // justifyContent: "center",
        // alignItems: 'center'
        // marginHorizontal: widthScale(15)
    }, commingSoonText: {
        fontFamily: res.fonts.bold,
        fontSize: widthScale(20),
        color: res.colors.appColor,
        marginVertical: heightScale(30)
    }, boxView: {
        marginHorizontal: 20,
        // height: heightScale(234),
        // width: "100%",
        backgroundColor: res.colors.white,
        marginTop: heightScale(50),
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 12
    }, btnTextStyle: {
        textAlign: 'center',
        fontFamily: res.fonts.semiBold,
        color: res.colors.formLabelGrey,
        fontSize: 18,
        letterSpacing: 0.86,
        lineHeight: heightScale(24)
    }, button: {
        height: heightScale(44),
        alignItems: 'center',
        backgroundColor: res.colors.butterscotch,
        width: '100%',
        borderBottomLeftRadius: 12,
        borderBottomRightRadius: 12,
        justifyContent: 'center'
    },

})
export default CommingSoonModal