import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image } from 'react-native';
import strings from '../../../res/constants/strings';
import res from '../../../res';
import { widthScale, heightScale } from '../../utility/Utils'
// import Input from '../../components/input/OtpField'



class OtpModal extends Component {
    constructor(props) {
        super(props);
        this.state = {

        }
    }
    componentDidMount() {

    }

    render() {
        const {closeModal} = this.props
        return (
            <View style={styles.containerView}>
                <TouchableOpacity style={styles.closeBtnView} onPress={()=> closeModal()}>
                <Image source={res.images.icnClose} resizeMode={'contain'} style={{height:40,width:40}}/>
                </TouchableOpacity>
                <Text style={styles.shareNowText}>{strings.SHARE_NOW}</Text>
                <View style={styles.iconsCOntainer}>
                {/* <Input
                    value={this.state.otp1}
                    onChangeText={this.onOtp1Change}
                    containerStyle={styles.textInputStyle}
                    inputProps={{
                        maxLength: 1,
                        keyboardType: "numeric",
                        secureTextEntry: true,
                        returnKeyType: "next",
                        ref: this.otpFirst,
                        textAlign: "center",
                    }}
                    inputStyles={{ borderBottomWidth: 0, width: 40, paddingVertical: 5, fontSize: 18, }}
                /> */}
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({

    containerView: {
        backgroundColor: res.colors.butterscotch,
        height: heightScale(250),
        marginTop: heightScale(150),
        borderRadius: widthScale(24),
        marginHorizontal: widthScale(15)
    },
    textInputStyle: {
        padding: 0,
        paddingBottom: 0,
        paddingLeft: 0,
        paddingVertical: 0,
        color: res.colors.white,
        fontFamily: 'Montserrat-Regular',
        borderBottomColor: res.colors.underlineColor,
        borderBottomWidth:2,
        height:50,

    },
    shareNowText: {
        fontSize: widthScale(18),
        color: res.colors.blackTwo,
        fontFamily: res.fonts.semiBold,
        textAlign: 'center',
        marginTop: heightScale(52)
    },
    
    closeBtnView:{
      position:'absolute',
      right:10,
      height:40,
      width:40,
      top:10  
    }
})


export default OtpModal;

