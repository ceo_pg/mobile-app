import React, { Component } from "react";
import { View, Text, TouchableOpacity, Image, Modal } from "react-native";
import { connect } from "react-redux";
import { bindActionCreators } from "redux"
import { openShareModal } from "../../../redux/actions/SharePostAction";
import styles from "./styles"
import R from "../../../../res";
import { heightScale, widthScale, myWidth } from "../../../utility/Utils";

class SharePost extends Component {
    constructor(props) {
        super(props);
        this.state = {

            showCreatePostModal: false,
        }
    }
    openShareModal = () => {
        console.log("hitopenShareModal")
        this.props.openShareModal(true?false:true);
    }
    hide = () => {

        this.props.openShareModal(false);
    };
    close = () => {
        this.props.openShareModal(false,);
    }
    render() {
        console.log("SHAREMODAL", this.props)
        return (
            <Modal

                animationType={"fade"}
                transparent={true}
                visible={this.props.isVisiblePostModal}
                onRequestClose={this.close}>
                <View style={styles.modalContainer}>
                    <View style={styles.modalContent}>

                        <TouchableOpacity style={styles.saveButton} onPress={()=>this.openShareModal()}>
                            <Text style={styles.shareText}>{R.strings.shareButtonText}</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </Modal>
        )
    }
}
function mapStateToProps(state) {
    return {
        isVisiblePostModal: state.SharePostReducer.showSharePostModal,

    };
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        openShareModal,

    }, dispatch);
}

const ShareComponentModal = connect(mapStateToProps, mapDispatchToProps)(SharePost);
export default ShareComponentModal