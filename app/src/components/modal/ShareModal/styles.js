import { StyleSheet, Platform } from 'react-native'
import res from '../../../../res'
import { heightScale, widthScale, myWidth } from "../../../utility/Utils";

const styles = StyleSheet.create({
    modalContainer: {
        position: "absolute",
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        zIndex: 5,
        elevation: 5,
        justifyContent: "center",
        alignItems: "center",
        borderWidth: 0.1
    },
    modalContent: {
        marginHorizontal: widthScale(50),
        alignSelf: "stretch",
        borderRadius: 25,
        backgroundColor: "white",
        zIndex: 6,
        elevation: 6,
    },

})
export default styles