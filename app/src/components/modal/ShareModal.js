import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image } from 'react-native';
import strings from '../../../res/constants/strings';
import res from '../../../res';
import { widthScale, heightScale } from '../../utility/Utils'



class ShareModal extends Component {
    constructor(props) {
        super(props);
        this.state = {

        }
    }
    componentDidMount() {

    }

    render() {
        const {closeModal} = this.props
        return (
            <View style={styles.containerView}>
                <TouchableOpacity style={styles.closeBtnView} onPress={()=> closeModal()}>
                <Image source={res.images.icnClose} resizeMode={'contain'} style={{height:40,width:40}}/>
                </TouchableOpacity>
                <Text style={styles.shareNowText}>{strings.SHARE_NOW}</Text>
                <View style={styles.iconsCOntainer}>
                    <TouchableOpacity style={styles.imageView}>
                        <Image source={res.images.whatsapp_ic} resizeMode={'stretch'} />
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.imageView}>
                        <Image source={res.images.whatsapp_ic} resizeMode={'stretch'} />
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.imageView}>
                        <Image source={res.images.link_ic} resizeMode={'stretch'} />
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({

    containerView: {
        backgroundColor: res.colors.butterscotch,
        height: heightScale(250),
        marginTop: heightScale(150),
        borderRadius: widthScale(24),
        marginHorizontal: widthScale(15)
    },
    shareNowText: {
        fontSize: widthScale(18),
        color: res.colors.blackTwo,
        fontFamily: res.fonts.semiBold,
        textAlign: 'center',
        marginTop: heightScale(52)
    },
    iconsCOntainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginHorizontal: widthScale(30),
        marginTop: heightScale(40)
    },
    imageView: {
        backgroundColor: res.colors.white,
        padding: widthScale(14),
        height: widthScale(80),
        width: widthScale(80),
        borderRadius: 12,
        alignItems:'center',
        justifyContent:'center',
        marginHorizontal:widthScale(10)
    },
    closeBtnView:{
      position:'absolute',
      right:10,
      height:40,
      width:40,
      top:10  
    }
})


export default ShareModal;

