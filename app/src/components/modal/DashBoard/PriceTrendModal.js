import React, { Component } from "react";
import { View, Text, FlatList,Dimensions } from "react-native";
import res from '../../../../res'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import CardWithTitle from '../../../components/card/CardWithTitle'
import styles from '../../../layouts/dashBoard/projectPriceTrend/styles'
import { getRandomColor, widthScale } from '../../../utility/Utils'
import {
    LineChart,


} from "react-native-chart-kit"
const screenWidth = Dimensions.get("window").width * 0.75;
const chartConfig = {
    // propsForLabels: {
    //     stroke: 'red'
    // },

    propsForBackgroundLines: {
        strokeDasharray: "", // solid background lines with no dashes
        stroke: "rgb(76,76,76)",

    },
    backgroundGradientFrom: 'transparent',
    backgroundGradientFromOpacity: 0,
    backgroundGradientTo: res.colors.trasparents,
    backgroundGradientToOpacity: 0.0,
    color: (opacity = 1) => `rgb(0,0,0)`,
    strokeWidth: 3.0, // optional, default 3
    barPercentage: 0.5,
    useShadowColorFromDataset: false, // optional,,
    getDotColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
    labelColor: () => `rgb(32, 84, 155)`,
    dotColor: () => `rgb(255,0,0)`,
    propsForDots: {
        r: 4,
        strokeWidth: "2",
        stroke: "rgb(255,0,0)",
    }

};
class PriceTrendModal extends Component {

    constructor(props) {
        super(props);
        this.state = {
            arrayofLines: [],
            bgColor: [
                'red',
                'blue',
                'yellow',
            ],
            detailsData: [
                {
                    color: res.colors.milkChocolate,
                    value: 'My Home'
                }, {
                    color: res.colors.peacockBlue,
                    value: 'District One'
                }, {
                    color: res.colors.dustyOrange,
                    value: 'Janapriya'
                }, {
                    color: res.colors.bluePurple,
                    value: 'Happynest'
                }, {
                    color: res.colors.clearBlue,
                    value: 'Giridahri'
                }
            ]
        }
    }


renderHint = ({ item, index }) => {
    console.log("HINT", item)
    if (!item.name) {
        return <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <Text>{res.strings.NO_DATA_FOUND}</Text>
        </View>
    }
    return <View style={{
        flex: 1,
        marginTop: 5,
        backgroundColor: res.colors.butterscotch,
        alignItems: 'center',
        marginLeft: 20,
        flexDirection: 'row',
        minHeight: 25

    }}>

        <View style={{ width: 8, height: 8, borderRadius: 4, backgroundColor: getRandomColor(index) }} />
        <Text style={styles.hintText}>{item.name}</Text>
    </View>
}

fillLineGraphvalues = () => {

    const priceTrends = this.props.projectPriceTrend
    console.log("priceTrends", priceTrends)
    if (!priceTrends && priceTrends.length == 0 || priceTrends.sixMonthAvgBspRate) {
        return
    }


    let arrayOfLines = []
    priceTrends.map((item, index) => {
        let temporaryLineObject =
            [
                item.threeYearAvgBspRate ? item.threeYearAvgBspRate : 0,
                item.twoYearAvgBspRate ? item.twoYearAvgBspRate : 0,
                item.oneYearAvgBspRate ? item.oneYearAvgBspRate : 0,
                item.sixMonthAvgBspRate ? item.sixMonthAvgBspRate : 0,
            ]

        arrayOfLines.push({

            data: temporaryLineObject,
            color: () => getRandomColor(index),
            strokeWidth: 2,
        })
    })
    let labels = ["H1 2019   ", " H2 2019   ", " H1 2020   ", " H2 2020",]
    this.graphData = {
        labels: labels, datasets: arrayOfLines
    }

}
_getRandomColor() {
    var item = this.state.bgColor[Math.floor(Math.random() * this.state.bgColor.length)];
    this.setState({
        selectedColor: item,
    })
}


render() {
    console.log("PartnersAndLocationsModal", this.props)
    const handlerYAxis = (value) => {
        return parseInt(value)
    }
    { this.fillLineGraphvalues() }
    return (
        < View style={styles.modalStyle} >


            <View style={[styles.shadowHeader, { height: 42 }]}>
                <Text style={styles.corousalNameText}>{"Micro Market Price Trend"}</Text>
            </View>
                <Text style={styles.priceText}>Price(Rs)</Text>
                <View >

                    <LineChart
                        getDotColor={(opacity = 1) => `rgba(255, 255, 255, ${opacity})`}
                        data={this.graphData ? this.graphData : []}
                        width={screenWidth}
                        height={256}
                        verticalLabelRotation={0}
                        chartConfig={chartConfig}
                        withDots={true}
                        withShadow={false}
                        withInnerLines={true}
                        withVerticalLines={false}
                        withHorizontalLines={true}
                        withOuterLines={false}
                        formatYLabel={value => handlerYAxis(value)}
                        yAxisInterval={500}
                    />
                </View>

                {
                    this.props.projectPriceTrend && this.props.projectPriceTrend.name ?
                        <View /> :
                        <FlatList
                            style={{ flex: 1, overflow: 'hidden'}}
                            scrollEnabled={true}
                            numColumns={2}
                            data={this.props.projectPriceTrend}
                            renderItem={this.renderHint}
                        />
                }




        </ View>
    )
}
}
export default PriceTrendModal