import React, { Component } from "react";
import { View, Text, SafeAreaView } from "react-native";
import res from '../../../../res'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import CardWithTitle from '../../../components/card/CardWithTitle'
import styles from '../KnowYourProperty/Developer/styles'
import moment from 'moment';
import ImageWithTitle from '../../../components/card/ImageWithTitle'


class EmiScreenModal extends Component {

    constructor(props) {
        super(props);

    }




    render() {
       
        return (
            < View style={styles.modalStyle} >


                <View style={[styles.shadowHeader, { height: 42 }]}>
                    <Text style={styles.corousalNameText}>{"Home Loan"}</Text>
                </View>
                <KeyboardAwareScrollView style={{ flex: 1, marginTop: 10 }}>
                <ImageWithTitle
                        isShadow={true}
                        src={res.images.calender}
                        Value={this.props.emi}
                        Heading={"EMI"}
                    />
                    <ImageWithTitle
                        isShadow={true}
                        src={res.images.nextdrawdowndate}
                        Value={moment(this.props.data.dateOfEmi ? this.props.data.dateOfEmi : "21 Days", "YYYYMMDD").fromNow()}
                        Heading={"Remaining for Next EMI "}
                    />

                    <ImageWithTitle
                        isShadow={true}
                        src={res.images.principalamount}
                        Value={this.props.data.outstandingPrinciple ? this.props.data.outstandingPrinciple : ""}
                        Heading={"Outstanding Principal"}
                        unitText={"Rs "}
                    />
                    <ImageWithTitle
                        isShadow={true}
                        src={res.images.interestrate}
                        Value={this.props.data.outstandingInterest ? this.props.data.outstandingInterest : ""}
                        Heading={"Outstanding Interest"}
                        unitText={"Rs "}
                    />

                </KeyboardAwareScrollView>
            </ View>
        )
    }
}
export default EmiScreenModal