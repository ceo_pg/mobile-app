import React, { Component } from 'react'
import { View, Dimensions, Text, SafeAreaView, FlatList } from 'react-native'
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";

import styles from './styles'
import res from '../../../../res'
import { widthScale, heightScale, numberFormat } from '../../../utility/Utils';
import ConstructionBar from '../../../components/progressComponent/ConstructionBar'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import KeyClauseItemComp from '../../../components/legalDoc/KeyClauseItemComp'
import moment from 'moment'

class FinacialParamModal extends Component {
    constructor(props) {
        super(props)
        this.state = {
            Name: [
                {
                    name: res.strings.Loan_Principal_Amount,
                    image: res.images.principalamount,
                    label: res.strings.RS
                }, {
                    name: res.strings.Interest_Rate,
                    image: res.images.interestrate
                }, {
                    name: res.strings.Next_Drawdown_Date,
                    image: res.images.nextdrawdowndate
                }, {
                    name: res.strings.Next_Drawdown_Amount,
                    image: res.images.calender,
                    label: res.strings.RS
                }, {
                    name: res.strings.Total_Intrest_Paid,
                    image: res.images.calender,
                    label: res.strings.RS
                }, {
                    name: res.strings.Principle_amout_disbursed,
                    image: res.images.calender,
                    label: res.strings.RS
                }, {
                    name: res.strings.Pre_EMI,
                    image: res.images.calender,
                    label: res.strings.RS
                }, {
                    name: "Loan Terms",
                    image: res.images.nextdrawdowndate,
                    label: ""
                }
            ]
        }

    }

    renderFinacialParameter = ((item, index) => {
        const { loanStatement,homeLoan } = this.props.HomeLoan
        var last = loanStatement[loanStatement.length - 1]
        console.log("FINACIAL MODAL", loanStatement)
        return (
            <View style={{ marginTop: heightScale(30), justifyContent: 'space-between', flex: 1, marginBottom: heightScale(100) }}>
                <View style={{ marginVertical: heightScale(10) }}>
                    <KeyClauseItemComp unitText={"Rs. "} icon={res.images.principalamount} value={numberFormat(homeLoan.principalAmount)} label={""} img={res.images.proof_ic} subLabel={res.strings.Loan_Principal_Amount} />

                </View>
                <View style={{ marginVertical: heightScale(10) }}>
                    <KeyClauseItemComp unitText={"Rs. "} icon={res.images.interestrate} value={(homeLoan.rateOfInterest).toFixed(2)} label={"%"} img={res.images.proof_ic} subLabel={res.strings.Interest_Rate} />
                </View>
                {/* <View style={{ marginVertical: heightScale(10) }}>

                    <KeyClauseItemComp unitText={res.strings.RS} icon={res.images.nextdrawdowndate} value={moment(last.dateOfEmi).format('Do MMM  YYYY')} label={""} img={res.images.proof_ic} subLabel={res.strings.Next_Drawdown_Date} />

                </View>
                <View style={{ marginVertical: heightScale(10) }}>
                    <KeyClauseItemComp unitText={res.strings.RS} icon={res.images.principalamount} value={last.outstandingPrinciple} label={""} img={res.images.proof_ic} subLabel={res.strings.Next_Drawdown_Amount} />

                </View> */}
                <View style={{ marginVertical: heightScale(10) }}>
                    <KeyClauseItemComp unitText={"Rs. "} icon={res.images.total_intresttobepaid} value={numberFormat(last.interestPaid)} label={""} img={res.images.proof_ic} subLabel={res.strings.Total_Intrest_Paid} />
                </View>
                <View style={{ marginVertical: heightScale(10) }}>

                    <KeyClauseItemComp unitText={"Rs. "} icon={res.images.principalamountdisbursed} value={numberFormat(last.amountDisbursed)} label={""} img={res.images.proof_ic} subLabel={res.strings.Principle_amout_disbursed} />

                </View>
                <View style={{ marginVertical: heightScale(10) }}>

                    <KeyClauseItemComp unitText={"Rs. "} icon={res.images.calender} value={numberFormat(last.preEmi)} label={""} img={res.images.proof_ic} subLabel={res.strings.Pre_EMI} />

                </View>
                <View style={{ marginVertical: heightScale(10) }}>

                    <KeyClauseItemComp  icon={res.images.nextdrawdowndate} value={numberFormat(homeLoan.loanTenture)} label={" Months"} img={res.images.proof_ic} subLabel={"Loan Term"} />

                </View>
            </View>
        )

    }
    )
    render() {

        const { loanStatement } = this.props.HomeLoan
        if (!loanStatement) {
            return <View style={styles.noDataFound}>
                <Text>{res.strings.NO_DATA_FOUND}</Text>
            </View>
        }
        return (
            <View style={styles.container}>
                <View style={[styles.shadowHeader, { height: 42 }]}>
                    <Text style={styles.corousalNameText}>{res.strings.Financial_Parameters}</Text>
                </View>
                <KeyboardAwareScrollView>
                    {
                        loanStatement && loanStatement.length > 0
                        &&
                        this.renderFinacialParameter()

                    }
                </KeyboardAwareScrollView>



            </View>
        )

    }
}
const mapStateToProps = (state) => {
    return {
        HomeLoan: state.HomeLoanReducer.HomeLoan
    };
};

function mapDispatchToProps(dispatch) {
    return bindActionCreators({

    }, dispatch);
}
let FinacialParamModalContainer = connect(mapStateToProps, mapDispatchToProps)(FinacialParamModal);
export default FinacialParamModalContainer;
