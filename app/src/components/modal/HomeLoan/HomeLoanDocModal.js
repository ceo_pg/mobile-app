import React, { Component } from 'react'
import { View, Text, Linking, TouchableOpacity, Image, Dimensions, Platform } from 'react-native'
import ImagePicker from 'react-native-image-picker'
import DocumentPicker from 'react-native-document-picker'
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import AsyncStorage from '@react-native-community/async-storage'
import Toast from 'react-native-simple-toast';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import Pdf from 'react-native-pdf';

import Input from '../../../components/input/OtpField'
import styles from '../../../layouts/legalDocumentation/styles'
import Modal from 'react-native-modal'
import PDFView from 'react-native-view-pdf';
import ImageZoom from 'react-native-image-pan-zoom';
// import { heightScale, myWidth, checkIsImageOrPdfFileType, checkValidFileSize,} from '../../../utility/Utils'
import { DocItemComponent, Loader } from '../../../components/index'
import res from '../../../../res'
import { checkIsImageOrPdfFileType, checkValidFileSize, heightScale, validatePasscode } from '../../../utility/Utils'
import { PickerViewModal, CAMERA, DOCUMENT, IMAGE_PICK } from '../../../components/Picker/PickerViewModal'
import { fileUpload } from '../../../redux/actions/FileUploadAction'
import { updateDocumentsAPI } from '../../../redux/actions/HomeLoan/HomeLoanAction'
import DocumentItemComp from '../../../components/legalDoc/DocumentItemComp'
// import styles from './styles'

const options = {

    title: 'Select Image',
    mediaType: 'photo',
    allowsEditing: false,
    quality: 1.0,
    maxWidth: 500,
    maxHeight: 400,
    progress: {},
    storageOptions: {
        skipBackup: true,
        path: 'images',
    },
}
class HomeLoanDocModal extends Component {
    constructor(props) {
        super(props)
        this.modalRef = React.createRef();
        this.otpFirst = React.createRef();
        this.otpSecond = React.createRef();
        this.otpThird = React.createRef();
        this.otpFourth = React.createRef();
        this.state = {
            pickOptionsVisible: null,
            progress: {},
            isLoading: false,
            isModalVisible: false,
            isOtpModalVisible: false,
            otp1: '',
            otp2: '',
            otp3: '',
            otp4: '',
            isDataModal: false,
            isShare: false
        }

    }

    showPickerOptions = (uniqueID) => {
        setTimeout(() => {
            this.setState({
                pickOptionsVisible: 'bottom'
            })
        }, 1000);
        this.uniqueID = uniqueID;
    }
    onResultFromImagePicker = (response) => {
        console.log("onResultFromImagePicker", response)

        if (response.didCancel) {
            this.setState({
                pickOptionsVisible: null
            })
        } else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
            this.setState({
                pickOptionsVisible: null
            })
        } else {
            this.onResultFromPicker(response);
        }
    }
    onResultFromPicker = (response) => {
        if (!response) {
            Toast.show("Please try again")
            return
        }
        console.log("uri => " + response.uri + "\n " +
            "type => " + response.type + "\n Size => " +
            response.size ? response.size : response.fileSize)
        this.setState({
            pickOptionsVisible: null
        })
        if (checkIsImageOrPdfFileType(response.type)) {
            if (checkValidFileSize(response.size ? response.size : response.fileSize)) {
                console.log("checkValidFileSize", response.size)
            } else {
                return Toast.show("Size can not exceeds 10 MB")
            }
        } else {
            return Toast.show("Please select pdf,jpg,jpeg,png format documents only")
        }
        // UPLOAD File then hit update DOC API
        this.hitUploadAPI(response)

    }
    progressCallback = (uniqueID, progress) => {
        this.setState({ progress: { ...this.state.progress, [uniqueID]: (progress.loaded / (progress.total)) } }, () => {
            console.log(this.state.progress, "progress value")
        })
    }
    hitUploadAPI = async (fileDetails) => {
        console.log("Hithomedocapi", fileDetails)
        const userId = res.AsyncStorageContants.USERID;
        try {

            this.props.fileUpload(fileDetails, this.progressCallback.bind(this, this.uniqueID)).then((response => {
                console.log("file updaded response", response);
                if (response.statusCode == 200 && response.status) {
                    this.updateDocument(response.data.key);
                } else {
                    Toast.show(response.message ? response.message : strings.PLEASE_TRY_AFTER_SOMETIME);
                }

            }))
                .catch(error => {
                    console.log(error, "error");
                })

        } catch (e) {
            console.log("Error in homeloan doc.", e);
        }
    }

    openDocument = async () => {
        try {
            const res = await DocumentPicker.pick({
                type: [DocumentPicker.types.pdf, DocumentPicker.types.images],
            });
            console.log("Document", res)
            this.onResultFromPicker(res);
        } catch (err) {
            this.setState({
                pickOptionsVisible: null
            })
            if (DocumentPicker.isCancel(err)) {
                // User cancelled the picker, exit any dialogs or menus and move on
            } else {
                throw err;
            }
        }

    }
    onClickPickType = (type) => {
        this.setState({
            pickOptionsVisible: null
        })
        setTimeout(() => {
            switch (type) {
                case DOCUMENT:
                    // Open File picker
                    this.openDocument()
                    break;
                case IMAGE_PICK:
                    // Open Image Library:
                    ImagePicker.launchImageLibrary(options, this.onResultFromImagePicker);
                    break;
                case CAMERA:
                    // Launch Camera:
                    ImagePicker.launchCamera(options, this.onResultFromImagePicker);
                    break;
                default:
                    break;
            }
        }, 500)

    }
    onPressBackDrop = () => {
        this.setState({
            pickOptionsVisible: null
        })
    }
    updateDocument = (fileName) => {
        console.log("HOMELOANDOC2", this.props, fileName)
        const { HomeLoan } = this.props;
        let homeLoanObject = HomeLoan;
        if (homeLoanObject.applicationFormDulyFilled) {
            delete homeLoanObject.applicationFormDulyFilled.path
        } if (homeLoanObject.photos) {
            delete homeLoanObject.photos.path
        } if (homeLoanObject.addressProof) {
            delete homeLoanObject.addressProof.path
        } if (homeLoanObject.idProof) {
            delete homeLoanObject.idProof.path
        } if (homeLoanObject.employerIDCard) {
            delete homeLoanObject.employerIDCard.path
        } if (homeLoanObject.accountStatement) {
            delete homeLoanObject.accountStatement.path
        } if (homeLoanObject.previousLoan) {
            delete homeLoanObject.previousLoan.path
        }
        if (homeLoanObject.salarySlip) {
            delete homeLoanObject.salarySlip.path
        } if (homeLoanObject.form16) {
            delete homeLoanObject.form16.path
        } if (homeLoanObject.propertyPapers) {
            delete homeLoanObject.propertyPapers.path
        }
        if (homeLoanObject.paymentReceipts) {
            delete homeLoanObject.paymentReceipts.path
        } if (homeLoanObject.aggreement) {
            delete homeLoanObject.aggreement.path
        }
        if (homeLoanObject.nocFromBuilder) {
            delete homeLoanObject.nocFromBuilder.path
        } if (homeLoanObject.proofOfAssets) {
            delete homeLoanObject.proofOfAssets.path
        } if (homeLoanObject.companyProfile) {
            delete homeLoanObject.companyProfile.path
        }
        if (homeLoanObject.educationalCertificates) {
            delete homeLoanObject.educationalCertificates.path
        } if (homeLoanObject.employmentProof) {
            delete homeLoanObject.employmentProof.path
        } if (homeLoanObject.detailedCostEstimate) {
            delete homeLoanObject.detailedCostEstimate.path
        } if (homeLoanObject.sanctionLetter) {
            delete homeLoanObject.sanctionLetter.path
        } if (homeLoanObject.IRT) {
            delete homeLoanObject.IRT.path
        } if (homeLoanObject.ageProof) {
            delete homeLoanObject.ageProof.path
        }

        switch (this.uniqueID) {
            case "Loan_Application":
                homeLoanObject.applicationFormDulyFilled.name = fileName
                break;
            case "Photo":
                homeLoanObject.photos.name = fileName
                break;
            case "AddressProof":
                homeLoanObject.addressProof.name = fileName
                break
            case "ID_Proof":
                homeLoanObject.idProof.name = fileName
                break
            case "EmployerIDCard":
                homeLoanObject.employerIDCard.name = fileName
                break;
            case "AccountStatement":
                homeLoanObject.accountStatement.name = fileName
                break;
            case "PreviousLoan":
                homeLoanObject.previousLoan.name = fileName
                break
            case "Income_Proof":
                homeLoanObject.salarySlip.name = fileName
                break
            case "Form16":
                homeLoanObject.form16.name = fileName
                break;
            case "PropertyPapers":
                homeLoanObject.propertyPapers.name = fileName
                break;
            case "PaymentReceipts":
                homeLoanObject.paymentReceipts.name = fileName
                break
            case "Aggreement":
                homeLoanObject.aggreement.name = fileName
                break
            case "NOC_From_Builder":
                homeLoanObject.nocFromBuilder.name = fileName
                break;
            case "Proof_Of_Assets":
                homeLoanObject.proofOfAssets.name = fileName
                break;
            case "Company_Profile":
                homeLoanObject.companyProfile.name = fileName
                break
            case "Educational_Certificates":
                homeLoanObject.educationalCertificates.name = fileName
                break
            case "Employement_proof":
                homeLoanObject.employmentProof.name = fileName
                break;
            case "Detailed_Cost_Estimate":
                homeLoanObject.detailedCostEstimate.name = fileName
                break;
            case "Sanction_Letter":
                homeLoanObject.sanctionLetter.name = fileName
                break
            case "IRT":
                homeLoanObject.IRT.name = fileName
                break
            case "AGE_PROOF":
                homeLoanObject.ageProof.name = fileName
                break
            default:
            // code block
        }
        this.setState({ isLoading: true })
        this.props.updateDocumentsAPI(homeLoanObject, this.props.USER_ID).then((res => {
            console.log("updateDocumentsAPI", res)
            this.setState({ isLoading: false })
            if (res.statusCode == 200 && res.status) {
                this.props.hitHomeLoanDetailsApi(this.props.USER_ID);
                setTimeout(() => {
                    Toast.show(res.message, Toast.LONG);
                }, 2000);
            } else {
                Toast.show(res.message, Toast.LONG);
            }
        }))
            .catch(error => {
                this.setState({ isLoading: false })
                console.log(error, "error updateDocumentsAPI");
            })
    }
    onfocusFirstOtp = () => {

        this.otpFirst.current.focus();
    }
    onfocusSecondOtp = () => {

        this.otpSecond.current.focus();
    }
    onfocusThirdOtp = () => {
        this.otpThird.current.focus();
    }
    onfocusFourthOtp = () => {
        this.otpFourth.current.focus();
    }
    // onfocusFiveOtp = () => {
    //     this.otpFive.current.focus();
    // }
    // onfocusSixOtp = () => {
    //     this.otpSix.current.focus();
    // }
    onOtp1Change = (text) => {
        if (text.length == 1) {
            this.onfocusSecondOtp();
        }
        this.setState({
            otp1: text
        }, () => {
            this.validatePassCode();
        });
    }
    onOtp2Change = (text) => {
        if (text.length == 1) {
            this.onfocusThirdOtp();
        }
        this.setState({
            otp2: text
        }, () => {
            this.validatePassCode();
        });
    }
    onOtp3Change = (text) => {
        if (text.length == 1) {
            this.onfocusFourthOtp();
        }
        this.setState({
            otp3: text
        }, () => {
            this.validatePassCode();
        });
    }
    onOtp4Change = (text) => {
        // if (text.length == 1) {
        //     this.onfocusFiveOtp();
        // }
        this.setState({
            otp4: text
        }, () => {
            this.validatePassCode();
        });

    }

    validatePassCode = () => {
        // let appUsrObj = AppUser.getInstance();
        // let phoneNumber  = appUsrObj.phone;
        // var lastFourDigit = phoneNumber.substr(phoneNumber.length - 4)
        const { otp1, otp2, otp3, otp4, isShare } = this.state;
        if (otp1.length > 0 && otp2.length > 0 && otp3.length > 0 && otp4.length > 0) {
            let passCode = otp1 + otp2 + otp3 + otp4;
            console.log(validatePasscode(passCode), "validatePasscode");
            if (validatePasscode(passCode)) {

                !isShare ?
                    this.OpenWeb(this.selectedURL)
                    :
                    this.OpenURL(this.selectedURL);
                setTimeout(() => {
                    this.setState({ otp1: '', otp2: '', otp3: '', otp4: '' });
                }, 1000);
            } else {
                Toast.show("Passcode Dosen't matched", Toast.LONG);
            }
            // if(lastFourDigit == otp1 + otp2 + otp3 + otp4){
            //     this.OpenURL();
            // }
        }
    }
    OpenWeb = (url) => {
        this.toggleModal()
        Linking.canOpenURL(url).then(supported => {
            if (supported) {
                Linking.openURL(url);
            } else {
                console.log("Don't know how to open URI: " + url);
            }
        });
    }
    OpenURL = (url) => {
        this.toggleModal()
        console.log("URL", url)
        if (!url) {
            return <Text>No data found</Text>
        } else {
            this.toggleDataModal()
        }
    }
    displayOtpModal = (isShare) => {
        this.toggleModal(isShare)
    }
    closeModal = () => {
        this.toggleModal()
    }
    viewDocument = (data) => {
        // this.closeModal();
        setTimeout(() => {
            this.displayOtpModal();
            this.selectedURL = data.path ? data.path : ''
        }, 1000);
    }
    onBackFromOtp2 = (event) => {
        if (event === 'Backspace') {
            this.setState({ otp2: '' })
            this.onfocusFirstOtp();
        }
    }
    onBackFromOtp3 = (event) => {
        if (event === 'Backspace') {
            this.setState({ otp3: '' })
            this.onfocusSecondOtp();
        }

    }
    onBackFromOtp4 = (event) => {
        if (event === 'Backspace') {
            this.setState({ otp4: '' })
            this.onfocusThirdOtp();
        }
    }
    renderOtpModal = () => {
        return (
            <View style={styles.otpContainerView}>
                <TouchableOpacity style={styles.closeBtnView} onPress={() => this.closeModal()}>
                    <Image source={res.images.icnClose} resizeMode={'contain'} style={{ height: 40, width: 40 }} />
                </TouchableOpacity>
                <Text style={styles.passcodeText}>{res.strings.SHARE_PASSCODE_SHARED_ON_YOUR_DEVICE}</Text>
                <View style={styles.iconsCOntainer}>
                    <Input
                        value={this.state.otp1}
                        onChangeText={this.onOtp1Change}
                        containerStyle={styles.textInputStyle}
                        inputProps={{
                            maxLength: 1,
                            keyboardType: "numeric",
                            secureTextEntry: true,
                            returnKeyType: "next",
                            ref: this.otpFirst,
                            textAlign: "center",
                        }}
                        inputStyles={styles.inputStyle}
                    />
                    <Input
                        value={this.state.otp2}
                        onBackKeyPress={this.onBackFromOtp2}
                        onChangeText={this.onOtp2Change}
                        containerStyle={styles.textInputStyle}
                        inputProps={{
                            keyboardType: 'numeric',
                            secureTextEntry: true,
                            maxLength: 1,
                            returnKeyType: "next",
                            ref: this.otpSecond,
                            textAlign: "center",
                        }}
                        inputStyles={styles.inputStyle}
                    />
                    <Input
                        value={this.state.otp3}
                        onBackKeyPress={this.onBackFromOtp3}
                        onChangeText={this.onOtp3Change}
                        containerStyle={styles.textInputStyle}
                        inputProps={{
                            keyboardType: 'numeric',
                            returnKeyType: "next",
                            maxLength: 1,
                            secureTextEntry: true,
                            ref: this.otpThird,
                            textAlign: "center",
                        }}
                        inputStyles={styles.inputStyle}
                    />
                    <Input
                        value={this.state.otp4}
                        onBackKeyPress={this.onBackFromOtp4}
                        onChangeText={this.onOtp4Change}
                        containerStyle={styles.textInputStyle}
                        inputProps={{
                            keyboardType: 'numeric',
                            maxLength: 1,
                            returnKeyType: "next",
                            secureTextEntry: true,
                            ref: this.otpFourth,
                            textAlign: "center",
                        }}
                        inputStyles={styles.inputStyle}
                    />
                </View>
            </View>
        )
    }
    renderDataModal = () => {
        const brochurePdf = this.selectedURL
        const source = { uri: brochurePdf, cache: true }
        const resources = {
            file: Platform.OS === 'ios' ? 'downloadedDocument.pdf' : '/sdcard/Download/downloadedDocument.pdf',
            url: brochurePdf,
            base64: 'JVBERi0xLjMKJcfs...',
        }
        const resourceType = 'url';
        console.log("renderDataModal====.", brochurePdf)
        if (!brochurePdf) {
            return <View />
        }
        return (
            brochurePdf.toLowerCase().includes(".pdf") ?
                <View style={{ borderRadius: 10, flex: 1 }}>

                    <View style={styles.closeBtn}>
                        <TouchableOpacity onPress={() => this.toggleDataModal()}>
                            <Image source={res.images.icnClose} style={{ width: 20, height: 20, tintColor: res.colors.white }} resizeMode={'center'} />
                        </TouchableOpacity>
                    </View>
                    {

                        Platform.OS == 'ios' ?
                            <Pdf
                                source={source}
                                style={{ flex: 1 }}
                            />
                            :
                            <PDFView
                                // fadeInDuration={250.0}
                                style={{ flex: 1, }}
                                resource={resources[resourceType]}
                                resourceType={resourceType}
                                onLoad={() => console.log(`PDF rendered from ${resourceType}`)}
                                onError={(error) => console.log('Cannot render PDF', error)}
                            />
                    }

                </View>
                :

                <View style={{ borderRadius: 10, flex: 1 }}>
                    <View style={styles.closeBtn}>
                        <TouchableOpacity onPress={() => this.toggleDataModal()}>
                            <Image source={res.images.icnClose} style={{ width: 20, height: 20, tintColor: res.colors.white }} resizeMode={'center'} />
                        </TouchableOpacity>
                    </View>
                    <View style={{ width: "100%", height: "100%", zIndex: 9999, overflow: 'hidden' }}>
                        <ImageZoom cropWidth={Dimensions.get('window').width}
                            cropHeight={Dimensions.get('window').height}
                            imageWidth={Dimensions.get('window').width}
                            imageHeight={Dimensions.get('window').height}>

                            <Image source={brochurePdf ? { uri: brochurePdf } : res.images.parking} style={{ flex: 1 }} resizeMode={'contain'} />

                        </ImageZoom>

                    </View>
                </View>



        )
    }
    toggleModal = (isShare) => {
        const { isModalVisible } = this.state

      
        isShare ?

            setTimeout(() => {
                this.setState({ isModalVisible: !isModalVisible, isShare: true })
            }, 500)

            :
            setTimeout(() => {
                this.setState({ isModalVisible: !isModalVisible, isShare: false })
            }, 500);

    }
    toggleDataModal = () => {
        const { isDataModal } = this.state
        console.log("toggleDataModal", isDataModal)
        setTimeout(() => {
            this.setState({ isDataModal: !isDataModal })
        }, 1000);

    }
    shareDocument = (data, isShare) => {
        setTimeout(() => {
            this.displayOtpModal(isShare);
            this.selectedURL = data.path ? data.path : ''
        }, 1000);
    }
    render() {
        const { applicationFormDulyFilled, photos, addressProof, idProof, employerIDCard, accountStatement, previousLoan, salarySlip, form16, propertyPapers, paymentReceipts, aggreement, nocFromBuilder, proofOfAssets, companyProfile, educationalCertificates, employmentProof, detailedCostEstimate, sanctionLetter, IRT, ageProof } = this.props.HomeLoan;
        console.log("displayOtpModal", this.state.isOtpModalVisible)
        // if (!applicationFormDulyFilled) {
        //     return <View />
        // }
        return (

            <View style={styles.container}>
                <Modal isVisible={this.state.isModalVisible}
                    // onSwipeComplete={() => this.toggleModal()}
                    // swipeDirection={'down'}
                    onBackdropPress={() => this.toggleModal()}
                    style={styles.view}>
                    {this.props ? this.renderOtpModal() : <View />}
                </Modal>
                <Modal isVisible={this.state.isDataModal}
                    // onSwipeComplete={() => this.toggleModal()}
                    // swipeDirection={'down'}
                    onBackdropPress={() => this.toggleDataModal()}
                >
                    {this.renderDataModal()}
                </Modal>
                <View style={[styles.shadowHeader, { height: 42 }]}>
                    <Text style={styles.corousalNameText}>{res.strings.DOCUMENTS}</Text>
                </View>
                <KeyboardAwareScrollView
                    style={{ marginTop: 10 }}
                >
                    <View style={{ marginBottom: heightScale(150) }}>
                        <DocumentItemComp label={res.strings.Loan_Application} data={applicationFormDulyFilled} viewDocument={this.viewDocument} shareDocument={this.shareDocument} onUpdateDoc={this.showPickerOptions} uniqueID="Loan_Application" />
                        <DocumentItemComp label={res.strings.Photo} viewDocument={this.viewDocument} shareDocument={this.shareDocument} data={photos} onUpdateDoc={this.showPickerOptions} uniqueID="Photo" />
                        <DocumentItemComp label={res.strings.AddressProof} viewDocument={this.viewDocument} shareDocument={this.shareDocument} data={addressProof} onUpdateDoc={this.showPickerOptions} uniqueID="AddressProof" />
                        <DocumentItemComp label={res.strings.ID_Proof} viewDocument={this.viewDocument} shareDocument={this.shareDocument} data={idProof} onUpdateDoc={this.showPickerOptions} uniqueID="ID_Proof" />
                        <DocumentItemComp label={res.strings.EmployerIDCard} viewDocument={this.viewDocument} shareDocument={this.shareDocument} data={employerIDCard} onUpdateDoc={this.showPickerOptions} uniqueID="EmployerIDCard" />
                        <DocumentItemComp label={res.strings.AccountStatement} viewDocument={this.viewDocument} shareDocument={this.shareDocument} data={accountStatement} onUpdateDoc={this.showPickerOptions} uniqueID="AccountStatement" />
                        <DocumentItemComp label={res.strings.PreviousLoan} viewDocument={this.viewDocument} shareDocument={this.shareDocument} data={previousLoan} onUpdateDoc={this.showPickerOptions} uniqueID="PreviousLoan" />
                        <DocumentItemComp label={res.strings.Income_Proof} viewDocument={this.viewDocument} shareDocument={this.shareDocument} data={salarySlip} onUpdateDoc={this.showPickerOptions} uniqueID="Income_Proof" />
                        <DocumentItemComp label={res.strings.Form16} data={form16} viewDocument={this.viewDocument} shareDocument={this.shareDocument} onUpdateDoc={this.showPickerOptions} uniqueID="Form16" />
                        <DocumentItemComp label={res.strings.PropertyPapers} viewDocument={this.viewDocument} shareDocument={this.shareDocument} data={propertyPapers} onUpdateDoc={this.showPickerOptions} uniqueID="PropertyPapers" />
                        <DocumentItemComp label={res.strings.PaymentReceipts} viewDocument={this.viewDocument} shareDocument={this.shareDocument} data={paymentReceipts} onUpdateDoc={this.showPickerOptions} uniqueID="PaymentReceipts" />
                        <DocumentItemComp label={res.strings.Aggreement} data={aggreement} viewDocument={this.viewDocument} shareDocument={this.shareDocument} onUpdateDoc={this.showPickerOptions} uniqueID="Aggreement" />
                        <DocumentItemComp label={res.strings.NOC_From_Builder} viewDocument={this.viewDocument} shareDocument={this.shareDocument} data={nocFromBuilder} onUpdateDoc={this.showPickerOptions} uniqueID="NOC_From_Builder" />
                        <DocumentItemComp label={res.strings.Proof_Of_Assets} viewDocument={this.viewDocument} shareDocument={this.shareDocument} data={proofOfAssets} onUpdateDoc={this.showPickerOptions} uniqueID="Proof_Of_Assets" />
                        <DocumentItemComp label={res.strings.Company_Profile} viewDocument={this.viewDocument} shareDocument={this.shareDocument} data={companyProfile} onUpdateDoc={this.showPickerOptions} uniqueID="Company_Profile" />
                        <DocumentItemComp label={res.strings.Educational_Certificates} viewDocument={this.viewDocument} shareDocument={this.shareDocument} data={educationalCertificates} onUpdateDoc={this.showPickerOptions} uniqueID="Educational_Certificates" />
                        <DocumentItemComp label={res.strings.Employement_proof} viewDocument={this.viewDocument} shareDocument={this.shareDocument} data={employmentProof} onUpdateDoc={this.showPickerOptions} uniqueID="Employement_proof" />
                        <DocumentItemComp label={res.strings.Detailed_Cost_Estimate} viewDocument={this.viewDocument} shareDocument={this.shareDocument} data={detailedCostEstimate} onUpdateDoc={this.showPickerOptions} uniqueID="Detailed_Cost_Estimate" />
                        <DocumentItemComp label={res.strings.Sanction_Letter} viewDocument={this.viewDocument} shareDocument={this.shareDocument} data={sanctionLetter} onUpdateDoc={this.showPickerOptions} uniqueID="Sanction_Letter" />
                        <DocumentItemComp label={res.strings.ITR} data={IRT} viewDocument={this.viewDocument} shareDocument={this.shareDocument} data={IRT} onUpdateDoc={this.showPickerOptions} uniqueID="IRT" />
                        <DocumentItemComp label={res.strings.AGE_PROOF} viewDocument={this.viewDocument} shareDocument={this.shareDocument} data={ageProof} onUpdateDoc={this.showPickerOptions} uniqueID="AGE_PROOF" />
                    </View>

                    {this.state.pickOptionsVisible ?
                        <PickerViewModal
                            onClickPickType={this.onClickPickType}
                            visibleModal={this.state.pickOptionsVisible}
                            onPressBackDrop={this.onPressBackDrop} /> : <View />}
                </KeyboardAwareScrollView>

                {/* <Modal ref={this.modalRef} /> */}


            </View>
        )

    }
}
const mapStateToProps = (state) => {
    return {
        HomeLoan: state.HomeLoanReducer.HomeLoan
    };
};

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        fileUpload,
        updateDocumentsAPI
    }, dispatch);
}
let HomeLoanDocModalScreenContainer = connect(mapStateToProps, mapDispatchToProps)(HomeLoanDocModal);
export default HomeLoanDocModalScreenContainer;
// export default HomeLoanDocModal


