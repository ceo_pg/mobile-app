import React, { Component } from 'react'
import { View, Text} from 'react-native'
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

import styles from './styles'
import res from '../../../../res'
import { heightScale,numberFormat } from '../../../utility/Utils';
import KeyClauseItemComp from '../../../components/legalDoc/KeyClauseItemComp'

class OtherTermsModal extends Component {
    constructor(props) {
        super(props)
        this.state = {
            Name: [
                {
                    name: res.strings.Principal_Amount
                }, {
                    name: res.strings.Loan_Tenure
                }, {
                    name: res.strings.Next_Drawdown_Date
                }, {
                    name: res.strings.foreClosureCharges
                }, {
                    name: res.strings.Interest_Type
                }, {
                    name: res.strings.loanTransferCharges
                }, {
                    name: res.strings.prepaymentPenality
                }
            ]
        }

    }


    render() {
        const { homeLoan } = this.props.HomeLoan
        const { foreClosureCharges, interestType, loanTenture, loanTransferCharges, prepaymentPenality, principalAmount, rateOfInterest  } = homeLoan
        if (!homeLoan) {
            return
        }
      
        return (
            <View style={styles.container}>
                <View style={[styles.shadowHeader, { height: 42 }]}>
                    <Text style={styles.corousalNameText}>{res.strings.Other_Terms}</Text>
                </View>

                <KeyboardAwareScrollView
                    style={{ marginTop: 10 }}
                >

                    <View style={{ flex: 1, marginBottom: 200 }}>
                        <View style={{ marginTop: heightScale(30) }}>
                        <KeyClauseItemComp icon={res.images.principalamount} value={(homeLoan && homeLoan.interestType) ? homeLoan.interestType : "NA"} label={""} img={res.images.proof_ic} subLabel={res.strings.Floating_Interest} />

                        </View>
                        <View style={{ marginTop: heightScale(30) }}>
                        <KeyClauseItemComp icon={res.images.principalamount} unitText={"Rs. "} value={(homeLoan && homeLoan.foreClosureCharges) ? numberFormat(homeLoan.foreClosureCharges) : "0"} label={""} img={res.images.proof_ic} subLabel={res.strings.Foreclosures_Charges} />
                        </View>
                        <View style={{ marginTop: heightScale(30) }}>
                        <KeyClauseItemComp icon={res.images.principalamount} unitText={"Rs. "}value={(homeLoan && homeLoan.loanTransferCharges) ? numberFormat(homeLoan.loanTransferCharges) : "0"} label={""} img={res.images.proof_ic} subLabel={res.strings.Transfer_Charges} />
                        </View>
                        <View style={{ marginTop: heightScale(30) }}>
                        <KeyClauseItemComp icon={res.images.principalamount} unitText={"Rs. "} value={(homeLoan && homeLoan.prepaymentPenality) ? numberFormat(homeLoan.prepaymentPenality) : "0"} label={""} img={res.images.proof_ic} subLabel={res.strings.prepaymentPenality} />

                        </View>
                        {/* <View style={{ marginTop: heightScale(5) }}>
                            <KeyClauseItemComp icon={res.images.principalamount} value={rateOfInterest} label={" %"} img={res.images.proof_ic} subLabel={res.strings.Interest_Type} />
                        </View>
                        <View style={{ marginTop: heightScale(5) }}>
                            <KeyClauseItemComp icon={res.images.principalamount} value={"Rs. " +principalAmount} label={""} img={res.images.proof_ic} subLabel={res.strings.Principal_Amount} />
                        </View>
                        <View style={{ marginTop: heightScale(5) }}>
                            <KeyClauseItemComp icon={res.images.principalamount} value={"Rs. " +loanTenture} label={""} img={res.images.proof_ic} subLabel={res.strings.Loan_Tenure} />
                        </View>
                        <View style={{ marginTop: heightScale(5) }}>
                            <KeyClauseItemComp icon={res.images.principalamount} value={"Rs. " +prepaymentPenality} label={""} img={res.images.proof_ic} subLabel={res.strings.prepaymentPenality} />
                        </View> */}
                    </View>

                </KeyboardAwareScrollView>

            </View>
        )

    }
}
const mapStateToProps = (state) => {
    return {
        HomeLoan: state.HomeLoanReducer.HomeLoan
    };
};

function mapDispatchToProps(dispatch) {
    return bindActionCreators({

    }, dispatch);
}
let OtherTermsModalContainer = connect(mapStateToProps, mapDispatchToProps)(OtherTermsModal);
export default OtherTermsModalContainer;
