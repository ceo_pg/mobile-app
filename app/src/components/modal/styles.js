import { StyleSheet } from 'react-native'
import res from '../../../res'
import { widthScale, heightScale } from '../../utility/Utils';
const styles = StyleSheet.create({
    inputField: {
        color: res.colors.black,
        height: 50,
        backgroundColor: res.colors.white,
        borderRadius: 10,
        textAlign: 'center',
        marginHorizontal: 20,
    }, DeveloperinputField: {
        color: res.colors.black,
        // height: 50,
        backgroundColor: res.colors.white,
        borderRadius: 10,
        textAlign: 'center',
        marginHorizontal: 20,
    },
    container: {
        marginTop: 100,
        // borderWidth: 1,
        // borderColor: "#696969",
        borderRadius: 10,
        backgroundColor: res.colors.butterscotch,
        marginHorizontal: 16,
        paddingHorizontal: 16,
        paddingVertical: 16,
        flex:1,
        
        // backgroundColor: res.colors.btnBlue
    },
    cancelReasonStr: {
        fontSize: 15,

        fontWeight: "400",
        color: "#696969",
    },
    padding0: {
        paddingLeft: 0,
        paddingRight: 0,
        paddingBottom: 0,
        paddingTop: 0,
        paddingHorizontal: 0,
        paddingVertical: 0,
        padding: 0,
        textAlign: 'center'
    },
    reasonInput: {
        fontSize: 15,

        borderBottomWidth: 1,
        borderColor: "#696969",
        color: "#000000",

        marginTop: 10,
    },
    btnContainer: {
        marginTop: 10,
        flexDirection: "row-reverse",
    },
    okBtn: {
        paddingHorizontal: 16,
        paddingVertical: 8,
        borderRadius: 5,
        backgroundColor: res.colors.btnBlue,
    },
    cancelOrderStr: {
        fontSize: 15,

        fontWeight: "600",
        color: "#ffffff",
    },
    closeBtn: {
        position: "absolute",
        top: 0,
        right: 0,
        paddingLeft: 10,
        paddingBottom: 10,
        paddingTop: 5,
        paddingRight: 5
    },
    closeIcon: {
        height: 15,
        width: 15,
    },
    errorTextFild: {
        fontSize: 13,
        color: "#ff0000",
    },
    reasonViewCon: {
         marginTop: 10,
        flexDirection: 'column',
        marginHorizontal:10,
        borderRadius:10,
        marginBottom: 10
        
        
       

    },
    reasonBtn: {
        justifyContent:'center',
        // height: 40,
        marginHorizontal: 20,
        paddingVertical: 10,
        borderRadius: 5,
        backgroundColor: res.colors.butterscotch,
        marginBottom: 10,
        //  borderWidth: 2
    }, DevelperreasonBtn: {
        // height: 40,
        marginHorizontal: 20,
        paddingVertical: 8,
        borderRadius: 5,
        // backgroundColor: "#ffffff",
        backgroundColor: res.colors.butterscotch,
        marginBottom: 10,
        //  borderWidth: 2
    },
    reasonTextStyle: {
        fontSize: 15,
        fontWeight: "400",
        color: res.colors.black,
        alignSelf: 'center',
        width: '100%',
        textAlign: 'center'
    }, shadow: {
        backgroundColor:res.colors.butterscotch,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 0,
        },
        shadowOpacity: 0.3,
        shadowRadius: 4.11,
        zIndex: 99,
        elevation: 14,
    }, view: {
        justifyContent: 'flex-end',
        margin: 0,
    },
});
export default styles