import React, { Component } from 'react'
import { View, Dimensions, Text, SafeAreaView } from 'react-native'
import Carousel, { Pagination } from 'react-native-snap-carousel';

import Modal from 'react-native-modal'

import styles from './styles'
import res from '../../../../res'
import { widthScale, heightScale } from '../../../utility/Utils';
import ConstructionBar from '../../../components/progressComponent/ConstructionBar'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

class AmenitiesProgressModal extends Component {
    constructor(props) {
        super(props)
        this.state = {

        }

    }



    render() {
        const pheProgress = this.props.infra && this.props.infra.plumbing && this.props.infra.plumbing.percentOfCompletionOfFollowingItem ? this.props.infra.plumbing.percentOfCompletionOfFollowingItem : "0"
        const electricalProgress = this.props.infra && this.props.infra.electrical && this.props.infra.electrical.percentOfCompletionOfFollowingItem ? this.props.infra.electrical.percentOfCompletionOfFollowingItem : "0"
        const fireProgress = this.props.infra && this.props.infra.fireFighting && this.props.infra.fireFighting.percentOfCompletionOfFollowingItem ? this.props.infra.fireFighting.percentOfCompletionOfFollowingItem : "0"
        const HVACProgress = this.props.infra && this.props.infra.HVAC && this.props.infra.HVAC.percentOfCompletionOfFollowingItem ? this.props.infra.HVAC.percentOfCompletionOfFollowingItem : "0"
        const clubHouseAndAmenitiesProgress = this.props.infra && this.props.infra.clubHouseAndAmenities && this.props.infra.clubHouseAndAmenities.percentOfCompletionOfFollowingItem ? this.props.infra.clubHouseAndAmenities.percentOfCompletionOfFollowingItem : "0"
        const landscapingProgress = this.props.infra && this.props.infra.landscaping && this.props.infra.landscaping.percentOfCompletionOfFollowingItem ? this.props.infra.landscaping.percentOfCompletionOfFollowingItem : "0"


        return (
            <View style={styles.container}>
                <View style={[styles.shadowHeader, { height: 42 }]}>
                    <Text style={styles.corousalNameText}>{"Amenities Progress"}</Text>
                </View>
                <KeyboardAwareScrollView
                    style={{ marginTop: 10 }}
                >

                    <View style={{ flex: 1, marginBottom: 200 }}>
                        <ConstructionBar
                            header={"PHE"}
                            // value={"WaterStorageSystem, WaterSupplyPiping, Drain,GasPiping, WaterTreatment, WasteDisposal, RainWaterHarvesting, SewageChamber,SewageLine,SepticTank,STP,WTP"}
                            src={res.images.plumber}
                            percentage={parseFloat(pheProgress) / 100}
                            percentageText={parseFloat(pheProgress)}
                        />
                        <ConstructionBar
                            src={res.images.electricle}
                            header={"Electrical"}
                            value={"Substation, Transformer, PanelsAndSubPanels, RoughWiring, Generator, StreetAndCommonAreaLighting, SolarPowerUnit, SolarFencing"}
                            percentage={parseFloat(electricalProgress) / 100}
                            percentageText={parseFloat(electricalProgress)}
                        />
                        <ConstructionBar

                            header={"Fire Safety"}
                            value={"FireStorageTanks, PumpingSystem, Hydrant,Sprinklers"}
                            percentage={parseFloat(fireProgress) / 100}
                            percentageText={parseFloat(fireProgress)}
                            src={res.images.fireExtinguisher}
                        />
                        <ConstructionBar

                            header={"HVAC"}
                            value={"AirHandler, DuctWork, AirFilter,Piping,Radiators,HVACControls"}
                            percentage={parseFloat(HVACProgress) / 100}
                            percentageText={parseFloat(HVACProgress)}
                            src={res.images.HVAC}
                        />
                        <ConstructionBar

                            header={"Club House And Amenities"}
                            value={"AirHandler, DuctWork, AirFilter,Piping,Radiators,HVACControls"}
                            percentage={parseFloat(clubHouseAndAmenitiesProgress) / 100}
                            percentageText={parseFloat(clubHouseAndAmenitiesProgress)}
                            src={res.images.clubHouseAndAmenities}
                        />
                        <ConstructionBar

                            header={"Landscaping"}
                            value={"SoilWork, WateringSystem, Drainage, Grass"}
                            percentage={parseFloat(landscapingProgress) / 100}
                            percentageText={parseFloat(landscapingProgress)}
                            src={res.images.icn_forest}
                        />
                    </View>

                </KeyboardAwareScrollView>

            </View>
        )

    }
}
export default AmenitiesProgressModal