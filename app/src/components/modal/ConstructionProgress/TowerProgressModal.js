import React, { Component } from 'react'
import { View, Dimensions, Text, SafeAreaView } from 'react-native'
import Carousel, { Pagination } from 'react-native-snap-carousel';

import Modal from 'react-native-modal'

import styles from './styles'
import res from '../../../../res'
import { widthScale, heightScale } from '../../../utility/Utils';
import ConstructionBar from '../../../components/progressComponent/ConstructionBar'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

class TowerProgressModal extends Component {
    constructor(props) {
        super(props)
        this.state = {

        }

    }



    render() {
        const structureProgress = this.props.tower && this.props.tower.structure && this.props.tower.structure.percentOfCompletionOfFollowingItem ? this.props.tower.structure.percentOfCompletionOfFollowingItem : "0"
        const excavationProgress = this.props.tower && this.props.tower.excavation && this.props.tower.excavation.percentOfCompletionOfFollowingItem ? this.props.tower.excavation.percentOfCompletionOfFollowingItem : "0"
        const footingsProgress = this.props.tower && this.props.tower.footings && this.props.tower.footings.percentOfCompletionOfFollowingItem ? this.props.tower.footings.percentOfCompletionOfFollowingItem : "0"
        const brickMasonryWork = this.props.tower && this.props.tower.brickMasonryWork && this.props.tower.brickMasonryWork.percentOfCompletionOfFollowingItem ? this.props.tower.brickMasonryWork.percentOfCompletionOfFollowingItem : "0"
        const waterProofingTanksAndTerrace = this.props.tower && this.props.tower.waterProofingTanksAndTerrace && this.props.tower.waterProofingTanksAndTerrace.percentOfCompletionOfFollowingItem ? this.props.tower.waterProofingTanksAndTerrace.percentOfCompletionOfFollowingItem : "0"
        const externalPlastering = this.props.tower && this.props.tower.externalPlastering && this.props.tower.externalPlastering.percentOfCompletionOfFollowingItem ? this.props.tower.externalPlastering.percentOfCompletionOfFollowingItem : "0"
        const doorWindowFramingFixations = this.props.tower && this.props.tower.doorWindowFramingFixations && this.props.tower.doorWindowFramingFixations.percentOfCompletionOfFollowingItem ? this.props.tower.doorWindowFramingFixations.percentOfCompletionOfFollowingItem : "0"
        const facadeAndExteriorFinishing = this.props.tower && this.props.tower.facadeAndExteriorFinishing && this.props.tower.facadeAndExteriorFinishing.percentOfCompletionOfFollowingItem ? this.props.tower.facadeAndExteriorFinishing.percentOfCompletionOfFollowingItem : "0"
        const commonAreaFinishesProgress = this.props.tower && this.props.tower.commonAreaFinishes && this.props.tower.commonAreaFinishes.percentOfCompletionOfFollowingItem ? this.props.tower.commonAreaFinishes.percentOfCompletionOfFollowingItem : "0"
        const liftInstalationProgress = this.props.tower && this.props.tower.liftInstalation && this.props.tower.liftInstalation.percentOfCompletionOfFollowingItem ? this.props.tower.liftInstalation.percentOfCompletionOfFollowingItem : "0"
        const singagesProgress = this.props.tower && this.props.tower.singages && this.props.tower.singages.percentOfCompletionOfFollowingItem ? this.props.tower.singages.percentOfCompletionOfFollowingItem : "0"

        return (
            <View style={styles.container}>
                <View style={[styles.shadowHeader, { height: 42 }]}>
                    <Text style={styles.corousalNameText}>{"Tower Level"}</Text>
                </View>
                <KeyboardAwareScrollView>
                    <View style={{marginBottom:heightScale(150)}}>
                        <ConstructionBar
                            header={"Excavation"}
                            // value={"WaterStorageSystem, WaterSupplyPiping, Drain,GasPiping, WaterTreatment, WasteDisposal, RainWaterHarvesting, SewageChamber,SewageLine,SepticTank,STP,WTP"}
                            src={res.images.excavation}
                            percentage={parseFloat(excavationProgress) / 100}
                            percentageText={parseFloat(excavationProgress)}
                        />
                        <ConstructionBar
                            src={res.images.flooting}
                            header={"Footings"}
                            // value={"Substation, Transformer, PanelsAndSubPanels, RoughWiring, Generator, StreetAndCommonAreaLighting, SolarPowerUnit, SolarFencing"}
                            percentage={parseFloat(footingsProgress) / 100}
                            percentageText={parseFloat(footingsProgress)}
                        />
                        <ConstructionBar

                            header={"Structure"}
                            value={"Cellars, Floors"}
                            percentage={parseFloat(structureProgress) / 100}
                            percentageText={parseFloat(structureProgress)}
                            src={res.images.structure_tower}
                        />
                        <ConstructionBar
                            header={"Door Window Framing Fixations"}
                            // value={"WaterStorageSystem, WaterSupplyPiping, Drain,GasPiping, WaterTreatment, WasteDisposal, RainWaterHarvesting, SewageChamber,SewageLine,SepticTank,STP,WTP"}
                            src={res.images.doorWindowFramingFixations}
                            percentage={parseFloat(doorWindowFramingFixations) / 100}
                            percentageText={parseFloat(doorWindowFramingFixations)}
                        />
                        <ConstructionBar
                            src={res.images.liftInstalation}
                            header={"Lift Instalation"}
                            // value={"Substation, Transformer, PanelsAndSubPanels, RoughWiring, Generator, StreetAndCommonAreaLighting, SolarPowerUnit, SolarFencing"}
                            percentage={parseFloat(liftInstalationProgress) / 100}
                            percentageText={parseFloat(liftInstalationProgress)}
                        />
                        <ConstructionBar

                            header={"Brick Masonry Work"}
                            // value={"Cellars, Floors"}
                            percentage={parseFloat(brickMasonryWork) / 100}
                            percentageText={parseFloat(brickMasonryWork)}
                            src={res.images.brick}
                        />
                        <ConstructionBar
                            header={"Water Proofing Tanks And Terrace"}
                            // value={"WaterStorageSystem, WaterSupplyPiping, Drain,GasPiping, WaterTreatment, WasteDisposal, RainWaterHarvesting, SewageChamber,SewageLine,SepticTank,STP,WTP"}
                            src={res.images.waterProofingTanksAndTerrace}
                            percentage={parseFloat(waterProofingTanksAndTerrace) / 100}
                            percentageText={parseFloat(waterProofingTanksAndTerrace)}
                        />
                        <ConstructionBar
                            src={res.images.externalPlastering}
                            header={"External Plastering"}
                            // value={"Substation, Transformer, PanelsAndSubPanels, RoughWiring, Generator, StreetAndCommonAreaLighting, SolarPowerUnit, SolarFencing"}
                            percentage={parseFloat(externalPlastering) / 100}
                            percentageText={parseFloat(externalPlastering)}
                        />
                        <ConstructionBar

                            header={"Facade And Exterior Finishing"}
                            // value={"Cellars, Floors"}
                            percentage={parseFloat(facadeAndExteriorFinishing) / 100}
                            percentageText={parseFloat(facadeAndExteriorFinishing)}
                            src={res.images.facadeAndExteriorFinishing}
                        />
                        <ConstructionBar
                            header={"Common Area Finishes"}
                            // value={"WaterStorageSystem, WaterSupplyPiping, Drain,GasPiping, WaterTreatment, WasteDisposal, RainWaterHarvesting, SewageChamber,SewageLine,SepticTank,STP,WTP"}
                            src={res.images.commonAreaFinishes}
                            percentage={parseFloat(commonAreaFinishesProgress) / 100}
                            percentageText={parseFloat(commonAreaFinishesProgress)}
                        />
                        <ConstructionBar
                            src={res.images.sink}
                            header={"Singages"}
                            // value={"Substation, Transformer, PanelsAndSubPanels, RoughWiring, Generator, StreetAndCommonAreaLighting, SolarPowerUnit, SolarFencing"}
                            percentage={parseFloat(singagesProgress) / 100}
                            percentageText={parseFloat(singagesProgress)}
                        />

                    </View>

                </KeyboardAwareScrollView>

            </View>
        )

    }
}
export default TowerProgressModal