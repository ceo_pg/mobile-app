import React, { Component } from 'react'
import { View, Dimensions, Text, SafeAreaView } from 'react-native'
import Carousel, { Pagination } from 'react-native-snap-carousel';

import Modal from 'react-native-modal'

import styles from './styles'
import res from '../../../../res'
import { widthScale, heightScale } from '../../../utility/Utils';
import ConstructionBar from '../../../components/progressComponent/ConstructionBar'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

class UnitProgressModal extends Component {
    constructor(props) {
        super(props)
        this.state = {

        }

    }



    render() {
        const blockworkProgress = this.props.unit && this.props.unit.blockwork && this.props.unit.blockwork.percentOfCompletionOfFollowingItem ? this.props.unit.blockwork.percentOfCompletionOfFollowingItem : "0"
        const plumbingLinesProgress = this.props.unit && this.props.unit.plumbingLines && this.props.unit.plumbingLines.percentOfCompletionOfFollowingItem ? this.props.unit.plumbingLines.percentOfCompletionOfFollowingItem : "0"
        const electricalWallConduitProgress = this.props.unit && this.props.unit.electricalWallConduit && this.props.unit.electricalWallConduit.percentOfCompletionOfFollowingItem ? this.props.unit.electricalWallConduit.percentOfCompletionOfFollowingItem : "0"

        const plasteringProgress = this.props.unit && this.props.unit.plastering && this.props.unit.plastering.percentOfCompletionOfFollowingItem ? this.props.unit.plastering.percentOfCompletionOfFollowingItem : "0"
        const waterProofingProgress = this.props.unit && this.props.unit.waterProofing && this.props.unit.waterProofing.percentOfCompletionOfFollowingItem ? this.props.unit.waterProofing.percentOfCompletionOfFollowingItem : "0"
        const wallPuttyProgress = this.props.unit && this.props.unit.wallPutty && this.props.unit.wallPutty.percentOfCompletionOfFollowingItem ? this.props.unit.wallPutty.percentOfCompletionOfFollowingItem : "0"

        const toiletDadoAndFlooringProgress = this.props.unit && this.props.unit.toiletDadoAndFlooring && this.props.unit.toiletDadoAndFlooring.percentOfCompletionOfFollowingItem ? this.props.unit.toiletDadoAndFlooring.percentOfCompletionOfFollowingItem : "0"
        const flooringProgress = this.props.unit && this.props.unit.flooring && this.props.unit.flooring.percentOfCompletionOfFollowingItem ? this.props.unit.flooring.percentOfCompletionOfFollowingItem : "0"
        const toiletGridCeilingProgress = this.props.unit && this.props.unit.toiletGridCeiling && this.props.unit.toiletGridCeiling.percentOfCompletionOfFollowingItem ? this.props.unit.toiletGridCeiling.percentOfCompletionOfFollowingItem : "0"

        const internalPaintPrimerCoatProgress = this.props.unit && this.props.unit.internalPaintPrimerCoat && this.props.unit.internalPaintPrimerCoat.percentOfCompletionOfFollowingItem ? this.props.unit.internalPaintPrimerCoat.percentOfCompletionOfFollowingItem : "0"
        const doorFrameAndShuttersProgress = this.props.unit && this.props.unit.doorFrameAndShutters && this.props.unit.doorFrameAndShutters.percentOfCompletionOfFollowingItem ? this.props.unit.doorFrameAndShutters.percentOfCompletionOfFollowingItem : "0"
        const windowsInstallationProgress = this.props.unit && this.props.unit.windowsInstallation && this.props.unit.windowsInstallation.percentOfCompletionOfFollowingItem ? this.props.unit.windowsInstallation.percentOfCompletionOfFollowingItem : "0"

        const electricalWiringAndSwitchboardsProgress = this.props.unit && this.props.unit.electricalWiringAndSwitchboards && this.props.unit.electricalWiringAndSwitchboards.percentOfCompletionOfFollowingItem ? this.props.unit.electricalWiringAndSwitchboards.percentOfCompletionOfFollowingItem : "0"
        const internalPaintEmulsionProgress = this.props.unit && this.props.unit.internalPaintEmulsion && this.props.unit.internalPaintEmulsion.percentOfCompletionOfFollowingItem ? this.props.unit.internalPaintEmulsion.percentOfCompletionOfFollowingItem : "0"
        const railingsProgress = this.props.unit && this.props.unit.railings && this.props.unit.railings.percentOfCompletionOfFollowingItem ? this.props.unit.railings.percentOfCompletionOfFollowingItem : "0"
        const cpSanitaryFixturesProgress = this.props.unit && this.props.unit.cpSanitaryFixtures && this.props.unit.cpSanitaryFixtures.percentOfCompletionOfFollowingItem ? this.props.unit.cpSanitaryFixtures.percentOfCompletionOfFollowingItem : "0"

        return (
            <View style={styles.container}>
                <View style={[styles.shadowHeader, { height: 42 }]}>
                    <Text style={styles.corousalNameText}>{"Unit Level"}</Text>
                </View>
                <KeyboardAwareScrollView
                    style={{ marginTop: 10 }}
                >
                    <View style={{ flex: 1, marginBottom: 200 }}>
                        <ConstructionBar
                            header={"Blockwork"}
                            // value={"WaterStorageSystem, WaterSupplyPiping, Drain,GasPiping, WaterTreatment, WasteDisposal, RainWaterHarvesting, SewageChamber,SewageLine,SepticTank,STP,WTP"}
                            src={res.images.blockwork}
                            percentage={parseFloat(blockworkProgress) / 100}
                            percentageText={parseFloat(blockworkProgress)}
                        />
                        <ConstructionBar
                            src={res.images.plumbingLines}
                            header={"Plumbing Lines"}
                            // value={"Substation, Transformer, PanelsAndSubPanels, RoughWiring, Generator, StreetAndCommonAreaLighting, SolarPowerUnit, SolarFencing"}
                            percentage={parseFloat(plumbingLinesProgress) / 100}
                            percentageText={parseFloat(plumbingLinesProgress)}
                        />
                        <ConstructionBar

                            header={"Electrical Wall Conduit"}
                            // value={"Cellars, Floors"}
                            percentage={parseFloat(electricalWallConduitProgress) / 100}
                            percentageText={parseFloat(electricalWallConduitProgress)}
                            src={res.images.electrical}
                        />
                        <ConstructionBar

                            header={"Plastering"}
                            // value={"AirHandler, DuctWork, AirFilter,Piping,Radiators,HVACControls"}
                            percentage={parseFloat(plasteringProgress) / 100}
                            percentageText={parseFloat(plasteringProgress)}
                            src={res.images.plastering}
                        />
                        <ConstructionBar

                            header={"Water Proofing"}
                            // value={"AirHandler, DuctWork, AirFilter,Piping,Radiators,HVACControls"}
                            percentage={parseFloat(waterProofingProgress) / 100}
                            percentageText={parseFloat(waterProofingProgress)}
                            src={res.images.fireExtinguisher}
                        />
                        <ConstructionBar

                            header={"Wall Putty"}
                            // value={"SoilWork, WateringSystem, Drainage, Grass"}
                            percentage={parseFloat(wallPuttyProgress) / 100}
                            percentageText={parseFloat(wallPuttyProgress)}
                            src={res.images.waterProofing}
                        />
                        <ConstructionBar
                            header={"Flooring"}
                            // value={"WaterStorageSystem, WaterSupplyPiping, Drain,GasPiping, WaterTreatment, WasteDisposal, RainWaterHarvesting, SewageChamber,SewageLine,SepticTank,STP,WTP"}
                            src={res.images.flooring}
                            percentage={parseFloat(flooringProgress) / 100}
                            percentageText={parseFloat(flooringProgress)}
                        />
                        <ConstructionBar
                            src={res.images.toiletDadoAndFlooring}
                            header={"Toilet Dado And Flooring"}
                            // value={"Substation, Transformer, PanelsAndSubPanels, RoughWiring, Generator, StreetAndCommonAreaLighting, SolarPowerUnit, SolarFencing"}
                            percentage={parseFloat(toiletDadoAndFlooringProgress) / 100}
                            percentageText={parseFloat(toiletDadoAndFlooringProgress)}
                        />
                        <ConstructionBar

                            header={"Toilet Grid Ceiling"}
                            // value={"Cellars, Floors"}
                            percentage={parseFloat(toiletGridCeilingProgress) / 100}
                            percentageText={parseFloat(toiletGridCeilingProgress)}
                            src={res.images.toiletGridCeiling}
                        />
                        <ConstructionBar

                            header={"Internal Paint Primer Coat"}
                            // value={"AirHandler, DuctWork, AirFilter,Piping,Radiators,HVACControls"}
                            percentage={parseFloat(internalPaintPrimerCoatProgress) / 100}
                            percentageText={parseFloat(internalPaintPrimerCoatProgress)}
                            src={res.images.fireExtinguisher}
                        />
                        <ConstructionBar

                            header={"DoorFrame And Shutters"}
                            // value={"AirHandler, DuctWork, AirFilter,Piping,Radiators,HVACControls"}
                            percentage={parseFloat(doorFrameAndShuttersProgress) / 100}
                            percentageText={parseFloat(doorFrameAndShuttersProgress)}
                            src={res.images.doorFrameAndShutters}
                        />
                        <ConstructionBar

                            header={"Windows Installation"}
                            // value={"SoilWork, WateringSystem, Drainage, Grass"}
                            percentage={parseFloat(windowsInstallationProgress) / 100}
                            percentageText={parseFloat(windowsInstallationProgress)}
                            src={res.images.windows}
                        />
                        <ConstructionBar

                            header={"Electrical Wiring And Switchboards"}
                            // value={"SoilWork, WateringSystem, Drainage, Grass"}
                            percentage={parseFloat(electricalWiringAndSwitchboardsProgress) / 100}
                            percentageText={parseFloat(electricalWiringAndSwitchboardsProgress)}
                            src={res.images.electricalWiringAndSwitchboards}
                        />
                        <ConstructionBar

                            header={"Internal Paint Emulsion"}
                            // value={"SoilWork, WateringSystem, Drainage, Grass"}
                            percentage={parseFloat(internalPaintEmulsionProgress) / 100}
                            percentageText={parseFloat(internalPaintEmulsionProgress)}
                            src={res.images.icn_forest}
                        />
                        <ConstructionBar

                            header={"Railings"}
                            // value={"SoilWork, WateringSystem, Drainage, Grass"}
                            percentage={parseFloat(railingsProgress) / 100}
                            percentageText={parseFloat(railingsProgress)}
                            src={res.images.icn_forest}
                        />
                        <ConstructionBar

                            header={"CP Sanitary Fixtures"}
                            // value={"SoilWork, WateringSystem, Drainage, Grass"}
                            percentage={parseFloat(cpSanitaryFixturesProgress) / 100}
                            percentageText={parseFloat(cpSanitaryFixturesProgress)}
                            src={res.images.CpFitting}
                        />
                    </View>

                </KeyboardAwareScrollView>

            </View >
        )

    }
}
export default UnitProgressModal