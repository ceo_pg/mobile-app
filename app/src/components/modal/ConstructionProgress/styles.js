import { StyleSheet, Platform, StatusBar } from 'react-native'
import res from '../../../../res'
import { heightScale } from '../../../utility/Utils'
const styles = StyleSheet.create({
    amenitiesFullScreen: {
        borderRadius: 20,
        backgroundColor: res.colors.butterscotch
    }, container: {
        flex: 1,
        top: Platform.OS == 'ios' ? 100 : 100,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        backgroundColor: res.colors.butterscotch,

    }, shadowHeader: {
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        backgroundColor: res.colors.butterscotch,
        shadowColor: res.colors.shadowColor,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 10.58,
        shadowRadius: 10.00,
        elevation: 24,
    }, corousalNameText: {
        fontWeight: 'bold',
        fontFamily: res.fonts.semiBold,
        fontSize: 18,
        letterSpacing: 0.3,
        textAlign: 'center',
        color: res.colors.greyishBrown,
        marginTop: 10
    },
})
export default styles