import React, { Component } from "react";
import { View, Text, Image, TouchableOpacity, TextInput, StyleSheet, FlatList, Keyboard } from "react-native";
import styles from './styles'
import res from '../../../res'
// import PGInputField from '../input/TextInput'



class SearchModal extends Component {

    constructor(props) {
        super(props);

        this.state = {
            searchResult: "",
            data: []
        };
    }
    componentDidMount() {
        this.setState({
            data: this.props.list,
            fullData:  this.props.list,
        })

    }
    searchResult = (index) => {

        let data = this.state.data[index];
        console.log("CITY DATA", data)
        this.setState({ searchResult: data });
        this.props.onCancelModalPress(data)
    };

    onOkBtnPress = () => {
        Keyboard.dismiss()
    };
    renderSearchBtn = ({ item, index }) => {
        // console.log("ITEM", item)
        return (

            <View style={[styles.shadow, styles.reasonViewCon]}>
                <TouchableOpacity style={[styles.reasonBtn]} onPress={() => this.searchResult(index)}>
                    <Text style={styles.reasonTextStyle}>{item}</Text>
                </TouchableOpacity>
            </View>


        )
    }
    newData = () => {

    }
    handleKeyPress({ nativeEvent: { key: keyValue } }) {
        if (keyValue === 'Backspace') {
            // this.refs.refOfPreviousInput.focus();
        }
    }
    searchData = (text) => {
        console.log("SEARCH DTA", text)
        const { data, fullData } = this.state


        // this.setState({ searchResult: text });
        const newData = fullData.filter(item => {
            console.log("searchData", item)
            const itemData = `${item.toUpperCase()}`;
            const textData = text.toUpperCase();
            return itemData.indexOf(textData) > -1
            // return itemData.includes(textData); // this will return true if our itemData contains the textData
        });
        
        // console.log('>>>>>>> new data', newData)
        // alert(JSON.stringify(newData))

        if (text != '') {
            this.setState({
                data: newData,
                searchResult: text
            });
        } else {
            this.setState({
                data: this.state.fullData,
                searchResult: text
            });
        }

    };
    FlatListItemSeparator = () => {
        return (
            <View
                style={{
                    height: 1,
                    width: "100%",
                    backgroundColor: res.colors.underlineColor,
                }}
            />
        );
    }
    render() {
        // console.log("MODAL PROPS", this.props)
        const { searchResult, error, data } = this.state;
        const { closeModal } = this.props;
        return (
            <View style={styles.container}>

                <TextInput
                    placeholder={"Search"}
                    placeholderTextColor={res.colors.black}
                    style={[styles.shadow, styles.inputField]}
                    value={searchResult}
                    onChangeText={this.searchData}
                    onKeyPress={this.handleKeyPress}
                    inputProps={{
                        maxLength: 50,
                        returnKeyType: "next",
                        keyboardType: "email-address",
                        autoCapitalize: "none",
                        autoCorrect: false,
                        autoFocus: false,
                        numberOfLines: 1,
                    }}
                    onSubmitEditing={this.onOkBtnPress}
                />
                {error ? (<Text style={styles.errorTextFild}>{error}</Text>) : <View />}
                <View style={{flex:1}} >
                    <FlatList
                        style={{flex:1}}
                        keyExtractor={(item, index) => index.toString()}
                        data={data}
                        renderItem={this.renderSearchBtn}
                        extraData={this.state}
                    // ItemSeparatorComponent={this.FlatListItemSeparator}
                    />
                </View>


                <TouchableOpacity style={styles.closeBtn} onPress={closeModal}>
                    <Image source={res.images.icnClose} style={{ width: 20, height: 20, tintColor: res.colors.white }} />
                </TouchableOpacity>
            </View>
        );
    }
}



export default SearchModal;