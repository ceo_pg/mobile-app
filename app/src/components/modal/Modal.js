import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableWithoutFeedback,
  Animated,
  Dimensions,
} from 'react-native';

export default class Modal extends Component {
  constructor(props) {
    super(props);

    this.height = Dimensions.get('window').height;
    this.view = null;

    this.state = {
      isVisible: false,
      topValue: new Animated.Value(this.height),
      opacity: new Animated.Value(0),
      useNativeDriver: false
    };
  }

  show = (animation = false) => {
    this.setState(
      () => {
        return {
          isVisible: true,
        };
      },
      () => {
        if (animation) {
          Animated.parallel([
            Animated.timing(this.state.topValue, {
              toValue: 0,
              duration: 2000,
              useNativeDriver: false
            }),
            Animated.timing(this.state.opacity, {
              toValue: 0.8,
              duration: 2000,
              useNativeDriver: false
            }),
          ]).start();
        } else {
          this.state.topValue.setValue(0);
          this.state.opacity.setValue(0.8)
        }

      }
    );
  };

  hide = (animation = true) => {
    if (!animation) {
      this.state.topValue.setValue(this.height);
      this.state.opacity.setValue(0);
      this.setState(() => {
        return {
          isVisible: false,
        };
      }, () => {
        this.view = null;
      });
      return;
    }
    Animated.parallel([
      Animated.timing(this.state.topValue, {
        toValue: this.height,
        duration: 500,
        useNativeDriver: false
      }),
      Animated.timing(this.state.opacity, {
        toValue: 0,
        duration: 100,
        useNativeDriver: false
      }),
    ]).start(() => {

      this.setState(() => {
        return {
          isVisible: false,
        };
      }, () => {
        this.view = null;
      });
    });
  };

  setView = (view) => {
    this.view = view;
  }

  render() {
    if (!this.state.isVisible) {
      return null;
    }
    const bgColor = this.state.opacity.interpolate({
      inputRange: [0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8],
      outputRange: [
        'rgba(0, 0, 0, 0)',
        'rgba(0, 0, 0, 0.1)',
        'rgba(0, 0, 0, 0.2)',
        'rgba(0, 0, 0, 0.3)',
        'rgba(0, 0, 0, 0.4)',
        'rgba(0, 0, 0, 0.5)',
        'rgba(0, 0, 0, 0.6)',
        'rgba(0, 0, 0, 0.7)',
        'rgba(0, 0, 0, 0.8)',
      ],
    });
    return (<Animated.View
      style={[
        style.container,
        { top: this.state.topValue, backgroundColor: bgColor },
      ]}>
      <TouchableWithoutFeedback style={style.fullScreen} >
        {this.view ? this.view(this.hide) : <Text>No View set</Text>}
      </TouchableWithoutFeedback>
    </Animated.View>

    );
  }
}

const style = StyleSheet.create({
  container: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: 'rgba(0, 0, 0, 0.3)',
    flex: 1,
    // justifyContent: 'flex-end',
    alignItems: 'stretch',
    zIndex: 999,
    elevation: 999,
  },
  fullScreen: {
    flex: 1,
    // backgroundColor: 'pink',
    justifyContent:'center',
    alignItems:'center',
    padding: 5,
  },
});
