import React, { Component } from "react";
import { View, Text, Image, TouchableOpacity, TextInput, StyleSheet, FlatList, Keyboard } from "react-native";
import res from '../../../res'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import ImageWithTitle from '../../components/card/ImageWithTitle'
import strings from '../../../res/constants/strings';
import CardWithTitle from '../../components/card/CardWithTitle'
import {heightScale,widthScale} from '../../utility/Utils'
import { readDir } from "react-native-fs";

class DeveloperDetailModal extends Component {

    constructor(props) {
        super(props);

        this.state = {
            data: []
        };
    }
    componentDidMount() {
        this.setState({
            data: this.props.list
        })


    }


    render() {
        const { closeModal, title, developerData } = this.props;

        let memberShip = developerData.membership;
        let memberArray = []
        if (memberShip) {
            for (let index = 0; index < memberShip.length; index++) {
                if (memberShip[index].name) {
                    memberArray.push(memberShip[index].name ? memberShip[index].name : "")
                  

                }
            }
        }
        return (
            < View style={styles.modalStyle} >
                <View style={[styles.shadowHeader, { height: 42 }]}>
                    <Text style={styles.corousalNameText}>{res.strings.Profile}</Text>
                </View>
                <KeyboardAwareScrollView

                    style={{
                        flex: 1, borderTopLeftRadius: 20,
                        borderTopRightRadius: 20,
                        // backgroundColor: readDir,
                        // paddingBottom: 20,
                    }}
                >
                    {/* <View style={[styles.shadowHeader, { height: 42 }]}>
                        <Text style={styles.corousalNameText}>{title ? title : ''}</Text>
                    </View> */}

                    <View style={{
                        flex: 1,
                    }}>

                        <ImageWithTitle
                            isShadow={true}
                            src={res.images.complience}
                            Value={developerData.reraCompliance ? developerData.reraCompliance : "0"}
                            Heading={strings.RERA_COMPLIANCE} />
                        <ImageWithTitle
                            isShadow={true}
                            src={res.images.compaints}
                            Value={developerData.reraComplaints ? developerData.reraComplaints : "0"}
                            Heading={strings.RERA_COMPLAINTS} />
                        <ImageWithTitle
                            isShadow={true}
                            src={res.images.yearinBusiness}
                            Value={developerData.noOfYrsInBusiness ? developerData.noOfYrsInBusiness : "NA"}
                            Heading={strings.YEAR_IN_BUSINES} />
                        <ImageWithTitle
                            isShadow={true}
                            src={res.images.project}
                            Value={developerData.projectsDelivered ? developerData.projectsDelivered : "NA"}
                            Heading={strings.PROJECT_DELIVERED} />
                        <ImageWithTitle
                            isShadow={true}
                            src={res.images.unit}
                            Value={developerData.unitsDelivered ? developerData.unitsDelivered : "NA"}
                            Heading={strings.UNIT_DELIVERED} />
                        <ImageWithTitle
                            isShadow={true}
                            src={res.images.award}
                            Value={developerData.awardsDetails && developerData.awardsDetails.length ? developerData.awardsDetails.length : "NA"}
                            Heading={strings.AWARD_WON} />
                        {
                            memberArray.length > 0 &&
                            <ImageWithTitle
                                isShadow={true}
                                src={res.images.partners}

                                Value={memberArray.join(', ')}
                                Heading={strings.Association_Memberships} />

                        }
                        <View style={{height: 50}}/>

                    </View>
                    {/* <Text style={styles.AssociationText}>{res.strings.Association_Memberships}</Text>
                    <View style={{ flexDirection: 'row', flexWrap: 'wrap', width: '100%', justifyContent: 'space-evenly' }}>
                        {this.props.memberShip.map((item, index) => {
                            return (
                                <CardWithTitle titleStyle={styles.addMoreText} uri={item.url} />
                            )
                        })}
                        <CardWithTitle isShodow={true} imageStyle={{ resizeMode: 'stretch', width: 90, height: 58 }} title={""} />
                    </View> */}

                </KeyboardAwareScrollView>
            </View>)


    }
}

const styles = StyleSheet.create({
    modalStyle: {
        flex: 1,
        top: Platform.OS == 'ios' ? 100 : 50,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        backgroundColor: res.colors.butterscotch
    },
    shadowHeader: {
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        backgroundColor: res.colors.butterscotch,
        shadowColor: res.colors.shadowColor,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 10.58,
        shadowRadius: 10.00,
        elevation: 24,
    },
    corousalNameText: {
        fontFamily: res.fonts.semiBold,
        fontSize: 18,
        letterSpacing: 0.3,
        textAlign: 'center',
        color: res.colors.greyishBrown,
        marginTop: 10
    },AssociationText:{
        fontWeight: 'bold',
        marginVertical: heightScale(10),
        marginHorizontal: widthScale(10),
        fontSize: 15,
        fontFamily: res.fonts.bold,
        color: res.colors.greyishBrown
    }
});



export default DeveloperDetailModal;