import React from 'react'
import { View, Text, Image, SafeAreaView } from 'react-native'
import styles from './styles'
import {heightScale,widthScale,myWidth}from '../../utility/Utils'
const CardWithImages = (props) => {
    const { src,imageStyle,} = props
    return (

        < View style={[styles.imageList, styles.shadow]} >

            <Image style={{ width: (myWidth - 180) / 2.0, height: heightScale(104), borderRadius: 5 }} source={src} resizeMode={'contain'}  />

        </View>


    )
}
export default CardWithImages