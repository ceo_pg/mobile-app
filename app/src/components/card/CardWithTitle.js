import React from 'react'
import { View, Text, Image, SafeAreaView } from 'react-native'
import styles from './styles'
import { widthScale } from '../../utility/Utils'
const CardWithTitle = (props) => {
    const { src, uri, title, Value, imageStyle, titleStyle, valueStyle, isShodow, isProjectSpecs } = props
    console.log("isProjectSpecs",isProjectSpecs)
    return (

        <View style={[isProjectSpecs ? styles.projecSpecs : styles.projecSpeList, isShodow ? {} : styles.shadow]}>


            {!isProjectSpecs ?
                
                    (src || uri) ?
                        <Image style={[{ width: "100%", height: "100%" }, imageStyle]} source={src ? src : uri ? { uri: uri } : ''} resizeMode={'contain'} /> : null
           
                :
                <View>
                    <Image style={[{ width: widthScale(40), height: widthScale(40) },imageStyle]} source={props.src} resizeMode={'contain'} />

                </View>}


            {
                title ?
                    <Text style={[styles.specificationText, titleStyle]}>{title ? title : ""}</Text> :
                    null
            }
            {
                Value ?
                    <Text style={[styles.specificationvalueText, valueStyle]}>{Value ? Value : ""}</Text> :
                    null
            }

        </View>


    )
}

export default CardWithTitle