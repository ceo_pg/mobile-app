import { StyleSheet, Platform,Dimensions } from 'react-native'
import res from '../../../res'
import { myWidth, widthScale, heightScale } from '../../utility/Utils'
const styles = StyleSheet.create({
    modalStyel: {
        flex: 1, backgroundColor: res.colors.butterscotch, borderTopLeftRadius: 30, borderTopRightRadius: 30, marginTop: 10,
    }, amenitiesList: {

        //  margin: 10,
        minHeight: 104,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 5,
        flexDirection: 'column',
        width: '46%'
    }, shadow: {
        backgroundColor: res.colors.butterscotch,
        shadowColor: res.colors.shadowColor,
        shadowOffset: {
            width: 2,
            height: 2,
        },
        shadowOpacity: 5.25,
        shadowRadius: 3.84,
        elevation: 5,
    }, specificationText: {
        fontFamily: res.fonts.medium,
        fontSize: 14,
        color: res.colors.greyishBrown,
        textAlign: 'center',
        top: 10,

        paddingLeft: 10,
        paddingRight: 10
    }, specificationvalueText: {
        fontFamily: res.fonts.medium,
        fontSize: 11,
        color: res.colors.greyishBrown,
        textAlign: 'center',
        top: 10,

        paddingLeft: 10,
        paddingRight: 10
    }, UnitLevel: {
        minHeight: 80,
        marginHorizontal: 10,
        backgroundColor: res.colors.butterscotch,
        marginTop: 10,
        borderRadius: 5,
        //  alignItems: 'center',
        justifyContent: 'center',
        bottom: 10
    }, valueStyle: {
        fontFamily: res.fonts.medium,
        fontSize: 16,
        color: res.colors.peacockBlue
    }, headingStyle: {
        // flex:1,
        fontFamily: res.fonts.medium,
        fontSize: 14,
        color: res.colors.greyishBrown
    }, imageList: {
        marginHorizontal: widthScale(5.5),
        marginVertical: widthScale(4.5),
        minHeight: 104,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 5,
        flexDirection: 'column',
        width: '46%'
    },
    partnerAndLocationCard: {
        marginHorizontal: 10
    }, indexOfChangesView: {
        marginHorizontal: widthScale(4.5),
        marginVertical: widthScale(4.5),
        minHeight: 70,
        borderRadius: 5,
        flexDirection: 'column',
        width: '46%',


    },
    indexOfChangesHeader: {
        fontWeight: 'bold',
        fontFamily: res.fonts.bold,
        color: res.colors.greyishBrown,
        top: 10,
        paddingLeft: 10,
        paddingRight: 10
    },
    indexOfChangesValue: {
        fontFamily: res.fonts.regular,
        fontSize: 12,
        color: res.colors.greyishBrown,
        top: 25,
        paddingLeft: 10,
        paddingRight: 10
    }, WallFinishLevel: {
        minHeight: 110,
        marginHorizontal: 10,
        backgroundColor: res.colors.butterscotch,
        marginTop: 10,
        borderRadius: 5,
        bottom: 10,
    }, projecSpeList: {
        marginHorizontal: widthScale(5.5),
        marginVertical: widthScale(4.5),
        height: (Dimensions.get('window').width - 80)*0.35,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 5,
        flexDirection: 'column',
        width: '44%',

    }, projecSpecs: {
        marginHorizontal: widthScale(5.5),
        marginVertical: widthScale(4.5),
        minHeight: 104,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 5,
        flexDirection: 'column',
        width: '44%',
        // backgroundColor:'red'
    },
    doubleCard: {
        padding: 5,
        marginHorizontal: widthScale(5.5),
        marginVertical: widthScale(4.5),
        minHeight: 80,
        // alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 5,
        flexDirection: 'column',
        width: '44%',

    }, shadow2: {
        // backgroundColor: res.colors.butterscotch,
        shadowColor: res.colors.shadowColor,
        shadowOffset: {
            width: 2,
            height: 2,
        },
        shadowOpacity: 8.25,
        shadowRadius: 6.84,
        elevation: 2,
    }, bankList: {
        marginHorizontal: widthScale(5.5),
        marginVertical: widthScale(4.5),
        minHeight: 84,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 5,
        flexDirection: 'column',
        width: '46%'
    }, resultText: {
        fontFamily: res.fonts.regular,
        fontSize: 12,
        color: res.colors.liteBlack
    }
})
export default styles