import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image, StyleSheet, Linking } from 'react-native';
import { connect } from "react-redux";
import { bindActionCreators } from "redux"
import res from '../../../res'
import { widthScale, heightScale } from '../../utility/Utils'
import { openShareModal } from '../../redux/actions/SharePostAction';
import Modal from 'react-native-modal'
import style from '../button/styles';

class DocItemComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isModalVisible: false,
            selectedData: {}
        }
    }
    openShareModal = (data) => {
        const { isModalVisible } = this.state
        this.setState({
            isModalVisible: !isModalVisible,
            selectedData: data ? data : {}
        })

    }
    shareModal = () => {
        const { data, onUpdateDoc,uniqueID } = this.props;
        console.log("DOCUMENTDAT",data)
        return <View style={styles.shareModalView}>
            {/* <Text style={styles.shareNowText}>{res.strings.OPTIONS}</Text> */}
            <View style={[styles.iconStyle]}>
                <TouchableOpacity style={styles.actionLabelBackground} onPress={()=> {
                    this.openShareModal()
                    onUpdateDoc(uniqueID)}
                    }>
                    <Text style={styles.actionLabel}>{data && data.base && data.base.length > 0 ? res.strings.UPDATE : res.strings.UPLOAD}</Text>
                </TouchableOpacity>
                
                {data && (data.base && data.base.length > 0 || data.path&&data.path.length>0) && 
                 <TouchableOpacity style={styles.actionLabelBackground} onPress={()=> this.openFile(data)}>
                 <Text style={styles.actionLabel}>{res.strings.VIEW}</Text>
             </TouchableOpacity>
                }
               
            </View>

        </View>

    }

    openFile = (data) => {
        Linking.canOpenURL(data.base).then(supported => {
            if (supported) {
                Linking.openURL(data.base);
            } else {
                console.log("Don't know how to open URI: " + data.base);
            }
        });
    }

    render() {
        const { label, data } = this.props
        return (
            <View style={styles.containerView}>
                <Modal
                    onRequestClose={() => this.openShareModal()}
                    onBackdropPress={() => this.openShareModal(data)}
                    isVisible={this.state.isModalVisible}
                >
                    {this.shareModal()}
                </Modal>
                <View style={styles.uploadView}>


                    <View style={{ width: "15%" }}>
                        <Image source={res.images.interestrate} style={styles.imagesStyle} resizeMode={'contain'} />
                    </View>
                    <View style={{ width: "75%"}}>
                        <Text style={styles.label}>{label ? label : ''}</Text>
                    </View>
                    <TouchableOpacity style={styles.uploadIconView} hitSlop={{ top: 10, bottom: 10, left: 10, right: 10 }} onPress={() => this.openShareModal()}>
                        <Image source={res.images.dot_ic} resizeMode={'stretch'} />
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    uploadIconView: {
        paddingRight: widthScale(5)
    },
    imagesStyle: {
        width: widthScale(19),
        height: heightScale(19)
    },
    shareNowText: {
        top: -30,
        fontFamily: res.fonts.semiBold,
        fontSize: heightScale(18),
        color: res.colors.blackTwo
    },
    iconStyle: {
        alignItems: "center",
        justifyContent: 'space-around',
        flexDirection: 'row',
        width: '100%',
    },
    shareModalView: {
        backgroundColor: res.colors.butterscotch,
        height: heightScale(100),
        marginTop: heightScale(100),
        borderRadius: widthScale(24),
        marginHorizontal: widthScale(15),
        alignSelf: 'center',
        width: "100%",
        alignItems: "center",
        justifyContent: 'center',

    },

    containerView: {
        marginVertical: heightScale(8),
        flex: 1,
        marginHorizontal: widthScale(18),

    },
    uploadView: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        borderRadius: widthScale(12),
        backgroundColor: res.colors.butterscotch,
        shadowColor: res.colors.shadowColor,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 10.58,
        shadowRadius: 10.00,
        elevation: 24,
        paddingVertical: widthScale(3),
        paddingHorizontal:widthScale(10)
    },
    label: {
        fontFamily: res.fonts.semiBold,
        fontSize: widthScale(12),
        color: res.colors.formLabelGrey
    },
    actionLabel: {
        fontFamily: res.fonts.semiBold,
        fontSize: widthScale(15),
        color: res.colors.white
    },

    actionLabelBackground: {
        width: widthScale(80),
        height: heightScale(30),
        backgroundColor: res.colors.btnBlue,
        borderRadius: 5,
        alignItems: 'center',
        justifyContent: 'center'
    }
})

// export default DocumentItemComp;
function mapStateToProps(state) {
    return {
        isVisiblePostModal: state.SharePostReducer.showSharePostModal,

    };
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        openShareModal,

    }, dispatch);
}

const DocItemComponentCompComponent = connect(mapStateToProps, mapDispatchToProps)(DocItemComponent);
export default DocItemComponentCompComponent
