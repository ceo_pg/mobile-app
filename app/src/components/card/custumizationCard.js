import React, { Component } from 'react'
import { View, Text, Image, StyleSheet } from 'react-native'
import { heightScale, widthScale } from '../../utility/Utils'
import res from '../../../res'
const CustumizationCard = (props) => {
    const { src, uri, name, isChecked, imageStyle } = props
    return (<View style={[styles.progresBox, styles.shadow]}>
        <View style={styles.imageView}>
            {(src || uri) ?
                <Image style={[{ width: widthScale(30), height: widthScale(30) }, imageStyle]} source={src ? src : uri ? { uri: uri } : ''} resizeMode={'contain'} /> :
                <View></View>
            }
        </View>
        <View style={styles.processText}>
            <Text style={styles.nameText}>{name ? name : ""}</Text>
        </View>
        <View>
            {
                isChecked ?
                    <Image source={res.images.icn_check} /> :
                    <View />
            }

        </View>
    </View>)
}
const styles = StyleSheet.create({
    nameText: {
        fontFamily: res.fonts.semiBold,
        fontSize: heightScale(16),
        color:'rgba(76,76,76,0.8)'
    },
    progresBox: {
        marginTop: 10,
        height: heightScale(52),
        flexDirection: 'row',
        alignItems: 'center',
        width: '95%',
        backgroundColor: res.colors.white,
        borderRadius: 5,
        marginHorizontal: 10,
        marginBottom: 10
    }, imageView: {
        width: '20%',
        alignItems: 'center',
        justifyContent: 'center'
    }, processText: {
        width: '60%',
        justifyContent: 'center'
    }, shadow: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 4,
        },
        shadowOpacity: 0.30,
        shadowRadius: 4.65,

        elevation: 8,
    },
})
export default CustumizationCard