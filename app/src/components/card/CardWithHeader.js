import React from 'react'
import { View, Text, Image, SafeAreaView,StyleSheet } from 'react-native'
import res from '../../../res'

const CardWithHeader = (props) => {
    const { src, uri, title, Value, imageStyle, titleStyle, valueStyle } = props
    console.log(Value, "props");
    // if(Value && Array.isArray(Value)){
    //   isDescriptiVeArray = true
    // }else{
    //   isDescriptiVeArray = false
    // }
    return (
        <View style={[styles.containerStyle,styles.shadow]}>
            <View style={{marginTop:13}}>
              <Text style={[styles.specificationText, titleStyle]}>{title ? title : ""}</Text>
              </View>
              <View style={{flexDirection:'row', marginTop:22}}>
              {(src || uri ) ?
                <Image style={[{ width: 50, height: 50 }, imageStyle]} source={src ? src : uri ? { uri: uri } : ''} resizeMode={'cover'} /> :
                <View></View>
            }
            <View style={{marginLeft:10, paddingRight:27}}>
            {/* <Text  style={[styles.descriptiveText, valueStyle]}>{decriptiveValue}</Text> */}
            {renderDescriptiveText(Value)}
            </View>
              </View>
        </View>


    )
}

const renderDescriptiveText = (textValue) =>{
  console.log(textValue, "textArray")
  if(textValue && Array.isArray(textValue) && textValue.length > 0){
   return <View>{textValue.map((item) =>{
    return (   
    <Text style={[[styles.descriptiveText]]}>{item}</Text>
    )
    })}
    </View> 
  }else{
    return (<Text style={[styles.descriptiveText]}>{textValue}</Text>)
  }
}

const styles = StyleSheet.create({
  containerStyle:{
    minHeight: 80,
    marginHorizontal: 10,
    backgroundColor: res.colors.butterscotch,
    marginTop: 10,
    borderRadius: 5,
    justifyContent: 'center',
    bottom:10,
    paddingBottom:10,
    paddingHorizontal:22
  },
  shadow: {
    backgroundColor: res.colors.butterscotch,
    shadowColor: res.colors.shadowColor,
    shadowOffset: {
        width: 2,
        height: 2,
    },
    shadowOpacity: 5.25,
    shadowRadius: 3.84,
    elevation: 5,
},
descriptiveText: {
  fontFamily: res.fonts.regular,
  fontSize: 12,
  color: res.colors.liteBlack,
  letterSpacing:0.13,
  paddingRight:5
}
  
});
export default CardWithHeader