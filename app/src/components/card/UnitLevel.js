import React from 'react'
import { View, Text, Image } from 'react-native'
import styles from './styles'
import res from '../../../res'
import images from '../../../res/images'
const UnitLevel = (props) => {
    const { source, name, brand, dimensions, title, meter, imagesStyle } = props

    return (
        <View style={[styles.WallFinishLevel, styles.shadow]}>
            <Text style={{ marginLeft: 10, marginTop: 13, color: res.colors.greyishBrown }}>{title ? title : ""}</Text>
            <View style={{ flexDirection: 'row', marginHorizontal: 10, marginTop: 10 }}>
                <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                    <Image style={[{ width: 50, height: 58 }, imagesStyle]} source={source ? source : ""} resizeMode={'contain'} />
                </View>
                <View style={{ marginLeft: 10, justifyContent: 'center' }}>
                    {
                        name ?
                            <Text style={styles.resultText}>{name ? name : ""} {meter ? meter : ""}</Text>
                            :
                            null
                    }
                    {
                        brand ?
                            <Text style={styles.resultText}>{brand ? brand : ""}</Text> :
                            null
                    }
                    {
                        dimensions ?
                            <Text style={styles.resultText}>{dimensions ? dimensions : ""}</Text> :
                            null
                    }



                </View>

            </View>
        </View>


    )
}
export default UnitLevel