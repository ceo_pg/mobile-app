import React from 'react'
import { View, Text, Image, SafeAreaView } from 'react-native'
import styles from './styles'
const TitleWithValue = (props) => {
    const { indexOfChangesValue,indexOfChangesHeader,indexOfChangesView } = props
    return (
        < View style={[styles.indexOfChangesView,indexOfChangesView]} >
            <Text style={[styles.indexOfChangesHeader,indexOfChangesHeader]}>{props.title ? props.title : "NA"}</Text>
            <Text numberOfLines={3} style={[styles.indexOfChangesValue, indexOfChangesValue]}>{props.Value ? props.Value : "NA"}</Text>
        </View>


    )
}
export default TitleWithValue
