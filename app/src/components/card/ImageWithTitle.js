import React from 'react'
import { View, Text, Image, SafeAreaView, TouchableOpacity } from 'react-native'
import styles from './styles'

const ImageWithTitle = (props) => {
    const { src, uri, imageStyle, valueStyle, headingStyle, isShadow, isdoubleCard, unitText, isTouchable, toggleModal } = props
    return (
        !isTouchable ?
            <View style={[isdoubleCard ? styles.doubleCard : styles.UnitLevel, isShadow ? styles.shadow : {}]}>
                <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                    <View>
                        {(src || uri) ?
                            <Image style={[{ width: 54, height: 40 }, imageStyle]} source={src ? src : uri ? { uri: uri } : ''} resizeMode={'contain'} /> :
                            <View></View>
                        }
                    </View>
                    <View style={{ flexDirection: 'column', flex: 1, justifyContent: 'center', paddingLeft: 5 }}>
                        <View style={{ flexDirection: 'row' }}>
                            {
                                props.unitText ?
                                    <Text style={[styles.valueStyle, valueStyle]}>{props.unitText ? props.unitText : ""}</Text> :
                                    null
                            }
                            <Text style={[styles.valueStyle, valueStyle]}>{props.Value ? props.Value : "0"}</Text>

                        </View>



                        <Text style={[styles.headingStyle, headingStyle]}>{props.Heading ? props.Heading : ""}</Text>
                    </View>
                </View>

            </View>
            :
            <TouchableOpacity onPress={toggleModal} style={[isdoubleCard ? styles.doubleCard : styles.UnitLevel, isShadow ? styles.shadow : {}]}>
                <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                    <View>
                        {(src || uri) ?
                            <Image style={[{ width: 54, height: 40 }, imageStyle]} source={src ? src : uri ? { uri: uri } : ''} resizeMode={'contain'} /> :
                            <View></View>
                        }
                    </View>
                    <View style={{ flexDirection: 'column', flex: 1, justifyContent: 'center' }}>
                        <Text style={[styles.valueStyle, valueStyle]}>{props.Value ? props.Value : "0"}</Text>
                        {
                            props.unitText ?
                                <Text style={[styles.valueStyle, valueStyle]}>{props.unitText ? props.unitText : ""}</Text> :
                                null
                        }

                        <Text style={[styles.headingStyle, headingStyle]}>{props.Heading ? props.Heading : ""}</Text>
                    </View>
                </View>
            </TouchableOpacity>


    )
}
export default ImageWithTitle