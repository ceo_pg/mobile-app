import { StyleSheet, Platform } from 'react-native'
import res from '../../../res'
import { myWidth } from '../../utility/Utils'
const styles = StyleSheet.create({
    vaastuScore: {
        minHeight: 40,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        // backgroundColor: 'rgb(242,177,63)'
    }, isDarkVaastuView: {
        minHeight: 40,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: 'rgb(242,177,63)'
    }, snoText: {
        padding: 5,
        fontSize: 10,
        fontFamily: res.fonts.regular,
        color: res.colors.black,
        textAlign: 'left'
    }, vastuText: {
        padding: 5,
        fontSize: 10,
        fontFamily: res.fonts.regular,
        color: res.colors.black,
        textAlign: 'center'
    }, dataView: {
        width: '60%', justifyContent: 'flex-start'
    }, vaastuScoreView: {
        minHeight: 93,
        justifyContent: 'space-between',
        alignItems: 'center',
        // backgroundColor: 'rgb(242,177,63)',
        flexDirection: 'column',
        marginHorizontal: 10,
        marginTop: 10
    }, headerText: {
        padding: 5
        , fontSize: 12,
        fontFamily: res.fonts.regular,
        color: res.colors.black,

    }, totalText: {
        padding: 5
        , fontSize: 11,
        fontFamily: res.fonts.bold,
        color: res.colors.purpleBrown,
        textAlign: 'left'
    }, isDarkVaastuTotalView: {
        marginTop: 3,
        height: 25,
        flexDirection: 'row',
        justifyContent: 'space-between',


    }, vaastuTotalScore: {

        height: 40,
        flexDirection: 'row',
        justifyContent: 'space-between',

    },
    totalScoreFisrtView: {
        width: '90%',
        justifyContent: 'center',
        borderColor: 'rgb(242,177,63)',
        minHeight: 40,
        borderWidth: 1,

    },
    totalScoreSecondView:
    {
        width: '10%', justifyContent: 'center', borderColor: 'rgb(242,177,63)', minHeight: 40, borderWidth: 1,textAlign:'center'
    }, totalScoreDarkView: {
        width: '85%',
        justifyContent: 'center',
        borderColor: 'rgb(242,177,63)',
        height: 25,
        backgroundColor: 'rgb(242,177,63)',
    }, isDarkOtherParameter: {
        minHeight: 40,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: 'rgb(242,177,63)'
    }, isOtherParameter: {
        minHeight: 40,
        flexDirection: 'row',
        justifyContent: 'space-between',
    }, totalScoreText: {
        padding: 5,
        fontSize: 12,
        fontFamily: res.fonts.medium,
        color: res.colors.purpleBrown,
        textAlign: 'left'
    }, totalScoreValueView: {
         width: '10%',
        justifyContent: 'center',
        borderColor: 'rgb(242,177,63)',
        minHeight: 25,
        backgroundColor: 'rgb(242,177,63)',
    }, scoreStyle: {
        padding: 5,
        fontSize: 10,
        fontFamily: res.fonts.regular,
        color: res.colors.black,
        textAlign: 'left'
    },totalScorevalue: {
        padding: 5,
        fontSize: 12,
        fontFamily: res.fonts.medium,
        color: res.colors.purpleBrown,
        textAlign: 'center'
    }, totalValue: {
        padding: 5
        , fontSize: 11,
        fontFamily: res.fonts.bold,
        color: res.colors.purpleBrown,
        textAlign: 'center'
    },
})
export default styles