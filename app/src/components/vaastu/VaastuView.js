import React from 'react'
import { View, Text, Image, SafeAreaView } from 'react-native'
import styles from './styles'
import resources from '../../../res'
const VaastuData = (props) => {
    const { S_No, header, value, score, isDark, scoreStyle, vaastuTotalScoreStyle } = props
    return (

        < View style={[isDark ? styles.isDarkVaastuView : styles.vaastuScore]} >
            <View style={{ width: '10%', alignItems: 'center', justifyContent: 'center', borderColor: 'rgb(242,177,63)', minHeight: 40, borderWidth: 1, }}>
                <Text numberOfLines={2} style={styles.snoText}>
                    {S_No ? S_No : ""}
                </Text>
            </View>
            <View style={{ width: '55%', justifyContent: 'center', borderColor: 'rgb(242,177,63)', minHeight: 40, borderWidth: 1, }}>
                <Text style={styles.snoText}>
                    {header ? header : ""}
                </Text >
            </View>
            <View style={{ width: '25%', justifyContent: 'center', borderColor: 'rgb(242,177,63)', minHeight: 40, borderWidth: 1, }}>
                <Text style={[styles.snoText]}>
                    {value ? value : ""}
                </Text>
            </View>
            <View style={{ width: '10%', justifyContent: 'center', borderWidth: 1, borderColor: 'rgb(242,177,63)', minHeight: 40, alignItems: 'center' }}>
                <Text style={[styles.snoText, scoreStyle]}>
                    {score ? score : ""}
                </Text>
            </View>


        </View>


    )
}
const VaastuScore = (props) => {
    const { north, east, west, south, northEast, northWest, southEast, southWest, header, value, score, isDark, scoreStyle } = props
    return (

        < View style={[styles.vaastuScoreView, styles.shadow]} >

            <View style={{ width: '100%', justifyContent: 'center', minHeight: 37, backgroundColor: 'rgb(242,177,63)', }}>
                <Text style={[styles.headerText, { textAlign: 'center' }]}>
                    {header ? header : ""}
                </Text >
            </View>
            <View style={{ flexDirection: 'row', width: '100%', minHeight: 33, }}>
                <View style={{ width: "12%", borderWidth: 1, borderColor: 'rgb(242,177,63)', }}>
                    <Text style={[styles.snoText]}>
                        {resources.strings.North}
                    </Text>
                </View>
                <View style={{ width: "10%", borderWidth: 1, borderColor: 'rgb(242,177,63)', }}>
                    <Text style={[styles.snoText]}>
                        {resources.strings.East}
                    </Text>
                </View>
                <View style={{ width: "12%", borderWidth: 1, borderColor: 'rgb(242,177,63)', }}>
                    <Text style={[styles.snoText]}>
                        {resources.strings.West}
                    </Text>
                </View>

                <View style={{ width: "12%", borderWidth: 1, borderColor: 'rgb(242,177,63)', }}>
                    <Text style={[styles.snoText]}>
                        {resources.strings.South}
                    </Text>
                </View>
                <View style={{ width: "14%", borderWidth: 1, borderColor: 'rgb(242,177,63)', }}>
                    <Text style={[styles.snoText]}>
                        {resources.strings.NorthEast}
                    </Text>
                </View>
                <View style={{ width: "14%", borderWidth: 1, borderColor: 'rgb(242,177,63)', }}>
                    <Text style={[styles.snoText]}>
                        {resources.strings.NorthWest}
                    </Text>
                </View>


                <View style={{ width: "13%", borderWidth: 1, borderColor: 'rgb(242,177,63)', }}>
                    <Text style={[styles.snoText]}>
                        {resources.strings.SouthEast}
                    </Text>
                </View>
                <View style={{ width: "13%", borderWidth: 1, borderColor: 'rgb(242,177,63)', }}>
                    <Text style={[styles.snoText]}>
                        {resources.strings.SouthhWest}
                    </Text>
                </View>


            </View>

            <View style={{ flexDirection: 'row', width: '100%', minHeight: 33, }}>
                <View style={{ width: "12%", borderWidth: 1, borderColor: 'rgb(242,177,63)', }}>
                    <Text style={[styles.vastuText]}>
                        {north ? north : "0"}
                    </Text>
                </View>
                <View style={{ width: "10%", borderWidth: 1, borderColor: 'rgb(242,177,63)', }}>
                    <Text style={[styles.vastuText]}>
                        {east ? east : "0"}
                    </Text>
                </View>
                <View style={{ width: "12%", borderWidth: 1, borderColor: 'rgb(242,177,63)', }}>
                    <Text style={[styles.vastuText]}>
                        {west ? west : "0"}
                    </Text>
                </View>

                <View style={{ width: "12%", borderWidth: 1, borderColor: 'rgb(242,177,63)', }}>
                    <Text style={[styles.vastuText]}>
                        {south ? south : "0"}
                    </Text>
                </View>
                <View style={{ width: "14%", borderWidth: 1, borderColor: 'rgb(242,177,63)', }}>
                    <Text style={[styles.vastuText]}>
                        {northEast ? northEast : "0"}
                    </Text>
                </View>
                <View style={{ width: "14%", borderWidth: 1, borderColor: 'rgb(242,177,63)', }}>
                    <Text style={[styles.vastuText]}>
                        {northWest ? northWest : "0"}
                    </Text>
                </View>


                <View style={{ width: "13%", borderWidth: 1, borderColor: 'rgb(242,177,63)', }}>
                    <Text style={[styles.vastuText]}>
                        {southEast ? southEast : "0"}
                    </Text>
                </View>
                <View style={{ width: "13%", borderWidth: 1, borderColor: 'rgb(242,177,63)', }}>
                    <Text style={[styles.vastuText]}>
                        {southWest ? southWest : "0"}
                    </Text>
                </View>


            </View>
            {/* <View style={{ width: '10%', justifyContent: 'center', borderWidth: 1, borderColor: 'rgb(242,177,63)', minHeight: 40, alignItems: 'center' }}>
                <Text style={[styles.snoText, scoreStyle]}>
                    {score ? score : ""}
                </Text>
            </View> */}


        </View>


    )
}
const TotalScore = (props) => {
    const { header, value, isDark, vaastuTotalScoreStyle } = props
    return (

        < View style={[isDark ? styles.isDarkVaastuTotalView : styles.vaastuTotalScore, vaastuTotalScoreStyle]} >
            <View style={isDark ? styles.totalScoreDarkView : styles.totalScoreFisrtView}>
                <Text numberOfLines={2} style={isDark ? styles.totalScoreText : styles.totalText}>
                    {header ? header : ""}
                </Text>
            </View>
            <View style={isDark ? styles.totalScoreValueView : styles.totalScoreSecondView}>
                <Text style={isDark ? styles.totalScorevalue : styles.totalValue}>
                    {value ? value : ""}
                </Text >
            </View>
        </View>


    )
}
const DeveloperScore = (props) => {
    const { S_No, header, weight, score, isDark, scoreStyle, scoreParam } = props
    return (

        < View style={[isDark ? styles.isDarkVaastuView : styles.vaastuScore]} >
            <View style={{ width: '10%', alignItems: 'center', justifyContent: 'center', borderColor: 'rgb(242,177,63)', minHeight: 40, borderWidth: 1, }}>
                <Text numberOfLines={2} style={styles.snoText}>
                    {S_No ? S_No : ""}
                </Text>
            </View>
            <View style={{ width: '30%', justifyContent: 'center', borderColor: 'rgb(242,177,63)', minHeight: 40, borderWidth: 1, }}>
                <Text numberOfLines={2} style={styles.snoText}>
                    {header ? header : ""}
                </Text >
            </View>
            <View style={{ width: '20%', justifyContent: 'center', borderColor: 'rgb(242,177,63)', minHeight: 40, borderWidth: 1, }}>
                <Text style={[styles.snoText]}>
                    {score ? score : "0"}
                </Text>
            </View>
            <View style={{ width: '20%', justifyContent: 'center', borderWidth: 1, borderColor: 'rgb(242,177,63)', minHeight: 40, alignItems: 'center' }}>
                <Text style={[styles.snoText, scoreStyle]}>
                    {weight ? weight : "0"}
                </Text>
            </View>
            <View style={{ width: '20%', justifyContent: 'center', borderWidth: 1, borderColor: 'rgb(242,177,63)', minHeight: 40, alignItems: 'center' }}>
                <Text style={[styles.snoText, scoreStyle]}>
                    {scoreParam ? scoreParam : "0"}
                </Text>
            </View>

        </View>


    )
}
export { VaastuData, VaastuScore, TotalScore, DeveloperScore }