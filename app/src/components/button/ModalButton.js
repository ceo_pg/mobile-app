import React from 'react'
import {
    TouchableOpacity,
    View,
    Text,
    TouchableHighlight
} from 'react-native'
import styles from './styles'
import style from './styles'

const ModalButton = ({
    solid,
    children,
    rounded,
    outlined,
    customStyle,
    btnText,
    onPress,
    btnStyle,
    touchOpacityStyle, textStyleOver,
    disableTouch,
    labelName
}) => {
    let inlineStyle = []
    inlineStyle = inlineStyle.concat(styles.defaultStyleModal)
    if (rounded) {
        inlineStyle = inlineStyle.concat(styles.roundBorder)
    }
    if (outlined) {
        inlineStyle = inlineStyle.concat(styles.outlined)
    }
    if (customStyle) {
        inlineStyle = inlineStyle.concat(customStyle)
    }
    if (solid) {
        inlineStyle = inlineStyle.concat(styles.solid)
    }

    btnStyle = btnStyle ? btnStyle : {}

    return (

        <TouchableHighlight disabled={disableTouch ? disableTouch : false}
            underlayColor='transparent'
            style={[touchOpacityStyle ? touchOpacityStyle : styles.styleTouchableView]}
            onPress={() => onPress()}
            activeOpacity={1}>
            {
                btnText ?
                    <View style={[inlineStyle, btnStyle]}>


                        <Text style={style.slectedLabelNameText}>{labelName}</Text>
                        <Text style={[styles.textStyleModal, textStyleOver || {}]} textBreakStrategy={'simple'} numberOfLines={1}>{btnText}</Text>



                    </View> :
                    <View style={[inlineStyle, btnStyle]}>


                        <Text style={style.labelNameText}>{ labelName}</Text>
                        



                    </View>
            }

        </TouchableHighlight>


    )
}


export default ModalButton