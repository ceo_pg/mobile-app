import { StyleSheet } from 'react-native';
import resources from '../../../res';
const style = StyleSheet.create({
    defaultStyle: {
        // ...shadow(2),
        height: 48,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: resources.colors.btnBlue,
        alignSelf: "stretch",
        borderRadius: 5
    },
    styleTouchableView: {
        marginTop: 20,
        // height: 48,
        width: '100%',
        alignSelf: "center",
    },
    roundBorder: {
        borderRadius: 10
    },
    outlined: {
        backgroundColor: 'transparent',
        borderWidth: 1
    },
    textStyle: {
        fontFamily: resources.fonts.regular,
        fontSize: 14,
        color: resources.colors.white,

    },
    solid: {
        backgroundColor: 'blue',

    }, shadow: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,
    }, defaultStyleModal: {
        marginTop: 20,
        height: 48,
        justifyContent: "center",
        // alignItems: "center",
        backgroundColor: resources.colors.white,
        alignSelf: "stretch",
        // borderRadius: 5,
        borderBottomColor: resources.colors.underlineColor,
        borderBottomWidth: 2
    }, textStyleModal: {
        fontFamily: resources.fonts.regular,
        fontSize: 18,
        color: resources.colors.black,
        paddingBottom: 20,
        paddingLeft: 0,
        alignSelf: 'stretch',
        textAlign: 'left',
    }, percentageText: {
        fontFamily: resources.fonts.semiBold,
        fontSize: 12,
        color: resources.colors.charcoalGrey,
        marginLeft: 10
    }, percentageLabel: {
        fontFamily: resources.fonts.regular,
        fontSize: 12,
        color: resources.colors.charcoalGrey,
        flex: 1,
        marginLeft: 10
    }, labelNameText: {
        fontFamily: resources.fonts.regular,
        fontSize: 14,
        color: resources.colors.warmGrey2,
        // marginBottom: 11
    }, slectedLabelNameText: {
        fontFamily: resources.fonts.regular,
        fontSize: 14,
        color: resources.colors.black,
        paddingBottom: 9
    }, forgotPasswprdInstructiom: {
        fontFamily: resources.fonts.regular,
        fontSize: 14,
        color: resources.colors.warmGrey,
        textAlign: 'center',
        marginTop: 22
    }, dataContainer: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        width: '100%',
        justifyContent: 'space-evenly'
    }
})
export default style;