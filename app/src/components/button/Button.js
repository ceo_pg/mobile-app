import React from 'react'
import {
    TouchableOpacity,
    View,
    Text,
    TouchableHighlight
} from 'react-native'
import styles from './styles'

const PGButton = ({
    solid,
    children,
    rounded,
    outlined,
    customStyle,
    btnText,
    onPress,
    btnStyle,
    touchOpacityStyle, textStyleOver,
    disableTouch
}) => {
    let inlineStyle = []
    inlineStyle = inlineStyle.concat(styles.defaultStyle)
    if (rounded) {
        inlineStyle = inlineStyle.concat(styles.roundBorder)
    }
    if (outlined) {
        inlineStyle = inlineStyle.concat(styles.outlined)
    }
    if (customStyle) {
        inlineStyle = inlineStyle.concat(customStyle)
    }
    if (solid) {
        inlineStyle = inlineStyle.concat(styles.solid)
    }

    btnStyle = btnStyle ? btnStyle : {}

    return (
       
            <TouchableHighlight disabled={disableTouch ? disableTouch : false}
                underlayColor='#ffffff'
                style={[touchOpacityStyle ? touchOpacityStyle : styles.styleTouchableView]}
                onPress={() => onPress()}
                activeOpacity={0.8}>
                <View style={[inlineStyle, btnStyle,styles.shadow]}>
                    {children}
                    <Text style={[styles.textStyle, textStyleOver || {}]}>{btnText}</Text>
                </View>
            </TouchableHighlight>
   

    )
}


export default PGButton