import React, { Component } from 'react'
import { Text, StyleSheet } from 'react-native'
import res from '../../../res'
import ScrollableTabView, { ScrollableTabBar } from 'react-native-scrollable-tab-view';
import { heightScale } from '../../utility/Utils';


const TabVIew = (props) => {
    const { tabBarActiveTextColor, tabBarInactiveTextColor,
        tabBarUnderlineStyle, selectedIndex, listDataItem, categoryRef } = props

    const returnView = listDataItem.map((item, index) => {

        return <Text
            style={styles.labelText}
            tabLabel={item.cell}
            key={index.toString()}>
        </Text>
    })
    return (
        <ScrollableTabView
            style={{ backgroundColor: res.colors.appColor }}
            tabBarUnderlineStyle={tabBarUnderlineStyle}
            tabBarTextStyle={{ fontFamily: res.fonts.Sregular, fontSize: 15 }}
            tabBarInactiveTextColor={tabBarInactiveTextColor}
            tabBarActiveTextColor={tabBarActiveTextColor}
            contentProps={{ height: 50 }}
            initialPage={0}
            renderTabBar={() => <ScrollableTabBar
                style={{ borderBottomColor: 'transparent' }}
            />}
            onChangeTab={val => { selectedIndex(val.i) }}
        >
            {returnView}
        </ScrollableTabView>
    );
};

const styles = StyleSheet.create({
    labelText: {
        fontFamily: res.fonts.Sregular,
        fontSize: heightScale(14),
        color: res.colors.white
    }
})

export default TabVIew;