import React from "react"
import { View, Text, Image, TouchableOpacity } from "react-native"
import styles from "./styles"
import FloatingLabel from './FloatingInput'
/**
 * @function
 * 
 * This is a functional component to render a text imput with label on top.
 * @param {Object} props
 * @returns {XML} view.
 */
function PGInputField(props) {
    const { label, onChangeText, value, error, inputStyles, errStyle, inputProps, onBackKeyPress, onSubmitEditing, reference,
        onBlur, onFocus, } = props;

   
    return (
        <View>
            <View style={{ flexDirection: 'row', }}>
                <FloatingLabel
                    myRef={reference}
                    value={value}
                    autoCorrect={false}
                    keyboardType={inputProps && inputProps.keyboardType ? inputProps.keyboardType : 'default'}
                    enablesReturnKeyAutomatically={true}
                    onChangeText={onChangeText ? onChangeText : () => { }}
                    onSubmitEditing={onSubmitEditing ? onSubmitEditing : () => { }}
                    returnKeyType={inputProps.returnKeyType}
                    style={[{ backgroundColor: 'transparent', flex: 1 }, inputStyles]}
                    maxLength={inputProps && inputProps.maxLength}
                    editable={inputProps && inputProps.editable}
                    autoCapitalize={inputProps && inputProps.autoCaptialize}
                    secureTextEntry={inputProps && inputProps.secureTextEntry}
                    _onFocus={() => { onFocus && onFocus() }}
                    _onBlur={() => { onBlur && onBlur() }}
                    multiline={inputProps && inputProps.multiline}
                    onKeyPress={({ nativeEvent }) => { onBackKeyPress ? onBackKeyPress(nativeEvent.key) : () => { } }}
                    // labelStyle={additionalLabel && additionalLabel == resources.strings.VERIFIED ? styles.verifiedTextStyle : additionalLabel &&
                    //     additionalLabel == resources.strings.NOT_VERIFIED ? styles.notVerifiedTextStyle : {}}
                    // validText={additionalLabel ? additionalLabel : ""}
                >
                    {label}
                </FloatingLabel>
              
               
            </View>
            {
                error ? (
                    <Text
                        style={[styles.errorText, errStyle]}
                        numberOfLines={2}
                        // ellipsizeMode={'middle'}
                        >
                        {error}
                    </Text>
                ) : (<View style={{ height: 20 }} />)
            }
        </View >

    );
}



export default PGInputField;