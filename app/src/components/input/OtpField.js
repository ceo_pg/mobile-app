import React, { Component } from "react";
import { View, Text, TextInput ,StyleSheet} from "react-native";
import PropTypes from "prop-types";
import res from '../../../res'
/**
 * @function
 * 
 * This is a functional component to render a text imput with label on top.
 * @param {Object} props
 * @returns {XML} view.
 */
function Input(props) {
    const { label, onChangeText, value, inputStyles, labelStyles, containerStyle, inputProps, onBackKeyPress } = props;
    return (
        <View style={[styles.oldTopView, containerStyle]}>
            
            <TextInput style={[styles.textInputStyleOld, inputStyles]}
                autoCaptialize={'none'}
                autoCorrect={false}
                autoFocus={false}
                underlineColorAndroid={"transparent"}
                onChangeText={onChangeText ? onChangeText : () => { }}
                value={value}
                {...inputProps}
                onKeyPress={({ nativeEvent }) => { onBackKeyPress ? onBackKeyPress(nativeEvent.key) : () => { } }}
            />
        </View>
    );
}


export default Input;


Input.propTypes = {
    label: PropTypes.string,
    onChangeText: PropTypes.func.isRequired,
    value: PropTypes.string.isRequired,
};

Input.defaultProps = {
    inputStyles: {},
    labelStyles: {},
    containerStyle: {},
    label: " "
};
export const styles = StyleSheet.create({
    textStyle: {
        marginTop: 5,
        width: '85%',
        color: '#71828A',
        fontFamily: 'Montserrat-Regular',
    },
    textInputStyle: {
        padding: 0,
        paddingBottom: 0,
        paddingLeft: 0,
        paddingVertical: 0,
        color: res.colors.white,
        fontFamily: 'Montserrat-Regular',
        borderBottomColor: res.colors.underlineColor,
        borderBottomWidth:2,
        height:50,

    },textInputStyleOld: {
        padding: 0,
        paddingBottom: 0,
        paddingLeft: 0,
        paddingVertical: 0,
        color: res.colors.black,
        fontFamily: 'Montserrat-Regular',
        borderColor: '#D3DFEF',
    
        height:40
    },
    topView: {
        alignSelf: "stretch",
        alignItems: "stretch",
        flex:1
    },oldTopView: {
        justifyContent:'center',
        flex:1
    }
});