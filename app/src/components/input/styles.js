import { StyleSheet } from "react-native";
import res from '../../../res';
import resources from "../../../res";


const styles = StyleSheet.create({
    textStyle: {
        marginTop: 5,
        width: '100%',
        color: '#71828A',
    },
    textInputStyle: {
        backgroundColor: 'white',
        fontFamily: "Montserrat-Regular",
        color: res.colors.black,

    },
    topView: {
        height: 50,
        flex: 1
    }, labelStyles: {
        fontFamily: res.fonts.regular,
        fontSize: 14,
        color: res.colors.btnBlue
    }, textStyle: {
        marginTop: 10,
        // width: '85%',
        fontSize: 15,
        fontWeight: '600',
        color: res.colors.labelColor,
        fontFamily: resources.fonts.regular
    },
    textInputStyle: {
        flex: 1,
        padding: 0,
        paddingBottom: 0,
        paddingLeft: 0,
        paddingVertical: 0,
        fontSize: 14,
        color: res.colors.black,
        fontFamily: resources.fonts.regular,
        // borderBottomWidth: 1,
         borderColor: res.colors.underlineColor,
        top: 0,
    },
    topView: {
        alignSelf: "stretch",
        alignItems: "stretch"
    },
    errorText: {
        color: 'red',
        minHeight: 20,
        maxHeight:40,
        fontFamily: res.fonts.regular,
        marginTop:5
    },
    layoutHorizontal: {
        flexDirection: 'row',
        height: 45,
    },
    borderBottom: {
        // borderWidth: 2,
        borderBottomWidth: 2,
        marginBottom: 8,
        alignItems: 'center',
        justifyContent: 'center',
        borderColor: res.colors.labelColor
    },
    borderBottomThin: {
        // borderWidth: 2,
        borderBottomWidth: 1,
        alignItems: 'center',
        justifyContent: 'center',
        borderColor: res.colors.labelColor
    },
    borderBottomThick: {
        // borderWidth: 2,
        borderBottomWidth: 1,
        alignItems: 'center',
        justifyContent: 'center',
        borderColor: res.colors.labelColor
    },
    txtGetOTP: {
        fontFamily: resources.fonts.medium,
        color: resources.colors.txtGetOTP
    },
    verifiedTextStyle: {
        color: resources.colors.green,
    },
    notVerifiedTextStyle: {
        color: resources.colors.red,

    }, imgDropDown: {
        width: 9,
        height: 8,
        marginRight: 10,
        marginTop: 20,
        // backgroundColor:'red'
    }, isDropDown: {
        justifyContent: 'center',
        borderBottomWidth: 1,
        alignItems: 'center',
        justifyContent: 'center',
        borderColor: res.colors.labelColor,
        //  backgroundColor:'red'

    }
});

export default styles;