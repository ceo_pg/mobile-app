import * as React from "react";
import { Text, View, StyleSheet, Dimensions, TextInput, Image } from "react-native";
import * as Animatable from "react-native-animatable";
import PropTypes from "prop-types";
import res from '../../../res'
import DatePicker from 'react-native-datepicker'
import { widthScale, heightScale } from "../../utility/Utils";

const { height, width } = Dimensions.get("window");
const INPUT_HEIGHT = 45;
const FONT_SIZE = 16;
const PADDING = 20;
const BORDER_WIDTH = 1;
const BG_COLOR = "#fff";
const COLOR_UNFOCUSED = res.colors.lightGreyBlue;
const COLOR_FOCUSED = res.colors.appColor;
const DURATION = 350;

export default class PGDatePicker extends React.Component {
    state = {
        currColor: COLOR_UNFOCUSED,
        labelWidth: null,
        focused: false,
        dob: ""

    };
    labelRef = null;
    spacerRef = null;
    inputRef = null;





    render() {
        const { style, placeholder, label, format, minDate, maxDate, dob, onDateChange } = this.props;
        const { currColor, labelWidth, labelHeight, focused, } = this.state;
        return (
            <View
                style={{
                    ...styles.container,
                    ...style,
                    borderColor: res.colors.appColor,
                    marginTop: 20
                }}
            >
                <DatePicker

                    label={label}
                    style={{ ...styles.inputContainer, color: currColor }}
                    date={dob}
                    mode="date"
                    placeholder={placeholder}
                    placeholderText={{ color: 'red' }}
                    format={format}
                    minDate={minDate}
                    maxDate={maxDate}
                    confirmBtnText="Confirm"
                    cancelBtnText="Cancel"
                    showIcon={false}
                    customStyles={{
                        placeholderText: {
                            color: res.colors.peacockBlueLight
                        },
                        dateText: {
                            color: res.colors.peacockBlue
                        },
                        dateInput: {
                            height: 40,
                            justifyContent: 'center',
                            alignItems: 'flex-start',
                            backgroundColor: res.colors.white,
                            borderWidth: 0,
                            color: res.colors.peacockBlue
                            // borderBottomWidth: 1,
                            // borderBottomColor: resources.colors.labelColor
                        }
                        // ... You can check the source to find the other keys.
                    }}
                    onDateChange={(date) => { onDateChange(date) }}
                />

                <View style={{
                    position: "absolute",
                    top: -10,
                    width: labelWidth,
                    left: 20,
                    backgroundColor: res.colors.white,
                    minHeight: 10,
                }}>
                    <Text style={styles.labelStyle}>{label}</Text>
                </View>
                <View style={styles.calenderIcon}>
                    <Image source={res.images.icnDOb} style={{ flex: 1 }} resizeMode={'contain'} />
                </View>

            </View>
        );
    }
}

PGDatePicker.propTypes = {
    focusedColor: PropTypes.string,
    defaultColor: PropTypes.string,
    placeholder: PropTypes.string
};

PGDatePicker.defaultProps = {
    focusedColor: COLOR_FOCUSED,
    defaultColor: COLOR_UNFOCUSED,
    placeholder: "Placeholder"
};

const styles = StyleSheet.create({
    calenderIcon: {
        width: widthScale(20),
        height: heightScale(20),
        alignSelf: 'flex-end',
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 10,
        marginRight: 10


    },
    container: {
        height: INPUT_HEIGHT,
        borderWidth: 1,
        borderColor: res.colors.appColor,
        borderRadius: 5,
        backgroundColor: BG_COLOR
    },
    inputContainer: {
        flex: 1,
        paddingHorizontal: PADDING,
        fontSize: FONT_SIZE
    },
    labelStyle: {
        fontFamily: res.fonts.medium,
        color: res.colors.peacockBlue,
        fontSize: 12,
        letterSpacing: 0.5,
        lineHeight: 16

    }
});