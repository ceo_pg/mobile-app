import React from "react"
import { View, Text, Image, TextInput, StyleSheet } from "react-native"
import resources from "../../../res";
import { heightScale } from "../../utility/Utils";


/**
 * @function
 * 
 * This is a functional component to render a text imput with label on top.
 * @param {Object} props
 * @returns {XML} view.
 */
function SimpleInput(props) {
    const { placeholder, simpleInput, onChangeText, value, error, inputStyles, errStyle, inputProps, onBackKeyPress, onSubmitEditing, reference,
        onBlur, onFocus, } = props;


    return (
        <View>
            <View style={{ flexDirection: 'row', }}>
                <TextInput
                    style={[styles.simpleInput, simpleInput]}
                    placeholder={placeholder}
                    placeholderTextColor={resources.colors.peacockBlueLight}
                    onChangeText={onChangeText ? onChangeText : () => { }}
                    value={value}
                    autoCorrect={false}
                    keyboardType={inputProps && inputProps.keyboardType ? inputProps.keyboardType : 'default'}
                    enablesReturnKeyAutomatically={true}
                    // onChangeText={onChangeText ? onChangeText : () => { }}
                    onSubmitEditing={onSubmitEditing ? onSubmitEditing : () => { }}
                    // returnKeyType={inputProps.returnKeyType}
                // style={[{ backgroundColor: 'white', flex: 1 }, inputStyles]}
                // maxLength={inputProps && inputProps.maxLength}
                // editable={inputProps && inputProps.editable}
                // autoCapitalize={inputProps && inputProps.autoCaptialize}
                // secureTextEntry={inputProps && inputProps.secureTextEntry}
                // _onFocus={() => { onFocus && onFocus() }}
                // _onBlur={() => { onBlur && onBlur() }}
                multiline={inputProps && inputProps.multiline}
                // onKeyPress={({ nativeEvent }) => { onBackKeyPress ? onBackKeyPress(nativeEvent.key) : () => { } }}

                >

                </TextInput>


            </View>

        </View >

    );
}
const styles = StyleSheet.create({
    simpleInput: {
        // fontFamily:resources.fonts.semiBold,
        // fontsize:heightScale(16),
        paddingLeft: 16,
        borderWidth: 2,
        borderColor: resources.colors.peacockBlueLight,
        borderRadius: 5,
        width: "100%",
        height: heightScale(50)
    }
})


export default SimpleInput;