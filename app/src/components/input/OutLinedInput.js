import * as React from "react";
import { Text, View, StyleSheet, Dimensions, TextInput,Image } from "react-native";
import * as Animatable from "react-native-animatable";
import PropTypes from "prop-types";
import res from '../../../res'
const { height, width } = Dimensions.get("window");
const INPUT_HEIGHT = 45;
const FONT_SIZE = 16;
const PADDING = 20;
const BORDER_WIDTH = 1;
const BG_COLOR = "#fff";
const COLOR_UNFOCUSED = res.colors.lightGreyBlue;
const COLOR_FOCUSED = res.colors.appColor;
const DURATION = 350;

export default class InputOutline extends React.Component {
    state = {
        currColor: COLOR_UNFOCUSED,
        labelWidth: null,
        focused: false,

    };
    labelRef = null;
    spacerRef = null;
    inputRef = null;





    render() {
        const { style, placeholder, label, value,isDropDownImageVisible,isEdit,placeHolderStyle,placeholderTextColor,keyboardType,maxLength } = this.props;
        const { currColor, labelWidth, labelHeight, focused } = this.state;
        let isEditable = true;
        if(isEdit == false){
            isEditable = false
        }
        return (
            <View
                style={{
                    ...styles.container,
                    ...style,
                    borderColor: res.colors.appColor,
                    marginTop: 20,
                    opacity: isEditable ? 1 : 0.5
                }}
                
            >
                <TextInput
                    value={value}
                    style={[ {...styles.inputContainer, color: res.colors.peacockBlue},placeHolderStyle ? placeHolderStyle : {}] }
                    ref={ref => (this.inputRef = ref)}
                    placeholder={placeholder}
                    placeholderTextColor={placeholderTextColor ? placeholderTextColor : res.colors.peacockBlueLight}
                    onChangeText={this.props.onChangeText}
                    editable={isEditable}
                    keyboardType={keyboardType}
                    maxLength={maxLength}

                />
                {isDropDownImageVisible ?
                    <View style={styles.isDropDown}>
                        <Image source={res.images.dropdown} resizeMode={'stretch'} />
                    </View>

                    :
                    <View />
                }
                <View style={{
                    position: "absolute",
                    top: -10,
                    width: labelWidth,
                    left: 20,
                    backgroundColor: res.colors.white,
                    minHeight: 10,
                    flexDirection:'row'
                }}>
                    <Text style={styles.labelStyle}>{label}</Text>
                    
                </View>
                
            </View>
        );
    }
}

InputOutline.propTypes = {
    focusedColor: PropTypes.string,
    defaultColor: PropTypes.string,
    placeholder: PropTypes.string
};

InputOutline.defaultProps = {
    focusedColor: COLOR_FOCUSED,
    defaultColor: COLOR_UNFOCUSED,
    placeholder: "Placeholder"
};

const styles = StyleSheet.create({
    container: {
        height: INPUT_HEIGHT,
        borderWidth: 1,
        borderColor: res.colors.appColor,
        borderRadius: 5,
        backgroundColor: BG_COLOR
    },
    inputContainer: {
        flex: 1,
        paddingHorizontal: PADDING,
        fontSize: FONT_SIZE,
        color: res.colors.peacockBlue
    },
    labelStyle: {
        fontWeight:'bold',
        fontFamily: res.fonts.medium,
        color: res.colors.peacockBlue,
        fontSize: 12,
        letterSpacing: 0.58,
        lineHeight: 16

    },isDropDown:{
        justifyContent:'center',
        alignItems:'flex-end',
        position:'absolute',
        right:16,bottom:16,
       
    }
});