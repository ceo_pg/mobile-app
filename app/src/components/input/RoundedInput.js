import React, { Component } from 'react';
import { View, Text, TextInput, StyleSheet,Image } from 'react-native';
import res from '../../../res'
import { widthScale, heightScale } from '../../utility/Utils'
import style from '../button/styles';


class RoundedInput extends Component {
    constructor(props) {
        super(props);

    }

    render() {
        const { label, placeholder, onChange, value, keyboardType,isDropDownImageVisible } = this.props
        return (
            <View style={styles.mainContainer}>
                <Text style={styles.label}>{label ? label : ''}</Text>
                <View style={styles.containerView}>
                    <TextInput
                        style={styles.textInputStyle}
                        placeholder={placeholder}
                        placeholderTextColor={'#96AFD0'}
                        onChangeText={text => onChange(text)}
                        value={value}
                        keyboardType={keyboardType ? keyboardType : 'numeric'}
                    />
                    {isDropDownImageVisible ?
                    <View style={styles.isDropDown}>
                        <Image source={res.images.dropdown} resizeMode={'stretch'} />
                    </View>

                    :
                    <View />
                }
                </View>
            </View>
        )

    }
}

const styles = StyleSheet.create({

mainContainer:{
    flex:1,
},
    containerView: {
        borderColor: res.colors.appColor,
        borderWidth: 1,
        borderRadius: widthScale(5),
        marginTop:heightScale(12),
        flex: 1,
    },
    textInputStyle: {
        paddingVertical:0,
        paddingHorizontal: widthScale(12),
        height: heightScale(44),
        color:res.colors.appColor,
        fontSize:widthScale(16),
        fontFamily:res.fonts.semiBold,
    },
    uploadView: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        padding: widthScale(12),
    },
    label: {
        fontFamily: res.fonts.semiBold,
        fontSize: widthScale(16),
        color: res.colors.formLabelGrey
    },
    uploadIconView: {
        flex: 1,
        alignItems: 'flex-end',
        justifyContent: 'center'
    },isDropDown:{
        justifyContent:'center',
        alignItems:'flex-end',
        position:'absolute',
        right:16,bottom:25,
       
    }
})

export default RoundedInput;

