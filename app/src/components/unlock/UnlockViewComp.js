import React, { Component } from 'react';
import { View, Text, TouchableOpacity, ImageBackground, Image, StyleSheet } from 'react-native';
import res from '../../../res'
import { widthScale, heightScale } from '../../utility/Utils'
import strings from '../../../res/constants/strings'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { PGButton} from '../../components/index'




class UnlockViewComp extends Component {
    constructor(props) {
        super(props);
        this.state = {

        }
    }
    componentDidMount() {

    }

    render() {
        const {onNextPress} = this.props
        return (
 <View style={styles.unlockOuterView}>
                <View style={styles.innerView}>
                    <Text style={styles.unlockHeading}>{strings.UNLOCK_THIS_SECTION}</Text>
                    <View style={styles.contentView}>
                        <View style={styles.unlockImgView}>
                            <Image source={res.images.unlock_ic} resizeMode={'stretch'} />
                        </View>
                        <View style={styles.descView}>
                        <Text style={styles.unlockDescription}>{strings.UNLOCK_DESCRIPTION_FOR_CONSTRUCTION}</Text></View>
                        <View style={styles.bottomBtnContainer}>
                        <PGButton
                            btnText={strings.SUBMIT}
                            onPress={() => onNextPress ? onNextPress() : ''}
                            btnStyle={styles.nextBtnStyle}
                           textStyleOver={styles.nextBtnText}
                        />
                    </View>
                    </View>
                </View>
            </View>
           
        )
    }


}

const styles = StyleSheet.create({

    innerView: {
        margin: widthScale(10),
        borderRadius: widthScale(10),
        backgroundColor: res.colors.white,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        shadowColor: "rgba(0,0,0, 0.1)",
        shadowOffset: {
            width: 0,
            height: 4,
        },
        shadowOpacity: 0.8,
        shadowRadius: 10,
        elevation: 10,
        borderWidth:2
        // height:heightScale(560)
    },
    unlockOuterView: {
        flex: 1,
        backgroundColor: res.colors.butterscotch,
        marginHorizontal: widthScale(20),
        borderRadius: widthScale(20),
        marginTop: heightScale(64),
        // height:heightScale(568)

    },
    unlockHeading: {
        fontFamily: res.fonts.SsemiBold,
        fontSize: widthScale(18),
        color: res.colors.greyishBrown,
        marginTop: heightScale(30),
    },

    unlockDescription:{
        fontFamily: res.fonts.medium,
        fontSize: widthScale(16),
        color: res.colors.greyishBrown,  
        textAlign:'center',
        paddingHorizontal:widthScale(25)
    },
    contentView: {
        // marginHorizontal: widthScale(10),
        borderRadius: widthScale(10),
        alignItems: 'center',
        shadowColor: "rgba(0,0,0,0.1)",
        shadowOffset: {
            width: 0,
            height: 10,
        },
        shadowOpacity: 0.6,
        shadowRadius: 20,
        elevation: 20,
        flex:1,
        width:'95%',
        borderWidth:1,
        // marginTop:heightScale(40),

    },
    unlockImgView: {
        justifyContent: 'center',
    },
    descView:{
        justifyContent:'center',
        alignItems:'center',
        marginTop:heightScale(30)
    },
    bottomBtnContainer:{
        // marginTop:heightScale(35),
        // marginBottom:heightScale(120),
        width:'100%'
    },
    nextBtnStyle:{
        backgroundColor:res.colors.butterscotch,
        borderTopRightRadius:0,
        borderTopLeftRadius:0,
        borderBottomLeftRadius:widthScale(10),
        borderBottomRightRadius:widthScale(10)
    },
    nextBtnText:{
        fontFamily: res.fonts.semiBold,
        fontSize: widthScale(16),
        color: res.colors.greyishBrown, 
    }
})

export default UnlockViewComp;

