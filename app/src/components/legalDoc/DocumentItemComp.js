import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  StyleSheet,
  Linking,
} from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import res from '../../../res';
import { widthScale, heightScale } from '../../utility/Utils';
import { openShareModal } from '../../redux/actions/SharePostAction';
import Modal from 'react-native-modal';

class DocumentItemComp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isModalVisible: false,
      selectedData: {},
    };
  }
  openShareModal = (data) => {
    const { isModalVisible } = this.state;
    this.setState({
      isModalVisible: !isModalVisible,
      selectedData: data ? data : {},
    });
  };
  shareModal = () => {
    const { data, onUpdateDoc, uniqueID } = this.props;
    let isbase = data ? (data.base ? true : false) : false;
    console.log('DATAUPLOAD', this.props);
    return (
      <View style={styles.shareModalView}>
        <TouchableOpacity
          style={styles.actionLabelBackground}
          onPress={() => {
            this.openShareModal();
            onUpdateDoc(uniqueID);
          }}>
          {isbase ? (
            <Text style={styles.actionLabel}>
              {data && data.base && data.base.length > 0
                ? res.strings.UPDATE
                : res.strings.UPLOAD}
            </Text>
          ) : (
              <Text style={styles.actionLabel}>
                {data && data.path && data.path.length > 0
                  ? res.strings.UPDATE
                  : res.strings.UPLOAD}
              </Text>
            )}
        </TouchableOpacity>

        {data &&
          ((data.base && data.base.length > 0) ||
            (data.path && data.path.length > 0)) && (
            <TouchableOpacity
              style={styles.actionLabelBackground}
              onPress={() => this.openFile(data)}>
              <Text style={styles.actionLabel}>{res.strings.VIEW}</Text>
            </TouchableOpacity>
          )}
        {data &&
          ((data.base && data.base.length > 0) ||
            (data.path && data.path.length > 0)) && (
            <TouchableOpacity
              style={styles.actionLabelBackground}
              onPress={() => this.shareFile(data, true)}>
              <Text style={styles.actionLabel}>{res.strings.ShareAndDownload}</Text>
            </TouchableOpacity>
          )}
        {/* <Text style={styles.shareNowText}>{res.strings.OPTIONS}</Text>
        <View style={[styles.iconStyle]}>
          <TouchableOpacity
            style={styles.actionLabelBackground}
            onPress={() => {
              this.openShareModal();
              onUpdateDoc(uniqueID);
            }}>
            {isbase ? (
              <Text style={styles.actionLabel}>
                {data && data.base && data.base.length > 0
                  ? res.strings.UPDATE
                  : res.strings.UPLOAD}
              </Text>
            ) : (
                <Text style={styles.actionLabel}>
                  {data && data.path && data.path.length > 0
                    ? res.strings.UPDATE
                    : res.strings.UPLOAD}
                </Text>
              )}
          </TouchableOpacity>

          {data &&
            ((data.base && data.base.length > 0) ||
              (data.path && data.path.length > 0)) && (
              <TouchableOpacity
                style={styles.actionLabelBackground}
                onPress={() => this.openFile(data)}>
                <Text style={styles.actionLabel}>{res.strings.VIEW}</Text>
              </TouchableOpacity>
            )}
          {data &&
            ((data.base && data.base.length > 0) ||
              (data.path && data.path.length > 0)) && (
              <TouchableOpacity
                style={styles.actionLabelBackground}
                onPress={() => this.shareFile(data)}>
                <Text style={styles.actionLabel}>{res.strings.SHARE_NOW}</Text>
              </TouchableOpacity>
            )}
        </View> */}
      </View>
    );
  };

  openFile = (data) => {
    const { viewDocument } = this.props;
    this.setState({ isModalVisible: false });
    if (viewDocument) {
      viewDocument(data);
    }

  };
  shareFile = (data, isShare) => {
    const { shareDocument } = this.props;
    this.setState({ isModalVisible: false });
    if (shareDocument) {
      shareDocument(data, true);
    }

    // let url = data.base ? data.base : data.path

  }
  render() {
    const { label, data } = this.props;
    console.log('HOMELOANHOMELOAN', this.props);
    return (
      <View style={styles.containerView}>
        <View style={styles.uploadView}>
          <Modal
            onRequestClose={() => this.openShareModal(data)}
            onBackdropPress={() => this.openShareModal(data)}
            isVisible={this.state.isModalVisible}>
            {this.shareModal()}
          </Modal>
          <View
            style={{
              width: widthScale(180),
              flexDirection: 'column',
              // alignItems: 'center',
              marginLeft: widthScale(10),
            }}>
            <Text style={styles.label}>{label ? label : ''}</Text>
          </View>
          <TouchableOpacity
            style={styles.uploadIconView}
            hitSlop={{ top: 10, bottom: 10, left: 10, right: 10 }}
            onPress={() => this.openShareModal()}>
            <Image source={res.images.dot_ic} resizeMode={'stretch'} />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  shareNowText: {
    top: -30,
    fontFamily: res.fonts.semiBold,
    fontSize: heightScale(18),
    color: res.colors.blackTwo,
  },
  iconStyle: {
    alignItems: 'center',
    justifyContent: 'space-between',
    // flexDirection: 'row',
    width: '100%',
    flex: 1,
    paddingVertical: heightScale(10)
  },
  shareModalView: {

    backgroundColor: res.colors.butterscotch,
    height: heightScale(200),
    // marginTop: heightScale(100),
    borderRadius: widthScale(24),
    marginHorizontal: widthScale(15),
    // alignSelf: 'center',
    // width: '100%',
    alignItems: 'center',
    // justifyContent: 'center',
    flexDirection: 'column',
    justifyContent: 'space-around'
  },

  containerView: {
    marginVertical: heightScale(8),
    flex: 1,
    marginHorizontal: widthScale(20),
  },
  uploadView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderRadius: 12,
    backgroundColor: res.colors.butterscotch,
    shadowColor: res.colors.shadowColor,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 10.58,
    shadowRadius: 10.0,
    elevation: 24,
    padding: widthScale(3),
  },
  label: {
    fontFamily: res.fonts.semiBold,
    fontSize: widthScale(12),
    color: res.colors.formLabelGrey,
    fontWeight: 'bold',
  },
  actionLabel: {
    fontFamily: res.fonts.semiBold,
    fontSize: widthScale(15),
    color: res.colors.white,
  },

  actionLabelBackground: {
    width: '70%',
    height: heightScale(40),
    backgroundColor: res.colors.btnBlue,
    borderRadius: widthScale(10),
    alignItems: 'center',
    justifyContent: 'center',
  },
});

// export default DocumentItemComp;
function mapStateToProps(state) {
  return {
    isVisiblePostModal: state.SharePostReducer.showSharePostModal,
  };
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      openShareModal,
    },
    dispatch,
  );
}

const DocumentItemCompComponent = connect(
  mapStateToProps,
  mapDispatchToProps,
)(DocumentItemComp);
export default DocumentItemCompComponent;
