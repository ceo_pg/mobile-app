import React, { Component } from 'react';
import { View, Text, TouchableOpacity, ImageBackground, Image, StyleSheet } from 'react-native';
import res from '../../../res'
import { widthScale, heightScale } from '../../utility/Utils'



class KeyClauseItemComp extends Component {
    constructor(props) {
        super(props);

    }
    arrowDirection = () => {
        const { arrowDirection, marketDifference } = this.props
        console.log("arrowDirection", arrowDirection, marketDifference)
        if (arrowDirection == "up") {
            return <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                <Text style={styles.upMarkettext}>{parseFloat((marketDifference * 100).toFixed(1))  + '%'} </Text>
                <Image style={{width: 12, height: 12}} resizeMode={'contain'} source={res.images.graphUpArrao} />
            </View>


        }
        if (arrowDirection == "down") {
            return <View style={{ flexDirection: 'row', alignItems: 'center'}}>
                <Text style={styles.downMarkettext}>{parseFloat((marketDifference * 100).toFixed(1))  + '%'}</Text>
                <Image style={{width: 12, height: 12}} resizeMode={'contain'} source={res.images.graphdownArrow} />
            </View>
        }
        if (arrowDirection == "equal") {
            return <View style={{ flexDirection: 'row', alignItems: 'center'}}>
                <Text style={styles.equalMarkettext}>{parseFloat((marketDifference * 100).toFixed(1))  + '%'}</Text>
                <Image style={{width: 15, height: 15}} resizeMode={'contain'} source={res.images.graphEqualArrow} />
            </View>

        }
    }

    render() {
        const { label, subLabel, icon, value, unitText } = this.props
        return (
            <View style={styles.containerView}>
                <View style={styles.uploadView}>
                    <View style={{ width: '77%', flexDirection: 'row', alignItems: 'center' }}>
                        <View style={{ marginRight: widthScale(10) }}>
                            <Image source={icon ? icon : res.images.percent_ic} resizeMode={'contain'} style={{ height: widthScale(30), width: widthScale(30) }} />
                        </View>
                        <View style={{flex:1}}>
                            <View style={{ flexDirection: 'row' }}>
                                <Text style={styles.label}>{unitText ? unitText : ''}</Text>
                                <Text style={styles.label}>{value ? value + label : '0'}</Text>

                            </View>
                            <View style={styles.graphView}>
                               
                                    <Text style={styles.subHeading}>{subLabel ? subLabel : ''}</Text>

                            </View>

                        </View>

                    </View>
                    <View style={{paddingHorizontal:5, flex:1 , height: '100%'}}>
                    <View style={{position: 'absolute', bottom: 0 , right: 0, height:20}}>
                    {this.arrowDirection()}
                    </View>
                    </View>

                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    graphView: {

        // backgroundColor: 'red',
        alignItems: 'center',
        justifyContent: 'space-between',
        flexDirection: 'row',
    },
    containerView: {
        marginVertical: heightScale(8),
        flex: 1,
        marginHorizontal: widthScale(20),

    },
    uploadView: {

        flexDirection: 'row',
        alignItems: 'center',
        borderRadius: 12,
        backgroundColor: res.colors.butterscotch,
        shadowColor: res.colors.shadowColor,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 1.58,
        shadowRadius: 1.00,
        elevation: 4,
        padding: widthScale(8),
    },
    label: {
        fontFamily: res.fonts.regular,
        fontSize: widthScale(18),
        color: res.colors.formLabelGrey,
        // fontWeight: "800"
    },
    subHeading: {
        fontFamily: res.fonts.regular,
        fontSize: widthScale(12),
        color: res.colors.formLabelGrey
    }, upMarkettext: {
        color: res.colors.kelleyGreen,
        paddingRight: 2,
        fontSize: 12
    }, downMarkettext: {
        color: res.colors.pastelRed,
        paddingRight: 2,
        fontSize: 12
    }, equalMarkettext: {
        color: res.colors.kelleyGreen,
        paddingRight: 2,
        fontSize: 12
    }
})

export default KeyClauseItemComp;

