import React from 'react';
import { View, StyleSheet, TouchableOpacity } from 'react-native';
import ImageViewer from 'react-native-image-zoom-viewer';
// import irxuser from '../irxuser';

// let irx = irxuser.getInstance();
export default class Imageviewer extends React.Component {
  prepareImageUrls = () => {
    let images = this.props.images;
    let imgs = [];
    for (let i = 0; i < images.length; i++) {
      let img = images[i];
      imgs.push({
        url: img.image,
      });
    }
    console.log('hello array', imgs);

    return imgs;
  };

  onOutSidePress = () => {
    this.props.closePopUp?.();
  };
  render() {
    return (
      <View style={{ flex: 1 }}>
        {/* <TouchableOpacity
          onPress={this.onOutSidePress}
          style={styles.container}
        />
        <View style={styles.subCont}> */}
        <ImageViewer imageUrls={this.prepareImageUrls()} />
        {/* </View> */}
      </View>
    );
  }
}

var styles = StyleSheet.create({
  container: {
    width: '100%',
    height: '100%',
    top: 0,
    backgroundColor: 'rgba(0,0,0,0.3)',
    position: 'absolute',
  },
  subCont: {
    flex: 1,
    marginHorizontal: 20,
    marginVertical: 50,
    backgroundColor: '#fff',
    shadowOffset: {
      width: 4,
      height: 4,
    },
    shadowOpacity: 0.6,
    shadowRadius: 4,

    elevation: 5,
  },
});
